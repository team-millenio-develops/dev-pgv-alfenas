<?php 
@ini_set("display_errors","1");
@ini_set("display_startup_errors","1");
require_once("include/dbcommon.php");
header("Expires: Thu, 01 Jan 1970 00:00:01 GMT"); 

require_once("classes/searchclause.php");
require_once("classes/sql.php");

require_once("include/public_geo_face_quadra_variables.php");

if( !Security::processPageSecurity( $strtablename, 'P' ) )
	return;




$layout = new TLayout("export2", "CityCity", "MobileCity");
$layout->version = 2;
$layout->blocks["top"] = array();
$layout->containers["export"] = array();
$layout->container_properties["export"] = array(  );
$layout->containers["export"][] = array("name"=>"exportheader",
	"block"=>"", "substyle"=>2  );

$layout->containers["export"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"range" );
$layout->containers["range"] = array();
$layout->container_properties["range"] = array(  );
$layout->containers["range"][] = array("name"=>"exprange",
	"block"=>"range_block", "substyle"=>1  );

$layout->skins["range"] = "fields";


$layout->containers["export"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"format" );
$layout->containers["format"] = array();
$layout->container_properties["format"] = array(  );
$layout->containers["format"][] = array("name"=>"expformat",
	"block"=>"exportformat", "substyle"=>1  );

$layout->skins["format"] = "fields";


$layout->containers["export"][] = array("name"=>"wrapper",
	"block"=>"", "substyle"=>1 , "container"=>"fields" );
$layout->containers["fields"] = array();
$layout->container_properties["fields"] = array(  );
$layout->containers["fields"][] = array("name"=>"expoutput",
	"block"=>"", "substyle"=>1  );

$layout->skins["fields"] = "fields";


$layout->containers["export"][] = array("name"=>"expbuttons",
	"block"=>"", "substyle"=>2  );

$layout->skins["export"] = "1";

$layout->blocks["top"][] = "export";
$page_layouts["public_geo_face_quadra_export"] = $layout;

$layout->skinsparams = array();
$layout->skinsparams["empty"] = array("button"=>"button1");
$layout->skinsparams["menu"] = array("button"=>"button1");
$layout->skinsparams["hmenu"] = array("button"=>"button1");
$layout->skinsparams["undermenu"] = array("button"=>"button1");
$layout->skinsparams["fields"] = array("button"=>"button1");
$layout->skinsparams["form"] = array("button"=>"button1");
$layout->skinsparams["1"] = array("button"=>"button1");
$layout->skinsparams["2"] = array("button"=>"button1");
$layout->skinsparams["3"] = array("button"=>"button1");



require_once("include/export_functions.php");
require_once("classes/exportpage.php");
require_once("include/xtempl.php");

$xt = new Xtempl();

//array of params for classes
$params = array();
$params["id"] = postvalue("id");
$params["xt"] = &$xt;
$params["tName"] = $strTableName;
$params["pageType"] = PAGE_EXPORT;

if( !$eventObj->exists("ListGetRowCount") && !$eventObj->exists("ListQuery") )
	$params["needSearchClauseObj"] = false;

$params["selectedFields"] = postvalue("exportFields");
$params["exportType"] = postvalue("type");
$params["action"] = postvalue("a");	
$params["records"] = postvalue("records");	
$params["selection"] = postvalue("selection"); 
$params["csvDelimiter"] = postvalue("delimiter"); 

if( postvalue("txtformatting") == "raw" )
	$params["useRawValues"] = true;

$params["mode"] = ExportPage::readModeFromRequest();
	
$pageObject = new ExportPage( $params );
$pageObject->init();

$pageObject->process();
?>