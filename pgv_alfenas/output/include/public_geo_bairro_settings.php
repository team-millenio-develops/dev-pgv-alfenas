<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_geo_bairro = array();
	$tdatapublic_geo_bairro[".truncateText"] = true;
	$tdatapublic_geo_bairro[".NumberOfChars"] = 80;
	$tdatapublic_geo_bairro[".ShortName"] = "public_geo_bairro";
	$tdatapublic_geo_bairro[".OwnerID"] = "";
	$tdatapublic_geo_bairro[".OriginalTable"] = "public.geo_bairro";

//	field labels
$fieldLabelspublic_geo_bairro = array();
$fieldToolTipspublic_geo_bairro = array();
$pageTitlespublic_geo_bairro = array();
$placeHolderspublic_geo_bairro = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelspublic_geo_bairro["Portuguese(Brazil)"] = array();
	$fieldToolTipspublic_geo_bairro["Portuguese(Brazil)"] = array();
	$placeHolderspublic_geo_bairro["Portuguese(Brazil)"] = array();
	$pageTitlespublic_geo_bairro["Portuguese(Brazil)"] = array();
	$fieldLabelspublic_geo_bairro["Portuguese(Brazil)"]["gid"] = "Gid";
	$fieldToolTipspublic_geo_bairro["Portuguese(Brazil)"]["gid"] = "";
	$placeHolderspublic_geo_bairro["Portuguese(Brazil)"]["gid"] = "";
	$fieldLabelspublic_geo_bairro["Portuguese(Brazil)"]["the_geom"] = "The Geom";
	$fieldToolTipspublic_geo_bairro["Portuguese(Brazil)"]["the_geom"] = "";
	$placeHolderspublic_geo_bairro["Portuguese(Brazil)"]["the_geom"] = "";
	$fieldLabelspublic_geo_bairro["Portuguese(Brazil)"]["nomebairro"] = "Nomebairro";
	$fieldToolTipspublic_geo_bairro["Portuguese(Brazil)"]["nomebairro"] = "";
	$placeHolderspublic_geo_bairro["Portuguese(Brazil)"]["nomebairro"] = "";
	$fieldLabelspublic_geo_bairro["Portuguese(Brazil)"]["cod_bairro"] = "Cod Bairro";
	$fieldToolTipspublic_geo_bairro["Portuguese(Brazil)"]["cod_bairro"] = "";
	$placeHolderspublic_geo_bairro["Portuguese(Brazil)"]["cod_bairro"] = "";
	$fieldLabelspublic_geo_bairro["Portuguese(Brazil)"]["obs"] = "Obs";
	$fieldToolTipspublic_geo_bairro["Portuguese(Brazil)"]["obs"] = "";
	$placeHolderspublic_geo_bairro["Portuguese(Brazil)"]["obs"] = "";
	$fieldLabelspublic_geo_bairro["Portuguese(Brazil)"]["coleta_lixo"] = "Coleta Lixo";
	$fieldToolTipspublic_geo_bairro["Portuguese(Brazil)"]["coleta_lixo"] = "";
	$placeHolderspublic_geo_bairro["Portuguese(Brazil)"]["coleta_lixo"] = "";
	$fieldLabelspublic_geo_bairro["Portuguese(Brazil)"]["obs_lixo"] = "Obs Lixo";
	$fieldToolTipspublic_geo_bairro["Portuguese(Brazil)"]["obs_lixo"] = "";
	$placeHolderspublic_geo_bairro["Portuguese(Brazil)"]["obs_lixo"] = "";
	$fieldLabelspublic_geo_bairro["Portuguese(Brazil)"]["expansao"] = "Expansao";
	$fieldToolTipspublic_geo_bairro["Portuguese(Brazil)"]["expansao"] = "";
	$placeHolderspublic_geo_bairro["Portuguese(Brazil)"]["expansao"] = "";
	if (count($fieldToolTipspublic_geo_bairro["Portuguese(Brazil)"]))
		$tdatapublic_geo_bairro[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_geo_bairro[""] = array();
	$fieldToolTipspublic_geo_bairro[""] = array();
	$placeHolderspublic_geo_bairro[""] = array();
	$pageTitlespublic_geo_bairro[""] = array();
	if (count($fieldToolTipspublic_geo_bairro[""]))
		$tdatapublic_geo_bairro[".isUseToolTips"] = true;
}


	$tdatapublic_geo_bairro[".NCSearch"] = true;



$tdatapublic_geo_bairro[".shortTableName"] = "public_geo_bairro";
$tdatapublic_geo_bairro[".nSecOptions"] = 0;
$tdatapublic_geo_bairro[".recsPerRowPrint"] = 1;
$tdatapublic_geo_bairro[".mainTableOwnerID"] = "";
$tdatapublic_geo_bairro[".moveNext"] = 1;
$tdatapublic_geo_bairro[".entityType"] = 0;

$tdatapublic_geo_bairro[".strOriginalTableName"] = "public.geo_bairro";

	



$tdatapublic_geo_bairro[".showAddInPopup"] = false;

$tdatapublic_geo_bairro[".showEditInPopup"] = false;

$tdatapublic_geo_bairro[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapublic_geo_bairro[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_geo_bairro[".fieldsForRegister"] = array();

$tdatapublic_geo_bairro[".listAjax"] = false;

	$tdatapublic_geo_bairro[".audit"] = false;

	$tdatapublic_geo_bairro[".locking"] = false;



$tdatapublic_geo_bairro[".list"] = true;

$tdatapublic_geo_bairro[".inlineEdit"] = true;


$tdatapublic_geo_bairro[".reorderRecordsByHeader"] = true;



$tdatapublic_geo_bairro[".inlineAdd"] = true;



$tdatapublic_geo_bairro[".printFriendly"] = true;


$tdatapublic_geo_bairro[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatapublic_geo_bairro[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatapublic_geo_bairro[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatapublic_geo_bairro[".searchSaving"] = false;
//

$tdatapublic_geo_bairro[".showSearchPanel"] = true;
		$tdatapublic_geo_bairro[".flexibleSearch"] = true;

$tdatapublic_geo_bairro[".isUseAjaxSuggest"] = true;






$tdatapublic_geo_bairro[".ajaxCodeSnippetAdded"] = false;

$tdatapublic_geo_bairro[".buttonsAdded"] = false;

$tdatapublic_geo_bairro[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapublic_geo_bairro[".isUseTimeForSearch"] = false;



$tdatapublic_geo_bairro[".badgeColor"] = "DC143C";


$tdatapublic_geo_bairro[".allSearchFields"] = array();
$tdatapublic_geo_bairro[".filterFields"] = array();
$tdatapublic_geo_bairro[".requiredSearchFields"] = array();

$tdatapublic_geo_bairro[".allSearchFields"][] = "nomebairro";
	

$tdatapublic_geo_bairro[".googleLikeFields"] = array();
$tdatapublic_geo_bairro[".googleLikeFields"][] = "nomebairro";


$tdatapublic_geo_bairro[".advSearchFields"] = array();
$tdatapublic_geo_bairro[".advSearchFields"][] = "nomebairro";

$tdatapublic_geo_bairro[".tableType"] = "list";

$tdatapublic_geo_bairro[".printerPageOrientation"] = 0;
$tdatapublic_geo_bairro[".nPrinterPageScale"] = 100;

$tdatapublic_geo_bairro[".nPrinterSplitRecords"] = 40;

$tdatapublic_geo_bairro[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_geo_bairro[".geocodingEnabled"] = false;





$tdatapublic_geo_bairro[".listGridLayout"] = 3;

$tdatapublic_geo_bairro[".isDisplayLoading"] = true;




// view page pdf

// print page pdf


$tdatapublic_geo_bairro[".pageSize"] = 20;

$tdatapublic_geo_bairro[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_geo_bairro[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_geo_bairro[".orderindexes"] = array();

$tdatapublic_geo_bairro[".sqlHead"] = "SELECT gid,  	the_geom,  	nomebairro,  	cod_bairro,  	obs,  	coleta_lixo,  	obs_lixo,  	expansao";
$tdatapublic_geo_bairro[".sqlFrom"] = "FROM \"public\".geo_bairro";
$tdatapublic_geo_bairro[".sqlWhereExpr"] = "";
$tdatapublic_geo_bairro[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_geo_bairro[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_geo_bairro[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_geo_bairro[".highlightSearchResults"] = true;

$tableKeyspublic_geo_bairro = array();
$tableKeyspublic_geo_bairro[] = "gid";
$tdatapublic_geo_bairro[".Keys"] = $tableKeyspublic_geo_bairro;

$tdatapublic_geo_bairro[".listFields"] = array();
$tdatapublic_geo_bairro[".listFields"][] = "cod_bairro";
$tdatapublic_geo_bairro[".listFields"][] = "nomebairro";
$tdatapublic_geo_bairro[".listFields"][] = "obs";

$tdatapublic_geo_bairro[".hideMobileList"] = array();


$tdatapublic_geo_bairro[".viewFields"] = array();

$tdatapublic_geo_bairro[".addFields"] = array();

$tdatapublic_geo_bairro[".masterListFields"] = array();
$tdatapublic_geo_bairro[".masterListFields"][] = "gid";
$tdatapublic_geo_bairro[".masterListFields"][] = "the_geom";
$tdatapublic_geo_bairro[".masterListFields"][] = "nomebairro";
$tdatapublic_geo_bairro[".masterListFields"][] = "cod_bairro";
$tdatapublic_geo_bairro[".masterListFields"][] = "obs";
$tdatapublic_geo_bairro[".masterListFields"][] = "coleta_lixo";
$tdatapublic_geo_bairro[".masterListFields"][] = "obs_lixo";
$tdatapublic_geo_bairro[".masterListFields"][] = "expansao";

$tdatapublic_geo_bairro[".inlineAddFields"] = array();
$tdatapublic_geo_bairro[".inlineAddFields"][] = "cod_bairro";
$tdatapublic_geo_bairro[".inlineAddFields"][] = "nomebairro";
$tdatapublic_geo_bairro[".inlineAddFields"][] = "obs";

$tdatapublic_geo_bairro[".editFields"] = array();

$tdatapublic_geo_bairro[".inlineEditFields"] = array();
$tdatapublic_geo_bairro[".inlineEditFields"][] = "cod_bairro";
$tdatapublic_geo_bairro[".inlineEditFields"][] = "nomebairro";
$tdatapublic_geo_bairro[".inlineEditFields"][] = "obs";

$tdatapublic_geo_bairro[".updateSelectedFields"] = array();


$tdatapublic_geo_bairro[".exportFields"] = array();

$tdatapublic_geo_bairro[".importFields"] = array();

$tdatapublic_geo_bairro[".printFields"] = array();
$tdatapublic_geo_bairro[".printFields"][] = "nomebairro";
$tdatapublic_geo_bairro[".printFields"][] = "cod_bairro";
$tdatapublic_geo_bairro[".printFields"][] = "obs";


//	gid
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "gid";
	$fdata["GoodName"] = "gid";
	$fdata["ownerTable"] = "public.geo_bairro";
	$fdata["Label"] = GetFieldLabel("public_geo_bairro","gid");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "gid";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "gid";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_bairro["gid"] = $fdata;
//	the_geom
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "the_geom";
	$fdata["GoodName"] = "the_geom";
	$fdata["ownerTable"] = "public.geo_bairro";
	$fdata["Label"] = GetFieldLabel("public_geo_bairro","the_geom");
	$fdata["FieldType"] = 13;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "the_geom";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "the_geom";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_bairro["the_geom"] = $fdata;
//	nomebairro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "nomebairro";
	$fdata["GoodName"] = "nomebairro";
	$fdata["ownerTable"] = "public.geo_bairro";
	$fdata["Label"] = GetFieldLabel("public_geo_bairro","nomebairro");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
		$fdata["bInlineAdd"] = true;

	
		$fdata["bInlineEdit"] = true;

	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "nomebairro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nomebairro";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_bairro["nomebairro"] = $fdata;
//	cod_bairro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "cod_bairro";
	$fdata["GoodName"] = "cod_bairro";
	$fdata["ownerTable"] = "public.geo_bairro";
	$fdata["Label"] = GetFieldLabel("public_geo_bairro","cod_bairro");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
		$fdata["bInlineAdd"] = true;

	
		$fdata["bInlineEdit"] = true;

	

	
	
		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "cod_bairro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cod_bairro";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_bairro["cod_bairro"] = $fdata;
//	obs
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "obs";
	$fdata["GoodName"] = "obs";
	$fdata["ownerTable"] = "public.geo_bairro";
	$fdata["Label"] = GetFieldLabel("public_geo_bairro","obs");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
		$fdata["bInlineAdd"] = true;

	
		$fdata["bInlineEdit"] = true;

	

	
	
		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "obs";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "obs";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=254";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_bairro["obs"] = $fdata;
//	coleta_lixo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "coleta_lixo";
	$fdata["GoodName"] = "coleta_lixo";
	$fdata["ownerTable"] = "public.geo_bairro";
	$fdata["Label"] = GetFieldLabel("public_geo_bairro","coleta_lixo");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "coleta_lixo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "coleta_lixo";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_bairro["coleta_lixo"] = $fdata;
//	obs_lixo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "obs_lixo";
	$fdata["GoodName"] = "obs_lixo";
	$fdata["ownerTable"] = "public.geo_bairro";
	$fdata["Label"] = GetFieldLabel("public_geo_bairro","obs_lixo");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "obs_lixo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "obs_lixo";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_bairro["obs_lixo"] = $fdata;
//	expansao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "expansao";
	$fdata["GoodName"] = "expansao";
	$fdata["ownerTable"] = "public.geo_bairro";
	$fdata["Label"] = GetFieldLabel("public_geo_bairro","expansao");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "expansao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "expansao";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_bairro["expansao"] = $fdata;


$tables_data["public.geo_bairro"]=&$tdatapublic_geo_bairro;
$field_labels["public_geo_bairro"] = &$fieldLabelspublic_geo_bairro;
$fieldToolTips["public_geo_bairro"] = &$fieldToolTipspublic_geo_bairro;
$placeHolders["public_geo_bairro"] = &$placeHolderspublic_geo_bairro;
$page_titles["public_geo_bairro"] = &$pageTitlespublic_geo_bairro;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.geo_bairro"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.geo_bairro"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_geo_bairro()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "gid,  	the_geom,  	nomebairro,  	cod_bairro,  	obs,  	coleta_lixo,  	obs_lixo,  	expansao";
$proto0["m_strFrom"] = "FROM \"public\".geo_bairro";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "gid",
	"m_strTable" => "public.geo_bairro",
	"m_srcTableName" => "public.geo_bairro"
));

$proto6["m_sql"] = "gid";
$proto6["m_srcTableName"] = "public.geo_bairro";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "the_geom",
	"m_strTable" => "public.geo_bairro",
	"m_srcTableName" => "public.geo_bairro"
));

$proto8["m_sql"] = "the_geom";
$proto8["m_srcTableName"] = "public.geo_bairro";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "nomebairro",
	"m_strTable" => "public.geo_bairro",
	"m_srcTableName" => "public.geo_bairro"
));

$proto10["m_sql"] = "nomebairro";
$proto10["m_srcTableName"] = "public.geo_bairro";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "cod_bairro",
	"m_strTable" => "public.geo_bairro",
	"m_srcTableName" => "public.geo_bairro"
));

$proto12["m_sql"] = "cod_bairro";
$proto12["m_srcTableName"] = "public.geo_bairro";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "obs",
	"m_strTable" => "public.geo_bairro",
	"m_srcTableName" => "public.geo_bairro"
));

$proto14["m_sql"] = "obs";
$proto14["m_srcTableName"] = "public.geo_bairro";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "coleta_lixo",
	"m_strTable" => "public.geo_bairro",
	"m_srcTableName" => "public.geo_bairro"
));

$proto16["m_sql"] = "coleta_lixo";
$proto16["m_srcTableName"] = "public.geo_bairro";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "obs_lixo",
	"m_strTable" => "public.geo_bairro",
	"m_srcTableName" => "public.geo_bairro"
));

$proto18["m_sql"] = "obs_lixo";
$proto18["m_srcTableName"] = "public.geo_bairro";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "expansao",
	"m_strTable" => "public.geo_bairro",
	"m_srcTableName" => "public.geo_bairro"
));

$proto20["m_sql"] = "expansao";
$proto20["m_srcTableName"] = "public.geo_bairro";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto22=array();
$proto22["m_link"] = "SQLL_MAIN";
			$proto23=array();
$proto23["m_strName"] = "public.geo_bairro";
$proto23["m_srcTableName"] = "public.geo_bairro";
$proto23["m_columns"] = array();
$proto23["m_columns"][] = "gid";
$proto23["m_columns"][] = "the_geom";
$proto23["m_columns"][] = "nomebairro";
$proto23["m_columns"][] = "cod_bairro";
$proto23["m_columns"][] = "obs";
$proto23["m_columns"][] = "coleta_lixo";
$proto23["m_columns"][] = "obs_lixo";
$proto23["m_columns"][] = "expansao";
$obj = new SQLTable($proto23);

$proto22["m_table"] = $obj;
$proto22["m_sql"] = "\"public\".geo_bairro";
$proto22["m_alias"] = "";
$proto22["m_srcTableName"] = "public.geo_bairro";
$proto24=array();
$proto24["m_sql"] = "";
$proto24["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto24["m_column"]=$obj;
$proto24["m_contained"] = array();
$proto24["m_strCase"] = "";
$proto24["m_havingmode"] = false;
$proto24["m_inBrackets"] = false;
$proto24["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto24);

$proto22["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto22);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.geo_bairro";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_public_geo_bairro = createSqlQuery_public_geo_bairro();


	
		;

								

$tdatapublic_geo_bairro[".sqlquery"] = $queryData_public_geo_bairro;

$tableEvents["public.geo_bairro"] = new eventsBase;
$tdatapublic_geo_bairro[".hasEvents"] = false;

?>