<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_tbl_pesquisa = array();
	$tdatapublic_tbl_pesquisa[".truncateText"] = true;
	$tdatapublic_tbl_pesquisa[".NumberOfChars"] = 80;
	$tdatapublic_tbl_pesquisa[".ShortName"] = "public_tbl_pesquisa";
	$tdatapublic_tbl_pesquisa[".OwnerID"] = "";
	$tdatapublic_tbl_pesquisa[".OriginalTable"] = "public.tbl_pesquisa";

//	field labels
$fieldLabelspublic_tbl_pesquisa = array();
$fieldToolTipspublic_tbl_pesquisa = array();
$pageTitlespublic_tbl_pesquisa = array();
$placeHolderspublic_tbl_pesquisa = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"] = array();
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"] = array();
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"] = array();
	$pageTitlespublic_tbl_pesquisa["Portuguese(Brazil)"] = array();
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["pesquisa"] = "Nº Pesquisa:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["pesquisa"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["pesquisa"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["data"] = "Data:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["data"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["data"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["fotos"] = "Fotos:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["fotos"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["fotos"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["codlog"] = "Cod_log:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["codlog"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["codlog"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["endereco_imovel"] = "Logradouro:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["endereco_imovel"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["endereco_imovel"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["numero"] = "Numero:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["numero"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["numero"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["complemento"] = "Complem.:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["complemento"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["complemento"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["bairro_loteamento"] = "Bairro:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["bairro_loteamento"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["bairro_loteamento"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["preco"] = "Preço:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["preco"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["preco"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["condicoes_pagamento"] = "Cond. Pag.:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["condicoes_pagamento"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["condicoes_pagamento"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["documentacao"] = "Documentação:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["documentacao"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["documentacao"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["modalidade"] = "Tipo Pesquisa:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["modalidade"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["modalidade"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["data_modalidade"] = "Data:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["data_modalidade"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["data_modalidade"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["fonte"] = "Fonte:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["fonte"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["fonte"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["nome_fonte"] = "Nome Fonte:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["nome_fonte"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["nome_fonte"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["telefone_fonte"] = "Telefone Fonte:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["telefone_fonte"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["telefone_fonte"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["ic_imobiliaria"] = "IC:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["ic_imobiliaria"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["ic_imobiliaria"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["zona_homogenea"] = "Zona Homogenea:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["zona_homogenea"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["zona_homogenea"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["testada"] = "Testada:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["testada"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["testada"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["area_terreno"] = "área Terreno:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["area_terreno"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["area_terreno"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["area_iptu"] = "Área Cadastro:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["area_iptu"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["area_iptu"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["area_estimada"] = "Área Estimada:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["area_estimada"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["area_estimada"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["area_atualizacao"] = "Área Informada";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["area_atualizacao"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["area_atualizacao"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["forma"] = "Forma:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["forma"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["forma"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["topografia"] = "Topografia:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["topografia"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["topografia"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["posicao"] = "Posição:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["posicao"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["posicao"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["benfeitoria"] = "Benfeitoria:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["benfeitoria"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["benfeitoria"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["classe"] = "Classe:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["classe"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["classe"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["tipologia_construtiva"] = "Tipologia:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["tipologia_construtiva"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["tipologia_construtiva"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["padrao_construtivo"] = "Padrão:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["padrao_construtivo"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["padrao_construtivo"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["conservacao"] = "Conservação";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["conservacao"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["conservacao"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["estrutura"] = "Estrutura:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["estrutura"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["estrutura"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["dependencia"] = "Dependência:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["dependencia"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["dependencia"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["elevador"] = "Elevador:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["elevador"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["elevador"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["vao"] = "Vão:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["vao"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["vao"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["cobertura"] = "Cobertura:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["cobertura"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["cobertura"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["condominio"] = "Condomínio:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["condominio"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["condominio"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["idade_edificacao"] = "Idade:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["idade_edificacao"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["idade_edificacao"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["qt_dormitorio"] = "Qt. Dormitório:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["qt_dormitorio"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["qt_dormitorio"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["qt_banheiro"] = "Qt. Banheiro:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["qt_banheiro"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["qt_banheiro"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["observacao"] = "Observação:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["observacao"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["observacao"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["cadastrador"] = "Cadastrador:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["cadastrador"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["cadastrador"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["data_cadastro"] = "Data:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["data_cadastro"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["data_cadastro"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["ident"] = "Ident";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["ident"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["ident"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["agua"] = "Agua";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["agua"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["agua"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["esgoto"] = "Esgoto";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["esgoto"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["esgoto"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["energia"] = "Energia";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["energia"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["energia"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["iluminacao"] = "Iluminação";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["iluminacao"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["iluminacao"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["guia_sargeta"] = "Guia Sargeta";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["guia_sargeta"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["guia_sargeta"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["pavimento"] = "Pavimento";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["pavimento"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["pavimento"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["drenagem"] = "Drenagem";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["drenagem"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["drenagem"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["telefone"] = "Telefone";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["telefone"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["telefone"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["coleta"] = "Coleta";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["coleta"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["coleta"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["transporte"] = "Transporte";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["transporte"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["transporte"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["foto_print"] = "Foto Print";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["foto_print"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["foto_print"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["ocupacao"] = "Ocupação:";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["ocupacao"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["ocupacao"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["concluido"] = "Concluido";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["concluido"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["concluido"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["consist_solo"] = "Consist Solo";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["consist_solo"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["consist_solo"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["zh_mill"] = "Zh Mill";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["zh_mill"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["zh_mill"] = "";
	$fieldLabelspublic_tbl_pesquisa["Portuguese(Brazil)"]["ordem"] = "Ordem";
	$fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]["ordem"] = "";
	$placeHolderspublic_tbl_pesquisa["Portuguese(Brazil)"]["ordem"] = "";
	if (count($fieldToolTipspublic_tbl_pesquisa["Portuguese(Brazil)"]))
		$tdatapublic_tbl_pesquisa[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_tbl_pesquisa[""] = array();
	$fieldToolTipspublic_tbl_pesquisa[""] = array();
	$placeHolderspublic_tbl_pesquisa[""] = array();
	$pageTitlespublic_tbl_pesquisa[""] = array();
	if (count($fieldToolTipspublic_tbl_pesquisa[""]))
		$tdatapublic_tbl_pesquisa[".isUseToolTips"] = true;
}


	$tdatapublic_tbl_pesquisa[".NCSearch"] = true;



$tdatapublic_tbl_pesquisa[".shortTableName"] = "public_tbl_pesquisa";
$tdatapublic_tbl_pesquisa[".nSecOptions"] = 0;
$tdatapublic_tbl_pesquisa[".recsPerRowPrint"] = 1;
$tdatapublic_tbl_pesquisa[".mainTableOwnerID"] = "";
$tdatapublic_tbl_pesquisa[".moveNext"] = 1;
$tdatapublic_tbl_pesquisa[".entityType"] = 0;

$tdatapublic_tbl_pesquisa[".strOriginalTableName"] = "public.tbl_pesquisa";

	



$tdatapublic_tbl_pesquisa[".showAddInPopup"] = false;

$tdatapublic_tbl_pesquisa[".showEditInPopup"] = false;

$tdatapublic_tbl_pesquisa[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapublic_tbl_pesquisa[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_tbl_pesquisa[".fieldsForRegister"] = array();

$tdatapublic_tbl_pesquisa[".listAjax"] = false;

	$tdatapublic_tbl_pesquisa[".audit"] = true;

	$tdatapublic_tbl_pesquisa[".locking"] = false;

$tdatapublic_tbl_pesquisa[".edit"] = true;
$tdatapublic_tbl_pesquisa[".afterEditAction"] = 1;
$tdatapublic_tbl_pesquisa[".closePopupAfterEdit"] = 1;
$tdatapublic_tbl_pesquisa[".afterEditActionDetTable"] = "";

$tdatapublic_tbl_pesquisa[".add"] = true;
$tdatapublic_tbl_pesquisa[".afterAddAction"] = 1;
$tdatapublic_tbl_pesquisa[".closePopupAfterAdd"] = 1;
$tdatapublic_tbl_pesquisa[".afterAddActionDetTable"] = "";

$tdatapublic_tbl_pesquisa[".list"] = true;



$tdatapublic_tbl_pesquisa[".reorderRecordsByHeader"] = true;


$tdatapublic_tbl_pesquisa[".exportFormatting"] = 2;
$tdatapublic_tbl_pesquisa[".exportDelimiter"] = ",";
		


$tdatapublic_tbl_pesquisa[".exportTo"] = true;

$tdatapublic_tbl_pesquisa[".printFriendly"] = true;

$tdatapublic_tbl_pesquisa[".delete"] = true;

$tdatapublic_tbl_pesquisa[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatapublic_tbl_pesquisa[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatapublic_tbl_pesquisa[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatapublic_tbl_pesquisa[".searchSaving"] = false;
//

$tdatapublic_tbl_pesquisa[".showSearchPanel"] = true;
		$tdatapublic_tbl_pesquisa[".flexibleSearch"] = true;

$tdatapublic_tbl_pesquisa[".isUseAjaxSuggest"] = true;






$tdatapublic_tbl_pesquisa[".ajaxCodeSnippetAdded"] = false;

$tdatapublic_tbl_pesquisa[".buttonsAdded"] = false;

$tdatapublic_tbl_pesquisa[".addPageEvents"] = true;

// use timepicker for search panel
$tdatapublic_tbl_pesquisa[".isUseTimeForSearch"] = false;





$tdatapublic_tbl_pesquisa[".allSearchFields"] = array();
$tdatapublic_tbl_pesquisa[".filterFields"] = array();
$tdatapublic_tbl_pesquisa[".requiredSearchFields"] = array();

$tdatapublic_tbl_pesquisa[".allSearchFields"][] = "ic_imobiliaria";
	$tdatapublic_tbl_pesquisa[".allSearchFields"][] = "zona_homogenea";
	$tdatapublic_tbl_pesquisa[".allSearchFields"][] = "pesquisa";
	$tdatapublic_tbl_pesquisa[".allSearchFields"][] = "data";
	$tdatapublic_tbl_pesquisa[".allSearchFields"][] = "endereco_imovel";
	$tdatapublic_tbl_pesquisa[".allSearchFields"][] = "bairro_loteamento";
	$tdatapublic_tbl_pesquisa[".allSearchFields"][] = "fotos";
	

$tdatapublic_tbl_pesquisa[".googleLikeFields"] = array();
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "pesquisa";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "data";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "fotos";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "codlog";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "endereco_imovel";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "numero";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "complemento";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "bairro_loteamento";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "preco";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "condicoes_pagamento";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "documentacao";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "modalidade";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "data_modalidade";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "fonte";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "nome_fonte";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "telefone_fonte";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "ic_imobiliaria";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "zona_homogenea";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "testada";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "area_terreno";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "area_iptu";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "area_estimada";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "area_atualizacao";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "forma";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "topografia";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "posicao";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "benfeitoria";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "classe";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "tipologia_construtiva";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "padrao_construtivo";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "conservacao";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "estrutura";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "dependencia";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "elevador";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "vao";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "cobertura";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "condominio";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "idade_edificacao";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "qt_dormitorio";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "qt_banheiro";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "observacao";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "cadastrador";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "data_cadastro";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "ident";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "agua";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "esgoto";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "energia";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "iluminacao";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "guia_sargeta";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "pavimento";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "drenagem";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "telefone";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "coleta";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "transporte";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "foto_print";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "ocupacao";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "concluido";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "consist_solo";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "zh_mill";
$tdatapublic_tbl_pesquisa[".googleLikeFields"][] = "ordem";


$tdatapublic_tbl_pesquisa[".advSearchFields"] = array();
$tdatapublic_tbl_pesquisa[".advSearchFields"][] = "ic_imobiliaria";
$tdatapublic_tbl_pesquisa[".advSearchFields"][] = "zona_homogenea";
$tdatapublic_tbl_pesquisa[".advSearchFields"][] = "pesquisa";
$tdatapublic_tbl_pesquisa[".advSearchFields"][] = "data";
$tdatapublic_tbl_pesquisa[".advSearchFields"][] = "endereco_imovel";
$tdatapublic_tbl_pesquisa[".advSearchFields"][] = "bairro_loteamento";
$tdatapublic_tbl_pesquisa[".advSearchFields"][] = "fotos";

$tdatapublic_tbl_pesquisa[".tableType"] = "list";

$tdatapublic_tbl_pesquisa[".printerPageOrientation"] = 0;
$tdatapublic_tbl_pesquisa[".nPrinterPageScale"] = 100;

$tdatapublic_tbl_pesquisa[".nPrinterSplitRecords"] = 40;

$tdatapublic_tbl_pesquisa[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_tbl_pesquisa[".geocodingEnabled"] = false;




$tdatapublic_tbl_pesquisa[".printGridLayout"] = 1;

$tdatapublic_tbl_pesquisa[".listGridLayout"] = 3;

$tdatapublic_tbl_pesquisa[".isDisplayLoading"] = true;




// view page pdf

// print page pdf


$tdatapublic_tbl_pesquisa[".pageSize"] = 20;

$tdatapublic_tbl_pesquisa[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_tbl_pesquisa[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_tbl_pesquisa[".orderindexes"] = array();

$tdatapublic_tbl_pesquisa[".sqlHead"] = "SELECT pesquisa,  	\"data\",  	fotos,  	codlog,  	endereco_imovel,  	numero,  	complemento,  	bairro_loteamento,  	preco,  	condicoes_pagamento,  	documentacao,  	modalidade,  	data_modalidade,  	fonte,  	nome_fonte,  	telefone_fonte,  	ic_imobiliaria,  	zona_homogenea,  	testada,  	area_terreno,  	area_iptu,  	area_estimada,  	area_atualizacao,  	forma,  	topografia,  	posicao,  	benfeitoria,  	classe,  	tipologia_construtiva,  	padrao_construtivo,  	conservacao,  	estrutura,  	dependencia,  	elevador,  	vao,  	cobertura,  	condominio,  	idade_edificacao,  	qt_dormitorio,  	qt_banheiro,  	observacao,  	cadastrador,  	data_cadastro,  	ident,  	agua,  	esgoto,  	energia,  	iluminacao,  	guia_sargeta,  	pavimento,  	drenagem,  	telefone,  	coleta,  	transporte,  	foto_print,  	ocupacao,  	concluido,  	consist_solo,  	zh_mill,  	ordem";
$tdatapublic_tbl_pesquisa[".sqlFrom"] = "FROM \"public\".tbl_pesquisa";
$tdatapublic_tbl_pesquisa[".sqlWhereExpr"] = "";
$tdatapublic_tbl_pesquisa[".sqlTail"] = "";


//fill array of tabs for edit page
$arrEditTabs = array();
	$tabFields = array();
	
	
		$tabFields[] = "ic_imobiliaria";
	
		$tabFields[] = "zona_homogenea";
	
		$tabFields[] = "pesquisa";
	
		$tabFields[] = "data";
	
		$tabFields[] = "codlog";
	
		$tabFields[] = "endereco_imovel";
	
		$tabFields[] = "numero";
	
		$tabFields[] = "complemento";
	
		$tabFields[] = "bairro_loteamento";
	
		$tabFields[] = "preco";
	
		$tabFields[] = "condicoes_pagamento";
	
		$tabFields[] = "documentacao";
	
		$tabFields[] = "modalidade";
	
		$tabFields[] = "data_modalidade";
	
		$tabFields[] = "fonte";
	
		$tabFields[] = "nome_fonte";
	
		$tabFields[] = "telefone_fonte";
$arrEditTabs[] = array('tabId'=>'Step1',
					   'tabName'=>"Pesquisa",
					   'nType'=>'2',
					   'nOrder'=>1,
					   'tabGroup'=>0,
					   'arrFields'=> $tabFields,
					   'expandSec'=>1);
	$tabFields = array();
	
	
		$tabFields[] = "testada";
	
		$tabFields[] = "area_terreno";
	
		$tabFields[] = "forma";
	
		$tabFields[] = "topografia";
	
		$tabFields[] = "consist_solo";
	
		$tabFields[] = "posicao";
	
		$tabFields[] = "benfeitoria";
	
		$tabFields[] = "ocupacao";
$arrEditTabs[] = array('tabId'=>'Step2',
					   'tabName'=>"Dados do Imóvel",
					   'nType'=>'2',
					   'nOrder'=>19,
					   'tabGroup'=>0,
					   'arrFields'=> $tabFields,
					   'expandSec'=>1);
	$tabFields = array();
	
	
		$tabFields[] = "agua";
	
		$tabFields[] = "esgoto";
	
		$tabFields[] = "energia";
	
		$tabFields[] = "iluminacao";
	
		$tabFields[] = "guia_sargeta";
	
		$tabFields[] = "pavimento";
	
		$tabFields[] = "drenagem";
	
		$tabFields[] = "telefone";
	
		$tabFields[] = "coleta";
	
		$tabFields[] = "transporte";
$arrEditTabs[] = array('tabId'=>'Infraestrutura1',
					   'tabName'=>"Infraestrutura",
					   'nType'=>'2',
					   'nOrder'=>28,
					   'tabGroup'=>0,
					   'arrFields'=> $tabFields,
					   'expandSec'=>0);
	$tabFields = array();
	
	
		$tabFields[] = "area_iptu";
	
		$tabFields[] = "area_estimada";
	
		$tabFields[] = "area_atualizacao";
	
		$tabFields[] = "classe";
	
		$tabFields[] = "tipologia_construtiva";
	
		$tabFields[] = "padrao_construtivo";
	
		$tabFields[] = "estrutura";
	
		$tabFields[] = "conservacao";
	
		$tabFields[] = "dependencia";
	
		$tabFields[] = "elevador";
	
		$tabFields[] = "vao";
	
		$tabFields[] = "cobertura";
	
		$tabFields[] = "condominio";
	
		$tabFields[] = "idade_edificacao";
	
		$tabFields[] = "qt_dormitorio";
	
		$tabFields[] = "qt_banheiro";
$arrEditTabs[] = array('tabId'=>'Dados_da_Edifica__o1',
					   'tabName'=>"Dados da Edificação",
					   'nType'=>'2',
					   'nOrder'=>39,
					   'tabGroup'=>0,
					   'arrFields'=> $tabFields,
					   'expandSec'=>0);
	$tabFields = array();
	
	
		$tabFields[] = "observacao";
	
		$tabFields[] = "cadastrador";
	
		$tabFields[] = "data_cadastro";
	
		$tabFields[] = "fotos";
	
		$tabFields[] = "concluido";
$arrEditTabs[] = array('tabId'=>'Info_fotos1',
					   'tabName'=>"Info/fotos",
					   'nType'=>'2',
					   'nOrder'=>56,
					   'tabGroup'=>0,
					   'arrFields'=> $tabFields,
					   'expandSec'=>0);
$tdatapublic_tbl_pesquisa[".arrEditTabs"] = $arrEditTabs;


//fill array of tabs for add page
$arrAddTabs = array();
	$tabFields = array();
	
	
		$tabFields[] = "ic_imobiliaria";
	
		$tabFields[] = "zona_homogenea";
	
		$tabFields[] = "pesquisa";
	
		$tabFields[] = "data";
	
		$tabFields[] = "codlog";
	
		$tabFields[] = "endereco_imovel";
	
		$tabFields[] = "numero";
	
		$tabFields[] = "complemento";
	
		$tabFields[] = "bairro_loteamento";
	
		$tabFields[] = "preco";
	
		$tabFields[] = "condicoes_pagamento";
	
		$tabFields[] = "documentacao";
	
		$tabFields[] = "modalidade";
	
		$tabFields[] = "data_modalidade";
	
		$tabFields[] = "fonte";
	
		$tabFields[] = "nome_fonte";
	
		$tabFields[] = "telefone_fonte";
$arrAddTabs[] = array('tabId'=>'Step1',
					  'tabName'=>"Pesquisa",
					  'nType'=>'2',
					  'nOrder'=>1,
					  'tabGroup'=>0,
					  'arrFields'=> $tabFields,
					  'expandSec'=>1);
	$tabFields = array();
	
	
		$tabFields[] = "testada";
	
		$tabFields[] = "area_terreno";
	
		$tabFields[] = "forma";
	
		$tabFields[] = "topografia";
	
		$tabFields[] = "consist_solo";
	
		$tabFields[] = "posicao";
	
		$tabFields[] = "benfeitoria";
	
		$tabFields[] = "ocupacao";
$arrAddTabs[] = array('tabId'=>'Step2',
					  'tabName'=>"Dados do Imóvel",
					  'nType'=>'2',
					  'nOrder'=>19,
					  'tabGroup'=>0,
					  'arrFields'=> $tabFields,
					  'expandSec'=>1);
	$tabFields = array();
	
	
		$tabFields[] = "agua";
	
		$tabFields[] = "esgoto";
	
		$tabFields[] = "energia";
	
		$tabFields[] = "iluminacao";
	
		$tabFields[] = "guia_sargeta";
	
		$tabFields[] = "pavimento";
	
		$tabFields[] = "drenagem";
	
		$tabFields[] = "telefone";
	
		$tabFields[] = "coleta";
	
		$tabFields[] = "transporte";
$arrAddTabs[] = array('tabId'=>'Infraestrutura1',
					  'tabName'=>"Infraestrutura",
					  'nType'=>'2',
					  'nOrder'=>28,
					  'tabGroup'=>0,
					  'arrFields'=> $tabFields,
					  'expandSec'=>0);
	$tabFields = array();
	
	
		$tabFields[] = "area_iptu";
	
		$tabFields[] = "area_estimada";
	
		$tabFields[] = "area_atualizacao";
	
		$tabFields[] = "classe";
	
		$tabFields[] = "tipologia_construtiva";
	
		$tabFields[] = "padrao_construtivo";
	
		$tabFields[] = "estrutura";
	
		$tabFields[] = "conservacao";
	
		$tabFields[] = "dependencia";
	
		$tabFields[] = "elevador";
	
		$tabFields[] = "vao";
	
		$tabFields[] = "cobertura";
	
		$tabFields[] = "condominio";
	
		$tabFields[] = "idade_edificacao";
	
		$tabFields[] = "qt_dormitorio";
	
		$tabFields[] = "qt_banheiro";
$arrAddTabs[] = array('tabId'=>'Dados_da_Edifica__o1',
					  'tabName'=>"Dados da Edificação",
					  'nType'=>'2',
					  'nOrder'=>39,
					  'tabGroup'=>0,
					  'arrFields'=> $tabFields,
					  'expandSec'=>0);
	$tabFields = array();
	
	
		$tabFields[] = "observacao";
	
		$tabFields[] = "cadastrador";
	
		$tabFields[] = "data_cadastro";
	
		$tabFields[] = "fotos";
	
		$tabFields[] = "concluido";
$arrAddTabs[] = array('tabId'=>'Info_fotos1',
					  'tabName'=>"Info/fotos",
					  'nType'=>'2',
					  'nOrder'=>56,
					  'tabGroup'=>0,
					  'arrFields'=> $tabFields,
					  'expandSec'=>0);
$tdatapublic_tbl_pesquisa[".arrAddTabs"] = $arrAddTabs;



$tdatapublic_tbl_pesquisa[".addMultistep"] = true;

$tdatapublic_tbl_pesquisa[".editMultistep"] = true;




//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_tbl_pesquisa[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_tbl_pesquisa[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_tbl_pesquisa[".highlightSearchResults"] = true;

$tableKeyspublic_tbl_pesquisa = array();
$tableKeyspublic_tbl_pesquisa[] = "pesquisa";
$tdatapublic_tbl_pesquisa[".Keys"] = $tableKeyspublic_tbl_pesquisa;

$tdatapublic_tbl_pesquisa[".listFields"] = array();
$tdatapublic_tbl_pesquisa[".listFields"][] = "ident";
$tdatapublic_tbl_pesquisa[".listFields"][] = "ic_imobiliaria";
$tdatapublic_tbl_pesquisa[".listFields"][] = "zona_homogenea";
$tdatapublic_tbl_pesquisa[".listFields"][] = "pesquisa";
$tdatapublic_tbl_pesquisa[".listFields"][] = "data";
$tdatapublic_tbl_pesquisa[".listFields"][] = "codlog";
$tdatapublic_tbl_pesquisa[".listFields"][] = "endereco_imovel";
$tdatapublic_tbl_pesquisa[".listFields"][] = "numero";
$tdatapublic_tbl_pesquisa[".listFields"][] = "complemento";
$tdatapublic_tbl_pesquisa[".listFields"][] = "bairro_loteamento";
$tdatapublic_tbl_pesquisa[".listFields"][] = "preco";
$tdatapublic_tbl_pesquisa[".listFields"][] = "condicoes_pagamento";
$tdatapublic_tbl_pesquisa[".listFields"][] = "documentacao";
$tdatapublic_tbl_pesquisa[".listFields"][] = "modalidade";
$tdatapublic_tbl_pesquisa[".listFields"][] = "data_modalidade";
$tdatapublic_tbl_pesquisa[".listFields"][] = "fonte";
$tdatapublic_tbl_pesquisa[".listFields"][] = "nome_fonte";
$tdatapublic_tbl_pesquisa[".listFields"][] = "telefone_fonte";
$tdatapublic_tbl_pesquisa[".listFields"][] = "testada";
$tdatapublic_tbl_pesquisa[".listFields"][] = "area_terreno";
$tdatapublic_tbl_pesquisa[".listFields"][] = "forma";
$tdatapublic_tbl_pesquisa[".listFields"][] = "topografia";
$tdatapublic_tbl_pesquisa[".listFields"][] = "consist_solo";
$tdatapublic_tbl_pesquisa[".listFields"][] = "posicao";
$tdatapublic_tbl_pesquisa[".listFields"][] = "benfeitoria";
$tdatapublic_tbl_pesquisa[".listFields"][] = "ocupacao";
$tdatapublic_tbl_pesquisa[".listFields"][] = "agua";
$tdatapublic_tbl_pesquisa[".listFields"][] = "esgoto";
$tdatapublic_tbl_pesquisa[".listFields"][] = "energia";
$tdatapublic_tbl_pesquisa[".listFields"][] = "iluminacao";
$tdatapublic_tbl_pesquisa[".listFields"][] = "guia_sargeta";
$tdatapublic_tbl_pesquisa[".listFields"][] = "pavimento";
$tdatapublic_tbl_pesquisa[".listFields"][] = "drenagem";
$tdatapublic_tbl_pesquisa[".listFields"][] = "telefone";
$tdatapublic_tbl_pesquisa[".listFields"][] = "coleta";
$tdatapublic_tbl_pesquisa[".listFields"][] = "transporte";
$tdatapublic_tbl_pesquisa[".listFields"][] = "area_iptu";
$tdatapublic_tbl_pesquisa[".listFields"][] = "area_estimada";
$tdatapublic_tbl_pesquisa[".listFields"][] = "area_atualizacao";
$tdatapublic_tbl_pesquisa[".listFields"][] = "classe";
$tdatapublic_tbl_pesquisa[".listFields"][] = "tipologia_construtiva";
$tdatapublic_tbl_pesquisa[".listFields"][] = "padrao_construtivo";
$tdatapublic_tbl_pesquisa[".listFields"][] = "estrutura";
$tdatapublic_tbl_pesquisa[".listFields"][] = "conservacao";
$tdatapublic_tbl_pesquisa[".listFields"][] = "dependencia";
$tdatapublic_tbl_pesquisa[".listFields"][] = "elevador";
$tdatapublic_tbl_pesquisa[".listFields"][] = "vao";
$tdatapublic_tbl_pesquisa[".listFields"][] = "cobertura";
$tdatapublic_tbl_pesquisa[".listFields"][] = "condominio";
$tdatapublic_tbl_pesquisa[".listFields"][] = "idade_edificacao";
$tdatapublic_tbl_pesquisa[".listFields"][] = "qt_dormitorio";
$tdatapublic_tbl_pesquisa[".listFields"][] = "qt_banheiro";
$tdatapublic_tbl_pesquisa[".listFields"][] = "observacao";
$tdatapublic_tbl_pesquisa[".listFields"][] = "cadastrador";
$tdatapublic_tbl_pesquisa[".listFields"][] = "data_cadastro";
$tdatapublic_tbl_pesquisa[".listFields"][] = "fotos";
$tdatapublic_tbl_pesquisa[".listFields"][] = "concluido";

$tdatapublic_tbl_pesquisa[".hideMobileList"] = array();


$tdatapublic_tbl_pesquisa[".viewFields"] = array();

$tdatapublic_tbl_pesquisa[".addFields"] = array();
$tdatapublic_tbl_pesquisa[".addFields"][] = "ic_imobiliaria";
$tdatapublic_tbl_pesquisa[".addFields"][] = "zona_homogenea";
$tdatapublic_tbl_pesquisa[".addFields"][] = "pesquisa";
$tdatapublic_tbl_pesquisa[".addFields"][] = "data";
$tdatapublic_tbl_pesquisa[".addFields"][] = "codlog";
$tdatapublic_tbl_pesquisa[".addFields"][] = "endereco_imovel";
$tdatapublic_tbl_pesquisa[".addFields"][] = "numero";
$tdatapublic_tbl_pesquisa[".addFields"][] = "complemento";
$tdatapublic_tbl_pesquisa[".addFields"][] = "bairro_loteamento";
$tdatapublic_tbl_pesquisa[".addFields"][] = "preco";
$tdatapublic_tbl_pesquisa[".addFields"][] = "condicoes_pagamento";
$tdatapublic_tbl_pesquisa[".addFields"][] = "documentacao";
$tdatapublic_tbl_pesquisa[".addFields"][] = "modalidade";
$tdatapublic_tbl_pesquisa[".addFields"][] = "data_modalidade";
$tdatapublic_tbl_pesquisa[".addFields"][] = "fonte";
$tdatapublic_tbl_pesquisa[".addFields"][] = "nome_fonte";
$tdatapublic_tbl_pesquisa[".addFields"][] = "telefone_fonte";
$tdatapublic_tbl_pesquisa[".addFields"][] = "testada";
$tdatapublic_tbl_pesquisa[".addFields"][] = "area_terreno";
$tdatapublic_tbl_pesquisa[".addFields"][] = "forma";
$tdatapublic_tbl_pesquisa[".addFields"][] = "topografia";
$tdatapublic_tbl_pesquisa[".addFields"][] = "consist_solo";
$tdatapublic_tbl_pesquisa[".addFields"][] = "posicao";
$tdatapublic_tbl_pesquisa[".addFields"][] = "benfeitoria";
$tdatapublic_tbl_pesquisa[".addFields"][] = "ocupacao";
$tdatapublic_tbl_pesquisa[".addFields"][] = "agua";
$tdatapublic_tbl_pesquisa[".addFields"][] = "esgoto";
$tdatapublic_tbl_pesquisa[".addFields"][] = "energia";
$tdatapublic_tbl_pesquisa[".addFields"][] = "iluminacao";
$tdatapublic_tbl_pesquisa[".addFields"][] = "guia_sargeta";
$tdatapublic_tbl_pesquisa[".addFields"][] = "pavimento";
$tdatapublic_tbl_pesquisa[".addFields"][] = "drenagem";
$tdatapublic_tbl_pesquisa[".addFields"][] = "telefone";
$tdatapublic_tbl_pesquisa[".addFields"][] = "coleta";
$tdatapublic_tbl_pesquisa[".addFields"][] = "transporte";
$tdatapublic_tbl_pesquisa[".addFields"][] = "area_iptu";
$tdatapublic_tbl_pesquisa[".addFields"][] = "area_estimada";
$tdatapublic_tbl_pesquisa[".addFields"][] = "area_atualizacao";
$tdatapublic_tbl_pesquisa[".addFields"][] = "classe";
$tdatapublic_tbl_pesquisa[".addFields"][] = "tipologia_construtiva";
$tdatapublic_tbl_pesquisa[".addFields"][] = "padrao_construtivo";
$tdatapublic_tbl_pesquisa[".addFields"][] = "estrutura";
$tdatapublic_tbl_pesquisa[".addFields"][] = "conservacao";
$tdatapublic_tbl_pesquisa[".addFields"][] = "dependencia";
$tdatapublic_tbl_pesquisa[".addFields"][] = "elevador";
$tdatapublic_tbl_pesquisa[".addFields"][] = "vao";
$tdatapublic_tbl_pesquisa[".addFields"][] = "cobertura";
$tdatapublic_tbl_pesquisa[".addFields"][] = "condominio";
$tdatapublic_tbl_pesquisa[".addFields"][] = "idade_edificacao";
$tdatapublic_tbl_pesquisa[".addFields"][] = "qt_dormitorio";
$tdatapublic_tbl_pesquisa[".addFields"][] = "qt_banheiro";
$tdatapublic_tbl_pesquisa[".addFields"][] = "observacao";
$tdatapublic_tbl_pesquisa[".addFields"][] = "cadastrador";
$tdatapublic_tbl_pesquisa[".addFields"][] = "data_cadastro";
$tdatapublic_tbl_pesquisa[".addFields"][] = "fotos";
$tdatapublic_tbl_pesquisa[".addFields"][] = "concluido";

$tdatapublic_tbl_pesquisa[".masterListFields"] = array();
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "ident";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "ic_imobiliaria";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "zona_homogenea";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "pesquisa";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "zh_mill";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "data";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "codlog";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "endereco_imovel";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "numero";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "complemento";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "bairro_loteamento";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "preco";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "condicoes_pagamento";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "documentacao";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "modalidade";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "data_modalidade";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "fonte";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "nome_fonte";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "telefone_fonte";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "testada";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "area_terreno";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "forma";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "topografia";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "consist_solo";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "posicao";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "benfeitoria";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "ocupacao";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "agua";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "esgoto";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "energia";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "iluminacao";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "guia_sargeta";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "pavimento";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "drenagem";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "telefone";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "coleta";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "transporte";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "area_iptu";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "area_estimada";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "area_atualizacao";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "classe";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "tipologia_construtiva";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "padrao_construtivo";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "estrutura";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "conservacao";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "dependencia";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "elevador";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "vao";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "cobertura";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "condominio";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "idade_edificacao";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "qt_dormitorio";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "qt_banheiro";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "observacao";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "cadastrador";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "data_cadastro";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "fotos";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "concluido";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "foto_print";
$tdatapublic_tbl_pesquisa[".masterListFields"][] = "ordem";

$tdatapublic_tbl_pesquisa[".inlineAddFields"] = array();

$tdatapublic_tbl_pesquisa[".editFields"] = array();
$tdatapublic_tbl_pesquisa[".editFields"][] = "ic_imobiliaria";
$tdatapublic_tbl_pesquisa[".editFields"][] = "zona_homogenea";
$tdatapublic_tbl_pesquisa[".editFields"][] = "pesquisa";
$tdatapublic_tbl_pesquisa[".editFields"][] = "data";
$tdatapublic_tbl_pesquisa[".editFields"][] = "codlog";
$tdatapublic_tbl_pesquisa[".editFields"][] = "endereco_imovel";
$tdatapublic_tbl_pesquisa[".editFields"][] = "numero";
$tdatapublic_tbl_pesquisa[".editFields"][] = "complemento";
$tdatapublic_tbl_pesquisa[".editFields"][] = "bairro_loteamento";
$tdatapublic_tbl_pesquisa[".editFields"][] = "preco";
$tdatapublic_tbl_pesquisa[".editFields"][] = "condicoes_pagamento";
$tdatapublic_tbl_pesquisa[".editFields"][] = "documentacao";
$tdatapublic_tbl_pesquisa[".editFields"][] = "modalidade";
$tdatapublic_tbl_pesquisa[".editFields"][] = "data_modalidade";
$tdatapublic_tbl_pesquisa[".editFields"][] = "fonte";
$tdatapublic_tbl_pesquisa[".editFields"][] = "nome_fonte";
$tdatapublic_tbl_pesquisa[".editFields"][] = "telefone_fonte";
$tdatapublic_tbl_pesquisa[".editFields"][] = "testada";
$tdatapublic_tbl_pesquisa[".editFields"][] = "area_terreno";
$tdatapublic_tbl_pesquisa[".editFields"][] = "forma";
$tdatapublic_tbl_pesquisa[".editFields"][] = "topografia";
$tdatapublic_tbl_pesquisa[".editFields"][] = "consist_solo";
$tdatapublic_tbl_pesquisa[".editFields"][] = "posicao";
$tdatapublic_tbl_pesquisa[".editFields"][] = "benfeitoria";
$tdatapublic_tbl_pesquisa[".editFields"][] = "ocupacao";
$tdatapublic_tbl_pesquisa[".editFields"][] = "agua";
$tdatapublic_tbl_pesquisa[".editFields"][] = "esgoto";
$tdatapublic_tbl_pesquisa[".editFields"][] = "energia";
$tdatapublic_tbl_pesquisa[".editFields"][] = "iluminacao";
$tdatapublic_tbl_pesquisa[".editFields"][] = "guia_sargeta";
$tdatapublic_tbl_pesquisa[".editFields"][] = "pavimento";
$tdatapublic_tbl_pesquisa[".editFields"][] = "drenagem";
$tdatapublic_tbl_pesquisa[".editFields"][] = "telefone";
$tdatapublic_tbl_pesquisa[".editFields"][] = "coleta";
$tdatapublic_tbl_pesquisa[".editFields"][] = "transporte";
$tdatapublic_tbl_pesquisa[".editFields"][] = "area_iptu";
$tdatapublic_tbl_pesquisa[".editFields"][] = "area_estimada";
$tdatapublic_tbl_pesquisa[".editFields"][] = "area_atualizacao";
$tdatapublic_tbl_pesquisa[".editFields"][] = "classe";
$tdatapublic_tbl_pesquisa[".editFields"][] = "tipologia_construtiva";
$tdatapublic_tbl_pesquisa[".editFields"][] = "padrao_construtivo";
$tdatapublic_tbl_pesquisa[".editFields"][] = "estrutura";
$tdatapublic_tbl_pesquisa[".editFields"][] = "conservacao";
$tdatapublic_tbl_pesquisa[".editFields"][] = "dependencia";
$tdatapublic_tbl_pesquisa[".editFields"][] = "elevador";
$tdatapublic_tbl_pesquisa[".editFields"][] = "vao";
$tdatapublic_tbl_pesquisa[".editFields"][] = "cobertura";
$tdatapublic_tbl_pesquisa[".editFields"][] = "condominio";
$tdatapublic_tbl_pesquisa[".editFields"][] = "idade_edificacao";
$tdatapublic_tbl_pesquisa[".editFields"][] = "qt_dormitorio";
$tdatapublic_tbl_pesquisa[".editFields"][] = "qt_banheiro";
$tdatapublic_tbl_pesquisa[".editFields"][] = "observacao";
$tdatapublic_tbl_pesquisa[".editFields"][] = "cadastrador";
$tdatapublic_tbl_pesquisa[".editFields"][] = "data_cadastro";
$tdatapublic_tbl_pesquisa[".editFields"][] = "fotos";
$tdatapublic_tbl_pesquisa[".editFields"][] = "concluido";

$tdatapublic_tbl_pesquisa[".inlineEditFields"] = array();

$tdatapublic_tbl_pesquisa[".updateSelectedFields"] = array();
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "ic_imobiliaria";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "zona_homogenea";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "pesquisa";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "data";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "codlog";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "endereco_imovel";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "numero";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "complemento";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "bairro_loteamento";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "preco";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "condicoes_pagamento";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "documentacao";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "modalidade";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "data_modalidade";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "fonte";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "nome_fonte";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "telefone_fonte";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "testada";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "area_terreno";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "forma";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "topografia";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "consist_solo";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "posicao";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "benfeitoria";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "ocupacao";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "agua";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "esgoto";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "energia";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "iluminacao";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "guia_sargeta";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "pavimento";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "drenagem";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "telefone";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "coleta";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "transporte";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "area_iptu";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "area_estimada";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "area_atualizacao";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "classe";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "tipologia_construtiva";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "padrao_construtivo";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "estrutura";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "conservacao";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "dependencia";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "elevador";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "vao";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "cobertura";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "condominio";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "idade_edificacao";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "qt_dormitorio";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "qt_banheiro";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "observacao";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "cadastrador";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "data_cadastro";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "fotos";
$tdatapublic_tbl_pesquisa[".updateSelectedFields"][] = "concluido";


$tdatapublic_tbl_pesquisa[".exportFields"] = array();
$tdatapublic_tbl_pesquisa[".exportFields"][] = "ident";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "ic_imobiliaria";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "zona_homogenea";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "pesquisa";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "data";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "codlog";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "endereco_imovel";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "numero";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "complemento";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "bairro_loteamento";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "preco";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "condicoes_pagamento";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "documentacao";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "modalidade";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "data_modalidade";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "fonte";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "nome_fonte";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "telefone_fonte";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "testada";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "area_terreno";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "forma";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "topografia";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "consist_solo";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "posicao";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "benfeitoria";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "ocupacao";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "agua";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "esgoto";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "energia";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "iluminacao";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "guia_sargeta";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "pavimento";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "drenagem";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "telefone";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "coleta";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "transporte";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "area_iptu";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "area_estimada";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "area_atualizacao";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "classe";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "tipologia_construtiva";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "padrao_construtivo";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "estrutura";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "conservacao";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "dependencia";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "elevador";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "vao";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "cobertura";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "condominio";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "idade_edificacao";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "qt_dormitorio";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "qt_banheiro";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "observacao";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "cadastrador";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "data_cadastro";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "fotos";
$tdatapublic_tbl_pesquisa[".exportFields"][] = "concluido";

$tdatapublic_tbl_pesquisa[".importFields"] = array();

$tdatapublic_tbl_pesquisa[".printFields"] = array();
$tdatapublic_tbl_pesquisa[".printFields"][] = "ident";
$tdatapublic_tbl_pesquisa[".printFields"][] = "ic_imobiliaria";
$tdatapublic_tbl_pesquisa[".printFields"][] = "zona_homogenea";
$tdatapublic_tbl_pesquisa[".printFields"][] = "pesquisa";
$tdatapublic_tbl_pesquisa[".printFields"][] = "data";
$tdatapublic_tbl_pesquisa[".printFields"][] = "codlog";
$tdatapublic_tbl_pesquisa[".printFields"][] = "endereco_imovel";
$tdatapublic_tbl_pesquisa[".printFields"][] = "numero";
$tdatapublic_tbl_pesquisa[".printFields"][] = "complemento";
$tdatapublic_tbl_pesquisa[".printFields"][] = "bairro_loteamento";
$tdatapublic_tbl_pesquisa[".printFields"][] = "preco";
$tdatapublic_tbl_pesquisa[".printFields"][] = "condicoes_pagamento";
$tdatapublic_tbl_pesquisa[".printFields"][] = "documentacao";
$tdatapublic_tbl_pesquisa[".printFields"][] = "modalidade";
$tdatapublic_tbl_pesquisa[".printFields"][] = "data_modalidade";
$tdatapublic_tbl_pesquisa[".printFields"][] = "fonte";
$tdatapublic_tbl_pesquisa[".printFields"][] = "nome_fonte";
$tdatapublic_tbl_pesquisa[".printFields"][] = "telefone_fonte";
$tdatapublic_tbl_pesquisa[".printFields"][] = "testada";
$tdatapublic_tbl_pesquisa[".printFields"][] = "area_terreno";
$tdatapublic_tbl_pesquisa[".printFields"][] = "forma";
$tdatapublic_tbl_pesquisa[".printFields"][] = "topografia";
$tdatapublic_tbl_pesquisa[".printFields"][] = "consist_solo";
$tdatapublic_tbl_pesquisa[".printFields"][] = "posicao";
$tdatapublic_tbl_pesquisa[".printFields"][] = "benfeitoria";
$tdatapublic_tbl_pesquisa[".printFields"][] = "ocupacao";
$tdatapublic_tbl_pesquisa[".printFields"][] = "agua";
$tdatapublic_tbl_pesquisa[".printFields"][] = "esgoto";
$tdatapublic_tbl_pesquisa[".printFields"][] = "energia";
$tdatapublic_tbl_pesquisa[".printFields"][] = "iluminacao";
$tdatapublic_tbl_pesquisa[".printFields"][] = "guia_sargeta";
$tdatapublic_tbl_pesquisa[".printFields"][] = "pavimento";
$tdatapublic_tbl_pesquisa[".printFields"][] = "drenagem";
$tdatapublic_tbl_pesquisa[".printFields"][] = "telefone";
$tdatapublic_tbl_pesquisa[".printFields"][] = "coleta";
$tdatapublic_tbl_pesquisa[".printFields"][] = "transporte";
$tdatapublic_tbl_pesquisa[".printFields"][] = "area_iptu";
$tdatapublic_tbl_pesquisa[".printFields"][] = "area_estimada";
$tdatapublic_tbl_pesquisa[".printFields"][] = "area_atualizacao";
$tdatapublic_tbl_pesquisa[".printFields"][] = "classe";
$tdatapublic_tbl_pesquisa[".printFields"][] = "tipologia_construtiva";
$tdatapublic_tbl_pesquisa[".printFields"][] = "padrao_construtivo";
$tdatapublic_tbl_pesquisa[".printFields"][] = "estrutura";
$tdatapublic_tbl_pesquisa[".printFields"][] = "conservacao";
$tdatapublic_tbl_pesquisa[".printFields"][] = "dependencia";
$tdatapublic_tbl_pesquisa[".printFields"][] = "elevador";
$tdatapublic_tbl_pesquisa[".printFields"][] = "vao";
$tdatapublic_tbl_pesquisa[".printFields"][] = "cobertura";
$tdatapublic_tbl_pesquisa[".printFields"][] = "condominio";
$tdatapublic_tbl_pesquisa[".printFields"][] = "idade_edificacao";
$tdatapublic_tbl_pesquisa[".printFields"][] = "qt_dormitorio";
$tdatapublic_tbl_pesquisa[".printFields"][] = "qt_banheiro";
$tdatapublic_tbl_pesquisa[".printFields"][] = "observacao";
$tdatapublic_tbl_pesquisa[".printFields"][] = "cadastrador";
$tdatapublic_tbl_pesquisa[".printFields"][] = "foto_print";
$tdatapublic_tbl_pesquisa[".printFields"][] = "data_cadastro";
$tdatapublic_tbl_pesquisa[".printFields"][] = "fotos";
$tdatapublic_tbl_pesquisa[".printFields"][] = "concluido";


//	pesquisa
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "pesquisa";
	$fdata["GoodName"] = "pesquisa";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","pesquisa");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "pesquisa";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "pesquisa";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 42;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa["pesquisa"] = $fdata;
//	data
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "data";
	$fdata["GoodName"] = "data";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","data");
	$fdata["FieldType"] = 7;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "data";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"data\"";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 11;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 112;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings




	$tdatapublic_tbl_pesquisa["data"] = $fdata;
//	fotos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "fotos";
	$fdata["GoodName"] = "fotos";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","fotos");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "fotos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fotos";

		$fdata["DeleteAssociatedFile"] = true;

	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "File-based Image");

	
	
				$vdata["ShowThumbnail"] = true;
	$vdata["ThumbWidth"] = 72;
	$vdata["ThumbHeight"] = 72;
			$vdata["ImageWidth"] = 457;
	$vdata["ImageHeight"] = 0;

	
	
	
	
	
	
	
	
	
	
		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 0;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

		$edata["CreateThumbnail"] = true;
	$edata["StrThumbnail"] = "th";
			$edata["ThumbnailSize"] = 150;

			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa["fotos"] = $fdata;
//	codlog
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "codlog";
	$fdata["GoodName"] = "codlog";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","codlog");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "codlog";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "codlog";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 72;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["codlog"] = $fdata;
//	endereco_imovel
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "endereco_imovel";
	$fdata["GoodName"] = "endereco_imovel";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","endereco_imovel");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "endereco_imovel";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "endereco_imovel";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=200";

		$edata["controlWidth"] = 542;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa["endereco_imovel"] = $fdata;
//	numero
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "numero";
	$fdata["GoodName"] = "numero";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","numero");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "numero";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "numero";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 72;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["numero"] = $fdata;
//	complemento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "complemento";
	$fdata["GoodName"] = "complemento";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","complemento");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "complemento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "complemento";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=150";

		$edata["controlWidth"] = 172;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["complemento"] = $fdata;
//	bairro_loteamento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "bairro_loteamento";
	$fdata["GoodName"] = "bairro_loteamento";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","bairro_loteamento");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "bairro_loteamento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "bairro_loteamento";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 322;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa["bairro_loteamento"] = $fdata;
//	preco
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "preco";
	$fdata["GoodName"] = "preco";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","preco");
	$fdata["FieldType"] = 5;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "preco";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "preco";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 152;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["preco"] = $fdata;
//	condicoes_pagamento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "condicoes_pagamento";
	$fdata["GoodName"] = "condicoes_pagamento";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","condicoes_pagamento");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "condicoes_pagamento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "condicoes_pagamento";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "A vista";
	$edata["LookupValues"][] = "A Prazo";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 142;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["condicoes_pagamento"] = $fdata;
//	documentacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "documentacao";
	$fdata["GoodName"] = "documentacao";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","documentacao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "documentacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "documentacao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Escritura";
	$edata["LookupValues"][] = "Financivel";
	$edata["LookupValues"][] = "Irregular";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 272;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["documentacao"] = $fdata;
//	modalidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "modalidade";
	$fdata["GoodName"] = "modalidade";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","modalidade");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "modalidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "modalidade";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Transação";
	$edata["LookupValues"][] = "Oferta";
	$edata["LookupValues"][] = "ITBI";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 162;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["modalidade"] = $fdata;
//	data_modalidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "data_modalidade";
	$fdata["GoodName"] = "data_modalidade";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","data_modalidade");
	$fdata["FieldType"] = 7;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "data_modalidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "data_modalidade";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 11;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["data_modalidade"] = $fdata;
//	fonte
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "fonte";
	$fdata["GoodName"] = "fonte";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","fonte");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "fonte";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fonte";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 412;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["fonte"] = $fdata;
//	nome_fonte
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "nome_fonte";
	$fdata["GoodName"] = "nome_fonte";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","nome_fonte");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "nome_fonte";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nome_fonte";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 412;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["nome_fonte"] = $fdata;
//	telefone_fonte
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "telefone_fonte";
	$fdata["GoodName"] = "telefone_fonte";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","telefone_fonte");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "telefone_fonte";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "telefone_fonte";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["telefone_fonte"] = $fdata;
//	ic_imobiliaria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "ic_imobiliaria";
	$fdata["GoodName"] = "ic_imobiliaria";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","ic_imobiliaria");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ic_imobiliaria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ic_imobiliaria";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.tbl_lotes";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["autoCompleteFields"][] = array('masterF'=>"codlog", 'lookupF'=>"cod_log");
	$edata["autoCompleteFields"][] = array('masterF'=>"endereco_imovel", 'lookupF'=>"Logradouro");
	$edata["autoCompleteFields"][] = array('masterF'=>"numero", 'lookupF'=>"Numero do Imovel");
	$edata["autoCompleteFields"][] = array('masterF'=>"complemento", 'lookupF'=>"Complemento");
	$edata["autoCompleteFields"][] = array('masterF'=>"bairro_loteamento", 'lookupF'=>"Bairro");
	$edata["autoCompleteFields"][] = array('masterF'=>"area_terreno", 'lookupF'=>"Area do Terreno");
	$edata["autoCompleteFields"][] = array('masterF'=>"area_iptu", 'lookupF'=>"Area Construida");
	$edata["LCType"] = 1;

	
		
	$edata["LinkField"] = "inscricao";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "inscricao";
	
	

	
	$edata["LookupOrderBy"] = "";

	
	
	
	

	
	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 162;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa["ic_imobiliaria"] = $fdata;
//	zona_homogenea
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 18;
	$fdata["strName"] = "zona_homogenea";
	$fdata["GoodName"] = "zona_homogenea";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","zona_homogenea");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "zona_homogenea";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "zona_homogenea";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 122;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa["zona_homogenea"] = $fdata;
//	testada
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 19;
	$fdata["strName"] = "testada";
	$fdata["GoodName"] = "testada";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","testada");
	$fdata["FieldType"] = 5;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "testada";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "testada";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 252;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["testada"] = $fdata;
//	area_terreno
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 20;
	$fdata["strName"] = "area_terreno";
	$fdata["GoodName"] = "area_terreno";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","area_terreno");
	$fdata["FieldType"] = 5;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "area_terreno";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "area_terreno";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 252;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["area_terreno"] = $fdata;
//	area_iptu
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 21;
	$fdata["strName"] = "area_iptu";
	$fdata["GoodName"] = "area_iptu";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","area_iptu");
	$fdata["FieldType"] = 5;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "area_iptu";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "area_iptu";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 102;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["area_iptu"] = $fdata;
//	area_estimada
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 22;
	$fdata["strName"] = "area_estimada";
	$fdata["GoodName"] = "area_estimada";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","area_estimada");
	$fdata["FieldType"] = 5;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "area_estimada";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "area_estimada";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 102;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["area_estimada"] = $fdata;
//	area_atualizacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 23;
	$fdata["strName"] = "area_atualizacao";
	$fdata["GoodName"] = "area_atualizacao";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","area_atualizacao");
	$fdata["FieldType"] = 5;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "area_atualizacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "area_atualizacao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 102;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["area_atualizacao"] = $fdata;
//	forma
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 24;
	$fdata["strName"] = "forma";
	$fdata["GoodName"] = "forma";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","forma");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "forma";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "forma";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Regular";
	$edata["LookupValues"][] = "Irregular";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 252;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["forma"] = $fdata;
//	topografia
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 25;
	$fdata["strName"] = "topografia";
	$fdata["GoodName"] = "topografia";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","topografia");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "topografia";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "topografia";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Plano";
	$edata["LookupValues"][] = "Aclive";
	$edata["LookupValues"][] = "Declive";
	$edata["LookupValues"][] = "Irregular";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 252;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["topografia"] = $fdata;
//	posicao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 26;
	$fdata["strName"] = "posicao";
	$fdata["GoodName"] = "posicao";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","posicao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "posicao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "posicao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Meio de Quadra";
	$edata["LookupValues"][] = "Esquina";
	$edata["LookupValues"][] = "Encravado";
	$edata["LookupValues"][] = "Gleba";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 252;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["posicao"] = $fdata;
//	benfeitoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 27;
	$fdata["strName"] = "benfeitoria";
	$fdata["GoodName"] = "benfeitoria";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","benfeitoria");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "benfeitoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "benfeitoria";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Nenhuma";
	$edata["LookupValues"][] = "Muro";
	$edata["LookupValues"][] = "Passeio";
	$edata["LookupValues"][] = "Muro / Passeio";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 252;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["benfeitoria"] = $fdata;
//	classe
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 28;
	$fdata["strName"] = "classe";
	$fdata["GoodName"] = "classe";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","classe");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "classe";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "classe";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Residencial";
	$edata["LookupValues"][] = "Com. Serv. Ind.";
	$edata["LookupValues"][] = "Especial";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 202;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["classe"] = $fdata;
//	tipologia_construtiva
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 29;
	$fdata["strName"] = "tipologia_construtiva";
	$fdata["GoodName"] = "tipologia_construtiva";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","tipologia_construtiva");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "tipologia_construtiva";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tipologia_construtiva";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Casa";
	$edata["LookupValues"][] = "Apartamento";
	$edata["LookupValues"][] = "Escritório";
	$edata["LookupValues"][] = "Loja";
	$edata["LookupValues"][] = "Galpão";
	$edata["LookupValues"][] = "Industrial";
	$edata["LookupValues"][] = "Galpão/Telheiro/Barracões";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 202;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["tipologia_construtiva"] = $fdata;
//	padrao_construtivo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 30;
	$fdata["strName"] = "padrao_construtivo";
	$fdata["GoodName"] = "padrao_construtivo";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","padrao_construtivo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "padrao_construtivo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "padrao_construtivo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

		$edata["HorizontalLookup"] = true;

	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "1-Alto";
	$edata["LookupValues"][] = "2-Bom";
	$edata["LookupValues"][] = "3-Médio";
	$edata["LookupValues"][] = "4-Popular";
	$edata["LookupValues"][] = "5-Rústico";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 202;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["padrao_construtivo"] = $fdata;
//	conservacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 31;
	$fdata["strName"] = "conservacao";
	$fdata["GoodName"] = "conservacao";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","conservacao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "conservacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "conservacao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "A";
	$edata["LookupValues"][] = "B";
	$edata["LookupValues"][] = "C";
	$edata["LookupValues"][] = "D";
	$edata["LookupValues"][] = "E";
	$edata["LookupValues"][] = "F";
	$edata["LookupValues"][] = "G";
	$edata["LookupValues"][] = "H";
	$edata["LookupValues"][] = "I";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["conservacao"] = $fdata;
//	estrutura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 32;
	$fdata["strName"] = "estrutura";
	$fdata["GoodName"] = "estrutura";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","estrutura");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "estrutura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "estrutura";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Madeira";
	$edata["LookupValues"][] = "Concreto";
	$edata["LookupValues"][] = "Metálica";
	$edata["LookupValues"][] = "Mista";
	$edata["LookupValues"][] = "Alvenaria";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 202;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["estrutura"] = $fdata;
//	dependencia
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 33;
	$fdata["strName"] = "dependencia";
	$fdata["GoodName"] = "dependencia";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","dependencia");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "dependencia";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "dependencia";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Piscina";
	$edata["LookupValues"][] = "Churrasqueira";
	$edata["LookupValues"][] = "Varanda Gourmer";
	$edata["LookupValues"][] = "Playground";
	$edata["LookupValues"][] = "Salão de festas";
	$edata["LookupValues"][] = "Outros";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 194;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["dependencia"] = $fdata;
//	elevador
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 34;
	$fdata["strName"] = "elevador";
	$fdata["GoodName"] = "elevador";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","elevador");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "elevador";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "elevador";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Não";
	$edata["LookupValues"][] = "Sim";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["elevador"] = $fdata;
//	vao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 35;
	$fdata["strName"] = "vao";
	$fdata["GoodName"] = "vao";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","vao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "vao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "vao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Até 3.5 m";
	$edata["LookupValues"][] = "de 3.5 m até 6 m";
	$edata["LookupValues"][] = "acima de 6 m";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["vao"] = $fdata;
//	cobertura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 36;
	$fdata["strName"] = "cobertura";
	$fdata["GoodName"] = "cobertura";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","cobertura");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "cobertura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cobertura";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Laje";
	$edata["LookupValues"][] = "Cerâmica";
	$edata["LookupValues"][] = "Fibrocimento";
	$edata["LookupValues"][] = "Metálica";
	$edata["LookupValues"][] = "Outros";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["cobertura"] = $fdata;
//	condominio
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 37;
	$fdata["strName"] = "condominio";
	$fdata["GoodName"] = "condominio";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","condominio");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "condominio";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "condominio";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Não";
	$edata["LookupValues"][] = "Sim";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 204;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["condominio"] = $fdata;
//	idade_edificacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 38;
	$fdata["strName"] = "idade_edificacao";
	$fdata["GoodName"] = "idade_edificacao";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","idade_edificacao");
	$fdata["FieldType"] = 20;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "idade_edificacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idade_edificacao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["idade_edificacao"] = $fdata;
//	qt_dormitorio
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 39;
	$fdata["strName"] = "qt_dormitorio";
	$fdata["GoodName"] = "qt_dormitorio";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","qt_dormitorio");
	$fdata["FieldType"] = 20;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "qt_dormitorio";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "qt_dormitorio";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["qt_dormitorio"] = $fdata;
//	qt_banheiro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 40;
	$fdata["strName"] = "qt_banheiro";
	$fdata["GoodName"] = "qt_banheiro";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","qt_banheiro");
	$fdata["FieldType"] = 20;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "qt_banheiro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "qt_banheiro";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["qt_banheiro"] = $fdata;
//	observacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 41;
	$fdata["strName"] = "observacao";
	$fdata["GoodName"] = "observacao";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","observacao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "observacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "observacao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=500";

		$edata["controlWidth"] = 702;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["observacao"] = $fdata;
//	cadastrador
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 42;
	$fdata["strName"] = "cadastrador";
	$fdata["GoodName"] = "cadastrador";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","cadastrador");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "cadastrador";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cadastrador";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["cadastrador"] = $fdata;
//	data_cadastro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 43;
	$fdata["strName"] = "data_cadastro";
	$fdata["GoodName"] = "data_cadastro";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","data_cadastro");
	$fdata["FieldType"] = 7;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "data_cadastro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "data_cadastro";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 11;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["data_cadastro"] = $fdata;
//	ident
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 44;
	$fdata["strName"] = "ident";
	$fdata["GoodName"] = "ident";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","ident");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ident";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ident";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["ident"] = $fdata;
//	agua
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 45;
	$fdata["strName"] = "agua";
	$fdata["GoodName"] = "agua";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","agua");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "agua";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "agua";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "S";
	$edata["LookupValues"][] = "N";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 82;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["agua"] = $fdata;
//	esgoto
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 46;
	$fdata["strName"] = "esgoto";
	$fdata["GoodName"] = "esgoto";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","esgoto");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "esgoto";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "esgoto";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "S";
	$edata["LookupValues"][] = "N";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 82;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["esgoto"] = $fdata;
//	energia
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 47;
	$fdata["strName"] = "energia";
	$fdata["GoodName"] = "energia";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","energia");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "energia";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "energia";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "S";
	$edata["LookupValues"][] = "N";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 82;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["energia"] = $fdata;
//	iluminacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 48;
	$fdata["strName"] = "iluminacao";
	$fdata["GoodName"] = "iluminacao";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","iluminacao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "iluminacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "iluminacao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "S";
	$edata["LookupValues"][] = "N";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 82;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["iluminacao"] = $fdata;
//	guia_sargeta
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 49;
	$fdata["strName"] = "guia_sargeta";
	$fdata["GoodName"] = "guia_sargeta";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","guia_sargeta");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "guia_sargeta";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "guia_sargeta";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "S";
	$edata["LookupValues"][] = "N";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 82;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["guia_sargeta"] = $fdata;
//	pavimento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 50;
	$fdata["strName"] = "pavimento";
	$fdata["GoodName"] = "pavimento";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","pavimento");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "pavimento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "pavimento";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "S";
	$edata["LookupValues"][] = "N";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 82;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["pavimento"] = $fdata;
//	drenagem
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 51;
	$fdata["strName"] = "drenagem";
	$fdata["GoodName"] = "drenagem";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","drenagem");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "drenagem";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "drenagem";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "S";
	$edata["LookupValues"][] = "N";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 82;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["drenagem"] = $fdata;
//	telefone
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 52;
	$fdata["strName"] = "telefone";
	$fdata["GoodName"] = "telefone";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","telefone");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "telefone";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "telefone";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "S";
	$edata["LookupValues"][] = "N";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 82;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["telefone"] = $fdata;
//	coleta
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 53;
	$fdata["strName"] = "coleta";
	$fdata["GoodName"] = "coleta";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","coleta");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "coleta";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "coleta";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "S";
	$edata["LookupValues"][] = "N";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 82;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["coleta"] = $fdata;
//	transporte
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 54;
	$fdata["strName"] = "transporte";
	$fdata["GoodName"] = "transporte";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","transporte");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "transporte";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "transporte";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "S";
	$edata["LookupValues"][] = "N";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 82;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["transporte"] = $fdata;
//	foto_print
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 55;
	$fdata["strName"] = "foto_print";
	$fdata["GoodName"] = "foto_print";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","foto_print");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "foto_print";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "foto_print";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Custom");

	
	
	
	
	
	
	
	
	
	
	
	
	
		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["foto_print"] = $fdata;
//	ocupacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 56;
	$fdata["strName"] = "ocupacao";
	$fdata["GoodName"] = "ocupacao";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","ocupacao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ocupacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ocupacao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Edificado";
	$edata["LookupValues"][] = "Vago";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 252;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["ocupacao"] = $fdata;
//	concluido
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 57;
	$fdata["strName"] = "concluido";
	$fdata["GoodName"] = "concluido";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","concluido");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "concluido";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "concluido";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=5";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["concluido"] = $fdata;
//	consist_solo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 58;
	$fdata["strName"] = "consist_solo";
	$fdata["GoodName"] = "consist_solo";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","consist_solo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "consist_solo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "consist_solo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

	
	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Firme";
	$edata["LookupValues"][] = "Sujeito a alagamento";
	$edata["LookupValues"][] = "Brejoso";
	$edata["LookupValues"][] = "Rochoso";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 252;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["consist_solo"] = $fdata;
//	zh_mill
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 59;
	$fdata["strName"] = "zh_mill";
	$fdata["GoodName"] = "zh_mill";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","zh_mill");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "zh_mill";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "zh_mill";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["zh_mill"] = $fdata;
//	ordem
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 60;
	$fdata["strName"] = "ordem";
	$fdata["GoodName"] = "ordem";
	$fdata["ownerTable"] = "public.tbl_pesquisa";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa","ordem");
	$fdata["FieldType"] = 2;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "ordem";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ordem";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_pesquisa["ordem"] = $fdata;


$tables_data["public.tbl_pesquisa"]=&$tdatapublic_tbl_pesquisa;
$field_labels["public_tbl_pesquisa"] = &$fieldLabelspublic_tbl_pesquisa;
$fieldToolTips["public_tbl_pesquisa"] = &$fieldToolTipspublic_tbl_pesquisa;
$placeHolders["public_tbl_pesquisa"] = &$placeHolderspublic_tbl_pesquisa;
$page_titles["public_tbl_pesquisa"] = &$pageTitlespublic_tbl_pesquisa;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.tbl_pesquisa"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.tbl_pesquisa"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_tbl_pesquisa()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "pesquisa,  	\"data\",  	fotos,  	codlog,  	endereco_imovel,  	numero,  	complemento,  	bairro_loteamento,  	preco,  	condicoes_pagamento,  	documentacao,  	modalidade,  	data_modalidade,  	fonte,  	nome_fonte,  	telefone_fonte,  	ic_imobiliaria,  	zona_homogenea,  	testada,  	area_terreno,  	area_iptu,  	area_estimada,  	area_atualizacao,  	forma,  	topografia,  	posicao,  	benfeitoria,  	classe,  	tipologia_construtiva,  	padrao_construtivo,  	conservacao,  	estrutura,  	dependencia,  	elevador,  	vao,  	cobertura,  	condominio,  	idade_edificacao,  	qt_dormitorio,  	qt_banheiro,  	observacao,  	cadastrador,  	data_cadastro,  	ident,  	agua,  	esgoto,  	energia,  	iluminacao,  	guia_sargeta,  	pavimento,  	drenagem,  	telefone,  	coleta,  	transporte,  	foto_print,  	ocupacao,  	concluido,  	consist_solo,  	zh_mill,  	ordem";
$proto0["m_strFrom"] = "FROM \"public\".tbl_pesquisa";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "pesquisa",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto6["m_sql"] = "pesquisa";
$proto6["m_srcTableName"] = "public.tbl_pesquisa";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "data",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto8["m_sql"] = "\"data\"";
$proto8["m_srcTableName"] = "public.tbl_pesquisa";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "fotos",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto10["m_sql"] = "fotos";
$proto10["m_srcTableName"] = "public.tbl_pesquisa";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "codlog",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto12["m_sql"] = "codlog";
$proto12["m_srcTableName"] = "public.tbl_pesquisa";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "endereco_imovel",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto14["m_sql"] = "endereco_imovel";
$proto14["m_srcTableName"] = "public.tbl_pesquisa";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "numero",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto16["m_sql"] = "numero";
$proto16["m_srcTableName"] = "public.tbl_pesquisa";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "complemento",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto18["m_sql"] = "complemento";
$proto18["m_srcTableName"] = "public.tbl_pesquisa";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "bairro_loteamento",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto20["m_sql"] = "bairro_loteamento";
$proto20["m_srcTableName"] = "public.tbl_pesquisa";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "preco",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto22["m_sql"] = "preco";
$proto22["m_srcTableName"] = "public.tbl_pesquisa";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "condicoes_pagamento",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto24["m_sql"] = "condicoes_pagamento";
$proto24["m_srcTableName"] = "public.tbl_pesquisa";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "documentacao",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto26["m_sql"] = "documentacao";
$proto26["m_srcTableName"] = "public.tbl_pesquisa";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "modalidade",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto28["m_sql"] = "modalidade";
$proto28["m_srcTableName"] = "public.tbl_pesquisa";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "data_modalidade",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto30["m_sql"] = "data_modalidade";
$proto30["m_srcTableName"] = "public.tbl_pesquisa";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "fonte",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto32["m_sql"] = "fonte";
$proto32["m_srcTableName"] = "public.tbl_pesquisa";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "nome_fonte",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto34["m_sql"] = "nome_fonte";
$proto34["m_srcTableName"] = "public.tbl_pesquisa";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto36=array();
			$obj = new SQLField(array(
	"m_strName" => "telefone_fonte",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto36["m_sql"] = "telefone_fonte";
$proto36["m_srcTableName"] = "public.tbl_pesquisa";
$proto36["m_expr"]=$obj;
$proto36["m_alias"] = "";
$obj = new SQLFieldListItem($proto36);

$proto0["m_fieldlist"][]=$obj;
						$proto38=array();
			$obj = new SQLField(array(
	"m_strName" => "ic_imobiliaria",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto38["m_sql"] = "ic_imobiliaria";
$proto38["m_srcTableName"] = "public.tbl_pesquisa";
$proto38["m_expr"]=$obj;
$proto38["m_alias"] = "";
$obj = new SQLFieldListItem($proto38);

$proto0["m_fieldlist"][]=$obj;
						$proto40=array();
			$obj = new SQLField(array(
	"m_strName" => "zona_homogenea",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto40["m_sql"] = "zona_homogenea";
$proto40["m_srcTableName"] = "public.tbl_pesquisa";
$proto40["m_expr"]=$obj;
$proto40["m_alias"] = "";
$obj = new SQLFieldListItem($proto40);

$proto0["m_fieldlist"][]=$obj;
						$proto42=array();
			$obj = new SQLField(array(
	"m_strName" => "testada",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto42["m_sql"] = "testada";
$proto42["m_srcTableName"] = "public.tbl_pesquisa";
$proto42["m_expr"]=$obj;
$proto42["m_alias"] = "";
$obj = new SQLFieldListItem($proto42);

$proto0["m_fieldlist"][]=$obj;
						$proto44=array();
			$obj = new SQLField(array(
	"m_strName" => "area_terreno",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto44["m_sql"] = "area_terreno";
$proto44["m_srcTableName"] = "public.tbl_pesquisa";
$proto44["m_expr"]=$obj;
$proto44["m_alias"] = "";
$obj = new SQLFieldListItem($proto44);

$proto0["m_fieldlist"][]=$obj;
						$proto46=array();
			$obj = new SQLField(array(
	"m_strName" => "area_iptu",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto46["m_sql"] = "area_iptu";
$proto46["m_srcTableName"] = "public.tbl_pesquisa";
$proto46["m_expr"]=$obj;
$proto46["m_alias"] = "";
$obj = new SQLFieldListItem($proto46);

$proto0["m_fieldlist"][]=$obj;
						$proto48=array();
			$obj = new SQLField(array(
	"m_strName" => "area_estimada",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto48["m_sql"] = "area_estimada";
$proto48["m_srcTableName"] = "public.tbl_pesquisa";
$proto48["m_expr"]=$obj;
$proto48["m_alias"] = "";
$obj = new SQLFieldListItem($proto48);

$proto0["m_fieldlist"][]=$obj;
						$proto50=array();
			$obj = new SQLField(array(
	"m_strName" => "area_atualizacao",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto50["m_sql"] = "area_atualizacao";
$proto50["m_srcTableName"] = "public.tbl_pesquisa";
$proto50["m_expr"]=$obj;
$proto50["m_alias"] = "";
$obj = new SQLFieldListItem($proto50);

$proto0["m_fieldlist"][]=$obj;
						$proto52=array();
			$obj = new SQLField(array(
	"m_strName" => "forma",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto52["m_sql"] = "forma";
$proto52["m_srcTableName"] = "public.tbl_pesquisa";
$proto52["m_expr"]=$obj;
$proto52["m_alias"] = "";
$obj = new SQLFieldListItem($proto52);

$proto0["m_fieldlist"][]=$obj;
						$proto54=array();
			$obj = new SQLField(array(
	"m_strName" => "topografia",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto54["m_sql"] = "topografia";
$proto54["m_srcTableName"] = "public.tbl_pesquisa";
$proto54["m_expr"]=$obj;
$proto54["m_alias"] = "";
$obj = new SQLFieldListItem($proto54);

$proto0["m_fieldlist"][]=$obj;
						$proto56=array();
			$obj = new SQLField(array(
	"m_strName" => "posicao",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto56["m_sql"] = "posicao";
$proto56["m_srcTableName"] = "public.tbl_pesquisa";
$proto56["m_expr"]=$obj;
$proto56["m_alias"] = "";
$obj = new SQLFieldListItem($proto56);

$proto0["m_fieldlist"][]=$obj;
						$proto58=array();
			$obj = new SQLField(array(
	"m_strName" => "benfeitoria",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto58["m_sql"] = "benfeitoria";
$proto58["m_srcTableName"] = "public.tbl_pesquisa";
$proto58["m_expr"]=$obj;
$proto58["m_alias"] = "";
$obj = new SQLFieldListItem($proto58);

$proto0["m_fieldlist"][]=$obj;
						$proto60=array();
			$obj = new SQLField(array(
	"m_strName" => "classe",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto60["m_sql"] = "classe";
$proto60["m_srcTableName"] = "public.tbl_pesquisa";
$proto60["m_expr"]=$obj;
$proto60["m_alias"] = "";
$obj = new SQLFieldListItem($proto60);

$proto0["m_fieldlist"][]=$obj;
						$proto62=array();
			$obj = new SQLField(array(
	"m_strName" => "tipologia_construtiva",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto62["m_sql"] = "tipologia_construtiva";
$proto62["m_srcTableName"] = "public.tbl_pesquisa";
$proto62["m_expr"]=$obj;
$proto62["m_alias"] = "";
$obj = new SQLFieldListItem($proto62);

$proto0["m_fieldlist"][]=$obj;
						$proto64=array();
			$obj = new SQLField(array(
	"m_strName" => "padrao_construtivo",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto64["m_sql"] = "padrao_construtivo";
$proto64["m_srcTableName"] = "public.tbl_pesquisa";
$proto64["m_expr"]=$obj;
$proto64["m_alias"] = "";
$obj = new SQLFieldListItem($proto64);

$proto0["m_fieldlist"][]=$obj;
						$proto66=array();
			$obj = new SQLField(array(
	"m_strName" => "conservacao",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto66["m_sql"] = "conservacao";
$proto66["m_srcTableName"] = "public.tbl_pesquisa";
$proto66["m_expr"]=$obj;
$proto66["m_alias"] = "";
$obj = new SQLFieldListItem($proto66);

$proto0["m_fieldlist"][]=$obj;
						$proto68=array();
			$obj = new SQLField(array(
	"m_strName" => "estrutura",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto68["m_sql"] = "estrutura";
$proto68["m_srcTableName"] = "public.tbl_pesquisa";
$proto68["m_expr"]=$obj;
$proto68["m_alias"] = "";
$obj = new SQLFieldListItem($proto68);

$proto0["m_fieldlist"][]=$obj;
						$proto70=array();
			$obj = new SQLField(array(
	"m_strName" => "dependencia",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto70["m_sql"] = "dependencia";
$proto70["m_srcTableName"] = "public.tbl_pesquisa";
$proto70["m_expr"]=$obj;
$proto70["m_alias"] = "";
$obj = new SQLFieldListItem($proto70);

$proto0["m_fieldlist"][]=$obj;
						$proto72=array();
			$obj = new SQLField(array(
	"m_strName" => "elevador",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto72["m_sql"] = "elevador";
$proto72["m_srcTableName"] = "public.tbl_pesquisa";
$proto72["m_expr"]=$obj;
$proto72["m_alias"] = "";
$obj = new SQLFieldListItem($proto72);

$proto0["m_fieldlist"][]=$obj;
						$proto74=array();
			$obj = new SQLField(array(
	"m_strName" => "vao",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto74["m_sql"] = "vao";
$proto74["m_srcTableName"] = "public.tbl_pesquisa";
$proto74["m_expr"]=$obj;
$proto74["m_alias"] = "";
$obj = new SQLFieldListItem($proto74);

$proto0["m_fieldlist"][]=$obj;
						$proto76=array();
			$obj = new SQLField(array(
	"m_strName" => "cobertura",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto76["m_sql"] = "cobertura";
$proto76["m_srcTableName"] = "public.tbl_pesquisa";
$proto76["m_expr"]=$obj;
$proto76["m_alias"] = "";
$obj = new SQLFieldListItem($proto76);

$proto0["m_fieldlist"][]=$obj;
						$proto78=array();
			$obj = new SQLField(array(
	"m_strName" => "condominio",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto78["m_sql"] = "condominio";
$proto78["m_srcTableName"] = "public.tbl_pesquisa";
$proto78["m_expr"]=$obj;
$proto78["m_alias"] = "";
$obj = new SQLFieldListItem($proto78);

$proto0["m_fieldlist"][]=$obj;
						$proto80=array();
			$obj = new SQLField(array(
	"m_strName" => "idade_edificacao",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto80["m_sql"] = "idade_edificacao";
$proto80["m_srcTableName"] = "public.tbl_pesquisa";
$proto80["m_expr"]=$obj;
$proto80["m_alias"] = "";
$obj = new SQLFieldListItem($proto80);

$proto0["m_fieldlist"][]=$obj;
						$proto82=array();
			$obj = new SQLField(array(
	"m_strName" => "qt_dormitorio",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto82["m_sql"] = "qt_dormitorio";
$proto82["m_srcTableName"] = "public.tbl_pesquisa";
$proto82["m_expr"]=$obj;
$proto82["m_alias"] = "";
$obj = new SQLFieldListItem($proto82);

$proto0["m_fieldlist"][]=$obj;
						$proto84=array();
			$obj = new SQLField(array(
	"m_strName" => "qt_banheiro",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto84["m_sql"] = "qt_banheiro";
$proto84["m_srcTableName"] = "public.tbl_pesquisa";
$proto84["m_expr"]=$obj;
$proto84["m_alias"] = "";
$obj = new SQLFieldListItem($proto84);

$proto0["m_fieldlist"][]=$obj;
						$proto86=array();
			$obj = new SQLField(array(
	"m_strName" => "observacao",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto86["m_sql"] = "observacao";
$proto86["m_srcTableName"] = "public.tbl_pesquisa";
$proto86["m_expr"]=$obj;
$proto86["m_alias"] = "";
$obj = new SQLFieldListItem($proto86);

$proto0["m_fieldlist"][]=$obj;
						$proto88=array();
			$obj = new SQLField(array(
	"m_strName" => "cadastrador",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto88["m_sql"] = "cadastrador";
$proto88["m_srcTableName"] = "public.tbl_pesquisa";
$proto88["m_expr"]=$obj;
$proto88["m_alias"] = "";
$obj = new SQLFieldListItem($proto88);

$proto0["m_fieldlist"][]=$obj;
						$proto90=array();
			$obj = new SQLField(array(
	"m_strName" => "data_cadastro",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto90["m_sql"] = "data_cadastro";
$proto90["m_srcTableName"] = "public.tbl_pesquisa";
$proto90["m_expr"]=$obj;
$proto90["m_alias"] = "";
$obj = new SQLFieldListItem($proto90);

$proto0["m_fieldlist"][]=$obj;
						$proto92=array();
			$obj = new SQLField(array(
	"m_strName" => "ident",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto92["m_sql"] = "ident";
$proto92["m_srcTableName"] = "public.tbl_pesquisa";
$proto92["m_expr"]=$obj;
$proto92["m_alias"] = "";
$obj = new SQLFieldListItem($proto92);

$proto0["m_fieldlist"][]=$obj;
						$proto94=array();
			$obj = new SQLField(array(
	"m_strName" => "agua",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto94["m_sql"] = "agua";
$proto94["m_srcTableName"] = "public.tbl_pesquisa";
$proto94["m_expr"]=$obj;
$proto94["m_alias"] = "";
$obj = new SQLFieldListItem($proto94);

$proto0["m_fieldlist"][]=$obj;
						$proto96=array();
			$obj = new SQLField(array(
	"m_strName" => "esgoto",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto96["m_sql"] = "esgoto";
$proto96["m_srcTableName"] = "public.tbl_pesquisa";
$proto96["m_expr"]=$obj;
$proto96["m_alias"] = "";
$obj = new SQLFieldListItem($proto96);

$proto0["m_fieldlist"][]=$obj;
						$proto98=array();
			$obj = new SQLField(array(
	"m_strName" => "energia",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto98["m_sql"] = "energia";
$proto98["m_srcTableName"] = "public.tbl_pesquisa";
$proto98["m_expr"]=$obj;
$proto98["m_alias"] = "";
$obj = new SQLFieldListItem($proto98);

$proto0["m_fieldlist"][]=$obj;
						$proto100=array();
			$obj = new SQLField(array(
	"m_strName" => "iluminacao",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto100["m_sql"] = "iluminacao";
$proto100["m_srcTableName"] = "public.tbl_pesquisa";
$proto100["m_expr"]=$obj;
$proto100["m_alias"] = "";
$obj = new SQLFieldListItem($proto100);

$proto0["m_fieldlist"][]=$obj;
						$proto102=array();
			$obj = new SQLField(array(
	"m_strName" => "guia_sargeta",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto102["m_sql"] = "guia_sargeta";
$proto102["m_srcTableName"] = "public.tbl_pesquisa";
$proto102["m_expr"]=$obj;
$proto102["m_alias"] = "";
$obj = new SQLFieldListItem($proto102);

$proto0["m_fieldlist"][]=$obj;
						$proto104=array();
			$obj = new SQLField(array(
	"m_strName" => "pavimento",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto104["m_sql"] = "pavimento";
$proto104["m_srcTableName"] = "public.tbl_pesquisa";
$proto104["m_expr"]=$obj;
$proto104["m_alias"] = "";
$obj = new SQLFieldListItem($proto104);

$proto0["m_fieldlist"][]=$obj;
						$proto106=array();
			$obj = new SQLField(array(
	"m_strName" => "drenagem",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto106["m_sql"] = "drenagem";
$proto106["m_srcTableName"] = "public.tbl_pesquisa";
$proto106["m_expr"]=$obj;
$proto106["m_alias"] = "";
$obj = new SQLFieldListItem($proto106);

$proto0["m_fieldlist"][]=$obj;
						$proto108=array();
			$obj = new SQLField(array(
	"m_strName" => "telefone",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto108["m_sql"] = "telefone";
$proto108["m_srcTableName"] = "public.tbl_pesquisa";
$proto108["m_expr"]=$obj;
$proto108["m_alias"] = "";
$obj = new SQLFieldListItem($proto108);

$proto0["m_fieldlist"][]=$obj;
						$proto110=array();
			$obj = new SQLField(array(
	"m_strName" => "coleta",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto110["m_sql"] = "coleta";
$proto110["m_srcTableName"] = "public.tbl_pesquisa";
$proto110["m_expr"]=$obj;
$proto110["m_alias"] = "";
$obj = new SQLFieldListItem($proto110);

$proto0["m_fieldlist"][]=$obj;
						$proto112=array();
			$obj = new SQLField(array(
	"m_strName" => "transporte",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto112["m_sql"] = "transporte";
$proto112["m_srcTableName"] = "public.tbl_pesquisa";
$proto112["m_expr"]=$obj;
$proto112["m_alias"] = "";
$obj = new SQLFieldListItem($proto112);

$proto0["m_fieldlist"][]=$obj;
						$proto114=array();
			$obj = new SQLField(array(
	"m_strName" => "foto_print",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto114["m_sql"] = "foto_print";
$proto114["m_srcTableName"] = "public.tbl_pesquisa";
$proto114["m_expr"]=$obj;
$proto114["m_alias"] = "";
$obj = new SQLFieldListItem($proto114);

$proto0["m_fieldlist"][]=$obj;
						$proto116=array();
			$obj = new SQLField(array(
	"m_strName" => "ocupacao",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto116["m_sql"] = "ocupacao";
$proto116["m_srcTableName"] = "public.tbl_pesquisa";
$proto116["m_expr"]=$obj;
$proto116["m_alias"] = "";
$obj = new SQLFieldListItem($proto116);

$proto0["m_fieldlist"][]=$obj;
						$proto118=array();
			$obj = new SQLField(array(
	"m_strName" => "concluido",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto118["m_sql"] = "concluido";
$proto118["m_srcTableName"] = "public.tbl_pesquisa";
$proto118["m_expr"]=$obj;
$proto118["m_alias"] = "";
$obj = new SQLFieldListItem($proto118);

$proto0["m_fieldlist"][]=$obj;
						$proto120=array();
			$obj = new SQLField(array(
	"m_strName" => "consist_solo",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto120["m_sql"] = "consist_solo";
$proto120["m_srcTableName"] = "public.tbl_pesquisa";
$proto120["m_expr"]=$obj;
$proto120["m_alias"] = "";
$obj = new SQLFieldListItem($proto120);

$proto0["m_fieldlist"][]=$obj;
						$proto122=array();
			$obj = new SQLField(array(
	"m_strName" => "zh_mill",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto122["m_sql"] = "zh_mill";
$proto122["m_srcTableName"] = "public.tbl_pesquisa";
$proto122["m_expr"]=$obj;
$proto122["m_alias"] = "";
$obj = new SQLFieldListItem($proto122);

$proto0["m_fieldlist"][]=$obj;
						$proto124=array();
			$obj = new SQLField(array(
	"m_strName" => "ordem",
	"m_strTable" => "public.tbl_pesquisa",
	"m_srcTableName" => "public.tbl_pesquisa"
));

$proto124["m_sql"] = "ordem";
$proto124["m_srcTableName"] = "public.tbl_pesquisa";
$proto124["m_expr"]=$obj;
$proto124["m_alias"] = "";
$obj = new SQLFieldListItem($proto124);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto126=array();
$proto126["m_link"] = "SQLL_MAIN";
			$proto127=array();
$proto127["m_strName"] = "public.tbl_pesquisa";
$proto127["m_srcTableName"] = "public.tbl_pesquisa";
$proto127["m_columns"] = array();
$proto127["m_columns"][] = "pesquisa";
$proto127["m_columns"][] = "data";
$proto127["m_columns"][] = "fotos";
$proto127["m_columns"][] = "codlog";
$proto127["m_columns"][] = "endereco_imovel";
$proto127["m_columns"][] = "numero";
$proto127["m_columns"][] = "complemento";
$proto127["m_columns"][] = "bairro_loteamento";
$proto127["m_columns"][] = "preco";
$proto127["m_columns"][] = "condicoes_pagamento";
$proto127["m_columns"][] = "documentacao";
$proto127["m_columns"][] = "modalidade";
$proto127["m_columns"][] = "data_modalidade";
$proto127["m_columns"][] = "fonte";
$proto127["m_columns"][] = "nome_fonte";
$proto127["m_columns"][] = "telefone_fonte";
$proto127["m_columns"][] = "ic_imobiliaria";
$proto127["m_columns"][] = "zona_homogenea";
$proto127["m_columns"][] = "testada";
$proto127["m_columns"][] = "area_terreno";
$proto127["m_columns"][] = "area_iptu";
$proto127["m_columns"][] = "area_estimada";
$proto127["m_columns"][] = "area_atualizacao";
$proto127["m_columns"][] = "forma";
$proto127["m_columns"][] = "topografia";
$proto127["m_columns"][] = "posicao";
$proto127["m_columns"][] = "benfeitoria";
$proto127["m_columns"][] = "classe";
$proto127["m_columns"][] = "tipologia_construtiva";
$proto127["m_columns"][] = "padrao_construtivo";
$proto127["m_columns"][] = "conservacao";
$proto127["m_columns"][] = "estrutura";
$proto127["m_columns"][] = "dependencia";
$proto127["m_columns"][] = "elevador";
$proto127["m_columns"][] = "vao";
$proto127["m_columns"][] = "cobertura";
$proto127["m_columns"][] = "condominio";
$proto127["m_columns"][] = "idade_edificacao";
$proto127["m_columns"][] = "qt_dormitorio";
$proto127["m_columns"][] = "qt_banheiro";
$proto127["m_columns"][] = "observacao";
$proto127["m_columns"][] = "cadastrador";
$proto127["m_columns"][] = "data_cadastro";
$proto127["m_columns"][] = "ident";
$proto127["m_columns"][] = "agua";
$proto127["m_columns"][] = "esgoto";
$proto127["m_columns"][] = "energia";
$proto127["m_columns"][] = "iluminacao";
$proto127["m_columns"][] = "guia_sargeta";
$proto127["m_columns"][] = "pavimento";
$proto127["m_columns"][] = "drenagem";
$proto127["m_columns"][] = "telefone";
$proto127["m_columns"][] = "coleta";
$proto127["m_columns"][] = "transporte";
$proto127["m_columns"][] = "foto_print";
$proto127["m_columns"][] = "ocupacao";
$proto127["m_columns"][] = "concluido";
$proto127["m_columns"][] = "consist_solo";
$proto127["m_columns"][] = "zh_mill";
$proto127["m_columns"][] = "ordem";
$obj = new SQLTable($proto127);

$proto126["m_table"] = $obj;
$proto126["m_sql"] = "\"public\".tbl_pesquisa";
$proto126["m_alias"] = "";
$proto126["m_srcTableName"] = "public.tbl_pesquisa";
$proto128=array();
$proto128["m_sql"] = "";
$proto128["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto128["m_column"]=$obj;
$proto128["m_contained"] = array();
$proto128["m_strCase"] = "";
$proto128["m_havingmode"] = false;
$proto128["m_inBrackets"] = false;
$proto128["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto128);

$proto126["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto126);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.tbl_pesquisa";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_public_tbl_pesquisa = createSqlQuery_public_tbl_pesquisa();


	
		;

																																																												

$tdatapublic_tbl_pesquisa[".sqlquery"] = $queryData_public_tbl_pesquisa;

include_once(getabspath("include/public_tbl_pesquisa_events.php"));
$tableEvents["public.tbl_pesquisa"] = new eventclass_public_tbl_pesquisa;
$tdatapublic_tbl_pesquisa[".hasEvents"] = true;

?>