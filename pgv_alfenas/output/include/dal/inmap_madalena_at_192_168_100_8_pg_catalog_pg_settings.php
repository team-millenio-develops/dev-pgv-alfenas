<?php
$dalTablepg_settings = array();
$dalTablepg_settings["name"] = array("type"=>201,"varname"=>"name", "name" => "name");
$dalTablepg_settings["setting"] = array("type"=>201,"varname"=>"setting", "name" => "setting");
$dalTablepg_settings["unit"] = array("type"=>201,"varname"=>"unit", "name" => "unit");
$dalTablepg_settings["category"] = array("type"=>201,"varname"=>"category", "name" => "category");
$dalTablepg_settings["short_desc"] = array("type"=>201,"varname"=>"short_desc", "name" => "short_desc");
$dalTablepg_settings["extra_desc"] = array("type"=>201,"varname"=>"extra_desc", "name" => "extra_desc");
$dalTablepg_settings["context"] = array("type"=>201,"varname"=>"context", "name" => "context");
$dalTablepg_settings["vartype"] = array("type"=>201,"varname"=>"vartype", "name" => "vartype");
$dalTablepg_settings["source"] = array("type"=>201,"varname"=>"source", "name" => "source");
$dalTablepg_settings["min_val"] = array("type"=>201,"varname"=>"min_val", "name" => "min_val");
$dalTablepg_settings["max_val"] = array("type"=>201,"varname"=>"max_val", "name" => "max_val");
$dalTablepg_settings["enumvals"] = array("type"=>13,"varname"=>"enumvals", "name" => "enumvals");
$dalTablepg_settings["boot_val"] = array("type"=>201,"varname"=>"boot_val", "name" => "boot_val");
$dalTablepg_settings["reset_val"] = array("type"=>201,"varname"=>"reset_val", "name" => "reset_val");
$dalTablepg_settings["sourcefile"] = array("type"=>201,"varname"=>"sourcefile", "name" => "sourcefile");
$dalTablepg_settings["sourceline"] = array("type"=>3,"varname"=>"sourceline", "name" => "sourceline");

$dal_info["inmap_madalena_at_192_168_100_8_pg_catalog_pg_settings"] = &$dalTablepg_settings;
?>