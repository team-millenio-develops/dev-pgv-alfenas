<?php
$dalTablegeo_face_quadra_exclusao = array();
$dalTablegeo_face_quadra_exclusao["cod_bairro"] = array("type"=>200,"varname"=>"cod_bairro", "name" => "cod_bairro");
$dalTablegeo_face_quadra_exclusao["cod_quadra"] = array("type"=>200,"varname"=>"cod_quadra", "name" => "cod_quadra");
$dalTablegeo_face_quadra_exclusao["cod_log"] = array("type"=>200,"varname"=>"cod_log", "name" => "cod_log");
$dalTablegeo_face_quadra_exclusao["zn"] = array("type"=>200,"varname"=>"zn", "name" => "zn");
$dalTablegeo_face_quadra_exclusao["grupos_per"] = array("type"=>200,"varname"=>"grupos_per", "name" => "grupos_per");
$dalTablegeo_face_quadra_exclusao["obs"] = array("type"=>200,"varname"=>"obs", "name" => "obs");
$dalTablegeo_face_quadra_exclusao["cod_face"] = array("type"=>200,"varname"=>"cod_face", "name" => "cod_face");
$dalTablegeo_face_quadra_exclusao["tipo_log"] = array("type"=>200,"varname"=>"tipo_log", "name" => "tipo_log");
$dalTablegeo_face_quadra_exclusao["logradouro"] = array("type"=>200,"varname"=>"logradouro", "name" => "logradouro");
$dalTablegeo_face_quadra_exclusao["zh"] = array("type"=>200,"varname"=>"zh", "name" => "zh");
$dalTablegeo_face_quadra_exclusao["revisao"] = array("type"=>200,"varname"=>"revisao", "name" => "revisao");
$dalTablegeo_face_quadra_exclusao["the_geom"] = array("type"=>13,"varname"=>"the_geom", "name" => "the_geom");
$dalTablegeo_face_quadra_exclusao["esgoto"] = array("type"=>200,"varname"=>"esgoto", "name" => "esgoto");
$dalTablegeo_face_quadra_exclusao["telefone"] = array("type"=>200,"varname"=>"telefone", "name" => "telefone");
$dalTablegeo_face_quadra_exclusao["agua"] = array("type"=>200,"varname"=>"agua", "name" => "agua");
$dalTablegeo_face_quadra_exclusao["iluminacao"] = array("type"=>200,"varname"=>"iluminacao", "name" => "iluminacao");
$dalTablegeo_face_quadra_exclusao["energia"] = array("type"=>200,"varname"=>"energia", "name" => "energia");
$dalTablegeo_face_quadra_exclusao["pavimento"] = array("type"=>200,"varname"=>"pavimento", "name" => "pavimento");
$dalTablegeo_face_quadra_exclusao["guia"] = array("type"=>200,"varname"=>"guia", "name" => "guia");
$dalTablegeo_face_quadra_exclusao["lixo"] = array("type"=>200,"varname"=>"lixo", "name" => "lixo");
$dalTablegeo_face_quadra_exclusao["transporte"] = array("type"=>200,"varname"=>"transporte", "name" => "transporte");
$dalTablegeo_face_quadra_exclusao["vbu_pmt"] = array("type"=>5,"varname"=>"vbu_pmt", "name" => "vbu_pmt");
$dalTablegeo_face_quadra_exclusao["vbu_pvg"] = array("type"=>5,"varname"=>"vbu_pvg", "name" => "vbu_pvg");
$dalTablegeo_face_quadra_exclusao["relacao_tbl_face_orig"] = array("type"=>200,"varname"=>"relacao_tbl_face_orig", "name" => "relacao_tbl_face_orig");
$dalTablegeo_face_quadra_exclusao["drenagem"] = array("type"=>200,"varname"=>"drenagem", "name" => "drenagem");
$dalTablegeo_face_quadra_exclusao["bairro"] = array("type"=>200,"varname"=>"bairro", "name" => "bairro");
$dalTablegeo_face_quadra_exclusao["gid"] = array("type"=>20,"varname"=>"gid", "name" => "gid");
$dalTablegeo_face_quadra_exclusao["inclusao"] = array("type"=>200,"varname"=>"inclusao", "name" => "inclusao");
$dalTablegeo_face_quadra_exclusao["limpeza"] = array("type"=>200,"varname"=>"limpeza", "name" => "limpeza");
$dalTablegeo_face_quadra_exclusao["observacao"] = array("type"=>200,"varname"=>"observacao", "name" => "observacao");

$dal_info["inmap_madalena_at_192_168_100_8_public_geo_face_quadra_exclusao"] = &$dalTablegeo_face_quadra_exclusao;
?>