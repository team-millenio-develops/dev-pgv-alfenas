<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_tbl_fonte = array();
	$tdatapublic_tbl_fonte[".truncateText"] = true;
	$tdatapublic_tbl_fonte[".NumberOfChars"] = 80;
	$tdatapublic_tbl_fonte[".ShortName"] = "public_tbl_fonte";
	$tdatapublic_tbl_fonte[".OwnerID"] = "";
	$tdatapublic_tbl_fonte[".OriginalTable"] = "public.tbl_fonte";

//	field labels
$fieldLabelspublic_tbl_fonte = array();
$fieldToolTipspublic_tbl_fonte = array();
$pageTitlespublic_tbl_fonte = array();
$placeHolderspublic_tbl_fonte = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelspublic_tbl_fonte["Portuguese(Brazil)"] = array();
	$fieldToolTipspublic_tbl_fonte["Portuguese(Brazil)"] = array();
	$placeHolderspublic_tbl_fonte["Portuguese(Brazil)"] = array();
	$pageTitlespublic_tbl_fonte["Portuguese(Brazil)"] = array();
	$fieldLabelspublic_tbl_fonte["Portuguese(Brazil)"]["ident"] = "Ident";
	$fieldToolTipspublic_tbl_fonte["Portuguese(Brazil)"]["ident"] = "";
	$placeHolderspublic_tbl_fonte["Portuguese(Brazil)"]["ident"] = "";
	$fieldLabelspublic_tbl_fonte["Portuguese(Brazil)"]["fonte"] = "Fonte";
	$fieldToolTipspublic_tbl_fonte["Portuguese(Brazil)"]["fonte"] = "";
	$placeHolderspublic_tbl_fonte["Portuguese(Brazil)"]["fonte"] = "";
	$fieldLabelspublic_tbl_fonte["Portuguese(Brazil)"]["nome_fonte"] = "Nome Fonte";
	$fieldToolTipspublic_tbl_fonte["Portuguese(Brazil)"]["nome_fonte"] = "";
	$placeHolderspublic_tbl_fonte["Portuguese(Brazil)"]["nome_fonte"] = "";
	$fieldLabelspublic_tbl_fonte["Portuguese(Brazil)"]["tel_fonte"] = "Tel Fonte";
	$fieldToolTipspublic_tbl_fonte["Portuguese(Brazil)"]["tel_fonte"] = "";
	$placeHolderspublic_tbl_fonte["Portuguese(Brazil)"]["tel_fonte"] = "";
	$fieldLabelspublic_tbl_fonte["Portuguese(Brazil)"]["qt"] = "Qt";
	$fieldToolTipspublic_tbl_fonte["Portuguese(Brazil)"]["qt"] = "";
	$placeHolderspublic_tbl_fonte["Portuguese(Brazil)"]["qt"] = "";
	if (count($fieldToolTipspublic_tbl_fonte["Portuguese(Brazil)"]))
		$tdatapublic_tbl_fonte[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_tbl_fonte[""] = array();
	$fieldToolTipspublic_tbl_fonte[""] = array();
	$placeHolderspublic_tbl_fonte[""] = array();
	$pageTitlespublic_tbl_fonte[""] = array();
	if (count($fieldToolTipspublic_tbl_fonte[""]))
		$tdatapublic_tbl_fonte[".isUseToolTips"] = true;
}


	$tdatapublic_tbl_fonte[".NCSearch"] = true;



$tdatapublic_tbl_fonte[".shortTableName"] = "public_tbl_fonte";
$tdatapublic_tbl_fonte[".nSecOptions"] = 0;
$tdatapublic_tbl_fonte[".recsPerRowPrint"] = 1;
$tdatapublic_tbl_fonte[".mainTableOwnerID"] = "";
$tdatapublic_tbl_fonte[".moveNext"] = 1;
$tdatapublic_tbl_fonte[".entityType"] = 0;

$tdatapublic_tbl_fonte[".strOriginalTableName"] = "public.tbl_fonte";

	



$tdatapublic_tbl_fonte[".showAddInPopup"] = false;

$tdatapublic_tbl_fonte[".showEditInPopup"] = false;

$tdatapublic_tbl_fonte[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapublic_tbl_fonte[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_tbl_fonte[".fieldsForRegister"] = array();

$tdatapublic_tbl_fonte[".listAjax"] = false;

	$tdatapublic_tbl_fonte[".audit"] = false;

	$tdatapublic_tbl_fonte[".locking"] = false;



$tdatapublic_tbl_fonte[".list"] = true;

$tdatapublic_tbl_fonte[".inlineEdit"] = true;


$tdatapublic_tbl_fonte[".reorderRecordsByHeader"] = true;



$tdatapublic_tbl_fonte[".inlineAdd"] = true;





$tdatapublic_tbl_fonte[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatapublic_tbl_fonte[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatapublic_tbl_fonte[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatapublic_tbl_fonte[".searchSaving"] = false;
//

$tdatapublic_tbl_fonte[".showSearchPanel"] = true;
		$tdatapublic_tbl_fonte[".flexibleSearch"] = true;

$tdatapublic_tbl_fonte[".isUseAjaxSuggest"] = true;






$tdatapublic_tbl_fonte[".ajaxCodeSnippetAdded"] = false;

$tdatapublic_tbl_fonte[".buttonsAdded"] = false;

$tdatapublic_tbl_fonte[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapublic_tbl_fonte[".isUseTimeForSearch"] = false;





$tdatapublic_tbl_fonte[".allSearchFields"] = array();
$tdatapublic_tbl_fonte[".filterFields"] = array();
$tdatapublic_tbl_fonte[".requiredSearchFields"] = array();

$tdatapublic_tbl_fonte[".allSearchFields"][] = "fonte";
	$tdatapublic_tbl_fonte[".allSearchFields"][] = "nome_fonte";
	$tdatapublic_tbl_fonte[".allSearchFields"][] = "tel_fonte";
	

$tdatapublic_tbl_fonte[".googleLikeFields"] = array();
$tdatapublic_tbl_fonte[".googleLikeFields"][] = "ident";
$tdatapublic_tbl_fonte[".googleLikeFields"][] = "fonte";
$tdatapublic_tbl_fonte[".googleLikeFields"][] = "nome_fonte";
$tdatapublic_tbl_fonte[".googleLikeFields"][] = "tel_fonte";
$tdatapublic_tbl_fonte[".googleLikeFields"][] = "qt";


$tdatapublic_tbl_fonte[".advSearchFields"] = array();
$tdatapublic_tbl_fonte[".advSearchFields"][] = "fonte";
$tdatapublic_tbl_fonte[".advSearchFields"][] = "nome_fonte";
$tdatapublic_tbl_fonte[".advSearchFields"][] = "tel_fonte";

$tdatapublic_tbl_fonte[".tableType"] = "list";

$tdatapublic_tbl_fonte[".printerPageOrientation"] = 0;
$tdatapublic_tbl_fonte[".nPrinterPageScale"] = 100;

$tdatapublic_tbl_fonte[".nPrinterSplitRecords"] = 40;

$tdatapublic_tbl_fonte[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_tbl_fonte[".geocodingEnabled"] = false;





$tdatapublic_tbl_fonte[".listGridLayout"] = 3;

$tdatapublic_tbl_fonte[".isDisplayLoading"] = true;




// view page pdf

// print page pdf


$tdatapublic_tbl_fonte[".pageSize"] = 20;

$tdatapublic_tbl_fonte[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_tbl_fonte[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_tbl_fonte[".orderindexes"] = array();

$tdatapublic_tbl_fonte[".sqlHead"] = "SELECT ident,  	fonte,  	nome_fonte,  	tel_fonte,  	qt";
$tdatapublic_tbl_fonte[".sqlFrom"] = "FROM \"public\".tbl_fonte";
$tdatapublic_tbl_fonte[".sqlWhereExpr"] = "";
$tdatapublic_tbl_fonte[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_tbl_fonte[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_tbl_fonte[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_tbl_fonte[".highlightSearchResults"] = true;

$tableKeyspublic_tbl_fonte = array();
$tableKeyspublic_tbl_fonte[] = "ident";
$tdatapublic_tbl_fonte[".Keys"] = $tableKeyspublic_tbl_fonte;

$tdatapublic_tbl_fonte[".listFields"] = array();
$tdatapublic_tbl_fonte[".listFields"][] = "ident";
$tdatapublic_tbl_fonte[".listFields"][] = "fonte";
$tdatapublic_tbl_fonte[".listFields"][] = "nome_fonte";
$tdatapublic_tbl_fonte[".listFields"][] = "tel_fonte";
$tdatapublic_tbl_fonte[".listFields"][] = "qt";

$tdatapublic_tbl_fonte[".hideMobileList"] = array();


$tdatapublic_tbl_fonte[".viewFields"] = array();

$tdatapublic_tbl_fonte[".addFields"] = array();

$tdatapublic_tbl_fonte[".masterListFields"] = array();
$tdatapublic_tbl_fonte[".masterListFields"][] = "ident";
$tdatapublic_tbl_fonte[".masterListFields"][] = "fonte";
$tdatapublic_tbl_fonte[".masterListFields"][] = "nome_fonte";
$tdatapublic_tbl_fonte[".masterListFields"][] = "tel_fonte";
$tdatapublic_tbl_fonte[".masterListFields"][] = "qt";

$tdatapublic_tbl_fonte[".inlineAddFields"] = array();
$tdatapublic_tbl_fonte[".inlineAddFields"][] = "fonte";
$tdatapublic_tbl_fonte[".inlineAddFields"][] = "nome_fonte";
$tdatapublic_tbl_fonte[".inlineAddFields"][] = "tel_fonte";
$tdatapublic_tbl_fonte[".inlineAddFields"][] = "qt";

$tdatapublic_tbl_fonte[".editFields"] = array();

$tdatapublic_tbl_fonte[".inlineEditFields"] = array();
$tdatapublic_tbl_fonte[".inlineEditFields"][] = "fonte";
$tdatapublic_tbl_fonte[".inlineEditFields"][] = "nome_fonte";
$tdatapublic_tbl_fonte[".inlineEditFields"][] = "tel_fonte";
$tdatapublic_tbl_fonte[".inlineEditFields"][] = "qt";

$tdatapublic_tbl_fonte[".updateSelectedFields"] = array();


$tdatapublic_tbl_fonte[".exportFields"] = array();

$tdatapublic_tbl_fonte[".importFields"] = array();

$tdatapublic_tbl_fonte[".printFields"] = array();


//	ident
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ident";
	$fdata["GoodName"] = "ident";
	$fdata["ownerTable"] = "public.tbl_fonte";
	$fdata["Label"] = GetFieldLabel("public_tbl_fonte","ident");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "ident";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ident";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_fonte["ident"] = $fdata;
//	fonte
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "fonte";
	$fdata["GoodName"] = "fonte";
	$fdata["ownerTable"] = "public.tbl_fonte";
	$fdata["Label"] = GetFieldLabel("public_tbl_fonte","fonte");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
		$fdata["bInlineAdd"] = true;

	
		$fdata["bInlineEdit"] = true;

	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "fonte";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fonte";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_fonte["fonte"] = $fdata;
//	nome_fonte
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "nome_fonte";
	$fdata["GoodName"] = "nome_fonte";
	$fdata["ownerTable"] = "public.tbl_fonte";
	$fdata["Label"] = GetFieldLabel("public_tbl_fonte","nome_fonte");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
		$fdata["bInlineAdd"] = true;

	
		$fdata["bInlineEdit"] = true;

	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "nome_fonte";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nome_fonte";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_fonte["nome_fonte"] = $fdata;
//	tel_fonte
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "tel_fonte";
	$fdata["GoodName"] = "tel_fonte";
	$fdata["ownerTable"] = "public.tbl_fonte";
	$fdata["Label"] = GetFieldLabel("public_tbl_fonte","tel_fonte");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
		$fdata["bInlineAdd"] = true;

	
		$fdata["bInlineEdit"] = true;

	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "tel_fonte";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tel_fonte";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_fonte["tel_fonte"] = $fdata;
//	qt
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "qt";
	$fdata["GoodName"] = "qt";
	$fdata["ownerTable"] = "public.tbl_fonte";
	$fdata["Label"] = GetFieldLabel("public_tbl_fonte","qt");
	$fdata["FieldType"] = 3;

	
	
	
			
		$fdata["bListPage"] = true;

	
		$fdata["bInlineAdd"] = true;

	
		$fdata["bInlineEdit"] = true;

	

	
	
	
	
		$fdata["strField"] = "qt";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "qt";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_fonte["qt"] = $fdata;


$tables_data["public.tbl_fonte"]=&$tdatapublic_tbl_fonte;
$field_labels["public_tbl_fonte"] = &$fieldLabelspublic_tbl_fonte;
$fieldToolTips["public_tbl_fonte"] = &$fieldToolTipspublic_tbl_fonte;
$placeHolders["public_tbl_fonte"] = &$placeHolderspublic_tbl_fonte;
$page_titles["public_tbl_fonte"] = &$pageTitlespublic_tbl_fonte;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.tbl_fonte"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.tbl_fonte"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_tbl_fonte()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ident,  	fonte,  	nome_fonte,  	tel_fonte,  	qt";
$proto0["m_strFrom"] = "FROM \"public\".tbl_fonte";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ident",
	"m_strTable" => "public.tbl_fonte",
	"m_srcTableName" => "public.tbl_fonte"
));

$proto6["m_sql"] = "ident";
$proto6["m_srcTableName"] = "public.tbl_fonte";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "fonte",
	"m_strTable" => "public.tbl_fonte",
	"m_srcTableName" => "public.tbl_fonte"
));

$proto8["m_sql"] = "fonte";
$proto8["m_srcTableName"] = "public.tbl_fonte";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "nome_fonte",
	"m_strTable" => "public.tbl_fonte",
	"m_srcTableName" => "public.tbl_fonte"
));

$proto10["m_sql"] = "nome_fonte";
$proto10["m_srcTableName"] = "public.tbl_fonte";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "tel_fonte",
	"m_strTable" => "public.tbl_fonte",
	"m_srcTableName" => "public.tbl_fonte"
));

$proto12["m_sql"] = "tel_fonte";
$proto12["m_srcTableName"] = "public.tbl_fonte";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "qt",
	"m_strTable" => "public.tbl_fonte",
	"m_srcTableName" => "public.tbl_fonte"
));

$proto14["m_sql"] = "qt";
$proto14["m_srcTableName"] = "public.tbl_fonte";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto16=array();
$proto16["m_link"] = "SQLL_MAIN";
			$proto17=array();
$proto17["m_strName"] = "public.tbl_fonte";
$proto17["m_srcTableName"] = "public.tbl_fonte";
$proto17["m_columns"] = array();
$proto17["m_columns"][] = "ident";
$proto17["m_columns"][] = "fonte";
$proto17["m_columns"][] = "nome_fonte";
$proto17["m_columns"][] = "tel_fonte";
$proto17["m_columns"][] = "qt";
$obj = new SQLTable($proto17);

$proto16["m_table"] = $obj;
$proto16["m_sql"] = "\"public\".tbl_fonte";
$proto16["m_alias"] = "";
$proto16["m_srcTableName"] = "public.tbl_fonte";
$proto18=array();
$proto18["m_sql"] = "";
$proto18["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto18["m_column"]=$obj;
$proto18["m_contained"] = array();
$proto18["m_strCase"] = "";
$proto18["m_havingmode"] = false;
$proto18["m_inBrackets"] = false;
$proto18["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto18);

$proto16["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto16);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.tbl_fonte";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_public_tbl_fonte = createSqlQuery_public_tbl_fonte();


	
		;

					

$tdatapublic_tbl_fonte[".sqlquery"] = $queryData_public_tbl_fonte;

$tableEvents["public.tbl_fonte"] = new eventsBase;
$tdatapublic_tbl_fonte[".hasEvents"] = false;

?>