<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_tbl_aux_tipologia = array();
	$tdatapublic_tbl_aux_tipologia[".truncateText"] = true;
	$tdatapublic_tbl_aux_tipologia[".NumberOfChars"] = 80;
	$tdatapublic_tbl_aux_tipologia[".ShortName"] = "public_tbl_aux_tipologia";
	$tdatapublic_tbl_aux_tipologia[".OwnerID"] = "";
	$tdatapublic_tbl_aux_tipologia[".OriginalTable"] = "public.tbl_aux_tipologia";

//	field labels
$fieldLabelspublic_tbl_aux_tipologia = array();
$fieldToolTipspublic_tbl_aux_tipologia = array();
$pageTitlespublic_tbl_aux_tipologia = array();
$placeHolderspublic_tbl_aux_tipologia = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelspublic_tbl_aux_tipologia["Portuguese(Brazil)"] = array();
	$fieldToolTipspublic_tbl_aux_tipologia["Portuguese(Brazil)"] = array();
	$placeHolderspublic_tbl_aux_tipologia["Portuguese(Brazil)"] = array();
	$pageTitlespublic_tbl_aux_tipologia["Portuguese(Brazil)"] = array();
	$fieldLabelspublic_tbl_aux_tipologia["Portuguese(Brazil)"]["ident"] = "Ident";
	$fieldToolTipspublic_tbl_aux_tipologia["Portuguese(Brazil)"]["ident"] = "";
	$placeHolderspublic_tbl_aux_tipologia["Portuguese(Brazil)"]["ident"] = "";
	$fieldLabelspublic_tbl_aux_tipologia["Portuguese(Brazil)"]["descricao"] = "Descricao";
	$fieldToolTipspublic_tbl_aux_tipologia["Portuguese(Brazil)"]["descricao"] = "";
	$placeHolderspublic_tbl_aux_tipologia["Portuguese(Brazil)"]["descricao"] = "";
	if (count($fieldToolTipspublic_tbl_aux_tipologia["Portuguese(Brazil)"]))
		$tdatapublic_tbl_aux_tipologia[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_tbl_aux_tipologia[""] = array();
	$fieldToolTipspublic_tbl_aux_tipologia[""] = array();
	$placeHolderspublic_tbl_aux_tipologia[""] = array();
	$pageTitlespublic_tbl_aux_tipologia[""] = array();
	if (count($fieldToolTipspublic_tbl_aux_tipologia[""]))
		$tdatapublic_tbl_aux_tipologia[".isUseToolTips"] = true;
}


	$tdatapublic_tbl_aux_tipologia[".NCSearch"] = true;



$tdatapublic_tbl_aux_tipologia[".shortTableName"] = "public_tbl_aux_tipologia";
$tdatapublic_tbl_aux_tipologia[".nSecOptions"] = 0;
$tdatapublic_tbl_aux_tipologia[".recsPerRowPrint"] = 1;
$tdatapublic_tbl_aux_tipologia[".mainTableOwnerID"] = "";
$tdatapublic_tbl_aux_tipologia[".moveNext"] = 1;
$tdatapublic_tbl_aux_tipologia[".entityType"] = 0;

$tdatapublic_tbl_aux_tipologia[".strOriginalTableName"] = "public.tbl_aux_tipologia";

	



$tdatapublic_tbl_aux_tipologia[".showAddInPopup"] = false;

$tdatapublic_tbl_aux_tipologia[".showEditInPopup"] = false;

$tdatapublic_tbl_aux_tipologia[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapublic_tbl_aux_tipologia[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_tbl_aux_tipologia[".fieldsForRegister"] = array();

$tdatapublic_tbl_aux_tipologia[".listAjax"] = false;

	$tdatapublic_tbl_aux_tipologia[".audit"] = false;

	$tdatapublic_tbl_aux_tipologia[".locking"] = false;






$tdatapublic_tbl_aux_tipologia[".reorderRecordsByHeader"] = true;








$tdatapublic_tbl_aux_tipologia[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatapublic_tbl_aux_tipologia[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatapublic_tbl_aux_tipologia[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatapublic_tbl_aux_tipologia[".searchSaving"] = false;
//

$tdatapublic_tbl_aux_tipologia[".showSearchPanel"] = true;
		$tdatapublic_tbl_aux_tipologia[".flexibleSearch"] = true;

$tdatapublic_tbl_aux_tipologia[".isUseAjaxSuggest"] = true;






$tdatapublic_tbl_aux_tipologia[".ajaxCodeSnippetAdded"] = false;

$tdatapublic_tbl_aux_tipologia[".buttonsAdded"] = false;

$tdatapublic_tbl_aux_tipologia[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapublic_tbl_aux_tipologia[".isUseTimeForSearch"] = false;





$tdatapublic_tbl_aux_tipologia[".allSearchFields"] = array();
$tdatapublic_tbl_aux_tipologia[".filterFields"] = array();
$tdatapublic_tbl_aux_tipologia[".requiredSearchFields"] = array();



$tdatapublic_tbl_aux_tipologia[".googleLikeFields"] = array();
$tdatapublic_tbl_aux_tipologia[".googleLikeFields"][] = "ident";
$tdatapublic_tbl_aux_tipologia[".googleLikeFields"][] = "descricao";



$tdatapublic_tbl_aux_tipologia[".tableType"] = "list";

$tdatapublic_tbl_aux_tipologia[".printerPageOrientation"] = 0;
$tdatapublic_tbl_aux_tipologia[".nPrinterPageScale"] = 100;

$tdatapublic_tbl_aux_tipologia[".nPrinterSplitRecords"] = 40;

$tdatapublic_tbl_aux_tipologia[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_tbl_aux_tipologia[".geocodingEnabled"] = false;





$tdatapublic_tbl_aux_tipologia[".listGridLayout"] = 3;

$tdatapublic_tbl_aux_tipologia[".isDisplayLoading"] = true;




// view page pdf

// print page pdf


$tdatapublic_tbl_aux_tipologia[".pageSize"] = 20;

$tdatapublic_tbl_aux_tipologia[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_tbl_aux_tipologia[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_tbl_aux_tipologia[".orderindexes"] = array();

$tdatapublic_tbl_aux_tipologia[".sqlHead"] = "SELECT ident,  	descricao";
$tdatapublic_tbl_aux_tipologia[".sqlFrom"] = "FROM \"public\".tbl_aux_tipologia";
$tdatapublic_tbl_aux_tipologia[".sqlWhereExpr"] = "";
$tdatapublic_tbl_aux_tipologia[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_tbl_aux_tipologia[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_tbl_aux_tipologia[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_tbl_aux_tipologia[".highlightSearchResults"] = true;

$tableKeyspublic_tbl_aux_tipologia = array();
$tdatapublic_tbl_aux_tipologia[".Keys"] = $tableKeyspublic_tbl_aux_tipologia;

$tdatapublic_tbl_aux_tipologia[".listFields"] = array();

$tdatapublic_tbl_aux_tipologia[".hideMobileList"] = array();


$tdatapublic_tbl_aux_tipologia[".viewFields"] = array();

$tdatapublic_tbl_aux_tipologia[".addFields"] = array();

$tdatapublic_tbl_aux_tipologia[".masterListFields"] = array();
$tdatapublic_tbl_aux_tipologia[".masterListFields"][] = "ident";
$tdatapublic_tbl_aux_tipologia[".masterListFields"][] = "descricao";

$tdatapublic_tbl_aux_tipologia[".inlineAddFields"] = array();

$tdatapublic_tbl_aux_tipologia[".editFields"] = array();

$tdatapublic_tbl_aux_tipologia[".inlineEditFields"] = array();

$tdatapublic_tbl_aux_tipologia[".updateSelectedFields"] = array();


$tdatapublic_tbl_aux_tipologia[".exportFields"] = array();

$tdatapublic_tbl_aux_tipologia[".importFields"] = array();

$tdatapublic_tbl_aux_tipologia[".printFields"] = array();


//	ident
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ident";
	$fdata["GoodName"] = "ident";
	$fdata["ownerTable"] = "public.tbl_aux_tipologia";
	$fdata["Label"] = GetFieldLabel("public_tbl_aux_tipologia","ident");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "ident";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ident";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_aux_tipologia["ident"] = $fdata;
//	descricao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "descricao";
	$fdata["GoodName"] = "descricao";
	$fdata["ownerTable"] = "public.tbl_aux_tipologia";
	$fdata["Label"] = GetFieldLabel("public_tbl_aux_tipologia","descricao");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "descricao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "descricao";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=25";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_aux_tipologia["descricao"] = $fdata;


$tables_data["public.tbl_aux_tipologia"]=&$tdatapublic_tbl_aux_tipologia;
$field_labels["public_tbl_aux_tipologia"] = &$fieldLabelspublic_tbl_aux_tipologia;
$fieldToolTips["public_tbl_aux_tipologia"] = &$fieldToolTipspublic_tbl_aux_tipologia;
$placeHolders["public_tbl_aux_tipologia"] = &$placeHolderspublic_tbl_aux_tipologia;
$page_titles["public_tbl_aux_tipologia"] = &$pageTitlespublic_tbl_aux_tipologia;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.tbl_aux_tipologia"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.tbl_aux_tipologia"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_tbl_aux_tipologia()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ident,  	descricao";
$proto0["m_strFrom"] = "FROM \"public\".tbl_aux_tipologia";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ident",
	"m_strTable" => "public.tbl_aux_tipologia",
	"m_srcTableName" => "public.tbl_aux_tipologia"
));

$proto6["m_sql"] = "ident";
$proto6["m_srcTableName"] = "public.tbl_aux_tipologia";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "descricao",
	"m_strTable" => "public.tbl_aux_tipologia",
	"m_srcTableName" => "public.tbl_aux_tipologia"
));

$proto8["m_sql"] = "descricao";
$proto8["m_srcTableName"] = "public.tbl_aux_tipologia";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto10=array();
$proto10["m_link"] = "SQLL_MAIN";
			$proto11=array();
$proto11["m_strName"] = "public.tbl_aux_tipologia";
$proto11["m_srcTableName"] = "public.tbl_aux_tipologia";
$proto11["m_columns"] = array();
$proto11["m_columns"][] = "ident";
$proto11["m_columns"][] = "descricao";
$obj = new SQLTable($proto11);

$proto10["m_table"] = $obj;
$proto10["m_sql"] = "\"public\".tbl_aux_tipologia";
$proto10["m_alias"] = "";
$proto10["m_srcTableName"] = "public.tbl_aux_tipologia";
$proto12=array();
$proto12["m_sql"] = "";
$proto12["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto12["m_column"]=$obj;
$proto12["m_contained"] = array();
$proto12["m_strCase"] = "";
$proto12["m_havingmode"] = false;
$proto12["m_inBrackets"] = false;
$proto12["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto12);

$proto10["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto10);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.tbl_aux_tipologia";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_public_tbl_aux_tipologia = createSqlQuery_public_tbl_aux_tipologia();


	
		;

		

$tdatapublic_tbl_aux_tipologia[".sqlquery"] = $queryData_public_tbl_aux_tipologia;

$tableEvents["public.tbl_aux_tipologia"] = new eventsBase;
$tdatapublic_tbl_aux_tipologia[".hasEvents"] = false;

?>