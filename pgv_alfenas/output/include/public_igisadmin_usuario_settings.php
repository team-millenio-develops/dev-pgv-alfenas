<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_igisadmin_usuario = array();
	$tdatapublic_igisadmin_usuario[".truncateText"] = true;
	$tdatapublic_igisadmin_usuario[".NumberOfChars"] = 80;
	$tdatapublic_igisadmin_usuario[".ShortName"] = "public_igisadmin_usuario";
	$tdatapublic_igisadmin_usuario[".OwnerID"] = "";
	$tdatapublic_igisadmin_usuario[".OriginalTable"] = "public.igisadmin_usuario";

//	field labels
$fieldLabelspublic_igisadmin_usuario = array();
$fieldToolTipspublic_igisadmin_usuario = array();
$pageTitlespublic_igisadmin_usuario = array();
$placeHolderspublic_igisadmin_usuario = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelspublic_igisadmin_usuario["Portuguese(Brazil)"] = array();
	$fieldToolTipspublic_igisadmin_usuario["Portuguese(Brazil)"] = array();
	$placeHolderspublic_igisadmin_usuario["Portuguese(Brazil)"] = array();
	$pageTitlespublic_igisadmin_usuario["Portuguese(Brazil)"] = array();
	$fieldLabelspublic_igisadmin_usuario["Portuguese(Brazil)"]["id"] = "Id";
	$fieldToolTipspublic_igisadmin_usuario["Portuguese(Brazil)"]["id"] = "";
	$placeHolderspublic_igisadmin_usuario["Portuguese(Brazil)"]["id"] = "";
	$fieldLabelspublic_igisadmin_usuario["Portuguese(Brazil)"]["nome"] = "Nome";
	$fieldToolTipspublic_igisadmin_usuario["Portuguese(Brazil)"]["nome"] = "";
	$placeHolderspublic_igisadmin_usuario["Portuguese(Brazil)"]["nome"] = "";
	$fieldLabelspublic_igisadmin_usuario["Portuguese(Brazil)"]["loginsys"] = "Loginsys";
	$fieldToolTipspublic_igisadmin_usuario["Portuguese(Brazil)"]["loginsys"] = "";
	$placeHolderspublic_igisadmin_usuario["Portuguese(Brazil)"]["loginsys"] = "";
	$fieldLabelspublic_igisadmin_usuario["Portuguese(Brazil)"]["senha"] = "Senha";
	$fieldToolTipspublic_igisadmin_usuario["Portuguese(Brazil)"]["senha"] = "";
	$placeHolderspublic_igisadmin_usuario["Portuguese(Brazil)"]["senha"] = "";
	$fieldLabelspublic_igisadmin_usuario["Portuguese(Brazil)"]["tipo"] = "Tipo";
	$fieldToolTipspublic_igisadmin_usuario["Portuguese(Brazil)"]["tipo"] = "";
	$placeHolderspublic_igisadmin_usuario["Portuguese(Brazil)"]["tipo"] = "";
	$fieldLabelspublic_igisadmin_usuario["Portuguese(Brazil)"]["active"] = "Active";
	$fieldToolTipspublic_igisadmin_usuario["Portuguese(Brazil)"]["active"] = "";
	$placeHolderspublic_igisadmin_usuario["Portuguese(Brazil)"]["active"] = "";
	$fieldLabelspublic_igisadmin_usuario["Portuguese(Brazil)"]["data"] = "Data";
	$fieldToolTipspublic_igisadmin_usuario["Portuguese(Brazil)"]["data"] = "";
	$placeHolderspublic_igisadmin_usuario["Portuguese(Brazil)"]["data"] = "";
	if (count($fieldToolTipspublic_igisadmin_usuario["Portuguese(Brazil)"]))
		$tdatapublic_igisadmin_usuario[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_igisadmin_usuario[""] = array();
	$fieldToolTipspublic_igisadmin_usuario[""] = array();
	$placeHolderspublic_igisadmin_usuario[""] = array();
	$pageTitlespublic_igisadmin_usuario[""] = array();
	if (count($fieldToolTipspublic_igisadmin_usuario[""]))
		$tdatapublic_igisadmin_usuario[".isUseToolTips"] = true;
}


	$tdatapublic_igisadmin_usuario[".NCSearch"] = true;



$tdatapublic_igisadmin_usuario[".shortTableName"] = "public_igisadmin_usuario";
$tdatapublic_igisadmin_usuario[".nSecOptions"] = 0;
$tdatapublic_igisadmin_usuario[".recsPerRowPrint"] = 1;
$tdatapublic_igisadmin_usuario[".mainTableOwnerID"] = "";
$tdatapublic_igisadmin_usuario[".moveNext"] = 1;
$tdatapublic_igisadmin_usuario[".entityType"] = 0;

$tdatapublic_igisadmin_usuario[".strOriginalTableName"] = "public.igisadmin_usuario";

	



$tdatapublic_igisadmin_usuario[".showAddInPopup"] = false;

$tdatapublic_igisadmin_usuario[".showEditInPopup"] = false;

$tdatapublic_igisadmin_usuario[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapublic_igisadmin_usuario[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_igisadmin_usuario[".fieldsForRegister"] = array();

	$tdatapublic_igisadmin_usuario[".listAjax"] = true;

	$tdatapublic_igisadmin_usuario[".audit"] = false;

	$tdatapublic_igisadmin_usuario[".locking"] = false;






$tdatapublic_igisadmin_usuario[".reorderRecordsByHeader"] = true;








$tdatapublic_igisadmin_usuario[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatapublic_igisadmin_usuario[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatapublic_igisadmin_usuario[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatapublic_igisadmin_usuario[".searchSaving"] = false;
//

$tdatapublic_igisadmin_usuario[".showSearchPanel"] = true;
		$tdatapublic_igisadmin_usuario[".flexibleSearch"] = true;

$tdatapublic_igisadmin_usuario[".isUseAjaxSuggest"] = true;






$tdatapublic_igisadmin_usuario[".ajaxCodeSnippetAdded"] = false;

$tdatapublic_igisadmin_usuario[".buttonsAdded"] = false;

$tdatapublic_igisadmin_usuario[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapublic_igisadmin_usuario[".isUseTimeForSearch"] = false;





$tdatapublic_igisadmin_usuario[".allSearchFields"] = array();
$tdatapublic_igisadmin_usuario[".filterFields"] = array();
$tdatapublic_igisadmin_usuario[".requiredSearchFields"] = array();



$tdatapublic_igisadmin_usuario[".googleLikeFields"] = array();
$tdatapublic_igisadmin_usuario[".googleLikeFields"][] = "id";
$tdatapublic_igisadmin_usuario[".googleLikeFields"][] = "nome";
$tdatapublic_igisadmin_usuario[".googleLikeFields"][] = "loginsys";
$tdatapublic_igisadmin_usuario[".googleLikeFields"][] = "senha";
$tdatapublic_igisadmin_usuario[".googleLikeFields"][] = "tipo";
$tdatapublic_igisadmin_usuario[".googleLikeFields"][] = "active";
$tdatapublic_igisadmin_usuario[".googleLikeFields"][] = "data";



$tdatapublic_igisadmin_usuario[".tableType"] = "list";

$tdatapublic_igisadmin_usuario[".printerPageOrientation"] = 0;
$tdatapublic_igisadmin_usuario[".nPrinterPageScale"] = 100;

$tdatapublic_igisadmin_usuario[".nPrinterSplitRecords"] = 40;

$tdatapublic_igisadmin_usuario[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_igisadmin_usuario[".geocodingEnabled"] = false;





$tdatapublic_igisadmin_usuario[".listGridLayout"] = 3;

$tdatapublic_igisadmin_usuario[".isDisplayLoading"] = true;




// view page pdf

// print page pdf


$tdatapublic_igisadmin_usuario[".pageSize"] = 20;

$tdatapublic_igisadmin_usuario[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_igisadmin_usuario[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_igisadmin_usuario[".orderindexes"] = array();

$tdatapublic_igisadmin_usuario[".sqlHead"] = "SELECT id,  nome,  loginsys,  senha,  tipo,  active,  \"data\"";
$tdatapublic_igisadmin_usuario[".sqlFrom"] = "FROM \"public\".igisadmin_usuario";
$tdatapublic_igisadmin_usuario[".sqlWhereExpr"] = "";
$tdatapublic_igisadmin_usuario[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_igisadmin_usuario[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_igisadmin_usuario[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_igisadmin_usuario[".highlightSearchResults"] = true;

$tableKeyspublic_igisadmin_usuario = array();
$tableKeyspublic_igisadmin_usuario[] = "id";
$tdatapublic_igisadmin_usuario[".Keys"] = $tableKeyspublic_igisadmin_usuario;

$tdatapublic_igisadmin_usuario[".listFields"] = array();

$tdatapublic_igisadmin_usuario[".hideMobileList"] = array();


$tdatapublic_igisadmin_usuario[".viewFields"] = array();

$tdatapublic_igisadmin_usuario[".addFields"] = array();

$tdatapublic_igisadmin_usuario[".masterListFields"] = array();
$tdatapublic_igisadmin_usuario[".masterListFields"][] = "id";
$tdatapublic_igisadmin_usuario[".masterListFields"][] = "nome";
$tdatapublic_igisadmin_usuario[".masterListFields"][] = "loginsys";
$tdatapublic_igisadmin_usuario[".masterListFields"][] = "senha";
$tdatapublic_igisadmin_usuario[".masterListFields"][] = "tipo";
$tdatapublic_igisadmin_usuario[".masterListFields"][] = "active";
$tdatapublic_igisadmin_usuario[".masterListFields"][] = "data";

$tdatapublic_igisadmin_usuario[".inlineAddFields"] = array();

$tdatapublic_igisadmin_usuario[".editFields"] = array();

$tdatapublic_igisadmin_usuario[".inlineEditFields"] = array();

$tdatapublic_igisadmin_usuario[".updateSelectedFields"] = array();


$tdatapublic_igisadmin_usuario[".exportFields"] = array();

$tdatapublic_igisadmin_usuario[".importFields"] = array();

$tdatapublic_igisadmin_usuario[".printFields"] = array();


//	id
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id";
	$fdata["GoodName"] = "id";
	$fdata["ownerTable"] = "public.igisadmin_usuario";
	$fdata["Label"] = GetFieldLabel("public_igisadmin_usuario","id");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "id";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_igisadmin_usuario["id"] = $fdata;
//	nome
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "nome";
	$fdata["GoodName"] = "nome";
	$fdata["ownerTable"] = "public.igisadmin_usuario";
	$fdata["Label"] = GetFieldLabel("public_igisadmin_usuario","nome");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "nome";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nome";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=100";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_igisadmin_usuario["nome"] = $fdata;
//	loginsys
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "loginsys";
	$fdata["GoodName"] = "loginsys";
	$fdata["ownerTable"] = "public.igisadmin_usuario";
	$fdata["Label"] = GetFieldLabel("public_igisadmin_usuario","loginsys");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "loginsys";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "loginsys";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_igisadmin_usuario["loginsys"] = $fdata;
//	senha
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "senha";
	$fdata["GoodName"] = "senha";
	$fdata["ownerTable"] = "public.igisadmin_usuario";
	$fdata["Label"] = GetFieldLabel("public_igisadmin_usuario","senha");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "senha";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "senha";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=20";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_igisadmin_usuario["senha"] = $fdata;
//	tipo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "tipo";
	$fdata["GoodName"] = "tipo";
	$fdata["ownerTable"] = "public.igisadmin_usuario";
	$fdata["Label"] = GetFieldLabel("public_igisadmin_usuario","tipo");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "tipo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tipo";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_igisadmin_usuario["tipo"] = $fdata;
//	active
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "active";
	$fdata["GoodName"] = "active";
	$fdata["ownerTable"] = "public.igisadmin_usuario";
	$fdata["Label"] = GetFieldLabel("public_igisadmin_usuario","active");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "active";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "active";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_igisadmin_usuario["active"] = $fdata;
//	data
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "data";
	$fdata["GoodName"] = "data";
	$fdata["ownerTable"] = "public.igisadmin_usuario";
	$fdata["Label"] = GetFieldLabel("public_igisadmin_usuario","data");
	$fdata["FieldType"] = 135;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "data";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"data\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

		$edata["ShowTime"] = true;

	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_igisadmin_usuario["data"] = $fdata;


$tables_data["public.igisadmin_usuario"]=&$tdatapublic_igisadmin_usuario;
$field_labels["public_igisadmin_usuario"] = &$fieldLabelspublic_igisadmin_usuario;
$fieldToolTips["public_igisadmin_usuario"] = &$fieldToolTipspublic_igisadmin_usuario;
$placeHolders["public_igisadmin_usuario"] = &$placeHolderspublic_igisadmin_usuario;
$page_titles["public_igisadmin_usuario"] = &$pageTitlespublic_igisadmin_usuario;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.igisadmin_usuario"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.igisadmin_usuario"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_igisadmin_usuario()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id,  nome,  loginsys,  senha,  tipo,  active,  \"data\"";
$proto0["m_strFrom"] = "FROM \"public\".igisadmin_usuario";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id",
	"m_strTable" => "public.igisadmin_usuario",
	"m_srcTableName" => "public.igisadmin_usuario"
));

$proto6["m_sql"] = "id";
$proto6["m_srcTableName"] = "public.igisadmin_usuario";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "nome",
	"m_strTable" => "public.igisadmin_usuario",
	"m_srcTableName" => "public.igisadmin_usuario"
));

$proto8["m_sql"] = "nome";
$proto8["m_srcTableName"] = "public.igisadmin_usuario";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "loginsys",
	"m_strTable" => "public.igisadmin_usuario",
	"m_srcTableName" => "public.igisadmin_usuario"
));

$proto10["m_sql"] = "loginsys";
$proto10["m_srcTableName"] = "public.igisadmin_usuario";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "senha",
	"m_strTable" => "public.igisadmin_usuario",
	"m_srcTableName" => "public.igisadmin_usuario"
));

$proto12["m_sql"] = "senha";
$proto12["m_srcTableName"] = "public.igisadmin_usuario";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "tipo",
	"m_strTable" => "public.igisadmin_usuario",
	"m_srcTableName" => "public.igisadmin_usuario"
));

$proto14["m_sql"] = "tipo";
$proto14["m_srcTableName"] = "public.igisadmin_usuario";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "active",
	"m_strTable" => "public.igisadmin_usuario",
	"m_srcTableName" => "public.igisadmin_usuario"
));

$proto16["m_sql"] = "active";
$proto16["m_srcTableName"] = "public.igisadmin_usuario";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "data",
	"m_strTable" => "public.igisadmin_usuario",
	"m_srcTableName" => "public.igisadmin_usuario"
));

$proto18["m_sql"] = "\"data\"";
$proto18["m_srcTableName"] = "public.igisadmin_usuario";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto20=array();
$proto20["m_link"] = "SQLL_MAIN";
			$proto21=array();
$proto21["m_strName"] = "public.igisadmin_usuario";
$proto21["m_srcTableName"] = "public.igisadmin_usuario";
$proto21["m_columns"] = array();
$proto21["m_columns"][] = "id";
$proto21["m_columns"][] = "nome";
$proto21["m_columns"][] = "loginsys";
$proto21["m_columns"][] = "senha";
$proto21["m_columns"][] = "interface";
$proto21["m_columns"][] = "cpf";
$proto21["m_columns"][] = "logradouro";
$proto21["m_columns"][] = "numero";
$proto21["m_columns"][] = "complemento";
$proto21["m_columns"][] = "bairro";
$proto21["m_columns"][] = "uf";
$proto21["m_columns"][] = "cidade";
$proto21["m_columns"][] = "cep";
$proto21["m_columns"][] = "telefone1";
$proto21["m_columns"][] = "telefone2";
$proto21["m_columns"][] = "tipo";
$proto21["m_columns"][] = "email";
$proto21["m_columns"][] = "active";
$proto21["m_columns"][] = "data";
$obj = new SQLTable($proto21);

$proto20["m_table"] = $obj;
$proto20["m_sql"] = "\"public\".igisadmin_usuario";
$proto20["m_alias"] = "";
$proto20["m_srcTableName"] = "public.igisadmin_usuario";
$proto22=array();
$proto22["m_sql"] = "";
$proto22["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto22["m_column"]=$obj;
$proto22["m_contained"] = array();
$proto22["m_strCase"] = "";
$proto22["m_havingmode"] = false;
$proto22["m_inBrackets"] = false;
$proto22["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto22);

$proto20["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto20);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.igisadmin_usuario";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_public_igisadmin_usuario = createSqlQuery_public_igisadmin_usuario();


	
		;

							

$tdatapublic_igisadmin_usuario[".sqlquery"] = $queryData_public_igisadmin_usuario;

$tableEvents["public.igisadmin_usuario"] = new eventsBase;
$tdatapublic_igisadmin_usuario[".hasEvents"] = false;

?>