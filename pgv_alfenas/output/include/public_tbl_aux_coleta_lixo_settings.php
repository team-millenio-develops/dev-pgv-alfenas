<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_tbl_aux_coleta_lixo = array();
	$tdatapublic_tbl_aux_coleta_lixo[".truncateText"] = true;
	$tdatapublic_tbl_aux_coleta_lixo[".NumberOfChars"] = 80;
	$tdatapublic_tbl_aux_coleta_lixo[".ShortName"] = "public_tbl_aux_coleta_lixo";
	$tdatapublic_tbl_aux_coleta_lixo[".OwnerID"] = "";
	$tdatapublic_tbl_aux_coleta_lixo[".OriginalTable"] = "public.tbl_aux_coleta_lixo";

//	field labels
$fieldLabelspublic_tbl_aux_coleta_lixo = array();
$fieldToolTipspublic_tbl_aux_coleta_lixo = array();
$pageTitlespublic_tbl_aux_coleta_lixo = array();
$placeHolderspublic_tbl_aux_coleta_lixo = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelspublic_tbl_aux_coleta_lixo["Portuguese(Brazil)"] = array();
	$fieldToolTipspublic_tbl_aux_coleta_lixo["Portuguese(Brazil)"] = array();
	$placeHolderspublic_tbl_aux_coleta_lixo["Portuguese(Brazil)"] = array();
	$pageTitlespublic_tbl_aux_coleta_lixo["Portuguese(Brazil)"] = array();
	$fieldLabelspublic_tbl_aux_coleta_lixo["Portuguese(Brazil)"]["ident"] = "Ident";
	$fieldToolTipspublic_tbl_aux_coleta_lixo["Portuguese(Brazil)"]["ident"] = "";
	$placeHolderspublic_tbl_aux_coleta_lixo["Portuguese(Brazil)"]["ident"] = "";
	$fieldLabelspublic_tbl_aux_coleta_lixo["Portuguese(Brazil)"]["descricao"] = "Descricao";
	$fieldToolTipspublic_tbl_aux_coleta_lixo["Portuguese(Brazil)"]["descricao"] = "";
	$placeHolderspublic_tbl_aux_coleta_lixo["Portuguese(Brazil)"]["descricao"] = "";
	if (count($fieldToolTipspublic_tbl_aux_coleta_lixo["Portuguese(Brazil)"]))
		$tdatapublic_tbl_aux_coleta_lixo[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_tbl_aux_coleta_lixo[""] = array();
	$fieldToolTipspublic_tbl_aux_coleta_lixo[""] = array();
	$placeHolderspublic_tbl_aux_coleta_lixo[""] = array();
	$pageTitlespublic_tbl_aux_coleta_lixo[""] = array();
	if (count($fieldToolTipspublic_tbl_aux_coleta_lixo[""]))
		$tdatapublic_tbl_aux_coleta_lixo[".isUseToolTips"] = true;
}


	$tdatapublic_tbl_aux_coleta_lixo[".NCSearch"] = true;



$tdatapublic_tbl_aux_coleta_lixo[".shortTableName"] = "public_tbl_aux_coleta_lixo";
$tdatapublic_tbl_aux_coleta_lixo[".nSecOptions"] = 0;
$tdatapublic_tbl_aux_coleta_lixo[".recsPerRowPrint"] = 1;
$tdatapublic_tbl_aux_coleta_lixo[".mainTableOwnerID"] = "";
$tdatapublic_tbl_aux_coleta_lixo[".moveNext"] = 1;
$tdatapublic_tbl_aux_coleta_lixo[".entityType"] = 0;

$tdatapublic_tbl_aux_coleta_lixo[".strOriginalTableName"] = "public.tbl_aux_coleta_lixo";

	



$tdatapublic_tbl_aux_coleta_lixo[".showAddInPopup"] = false;

$tdatapublic_tbl_aux_coleta_lixo[".showEditInPopup"] = false;

$tdatapublic_tbl_aux_coleta_lixo[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapublic_tbl_aux_coleta_lixo[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_tbl_aux_coleta_lixo[".fieldsForRegister"] = array();

$tdatapublic_tbl_aux_coleta_lixo[".listAjax"] = false;

	$tdatapublic_tbl_aux_coleta_lixo[".audit"] = false;

	$tdatapublic_tbl_aux_coleta_lixo[".locking"] = false;






$tdatapublic_tbl_aux_coleta_lixo[".reorderRecordsByHeader"] = true;








$tdatapublic_tbl_aux_coleta_lixo[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatapublic_tbl_aux_coleta_lixo[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatapublic_tbl_aux_coleta_lixo[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatapublic_tbl_aux_coleta_lixo[".searchSaving"] = false;
//

$tdatapublic_tbl_aux_coleta_lixo[".showSearchPanel"] = true;
		$tdatapublic_tbl_aux_coleta_lixo[".flexibleSearch"] = true;

$tdatapublic_tbl_aux_coleta_lixo[".isUseAjaxSuggest"] = true;






$tdatapublic_tbl_aux_coleta_lixo[".ajaxCodeSnippetAdded"] = false;

$tdatapublic_tbl_aux_coleta_lixo[".buttonsAdded"] = false;

$tdatapublic_tbl_aux_coleta_lixo[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapublic_tbl_aux_coleta_lixo[".isUseTimeForSearch"] = false;





$tdatapublic_tbl_aux_coleta_lixo[".allSearchFields"] = array();
$tdatapublic_tbl_aux_coleta_lixo[".filterFields"] = array();
$tdatapublic_tbl_aux_coleta_lixo[".requiredSearchFields"] = array();



$tdatapublic_tbl_aux_coleta_lixo[".googleLikeFields"] = array();
$tdatapublic_tbl_aux_coleta_lixo[".googleLikeFields"][] = "ident";
$tdatapublic_tbl_aux_coleta_lixo[".googleLikeFields"][] = "descricao";



$tdatapublic_tbl_aux_coleta_lixo[".tableType"] = "list";

$tdatapublic_tbl_aux_coleta_lixo[".printerPageOrientation"] = 0;
$tdatapublic_tbl_aux_coleta_lixo[".nPrinterPageScale"] = 100;

$tdatapublic_tbl_aux_coleta_lixo[".nPrinterSplitRecords"] = 40;

$tdatapublic_tbl_aux_coleta_lixo[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_tbl_aux_coleta_lixo[".geocodingEnabled"] = false;





$tdatapublic_tbl_aux_coleta_lixo[".listGridLayout"] = 3;

$tdatapublic_tbl_aux_coleta_lixo[".isDisplayLoading"] = true;




// view page pdf

// print page pdf


$tdatapublic_tbl_aux_coleta_lixo[".pageSize"] = 20;

$tdatapublic_tbl_aux_coleta_lixo[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_tbl_aux_coleta_lixo[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_tbl_aux_coleta_lixo[".orderindexes"] = array();

$tdatapublic_tbl_aux_coleta_lixo[".sqlHead"] = "SELECT ident,  	descricao";
$tdatapublic_tbl_aux_coleta_lixo[".sqlFrom"] = "FROM \"public\".tbl_aux_coleta_lixo";
$tdatapublic_tbl_aux_coleta_lixo[".sqlWhereExpr"] = "";
$tdatapublic_tbl_aux_coleta_lixo[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_tbl_aux_coleta_lixo[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_tbl_aux_coleta_lixo[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_tbl_aux_coleta_lixo[".highlightSearchResults"] = true;

$tableKeyspublic_tbl_aux_coleta_lixo = array();
$tdatapublic_tbl_aux_coleta_lixo[".Keys"] = $tableKeyspublic_tbl_aux_coleta_lixo;

$tdatapublic_tbl_aux_coleta_lixo[".listFields"] = array();

$tdatapublic_tbl_aux_coleta_lixo[".hideMobileList"] = array();


$tdatapublic_tbl_aux_coleta_lixo[".viewFields"] = array();

$tdatapublic_tbl_aux_coleta_lixo[".addFields"] = array();

$tdatapublic_tbl_aux_coleta_lixo[".masterListFields"] = array();
$tdatapublic_tbl_aux_coleta_lixo[".masterListFields"][] = "ident";
$tdatapublic_tbl_aux_coleta_lixo[".masterListFields"][] = "descricao";

$tdatapublic_tbl_aux_coleta_lixo[".inlineAddFields"] = array();

$tdatapublic_tbl_aux_coleta_lixo[".editFields"] = array();

$tdatapublic_tbl_aux_coleta_lixo[".inlineEditFields"] = array();

$tdatapublic_tbl_aux_coleta_lixo[".updateSelectedFields"] = array();


$tdatapublic_tbl_aux_coleta_lixo[".exportFields"] = array();

$tdatapublic_tbl_aux_coleta_lixo[".importFields"] = array();

$tdatapublic_tbl_aux_coleta_lixo[".printFields"] = array();


//	ident
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ident";
	$fdata["GoodName"] = "ident";
	$fdata["ownerTable"] = "public.tbl_aux_coleta_lixo";
	$fdata["Label"] = GetFieldLabel("public_tbl_aux_coleta_lixo","ident");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "ident";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ident";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_aux_coleta_lixo["ident"] = $fdata;
//	descricao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "descricao";
	$fdata["GoodName"] = "descricao";
	$fdata["ownerTable"] = "public.tbl_aux_coleta_lixo";
	$fdata["Label"] = GetFieldLabel("public_tbl_aux_coleta_lixo","descricao");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "descricao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "descricao";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=100";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_aux_coleta_lixo["descricao"] = $fdata;


$tables_data["public.tbl_aux_coleta_lixo"]=&$tdatapublic_tbl_aux_coleta_lixo;
$field_labels["public_tbl_aux_coleta_lixo"] = &$fieldLabelspublic_tbl_aux_coleta_lixo;
$fieldToolTips["public_tbl_aux_coleta_lixo"] = &$fieldToolTipspublic_tbl_aux_coleta_lixo;
$placeHolders["public_tbl_aux_coleta_lixo"] = &$placeHolderspublic_tbl_aux_coleta_lixo;
$page_titles["public_tbl_aux_coleta_lixo"] = &$pageTitlespublic_tbl_aux_coleta_lixo;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.tbl_aux_coleta_lixo"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.tbl_aux_coleta_lixo"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_tbl_aux_coleta_lixo()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ident,  	descricao";
$proto0["m_strFrom"] = "FROM \"public\".tbl_aux_coleta_lixo";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ident",
	"m_strTable" => "public.tbl_aux_coleta_lixo",
	"m_srcTableName" => "public.tbl_aux_coleta_lixo"
));

$proto6["m_sql"] = "ident";
$proto6["m_srcTableName"] = "public.tbl_aux_coleta_lixo";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "descricao",
	"m_strTable" => "public.tbl_aux_coleta_lixo",
	"m_srcTableName" => "public.tbl_aux_coleta_lixo"
));

$proto8["m_sql"] = "descricao";
$proto8["m_srcTableName"] = "public.tbl_aux_coleta_lixo";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto10=array();
$proto10["m_link"] = "SQLL_MAIN";
			$proto11=array();
$proto11["m_strName"] = "public.tbl_aux_coleta_lixo";
$proto11["m_srcTableName"] = "public.tbl_aux_coleta_lixo";
$proto11["m_columns"] = array();
$proto11["m_columns"][] = "ident";
$proto11["m_columns"][] = "descricao";
$obj = new SQLTable($proto11);

$proto10["m_table"] = $obj;
$proto10["m_sql"] = "\"public\".tbl_aux_coleta_lixo";
$proto10["m_alias"] = "";
$proto10["m_srcTableName"] = "public.tbl_aux_coleta_lixo";
$proto12=array();
$proto12["m_sql"] = "";
$proto12["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto12["m_column"]=$obj;
$proto12["m_contained"] = array();
$proto12["m_strCase"] = "";
$proto12["m_havingmode"] = false;
$proto12["m_inBrackets"] = false;
$proto12["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto12);

$proto10["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto10);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.tbl_aux_coleta_lixo";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_public_tbl_aux_coleta_lixo = createSqlQuery_public_tbl_aux_coleta_lixo();


	
		;

		

$tdatapublic_tbl_aux_coleta_lixo[".sqlquery"] = $queryData_public_tbl_aux_coleta_lixo;

$tableEvents["public.tbl_aux_coleta_lixo"] = new eventsBase;
$tdatapublic_tbl_aux_coleta_lixo[".hasEvents"] = false;

?>