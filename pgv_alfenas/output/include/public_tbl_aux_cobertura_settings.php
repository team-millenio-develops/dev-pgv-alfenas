<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_tbl_aux_cobertura = array();
	$tdatapublic_tbl_aux_cobertura[".truncateText"] = true;
	$tdatapublic_tbl_aux_cobertura[".NumberOfChars"] = 80;
	$tdatapublic_tbl_aux_cobertura[".ShortName"] = "public_tbl_aux_cobertura";
	$tdatapublic_tbl_aux_cobertura[".OwnerID"] = "";
	$tdatapublic_tbl_aux_cobertura[".OriginalTable"] = "public.tbl_aux_cobertura";

//	field labels
$fieldLabelspublic_tbl_aux_cobertura = array();
$fieldToolTipspublic_tbl_aux_cobertura = array();
$pageTitlespublic_tbl_aux_cobertura = array();
$placeHolderspublic_tbl_aux_cobertura = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelspublic_tbl_aux_cobertura["Portuguese(Brazil)"] = array();
	$fieldToolTipspublic_tbl_aux_cobertura["Portuguese(Brazil)"] = array();
	$placeHolderspublic_tbl_aux_cobertura["Portuguese(Brazil)"] = array();
	$pageTitlespublic_tbl_aux_cobertura["Portuguese(Brazil)"] = array();
	$fieldLabelspublic_tbl_aux_cobertura["Portuguese(Brazil)"]["ident"] = "Ident";
	$fieldToolTipspublic_tbl_aux_cobertura["Portuguese(Brazil)"]["ident"] = "";
	$placeHolderspublic_tbl_aux_cobertura["Portuguese(Brazil)"]["ident"] = "";
	$fieldLabelspublic_tbl_aux_cobertura["Portuguese(Brazil)"]["descricao"] = "Descricao";
	$fieldToolTipspublic_tbl_aux_cobertura["Portuguese(Brazil)"]["descricao"] = "";
	$placeHolderspublic_tbl_aux_cobertura["Portuguese(Brazil)"]["descricao"] = "";
	if (count($fieldToolTipspublic_tbl_aux_cobertura["Portuguese(Brazil)"]))
		$tdatapublic_tbl_aux_cobertura[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_tbl_aux_cobertura[""] = array();
	$fieldToolTipspublic_tbl_aux_cobertura[""] = array();
	$placeHolderspublic_tbl_aux_cobertura[""] = array();
	$pageTitlespublic_tbl_aux_cobertura[""] = array();
	if (count($fieldToolTipspublic_tbl_aux_cobertura[""]))
		$tdatapublic_tbl_aux_cobertura[".isUseToolTips"] = true;
}


	$tdatapublic_tbl_aux_cobertura[".NCSearch"] = true;



$tdatapublic_tbl_aux_cobertura[".shortTableName"] = "public_tbl_aux_cobertura";
$tdatapublic_tbl_aux_cobertura[".nSecOptions"] = 0;
$tdatapublic_tbl_aux_cobertura[".recsPerRowPrint"] = 1;
$tdatapublic_tbl_aux_cobertura[".mainTableOwnerID"] = "";
$tdatapublic_tbl_aux_cobertura[".moveNext"] = 1;
$tdatapublic_tbl_aux_cobertura[".entityType"] = 0;

$tdatapublic_tbl_aux_cobertura[".strOriginalTableName"] = "public.tbl_aux_cobertura";

	



$tdatapublic_tbl_aux_cobertura[".showAddInPopup"] = false;

$tdatapublic_tbl_aux_cobertura[".showEditInPopup"] = false;

$tdatapublic_tbl_aux_cobertura[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapublic_tbl_aux_cobertura[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_tbl_aux_cobertura[".fieldsForRegister"] = array();

$tdatapublic_tbl_aux_cobertura[".listAjax"] = false;

	$tdatapublic_tbl_aux_cobertura[".audit"] = false;

	$tdatapublic_tbl_aux_cobertura[".locking"] = false;






$tdatapublic_tbl_aux_cobertura[".reorderRecordsByHeader"] = true;








$tdatapublic_tbl_aux_cobertura[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatapublic_tbl_aux_cobertura[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatapublic_tbl_aux_cobertura[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatapublic_tbl_aux_cobertura[".searchSaving"] = false;
//

$tdatapublic_tbl_aux_cobertura[".showSearchPanel"] = true;
		$tdatapublic_tbl_aux_cobertura[".flexibleSearch"] = true;

$tdatapublic_tbl_aux_cobertura[".isUseAjaxSuggest"] = true;






$tdatapublic_tbl_aux_cobertura[".ajaxCodeSnippetAdded"] = false;

$tdatapublic_tbl_aux_cobertura[".buttonsAdded"] = false;

$tdatapublic_tbl_aux_cobertura[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapublic_tbl_aux_cobertura[".isUseTimeForSearch"] = false;





$tdatapublic_tbl_aux_cobertura[".allSearchFields"] = array();
$tdatapublic_tbl_aux_cobertura[".filterFields"] = array();
$tdatapublic_tbl_aux_cobertura[".requiredSearchFields"] = array();



$tdatapublic_tbl_aux_cobertura[".googleLikeFields"] = array();
$tdatapublic_tbl_aux_cobertura[".googleLikeFields"][] = "ident";
$tdatapublic_tbl_aux_cobertura[".googleLikeFields"][] = "descricao";



$tdatapublic_tbl_aux_cobertura[".tableType"] = "list";

$tdatapublic_tbl_aux_cobertura[".printerPageOrientation"] = 0;
$tdatapublic_tbl_aux_cobertura[".nPrinterPageScale"] = 100;

$tdatapublic_tbl_aux_cobertura[".nPrinterSplitRecords"] = 40;

$tdatapublic_tbl_aux_cobertura[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_tbl_aux_cobertura[".geocodingEnabled"] = false;





$tdatapublic_tbl_aux_cobertura[".listGridLayout"] = 3;

$tdatapublic_tbl_aux_cobertura[".isDisplayLoading"] = true;




// view page pdf

// print page pdf


$tdatapublic_tbl_aux_cobertura[".pageSize"] = 20;

$tdatapublic_tbl_aux_cobertura[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_tbl_aux_cobertura[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_tbl_aux_cobertura[".orderindexes"] = array();

$tdatapublic_tbl_aux_cobertura[".sqlHead"] = "SELECT ident,  	descricao";
$tdatapublic_tbl_aux_cobertura[".sqlFrom"] = "FROM \"public\".tbl_aux_cobertura";
$tdatapublic_tbl_aux_cobertura[".sqlWhereExpr"] = "";
$tdatapublic_tbl_aux_cobertura[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_tbl_aux_cobertura[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_tbl_aux_cobertura[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_tbl_aux_cobertura[".highlightSearchResults"] = true;

$tableKeyspublic_tbl_aux_cobertura = array();
$tdatapublic_tbl_aux_cobertura[".Keys"] = $tableKeyspublic_tbl_aux_cobertura;

$tdatapublic_tbl_aux_cobertura[".listFields"] = array();

$tdatapublic_tbl_aux_cobertura[".hideMobileList"] = array();


$tdatapublic_tbl_aux_cobertura[".viewFields"] = array();

$tdatapublic_tbl_aux_cobertura[".addFields"] = array();

$tdatapublic_tbl_aux_cobertura[".masterListFields"] = array();
$tdatapublic_tbl_aux_cobertura[".masterListFields"][] = "ident";
$tdatapublic_tbl_aux_cobertura[".masterListFields"][] = "descricao";

$tdatapublic_tbl_aux_cobertura[".inlineAddFields"] = array();

$tdatapublic_tbl_aux_cobertura[".editFields"] = array();

$tdatapublic_tbl_aux_cobertura[".inlineEditFields"] = array();

$tdatapublic_tbl_aux_cobertura[".updateSelectedFields"] = array();


$tdatapublic_tbl_aux_cobertura[".exportFields"] = array();

$tdatapublic_tbl_aux_cobertura[".importFields"] = array();

$tdatapublic_tbl_aux_cobertura[".printFields"] = array();


//	ident
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ident";
	$fdata["GoodName"] = "ident";
	$fdata["ownerTable"] = "public.tbl_aux_cobertura";
	$fdata["Label"] = GetFieldLabel("public_tbl_aux_cobertura","ident");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "ident";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ident";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_aux_cobertura["ident"] = $fdata;
//	descricao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "descricao";
	$fdata["GoodName"] = "descricao";
	$fdata["ownerTable"] = "public.tbl_aux_cobertura";
	$fdata["Label"] = GetFieldLabel("public_tbl_aux_cobertura","descricao");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "descricao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "descricao";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_aux_cobertura["descricao"] = $fdata;


$tables_data["public.tbl_aux_cobertura"]=&$tdatapublic_tbl_aux_cobertura;
$field_labels["public_tbl_aux_cobertura"] = &$fieldLabelspublic_tbl_aux_cobertura;
$fieldToolTips["public_tbl_aux_cobertura"] = &$fieldToolTipspublic_tbl_aux_cobertura;
$placeHolders["public_tbl_aux_cobertura"] = &$placeHolderspublic_tbl_aux_cobertura;
$page_titles["public_tbl_aux_cobertura"] = &$pageTitlespublic_tbl_aux_cobertura;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.tbl_aux_cobertura"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.tbl_aux_cobertura"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_tbl_aux_cobertura()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ident,  	descricao";
$proto0["m_strFrom"] = "FROM \"public\".tbl_aux_cobertura";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ident",
	"m_strTable" => "public.tbl_aux_cobertura",
	"m_srcTableName" => "public.tbl_aux_cobertura"
));

$proto6["m_sql"] = "ident";
$proto6["m_srcTableName"] = "public.tbl_aux_cobertura";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "descricao",
	"m_strTable" => "public.tbl_aux_cobertura",
	"m_srcTableName" => "public.tbl_aux_cobertura"
));

$proto8["m_sql"] = "descricao";
$proto8["m_srcTableName"] = "public.tbl_aux_cobertura";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto10=array();
$proto10["m_link"] = "SQLL_MAIN";
			$proto11=array();
$proto11["m_strName"] = "public.tbl_aux_cobertura";
$proto11["m_srcTableName"] = "public.tbl_aux_cobertura";
$proto11["m_columns"] = array();
$proto11["m_columns"][] = "ident";
$proto11["m_columns"][] = "descricao";
$obj = new SQLTable($proto11);

$proto10["m_table"] = $obj;
$proto10["m_sql"] = "\"public\".tbl_aux_cobertura";
$proto10["m_alias"] = "";
$proto10["m_srcTableName"] = "public.tbl_aux_cobertura";
$proto12=array();
$proto12["m_sql"] = "";
$proto12["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto12["m_column"]=$obj;
$proto12["m_contained"] = array();
$proto12["m_strCase"] = "";
$proto12["m_havingmode"] = false;
$proto12["m_inBrackets"] = false;
$proto12["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto12);

$proto10["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto10);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.tbl_aux_cobertura";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_public_tbl_aux_cobertura = createSqlQuery_public_tbl_aux_cobertura();


	
		;

		

$tdatapublic_tbl_aux_cobertura[".sqlquery"] = $queryData_public_tbl_aux_cobertura;

$tableEvents["public.tbl_aux_cobertura"] = new eventsBase;
$tdatapublic_tbl_aux_cobertura[".hasEvents"] = false;

?>