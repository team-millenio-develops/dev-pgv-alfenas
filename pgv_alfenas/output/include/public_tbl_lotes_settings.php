<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_tbl_lotes = array();
	$tdatapublic_tbl_lotes[".truncateText"] = true;
	$tdatapublic_tbl_lotes[".NumberOfChars"] = 80;
	$tdatapublic_tbl_lotes[".ShortName"] = "public_tbl_lotes";
	$tdatapublic_tbl_lotes[".OwnerID"] = "";
	$tdatapublic_tbl_lotes[".OriginalTable"] = "public.tbl_lotes";

//	field labels
$fieldLabelspublic_tbl_lotes = array();
$fieldToolTipspublic_tbl_lotes = array();
$pageTitlespublic_tbl_lotes = array();
$placeHolderspublic_tbl_lotes = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"] = array();
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"] = array();
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"] = array();
	$pageTitlespublic_tbl_lotes["Portuguese(Brazil)"] = array();
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["inscricao"] = "Inscricao";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["inscricao"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["inscricao"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["exercicio"] = "Exercicio";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["exercicio"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["exercicio"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["ICTerreno"] = "ICTerreno";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["ICTerreno"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["ICTerreno"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["chave"] = "Chave";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["chave"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["chave"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Codigo_Calculo"] = "Codigo Calculo";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Codigo_Calculo"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Codigo_Calculo"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Simulacao"] = "Simulacao";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Simulacao"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Simulacao"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Data_Calculo"] = "Data Calculo";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Data_Calculo"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Data_Calculo"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Data_Referencia_Imovel"] = "Data Referencia Imovel";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Data_Referencia_Imovel"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Data_Referencia_Imovel"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Data_Fatores_IPTU"] = "Data Fatores IPTU";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Data_Fatores_IPTU"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Data_Fatores_IPTU"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Data_Fatores_Calculo"] = "Data Fatores Calculo";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Data_Fatores_Calculo"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Data_Fatores_Calculo"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Situacao_Calculo_IPTU"] = "Situacao Calculo IPTU";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Situacao_Calculo_IPTU"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Situacao_Calculo_IPTU"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Reemissao"] = "Reemissao";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Reemissao"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Reemissao"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["zh"] = "Zh";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["zh"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["zh"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["cod_log"] = "Cod Log";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["cod_log"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["cod_log"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Tipo_de_Logradouro"] = "Tipo de Logradouro";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Tipo_de_Logradouro"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Tipo_de_Logradouro"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Logradouro"] = "Logradouro";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Logradouro"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Logradouro"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Numero_do_Imovel"] = "Numero do Imovel";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Numero_do_Imovel"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Numero_do_Imovel"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Complemento"] = "Complemento";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Complemento"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Complemento"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Tipo_de_Bairro"] = "Tipo de Bairro";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Tipo_de_Bairro"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Tipo_de_Bairro"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Bairro"] = "Bairro";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Bairro"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Bairro"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["CEP"] = "CEP";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["CEP"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["CEP"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Proprietario_Principal"] = "Proprietario Principal";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Proprietario_Principal"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Proprietario_Principal"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Mensagem"] = "Mensagem";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Mensagem"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Mensagem"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Pedologia"] = "Pedologia";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Pedologia"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Pedologia"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Topografia"] = "Topografia";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Topografia"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Topografia"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Situacao"] = "Situacao";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Situacao"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Situacao"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Fped"] = "Fped";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Fped"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Fped"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Ftop"] = "Ftop";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Ftop"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Ftop"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Fsit"] = "Fsit";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Fsit"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Fsit"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Testada_Referencia"] = "Testada Referencia";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Testada_Referencia"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Testada_Referencia"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["VBU"] = "VBU";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["VBU"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["VBU"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Fator_Testada"] = "Fator Testada";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Fator_Testada"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Fator_Testada"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Fator_Gleba"] = "Fator Gleba";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Fator_Gleba"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Fator_Gleba"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Profundidade_Maxima"] = "Profundidade Maxima";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Profundidade_Maxima"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Profundidade_Maxima"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Profundidade_Minima"] = "Profundidade Minima";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Profundidade_Minima"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Profundidade_Minima"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Fator_Profundidade"] = "Fator Profundidade";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Fator_Profundidade"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Fator_Profundidade"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Fator_Ponderacao"] = "Fator Ponderacao";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Fator_Ponderacao"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Fator_Ponderacao"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Testada_Principal"] = "Testada Principal";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Testada_Principal"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Testada_Principal"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Area_do_Terreno"] = "Area do Terreno";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Area_do_Terreno"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Area_do_Terreno"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Fracao_Ideal"] = "Fracao Ideal";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Fracao_Ideal"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Fracao_Ideal"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Area_Construida"] = "Area Construida";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Area_Construida"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Area_Construida"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Profundidade"] = "Profundidade";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Profundidade"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Profundidade"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Tipo_de_Profundidade"] = "Tipo de Profundidade";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Tipo_de_Profundidade"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Tipo_de_Profundidade"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["SP"] = "SP";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["SP"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["SP"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["RP"] = "RP";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["RP"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["RP"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["AP"] = "AP";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["AP"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["AP"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Transporte_Coletivo"] = "Transporte Coletivo";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Transporte_Coletivo"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Transporte_Coletivo"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Pavimentacao"] = "Pavimentacao";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Pavimentacao"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Pavimentacao"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Tipo_Imove"] = "Tipo Imove";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Tipo_Imove"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Tipo_Imove"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Isento"] = "Isento";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Isento"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Isento"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Quantidade_de_BPs_Analisados"] = "Quantidade de BPs Analisados";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Quantidade_de_BPs_Analisados"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Quantidade_de_BPs_Analisados"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Valor_Venal_do_Terreno"] = "Valor Venal do Terreno";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Valor_Venal_do_Terreno"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Valor_Venal_do_Terreno"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Valor_Venal_da_Edificacao"] = "Valor Venal da Edificacao";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Valor_Venal_da_Edificacao"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Valor_Venal_da_Edificacao"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Valor_Venal_do_Imovel"] = "Valor Venal do Imovel";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Valor_Venal_do_Imovel"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Valor_Venal_do_Imovel"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Ultima_Aliquota_IPTU"] = "Ultima Aliquota IPTU";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Ultima_Aliquota_IPTU"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Ultima_Aliquota_IPTU"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Valor_do_IPTU"] = "Valor do IPTU";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Valor_do_IPTU"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Valor_do_IPTU"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Situacao_de_Cobranca"] = "Situacao de Cobranca";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Situacao_de_Cobranca"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Situacao_de_Cobranca"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Codigo_Loteamento"] = "Codigo Loteamento";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Codigo_Loteamento"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Codigo_Loteamento"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Tipo_Loteamento"] = "Tipo Loteamento";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Tipo_Loteamento"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Tipo_Loteamento"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Nome_Loteamento"] = "Nome Loteamento";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Nome_Loteamento"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Nome_Loteamento"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["ObsArq01"] = "Obs Arq01";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["ObsArq01"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["ObsArq01"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["ObsArq02"] = "Obs Arq02";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["ObsArq02"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["ObsArq02"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["ObsArq03"] = "Obs Arq03";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["ObsArq03"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["ObsArq03"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["DestinacaoPredominante"] = "Destinacao Predominante";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["DestinacaoPredominante"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["DestinacaoPredominante"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["Inscricao_da_Face"] = "Inscricao da Face";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["Inscricao_da_Face"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["Inscricao_da_Face"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["ValidaFace"] = "Valida Face";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["ValidaFace"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["ValidaFace"] = "";
	$fieldLabelspublic_tbl_lotes["Portuguese(Brazil)"]["relacao"] = "Relacao";
	$fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]["relacao"] = "";
	$placeHolderspublic_tbl_lotes["Portuguese(Brazil)"]["relacao"] = "";
	if (count($fieldToolTipspublic_tbl_lotes["Portuguese(Brazil)"]))
		$tdatapublic_tbl_lotes[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_tbl_lotes[""] = array();
	$fieldToolTipspublic_tbl_lotes[""] = array();
	$placeHolderspublic_tbl_lotes[""] = array();
	$pageTitlespublic_tbl_lotes[""] = array();
	if (count($fieldToolTipspublic_tbl_lotes[""]))
		$tdatapublic_tbl_lotes[".isUseToolTips"] = true;
}


	$tdatapublic_tbl_lotes[".NCSearch"] = true;



$tdatapublic_tbl_lotes[".shortTableName"] = "public_tbl_lotes";
$tdatapublic_tbl_lotes[".nSecOptions"] = 0;
$tdatapublic_tbl_lotes[".recsPerRowPrint"] = 1;
$tdatapublic_tbl_lotes[".mainTableOwnerID"] = "";
$tdatapublic_tbl_lotes[".moveNext"] = 1;
$tdatapublic_tbl_lotes[".entityType"] = 0;

$tdatapublic_tbl_lotes[".strOriginalTableName"] = "public.tbl_lotes";

	



$tdatapublic_tbl_lotes[".showAddInPopup"] = false;

$tdatapublic_tbl_lotes[".showEditInPopup"] = false;

$tdatapublic_tbl_lotes[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapublic_tbl_lotes[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_tbl_lotes[".fieldsForRegister"] = array();

$tdatapublic_tbl_lotes[".listAjax"] = false;

	$tdatapublic_tbl_lotes[".audit"] = false;

	$tdatapublic_tbl_lotes[".locking"] = false;



$tdatapublic_tbl_lotes[".list"] = true;



$tdatapublic_tbl_lotes[".reorderRecordsByHeader"] = true;








$tdatapublic_tbl_lotes[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatapublic_tbl_lotes[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatapublic_tbl_lotes[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatapublic_tbl_lotes[".searchSaving"] = false;
//

$tdatapublic_tbl_lotes[".showSearchPanel"] = true;
		$tdatapublic_tbl_lotes[".flexibleSearch"] = true;

$tdatapublic_tbl_lotes[".isUseAjaxSuggest"] = true;






$tdatapublic_tbl_lotes[".ajaxCodeSnippetAdded"] = false;

$tdatapublic_tbl_lotes[".buttonsAdded"] = false;

$tdatapublic_tbl_lotes[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapublic_tbl_lotes[".isUseTimeForSearch"] = false;





$tdatapublic_tbl_lotes[".allSearchFields"] = array();
$tdatapublic_tbl_lotes[".filterFields"] = array();
$tdatapublic_tbl_lotes[".requiredSearchFields"] = array();

$tdatapublic_tbl_lotes[".allSearchFields"][] = "zh";
	$tdatapublic_tbl_lotes[".allSearchFields"][] = "inscricao";
	$tdatapublic_tbl_lotes[".allSearchFields"][] = "chave";
	$tdatapublic_tbl_lotes[".allSearchFields"][] = "Proprietario Principal";
	$tdatapublic_tbl_lotes[".allSearchFields"][] = "cod_log";
	$tdatapublic_tbl_lotes[".allSearchFields"][] = "Logradouro";
	$tdatapublic_tbl_lotes[".allSearchFields"][] = "Numero do Imovel";
	$tdatapublic_tbl_lotes[".allSearchFields"][] = "Complemento";
	$tdatapublic_tbl_lotes[".allSearchFields"][] = "Bairro";
	$tdatapublic_tbl_lotes[".allSearchFields"][] = "Testada Principal";
	$tdatapublic_tbl_lotes[".allSearchFields"][] = "Area do Terreno";
	$tdatapublic_tbl_lotes[".allSearchFields"][] = "Area Construida";
	$tdatapublic_tbl_lotes[".allSearchFields"][] = "Tipo Imove";
	$tdatapublic_tbl_lotes[".allSearchFields"][] = "relacao";
	

$tdatapublic_tbl_lotes[".googleLikeFields"] = array();
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "inscricao";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "exercicio";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "ICTerreno";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "chave";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Codigo Calculo";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Simulacao";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Data Calculo";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Data Referencia Imovel";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Data Fatores IPTU";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Data Fatores Calculo";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Situacao Calculo IPTU";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Reemissao";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "zh";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "cod_log";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Tipo de Logradouro";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Logradouro";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Numero do Imovel";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Complemento";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Tipo de Bairro";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Bairro";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "CEP";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Proprietario Principal";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Mensagem";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Pedologia";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Topografia";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Situacao";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Fped";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Ftop";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Fsit";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Testada Referencia";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "VBU";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Fator Testada";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Fator Gleba";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Profundidade Maxima";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Profundidade Minima";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Fator Profundidade";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Fator Ponderacao";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Testada Principal";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Area do Terreno";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Fracao Ideal";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Area Construida";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Profundidade";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Tipo de Profundidade";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "SP";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "RP";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "AP";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Transporte Coletivo";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Pavimentacao";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Tipo Imove";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Isento";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Quantidade de BPs Analisados";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Valor Venal do Terreno";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Valor Venal da Edificacao";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Valor Venal do Imovel";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Ultima Aliquota IPTU";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Valor do IPTU";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Situacao de Cobranca";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Codigo Loteamento";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Tipo Loteamento";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Nome Loteamento";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "ObsArq01";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "ObsArq02";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "ObsArq03";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "DestinacaoPredominante";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "Inscricao da Face";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "ValidaFace";
$tdatapublic_tbl_lotes[".googleLikeFields"][] = "relacao";


$tdatapublic_tbl_lotes[".advSearchFields"] = array();
$tdatapublic_tbl_lotes[".advSearchFields"][] = "zh";
$tdatapublic_tbl_lotes[".advSearchFields"][] = "inscricao";
$tdatapublic_tbl_lotes[".advSearchFields"][] = "chave";
$tdatapublic_tbl_lotes[".advSearchFields"][] = "Proprietario Principal";
$tdatapublic_tbl_lotes[".advSearchFields"][] = "cod_log";
$tdatapublic_tbl_lotes[".advSearchFields"][] = "Logradouro";
$tdatapublic_tbl_lotes[".advSearchFields"][] = "Numero do Imovel";
$tdatapublic_tbl_lotes[".advSearchFields"][] = "Complemento";
$tdatapublic_tbl_lotes[".advSearchFields"][] = "Bairro";
$tdatapublic_tbl_lotes[".advSearchFields"][] = "Testada Principal";
$tdatapublic_tbl_lotes[".advSearchFields"][] = "Area do Terreno";
$tdatapublic_tbl_lotes[".advSearchFields"][] = "Area Construida";
$tdatapublic_tbl_lotes[".advSearchFields"][] = "Tipo Imove";
$tdatapublic_tbl_lotes[".advSearchFields"][] = "relacao";

$tdatapublic_tbl_lotes[".tableType"] = "list";

$tdatapublic_tbl_lotes[".printerPageOrientation"] = 0;
$tdatapublic_tbl_lotes[".nPrinterPageScale"] = 100;

$tdatapublic_tbl_lotes[".nPrinterSplitRecords"] = 40;

$tdatapublic_tbl_lotes[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_tbl_lotes[".geocodingEnabled"] = false;





$tdatapublic_tbl_lotes[".listGridLayout"] = 3;

$tdatapublic_tbl_lotes[".isDisplayLoading"] = true;




// view page pdf

// print page pdf


$tdatapublic_tbl_lotes[".pageSize"] = 20;

$tdatapublic_tbl_lotes[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_tbl_lotes[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_tbl_lotes[".orderindexes"] = array();

$tdatapublic_tbl_lotes[".sqlHead"] = "SELECT inscricao,  	exercicio,  	\"ICTerreno\",  	chave,  	\"Codigo Calculo\",  	\"Simulacao\",  	\"Data Calculo\",  	\"Data Referencia Imovel\",  	\"Data Fatores IPTU\",  	\"Data Fatores Calculo\",  	\"Situacao Calculo IPTU\",  	\"Reemissao\",  	zh,  	cod_log,  	\"Tipo de Logradouro\",  	\"Logradouro\",  	\"Numero do Imovel\",  	\"Complemento\",  	\"Tipo de Bairro\",  	\"Bairro\",  	\"CEP\",  	\"Proprietario Principal\",  	\"Mensagem\",  	\"Pedologia\",  	\"Topografia\",  	\"Situacao\",  	\"Fped\",  	\"Ftop\",  	\"Fsit\",  	\"Testada Referencia\",  	\"VBU\",  	\"Fator Testada\",  	\"Fator Gleba\",  	\"Profundidade Maxima\",  	\"Profundidade Minima\",  	\"Fator Profundidade\",  	\"Fator Ponderacao\",  	\"Testada Principal\",  	\"Area do Terreno\",  	\"Fracao Ideal\",  	\"Area Construida\",  	\"Profundidade\",  	\"Tipo de Profundidade\",  	\"SP\",  	\"RP\",  	\"AP\",  	\"Transporte Coletivo\",  	\"Pavimentacao\",  	\"Tipo Imove\",  	\"Isento\",  	\"Quantidade de BPs Analisados\",  	\"Valor Venal do Terreno\",  	\"Valor Venal da Edificacao\",  	\"Valor Venal do Imovel\",  	\"Ultima Aliquota IPTU\",  	\"Valor do IPTU\",  	\"Situacao de Cobranca\",  	\"Codigo Loteamento\",  	\"Tipo Loteamento\",  	\"Nome Loteamento\",  	\"ObsArq01\",  	\"ObsArq02\",  	\"ObsArq03\",  	\"DestinacaoPredominante\",  	\"Inscricao da Face\",  	\"ValidaFace\",  	relacao";
$tdatapublic_tbl_lotes[".sqlFrom"] = "FROM \"public\".tbl_lotes";
$tdatapublic_tbl_lotes[".sqlWhereExpr"] = "";
$tdatapublic_tbl_lotes[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_tbl_lotes[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_tbl_lotes[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_tbl_lotes[".highlightSearchResults"] = true;

$tableKeyspublic_tbl_lotes = array();
$tdatapublic_tbl_lotes[".Keys"] = $tableKeyspublic_tbl_lotes;

$tdatapublic_tbl_lotes[".listFields"] = array();
$tdatapublic_tbl_lotes[".listFields"][] = "zh";
$tdatapublic_tbl_lotes[".listFields"][] = "inscricao";
$tdatapublic_tbl_lotes[".listFields"][] = "chave";
$tdatapublic_tbl_lotes[".listFields"][] = "Proprietario Principal";
$tdatapublic_tbl_lotes[".listFields"][] = "cod_log";
$tdatapublic_tbl_lotes[".listFields"][] = "Logradouro";
$tdatapublic_tbl_lotes[".listFields"][] = "Numero do Imovel";
$tdatapublic_tbl_lotes[".listFields"][] = "Complemento";
$tdatapublic_tbl_lotes[".listFields"][] = "Bairro";
$tdatapublic_tbl_lotes[".listFields"][] = "Testada Principal";
$tdatapublic_tbl_lotes[".listFields"][] = "Area do Terreno";
$tdatapublic_tbl_lotes[".listFields"][] = "Area Construida";
$tdatapublic_tbl_lotes[".listFields"][] = "Tipo Imove";
$tdatapublic_tbl_lotes[".listFields"][] = "relacao";

$tdatapublic_tbl_lotes[".hideMobileList"] = array();


$tdatapublic_tbl_lotes[".viewFields"] = array();

$tdatapublic_tbl_lotes[".addFields"] = array();

$tdatapublic_tbl_lotes[".masterListFields"] = array();
$tdatapublic_tbl_lotes[".masterListFields"][] = "inscricao";
$tdatapublic_tbl_lotes[".masterListFields"][] = "exercicio";
$tdatapublic_tbl_lotes[".masterListFields"][] = "ICTerreno";
$tdatapublic_tbl_lotes[".masterListFields"][] = "chave";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Codigo Calculo";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Simulacao";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Data Calculo";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Data Referencia Imovel";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Data Fatores IPTU";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Data Fatores Calculo";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Situacao Calculo IPTU";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Reemissao";
$tdatapublic_tbl_lotes[".masterListFields"][] = "zh";
$tdatapublic_tbl_lotes[".masterListFields"][] = "cod_log";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Tipo de Logradouro";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Logradouro";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Numero do Imovel";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Complemento";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Tipo de Bairro";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Bairro";
$tdatapublic_tbl_lotes[".masterListFields"][] = "CEP";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Proprietario Principal";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Mensagem";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Pedologia";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Topografia";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Situacao";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Fped";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Ftop";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Fsit";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Testada Referencia";
$tdatapublic_tbl_lotes[".masterListFields"][] = "VBU";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Fator Testada";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Fator Gleba";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Profundidade Maxima";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Profundidade Minima";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Fator Profundidade";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Fator Ponderacao";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Testada Principal";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Area do Terreno";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Fracao Ideal";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Area Construida";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Profundidade";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Tipo de Profundidade";
$tdatapublic_tbl_lotes[".masterListFields"][] = "SP";
$tdatapublic_tbl_lotes[".masterListFields"][] = "RP";
$tdatapublic_tbl_lotes[".masterListFields"][] = "AP";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Transporte Coletivo";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Pavimentacao";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Tipo Imove";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Isento";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Quantidade de BPs Analisados";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Valor Venal do Terreno";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Valor Venal da Edificacao";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Valor Venal do Imovel";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Ultima Aliquota IPTU";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Valor do IPTU";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Situacao de Cobranca";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Codigo Loteamento";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Tipo Loteamento";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Nome Loteamento";
$tdatapublic_tbl_lotes[".masterListFields"][] = "ObsArq01";
$tdatapublic_tbl_lotes[".masterListFields"][] = "ObsArq02";
$tdatapublic_tbl_lotes[".masterListFields"][] = "ObsArq03";
$tdatapublic_tbl_lotes[".masterListFields"][] = "DestinacaoPredominante";
$tdatapublic_tbl_lotes[".masterListFields"][] = "Inscricao da Face";
$tdatapublic_tbl_lotes[".masterListFields"][] = "ValidaFace";
$tdatapublic_tbl_lotes[".masterListFields"][] = "relacao";

$tdatapublic_tbl_lotes[".inlineAddFields"] = array();

$tdatapublic_tbl_lotes[".editFields"] = array();

$tdatapublic_tbl_lotes[".inlineEditFields"] = array();

$tdatapublic_tbl_lotes[".updateSelectedFields"] = array();


$tdatapublic_tbl_lotes[".exportFields"] = array();

$tdatapublic_tbl_lotes[".importFields"] = array();

$tdatapublic_tbl_lotes[".printFields"] = array();


//	inscricao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "inscricao";
	$fdata["GoodName"] = "inscricao";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","inscricao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "inscricao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "inscricao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_lotes["inscricao"] = $fdata;
//	exercicio
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "exercicio";
	$fdata["GoodName"] = "exercicio";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","exercicio");
	$fdata["FieldType"] = 20;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "exercicio";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "exercicio";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["exercicio"] = $fdata;
//	ICTerreno
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "ICTerreno";
	$fdata["GoodName"] = "ICTerreno";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","ICTerreno");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "ICTerreno";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"ICTerreno\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["ICTerreno"] = $fdata;
//	chave
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "chave";
	$fdata["GoodName"] = "chave";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","chave");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "chave";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "chave";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_lotes["chave"] = $fdata;
//	Codigo Calculo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "Codigo Calculo";
	$fdata["GoodName"] = "Codigo_Calculo";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Codigo_Calculo");
	$fdata["FieldType"] = 20;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Codigo Calculo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Codigo Calculo\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Codigo Calculo"] = $fdata;
//	Simulacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "Simulacao";
	$fdata["GoodName"] = "Simulacao";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Simulacao");
	$fdata["FieldType"] = 20;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Simulacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Simulacao\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Simulacao"] = $fdata;
//	Data Calculo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "Data Calculo";
	$fdata["GoodName"] = "Data_Calculo";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Data_Calculo");
	$fdata["FieldType"] = 135;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Data Calculo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Data Calculo\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

		$edata["ShowTime"] = true;

	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Data Calculo"] = $fdata;
//	Data Referencia Imovel
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "Data Referencia Imovel";
	$fdata["GoodName"] = "Data_Referencia_Imovel";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Data_Referencia_Imovel");
	$fdata["FieldType"] = 135;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Data Referencia Imovel";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Data Referencia Imovel\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

		$edata["ShowTime"] = true;

	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Data Referencia Imovel"] = $fdata;
//	Data Fatores IPTU
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "Data Fatores IPTU";
	$fdata["GoodName"] = "Data_Fatores_IPTU";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Data_Fatores_IPTU");
	$fdata["FieldType"] = 135;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Data Fatores IPTU";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Data Fatores IPTU\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

		$edata["ShowTime"] = true;

	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Data Fatores IPTU"] = $fdata;
//	Data Fatores Calculo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "Data Fatores Calculo";
	$fdata["GoodName"] = "Data_Fatores_Calculo";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Data_Fatores_Calculo");
	$fdata["FieldType"] = 135;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Data Fatores Calculo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Data Fatores Calculo\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

		$edata["ShowTime"] = true;

	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Data Fatores Calculo"] = $fdata;
//	Situacao Calculo IPTU
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "Situacao Calculo IPTU";
	$fdata["GoodName"] = "Situacao_Calculo_IPTU";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Situacao_Calculo_IPTU");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Situacao Calculo IPTU";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Situacao Calculo IPTU\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Situacao Calculo IPTU"] = $fdata;
//	Reemissao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "Reemissao";
	$fdata["GoodName"] = "Reemissao";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Reemissao");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Reemissao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Reemissao\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Reemissao"] = $fdata;
//	zh
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "zh";
	$fdata["GoodName"] = "zh";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","zh");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "zh";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "zh";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_lotes["zh"] = $fdata;
//	cod_log
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "cod_log";
	$fdata["GoodName"] = "cod_log";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","cod_log");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "cod_log";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cod_log";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_lotes["cod_log"] = $fdata;
//	Tipo de Logradouro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "Tipo de Logradouro";
	$fdata["GoodName"] = "Tipo_de_Logradouro";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Tipo_de_Logradouro");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Tipo de Logradouro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Tipo de Logradouro\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Tipo de Logradouro"] = $fdata;
//	Logradouro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "Logradouro";
	$fdata["GoodName"] = "Logradouro";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Logradouro");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Logradouro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Logradouro\"";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_lotes["Logradouro"] = $fdata;
//	Numero do Imovel
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "Numero do Imovel";
	$fdata["GoodName"] = "Numero_do_Imovel";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Numero_do_Imovel");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Numero do Imovel";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Numero do Imovel\"";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_lotes["Numero do Imovel"] = $fdata;
//	Complemento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 18;
	$fdata["strName"] = "Complemento";
	$fdata["GoodName"] = "Complemento";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Complemento");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Complemento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Complemento\"";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_lotes["Complemento"] = $fdata;
//	Tipo de Bairro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 19;
	$fdata["strName"] = "Tipo de Bairro";
	$fdata["GoodName"] = "Tipo_de_Bairro";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Tipo_de_Bairro");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Tipo de Bairro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Tipo de Bairro\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Tipo de Bairro"] = $fdata;
//	Bairro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 20;
	$fdata["strName"] = "Bairro";
	$fdata["GoodName"] = "Bairro";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Bairro");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Bairro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Bairro\"";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_lotes["Bairro"] = $fdata;
//	CEP
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 21;
	$fdata["strName"] = "CEP";
	$fdata["GoodName"] = "CEP";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","CEP");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "CEP";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"CEP\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["CEP"] = $fdata;
//	Proprietario Principal
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 22;
	$fdata["strName"] = "Proprietario Principal";
	$fdata["GoodName"] = "Proprietario_Principal";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Proprietario_Principal");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Proprietario Principal";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Proprietario Principal\"";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_lotes["Proprietario Principal"] = $fdata;
//	Mensagem
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 23;
	$fdata["strName"] = "Mensagem";
	$fdata["GoodName"] = "Mensagem";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Mensagem");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Mensagem";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Mensagem\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Mensagem"] = $fdata;
//	Pedologia
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 24;
	$fdata["strName"] = "Pedologia";
	$fdata["GoodName"] = "Pedologia";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Pedologia");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Pedologia";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Pedologia\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Pedologia"] = $fdata;
//	Topografia
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 25;
	$fdata["strName"] = "Topografia";
	$fdata["GoodName"] = "Topografia";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Topografia");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Topografia";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Topografia\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Topografia"] = $fdata;
//	Situacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 26;
	$fdata["strName"] = "Situacao";
	$fdata["GoodName"] = "Situacao";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Situacao");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Situacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Situacao\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Situacao"] = $fdata;
//	Fped
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 27;
	$fdata["strName"] = "Fped";
	$fdata["GoodName"] = "Fped";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Fped");
	$fdata["FieldType"] = 5;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Fped";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Fped\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Fped"] = $fdata;
//	Ftop
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 28;
	$fdata["strName"] = "Ftop";
	$fdata["GoodName"] = "Ftop";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Ftop");
	$fdata["FieldType"] = 5;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Ftop";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Ftop\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Ftop"] = $fdata;
//	Fsit
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 29;
	$fdata["strName"] = "Fsit";
	$fdata["GoodName"] = "Fsit";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Fsit");
	$fdata["FieldType"] = 5;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Fsit";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Fsit\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Fsit"] = $fdata;
//	Testada Referencia
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 30;
	$fdata["strName"] = "Testada Referencia";
	$fdata["GoodName"] = "Testada_Referencia";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Testada_Referencia");
	$fdata["FieldType"] = 5;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Testada Referencia";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Testada Referencia\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Testada Referencia"] = $fdata;
//	VBU
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 31;
	$fdata["strName"] = "VBU";
	$fdata["GoodName"] = "VBU";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","VBU");
	$fdata["FieldType"] = 5;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "VBU";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"VBU\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["VBU"] = $fdata;
//	Fator Testada
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 32;
	$fdata["strName"] = "Fator Testada";
	$fdata["GoodName"] = "Fator_Testada";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Fator_Testada");
	$fdata["FieldType"] = 5;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Fator Testada";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Fator Testada\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Fator Testada"] = $fdata;
//	Fator Gleba
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 33;
	$fdata["strName"] = "Fator Gleba";
	$fdata["GoodName"] = "Fator_Gleba";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Fator_Gleba");
	$fdata["FieldType"] = 5;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Fator Gleba";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Fator Gleba\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Fator Gleba"] = $fdata;
//	Profundidade Maxima
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 34;
	$fdata["strName"] = "Profundidade Maxima";
	$fdata["GoodName"] = "Profundidade_Maxima";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Profundidade_Maxima");
	$fdata["FieldType"] = 5;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Profundidade Maxima";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Profundidade Maxima\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Profundidade Maxima"] = $fdata;
//	Profundidade Minima
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 35;
	$fdata["strName"] = "Profundidade Minima";
	$fdata["GoodName"] = "Profundidade_Minima";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Profundidade_Minima");
	$fdata["FieldType"] = 5;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Profundidade Minima";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Profundidade Minima\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Profundidade Minima"] = $fdata;
//	Fator Profundidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 36;
	$fdata["strName"] = "Fator Profundidade";
	$fdata["GoodName"] = "Fator_Profundidade";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Fator_Profundidade");
	$fdata["FieldType"] = 5;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Fator Profundidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Fator Profundidade\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Fator Profundidade"] = $fdata;
//	Fator Ponderacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 37;
	$fdata["strName"] = "Fator Ponderacao";
	$fdata["GoodName"] = "Fator_Ponderacao";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Fator_Ponderacao");
	$fdata["FieldType"] = 5;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Fator Ponderacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Fator Ponderacao\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Fator Ponderacao"] = $fdata;
//	Testada Principal
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 38;
	$fdata["strName"] = "Testada Principal";
	$fdata["GoodName"] = "Testada_Principal";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Testada_Principal");
	$fdata["FieldType"] = 5;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Testada Principal";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Testada Principal\"";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_lotes["Testada Principal"] = $fdata;
//	Area do Terreno
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 39;
	$fdata["strName"] = "Area do Terreno";
	$fdata["GoodName"] = "Area_do_Terreno";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Area_do_Terreno");
	$fdata["FieldType"] = 5;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Area do Terreno";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Area do Terreno\"";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_lotes["Area do Terreno"] = $fdata;
//	Fracao Ideal
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 40;
	$fdata["strName"] = "Fracao Ideal";
	$fdata["GoodName"] = "Fracao_Ideal";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Fracao_Ideal");
	$fdata["FieldType"] = 5;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Fracao Ideal";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Fracao Ideal\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Fracao Ideal"] = $fdata;
//	Area Construida
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 41;
	$fdata["strName"] = "Area Construida";
	$fdata["GoodName"] = "Area_Construida";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Area_Construida");
	$fdata["FieldType"] = 5;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Area Construida";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Area Construida\"";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_lotes["Area Construida"] = $fdata;
//	Profundidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 42;
	$fdata["strName"] = "Profundidade";
	$fdata["GoodName"] = "Profundidade";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Profundidade");
	$fdata["FieldType"] = 5;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Profundidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Profundidade\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Profundidade"] = $fdata;
//	Tipo de Profundidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 43;
	$fdata["strName"] = "Tipo de Profundidade";
	$fdata["GoodName"] = "Tipo_de_Profundidade";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Tipo_de_Profundidade");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Tipo de Profundidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Tipo de Profundidade\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Tipo de Profundidade"] = $fdata;
//	SP
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 44;
	$fdata["strName"] = "SP";
	$fdata["GoodName"] = "SP";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","SP");
	$fdata["FieldType"] = 5;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "SP";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"SP\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["SP"] = $fdata;
//	RP
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 45;
	$fdata["strName"] = "RP";
	$fdata["GoodName"] = "RP";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","RP");
	$fdata["FieldType"] = 5;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "RP";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"RP\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["RP"] = $fdata;
//	AP
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 46;
	$fdata["strName"] = "AP";
	$fdata["GoodName"] = "AP";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","AP");
	$fdata["FieldType"] = 5;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "AP";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"AP\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["AP"] = $fdata;
//	Transporte Coletivo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 47;
	$fdata["strName"] = "Transporte Coletivo";
	$fdata["GoodName"] = "Transporte_Coletivo";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Transporte_Coletivo");
	$fdata["FieldType"] = 5;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Transporte Coletivo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Transporte Coletivo\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Transporte Coletivo"] = $fdata;
//	Pavimentacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 48;
	$fdata["strName"] = "Pavimentacao";
	$fdata["GoodName"] = "Pavimentacao";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Pavimentacao");
	$fdata["FieldType"] = 5;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Pavimentacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Pavimentacao\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Pavimentacao"] = $fdata;
//	Tipo Imove
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 49;
	$fdata["strName"] = "Tipo Imove";
	$fdata["GoodName"] = "Tipo_Imove";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Tipo_Imove");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "Tipo Imove";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Tipo Imove\"";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_lotes["Tipo Imove"] = $fdata;
//	Isento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 50;
	$fdata["strName"] = "Isento";
	$fdata["GoodName"] = "Isento";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Isento");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Isento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Isento\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Isento"] = $fdata;
//	Quantidade de BPs Analisados
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 51;
	$fdata["strName"] = "Quantidade de BPs Analisados";
	$fdata["GoodName"] = "Quantidade_de_BPs_Analisados";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Quantidade_de_BPs_Analisados");
	$fdata["FieldType"] = 20;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Quantidade de BPs Analisados";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Quantidade de BPs Analisados\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Quantidade de BPs Analisados"] = $fdata;
//	Valor Venal do Terreno
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 52;
	$fdata["strName"] = "Valor Venal do Terreno";
	$fdata["GoodName"] = "Valor_Venal_do_Terreno";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Valor_Venal_do_Terreno");
	$fdata["FieldType"] = 5;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Valor Venal do Terreno";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Valor Venal do Terreno\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Valor Venal do Terreno"] = $fdata;
//	Valor Venal da Edificacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 53;
	$fdata["strName"] = "Valor Venal da Edificacao";
	$fdata["GoodName"] = "Valor_Venal_da_Edificacao";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Valor_Venal_da_Edificacao");
	$fdata["FieldType"] = 5;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Valor Venal da Edificacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Valor Venal da Edificacao\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Valor Venal da Edificacao"] = $fdata;
//	Valor Venal do Imovel
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 54;
	$fdata["strName"] = "Valor Venal do Imovel";
	$fdata["GoodName"] = "Valor_Venal_do_Imovel";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Valor_Venal_do_Imovel");
	$fdata["FieldType"] = 5;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Valor Venal do Imovel";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Valor Venal do Imovel\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Valor Venal do Imovel"] = $fdata;
//	Ultima Aliquota IPTU
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 55;
	$fdata["strName"] = "Ultima Aliquota IPTU";
	$fdata["GoodName"] = "Ultima_Aliquota_IPTU";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Ultima_Aliquota_IPTU");
	$fdata["FieldType"] = 5;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Ultima Aliquota IPTU";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Ultima Aliquota IPTU\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Ultima Aliquota IPTU"] = $fdata;
//	Valor do IPTU
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 56;
	$fdata["strName"] = "Valor do IPTU";
	$fdata["GoodName"] = "Valor_do_IPTU";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Valor_do_IPTU");
	$fdata["FieldType"] = 5;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Valor do IPTU";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Valor do IPTU\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Valor do IPTU"] = $fdata;
//	Situacao de Cobranca
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 57;
	$fdata["strName"] = "Situacao de Cobranca";
	$fdata["GoodName"] = "Situacao_de_Cobranca";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Situacao_de_Cobranca");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Situacao de Cobranca";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Situacao de Cobranca\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Situacao de Cobranca"] = $fdata;
//	Codigo Loteamento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 58;
	$fdata["strName"] = "Codigo Loteamento";
	$fdata["GoodName"] = "Codigo_Loteamento";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Codigo_Loteamento");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Codigo Loteamento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Codigo Loteamento\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Codigo Loteamento"] = $fdata;
//	Tipo Loteamento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 59;
	$fdata["strName"] = "Tipo Loteamento";
	$fdata["GoodName"] = "Tipo_Loteamento";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Tipo_Loteamento");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Tipo Loteamento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Tipo Loteamento\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Tipo Loteamento"] = $fdata;
//	Nome Loteamento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 60;
	$fdata["strName"] = "Nome Loteamento";
	$fdata["GoodName"] = "Nome_Loteamento";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Nome_Loteamento");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Nome Loteamento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Nome Loteamento\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Nome Loteamento"] = $fdata;
//	ObsArq01
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 61;
	$fdata["strName"] = "ObsArq01";
	$fdata["GoodName"] = "ObsArq01";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","ObsArq01");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "ObsArq01";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"ObsArq01\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["ObsArq01"] = $fdata;
//	ObsArq02
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 62;
	$fdata["strName"] = "ObsArq02";
	$fdata["GoodName"] = "ObsArq02";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","ObsArq02");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "ObsArq02";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"ObsArq02\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["ObsArq02"] = $fdata;
//	ObsArq03
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 63;
	$fdata["strName"] = "ObsArq03";
	$fdata["GoodName"] = "ObsArq03";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","ObsArq03");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "ObsArq03";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"ObsArq03\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["ObsArq03"] = $fdata;
//	DestinacaoPredominante
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 64;
	$fdata["strName"] = "DestinacaoPredominante";
	$fdata["GoodName"] = "DestinacaoPredominante";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","DestinacaoPredominante");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "DestinacaoPredominante";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"DestinacaoPredominante\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["DestinacaoPredominante"] = $fdata;
//	Inscricao da Face
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 65;
	$fdata["strName"] = "Inscricao da Face";
	$fdata["GoodName"] = "Inscricao_da_Face";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","Inscricao_da_Face");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Inscricao da Face";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Inscricao da Face\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["Inscricao da Face"] = $fdata;
//	ValidaFace
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 66;
	$fdata["strName"] = "ValidaFace";
	$fdata["GoodName"] = "ValidaFace";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","ValidaFace");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "ValidaFace";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"ValidaFace\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_lotes["ValidaFace"] = $fdata;
//	relacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 67;
	$fdata["strName"] = "relacao";
	$fdata["GoodName"] = "relacao";
	$fdata["ownerTable"] = "public.tbl_lotes";
	$fdata["Label"] = GetFieldLabel("public_tbl_lotes","relacao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "relacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "relacao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=25";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_lotes["relacao"] = $fdata;


$tables_data["public.tbl_lotes"]=&$tdatapublic_tbl_lotes;
$field_labels["public_tbl_lotes"] = &$fieldLabelspublic_tbl_lotes;
$fieldToolTips["public_tbl_lotes"] = &$fieldToolTipspublic_tbl_lotes;
$placeHolders["public_tbl_lotes"] = &$placeHolderspublic_tbl_lotes;
$page_titles["public_tbl_lotes"] = &$pageTitlespublic_tbl_lotes;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.tbl_lotes"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.tbl_lotes"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_tbl_lotes()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "inscricao,  	exercicio,  	\"ICTerreno\",  	chave,  	\"Codigo Calculo\",  	\"Simulacao\",  	\"Data Calculo\",  	\"Data Referencia Imovel\",  	\"Data Fatores IPTU\",  	\"Data Fatores Calculo\",  	\"Situacao Calculo IPTU\",  	\"Reemissao\",  	zh,  	cod_log,  	\"Tipo de Logradouro\",  	\"Logradouro\",  	\"Numero do Imovel\",  	\"Complemento\",  	\"Tipo de Bairro\",  	\"Bairro\",  	\"CEP\",  	\"Proprietario Principal\",  	\"Mensagem\",  	\"Pedologia\",  	\"Topografia\",  	\"Situacao\",  	\"Fped\",  	\"Ftop\",  	\"Fsit\",  	\"Testada Referencia\",  	\"VBU\",  	\"Fator Testada\",  	\"Fator Gleba\",  	\"Profundidade Maxima\",  	\"Profundidade Minima\",  	\"Fator Profundidade\",  	\"Fator Ponderacao\",  	\"Testada Principal\",  	\"Area do Terreno\",  	\"Fracao Ideal\",  	\"Area Construida\",  	\"Profundidade\",  	\"Tipo de Profundidade\",  	\"SP\",  	\"RP\",  	\"AP\",  	\"Transporte Coletivo\",  	\"Pavimentacao\",  	\"Tipo Imove\",  	\"Isento\",  	\"Quantidade de BPs Analisados\",  	\"Valor Venal do Terreno\",  	\"Valor Venal da Edificacao\",  	\"Valor Venal do Imovel\",  	\"Ultima Aliquota IPTU\",  	\"Valor do IPTU\",  	\"Situacao de Cobranca\",  	\"Codigo Loteamento\",  	\"Tipo Loteamento\",  	\"Nome Loteamento\",  	\"ObsArq01\",  	\"ObsArq02\",  	\"ObsArq03\",  	\"DestinacaoPredominante\",  	\"Inscricao da Face\",  	\"ValidaFace\",  	relacao";
$proto0["m_strFrom"] = "FROM \"public\".tbl_lotes";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "inscricao",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto6["m_sql"] = "inscricao";
$proto6["m_srcTableName"] = "public.tbl_lotes";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "exercicio",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto8["m_sql"] = "exercicio";
$proto8["m_srcTableName"] = "public.tbl_lotes";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "ICTerreno",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto10["m_sql"] = "\"ICTerreno\"";
$proto10["m_srcTableName"] = "public.tbl_lotes";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "chave",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto12["m_sql"] = "chave";
$proto12["m_srcTableName"] = "public.tbl_lotes";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "Codigo Calculo",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto14["m_sql"] = "\"Codigo Calculo\"";
$proto14["m_srcTableName"] = "public.tbl_lotes";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "Simulacao",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto16["m_sql"] = "\"Simulacao\"";
$proto16["m_srcTableName"] = "public.tbl_lotes";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "Data Calculo",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto18["m_sql"] = "\"Data Calculo\"";
$proto18["m_srcTableName"] = "public.tbl_lotes";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "Data Referencia Imovel",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto20["m_sql"] = "\"Data Referencia Imovel\"";
$proto20["m_srcTableName"] = "public.tbl_lotes";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "Data Fatores IPTU",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto22["m_sql"] = "\"Data Fatores IPTU\"";
$proto22["m_srcTableName"] = "public.tbl_lotes";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "Data Fatores Calculo",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto24["m_sql"] = "\"Data Fatores Calculo\"";
$proto24["m_srcTableName"] = "public.tbl_lotes";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "Situacao Calculo IPTU",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto26["m_sql"] = "\"Situacao Calculo IPTU\"";
$proto26["m_srcTableName"] = "public.tbl_lotes";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "Reemissao",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto28["m_sql"] = "\"Reemissao\"";
$proto28["m_srcTableName"] = "public.tbl_lotes";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "zh",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto30["m_sql"] = "zh";
$proto30["m_srcTableName"] = "public.tbl_lotes";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "cod_log",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto32["m_sql"] = "cod_log";
$proto32["m_srcTableName"] = "public.tbl_lotes";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "Tipo de Logradouro",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto34["m_sql"] = "\"Tipo de Logradouro\"";
$proto34["m_srcTableName"] = "public.tbl_lotes";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto36=array();
			$obj = new SQLField(array(
	"m_strName" => "Logradouro",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto36["m_sql"] = "\"Logradouro\"";
$proto36["m_srcTableName"] = "public.tbl_lotes";
$proto36["m_expr"]=$obj;
$proto36["m_alias"] = "";
$obj = new SQLFieldListItem($proto36);

$proto0["m_fieldlist"][]=$obj;
						$proto38=array();
			$obj = new SQLField(array(
	"m_strName" => "Numero do Imovel",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto38["m_sql"] = "\"Numero do Imovel\"";
$proto38["m_srcTableName"] = "public.tbl_lotes";
$proto38["m_expr"]=$obj;
$proto38["m_alias"] = "";
$obj = new SQLFieldListItem($proto38);

$proto0["m_fieldlist"][]=$obj;
						$proto40=array();
			$obj = new SQLField(array(
	"m_strName" => "Complemento",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto40["m_sql"] = "\"Complemento\"";
$proto40["m_srcTableName"] = "public.tbl_lotes";
$proto40["m_expr"]=$obj;
$proto40["m_alias"] = "";
$obj = new SQLFieldListItem($proto40);

$proto0["m_fieldlist"][]=$obj;
						$proto42=array();
			$obj = new SQLField(array(
	"m_strName" => "Tipo de Bairro",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto42["m_sql"] = "\"Tipo de Bairro\"";
$proto42["m_srcTableName"] = "public.tbl_lotes";
$proto42["m_expr"]=$obj;
$proto42["m_alias"] = "";
$obj = new SQLFieldListItem($proto42);

$proto0["m_fieldlist"][]=$obj;
						$proto44=array();
			$obj = new SQLField(array(
	"m_strName" => "Bairro",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto44["m_sql"] = "\"Bairro\"";
$proto44["m_srcTableName"] = "public.tbl_lotes";
$proto44["m_expr"]=$obj;
$proto44["m_alias"] = "";
$obj = new SQLFieldListItem($proto44);

$proto0["m_fieldlist"][]=$obj;
						$proto46=array();
			$obj = new SQLField(array(
	"m_strName" => "CEP",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto46["m_sql"] = "\"CEP\"";
$proto46["m_srcTableName"] = "public.tbl_lotes";
$proto46["m_expr"]=$obj;
$proto46["m_alias"] = "";
$obj = new SQLFieldListItem($proto46);

$proto0["m_fieldlist"][]=$obj;
						$proto48=array();
			$obj = new SQLField(array(
	"m_strName" => "Proprietario Principal",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto48["m_sql"] = "\"Proprietario Principal\"";
$proto48["m_srcTableName"] = "public.tbl_lotes";
$proto48["m_expr"]=$obj;
$proto48["m_alias"] = "";
$obj = new SQLFieldListItem($proto48);

$proto0["m_fieldlist"][]=$obj;
						$proto50=array();
			$obj = new SQLField(array(
	"m_strName" => "Mensagem",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto50["m_sql"] = "\"Mensagem\"";
$proto50["m_srcTableName"] = "public.tbl_lotes";
$proto50["m_expr"]=$obj;
$proto50["m_alias"] = "";
$obj = new SQLFieldListItem($proto50);

$proto0["m_fieldlist"][]=$obj;
						$proto52=array();
			$obj = new SQLField(array(
	"m_strName" => "Pedologia",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto52["m_sql"] = "\"Pedologia\"";
$proto52["m_srcTableName"] = "public.tbl_lotes";
$proto52["m_expr"]=$obj;
$proto52["m_alias"] = "";
$obj = new SQLFieldListItem($proto52);

$proto0["m_fieldlist"][]=$obj;
						$proto54=array();
			$obj = new SQLField(array(
	"m_strName" => "Topografia",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto54["m_sql"] = "\"Topografia\"";
$proto54["m_srcTableName"] = "public.tbl_lotes";
$proto54["m_expr"]=$obj;
$proto54["m_alias"] = "";
$obj = new SQLFieldListItem($proto54);

$proto0["m_fieldlist"][]=$obj;
						$proto56=array();
			$obj = new SQLField(array(
	"m_strName" => "Situacao",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto56["m_sql"] = "\"Situacao\"";
$proto56["m_srcTableName"] = "public.tbl_lotes";
$proto56["m_expr"]=$obj;
$proto56["m_alias"] = "";
$obj = new SQLFieldListItem($proto56);

$proto0["m_fieldlist"][]=$obj;
						$proto58=array();
			$obj = new SQLField(array(
	"m_strName" => "Fped",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto58["m_sql"] = "\"Fped\"";
$proto58["m_srcTableName"] = "public.tbl_lotes";
$proto58["m_expr"]=$obj;
$proto58["m_alias"] = "";
$obj = new SQLFieldListItem($proto58);

$proto0["m_fieldlist"][]=$obj;
						$proto60=array();
			$obj = new SQLField(array(
	"m_strName" => "Ftop",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto60["m_sql"] = "\"Ftop\"";
$proto60["m_srcTableName"] = "public.tbl_lotes";
$proto60["m_expr"]=$obj;
$proto60["m_alias"] = "";
$obj = new SQLFieldListItem($proto60);

$proto0["m_fieldlist"][]=$obj;
						$proto62=array();
			$obj = new SQLField(array(
	"m_strName" => "Fsit",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto62["m_sql"] = "\"Fsit\"";
$proto62["m_srcTableName"] = "public.tbl_lotes";
$proto62["m_expr"]=$obj;
$proto62["m_alias"] = "";
$obj = new SQLFieldListItem($proto62);

$proto0["m_fieldlist"][]=$obj;
						$proto64=array();
			$obj = new SQLField(array(
	"m_strName" => "Testada Referencia",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto64["m_sql"] = "\"Testada Referencia\"";
$proto64["m_srcTableName"] = "public.tbl_lotes";
$proto64["m_expr"]=$obj;
$proto64["m_alias"] = "";
$obj = new SQLFieldListItem($proto64);

$proto0["m_fieldlist"][]=$obj;
						$proto66=array();
			$obj = new SQLField(array(
	"m_strName" => "VBU",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto66["m_sql"] = "\"VBU\"";
$proto66["m_srcTableName"] = "public.tbl_lotes";
$proto66["m_expr"]=$obj;
$proto66["m_alias"] = "";
$obj = new SQLFieldListItem($proto66);

$proto0["m_fieldlist"][]=$obj;
						$proto68=array();
			$obj = new SQLField(array(
	"m_strName" => "Fator Testada",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto68["m_sql"] = "\"Fator Testada\"";
$proto68["m_srcTableName"] = "public.tbl_lotes";
$proto68["m_expr"]=$obj;
$proto68["m_alias"] = "";
$obj = new SQLFieldListItem($proto68);

$proto0["m_fieldlist"][]=$obj;
						$proto70=array();
			$obj = new SQLField(array(
	"m_strName" => "Fator Gleba",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto70["m_sql"] = "\"Fator Gleba\"";
$proto70["m_srcTableName"] = "public.tbl_lotes";
$proto70["m_expr"]=$obj;
$proto70["m_alias"] = "";
$obj = new SQLFieldListItem($proto70);

$proto0["m_fieldlist"][]=$obj;
						$proto72=array();
			$obj = new SQLField(array(
	"m_strName" => "Profundidade Maxima",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto72["m_sql"] = "\"Profundidade Maxima\"";
$proto72["m_srcTableName"] = "public.tbl_lotes";
$proto72["m_expr"]=$obj;
$proto72["m_alias"] = "";
$obj = new SQLFieldListItem($proto72);

$proto0["m_fieldlist"][]=$obj;
						$proto74=array();
			$obj = new SQLField(array(
	"m_strName" => "Profundidade Minima",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto74["m_sql"] = "\"Profundidade Minima\"";
$proto74["m_srcTableName"] = "public.tbl_lotes";
$proto74["m_expr"]=$obj;
$proto74["m_alias"] = "";
$obj = new SQLFieldListItem($proto74);

$proto0["m_fieldlist"][]=$obj;
						$proto76=array();
			$obj = new SQLField(array(
	"m_strName" => "Fator Profundidade",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto76["m_sql"] = "\"Fator Profundidade\"";
$proto76["m_srcTableName"] = "public.tbl_lotes";
$proto76["m_expr"]=$obj;
$proto76["m_alias"] = "";
$obj = new SQLFieldListItem($proto76);

$proto0["m_fieldlist"][]=$obj;
						$proto78=array();
			$obj = new SQLField(array(
	"m_strName" => "Fator Ponderacao",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto78["m_sql"] = "\"Fator Ponderacao\"";
$proto78["m_srcTableName"] = "public.tbl_lotes";
$proto78["m_expr"]=$obj;
$proto78["m_alias"] = "";
$obj = new SQLFieldListItem($proto78);

$proto0["m_fieldlist"][]=$obj;
						$proto80=array();
			$obj = new SQLField(array(
	"m_strName" => "Testada Principal",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto80["m_sql"] = "\"Testada Principal\"";
$proto80["m_srcTableName"] = "public.tbl_lotes";
$proto80["m_expr"]=$obj;
$proto80["m_alias"] = "";
$obj = new SQLFieldListItem($proto80);

$proto0["m_fieldlist"][]=$obj;
						$proto82=array();
			$obj = new SQLField(array(
	"m_strName" => "Area do Terreno",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto82["m_sql"] = "\"Area do Terreno\"";
$proto82["m_srcTableName"] = "public.tbl_lotes";
$proto82["m_expr"]=$obj;
$proto82["m_alias"] = "";
$obj = new SQLFieldListItem($proto82);

$proto0["m_fieldlist"][]=$obj;
						$proto84=array();
			$obj = new SQLField(array(
	"m_strName" => "Fracao Ideal",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto84["m_sql"] = "\"Fracao Ideal\"";
$proto84["m_srcTableName"] = "public.tbl_lotes";
$proto84["m_expr"]=$obj;
$proto84["m_alias"] = "";
$obj = new SQLFieldListItem($proto84);

$proto0["m_fieldlist"][]=$obj;
						$proto86=array();
			$obj = new SQLField(array(
	"m_strName" => "Area Construida",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto86["m_sql"] = "\"Area Construida\"";
$proto86["m_srcTableName"] = "public.tbl_lotes";
$proto86["m_expr"]=$obj;
$proto86["m_alias"] = "";
$obj = new SQLFieldListItem($proto86);

$proto0["m_fieldlist"][]=$obj;
						$proto88=array();
			$obj = new SQLField(array(
	"m_strName" => "Profundidade",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto88["m_sql"] = "\"Profundidade\"";
$proto88["m_srcTableName"] = "public.tbl_lotes";
$proto88["m_expr"]=$obj;
$proto88["m_alias"] = "";
$obj = new SQLFieldListItem($proto88);

$proto0["m_fieldlist"][]=$obj;
						$proto90=array();
			$obj = new SQLField(array(
	"m_strName" => "Tipo de Profundidade",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto90["m_sql"] = "\"Tipo de Profundidade\"";
$proto90["m_srcTableName"] = "public.tbl_lotes";
$proto90["m_expr"]=$obj;
$proto90["m_alias"] = "";
$obj = new SQLFieldListItem($proto90);

$proto0["m_fieldlist"][]=$obj;
						$proto92=array();
			$obj = new SQLField(array(
	"m_strName" => "SP",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto92["m_sql"] = "\"SP\"";
$proto92["m_srcTableName"] = "public.tbl_lotes";
$proto92["m_expr"]=$obj;
$proto92["m_alias"] = "";
$obj = new SQLFieldListItem($proto92);

$proto0["m_fieldlist"][]=$obj;
						$proto94=array();
			$obj = new SQLField(array(
	"m_strName" => "RP",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto94["m_sql"] = "\"RP\"";
$proto94["m_srcTableName"] = "public.tbl_lotes";
$proto94["m_expr"]=$obj;
$proto94["m_alias"] = "";
$obj = new SQLFieldListItem($proto94);

$proto0["m_fieldlist"][]=$obj;
						$proto96=array();
			$obj = new SQLField(array(
	"m_strName" => "AP",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto96["m_sql"] = "\"AP\"";
$proto96["m_srcTableName"] = "public.tbl_lotes";
$proto96["m_expr"]=$obj;
$proto96["m_alias"] = "";
$obj = new SQLFieldListItem($proto96);

$proto0["m_fieldlist"][]=$obj;
						$proto98=array();
			$obj = new SQLField(array(
	"m_strName" => "Transporte Coletivo",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto98["m_sql"] = "\"Transporte Coletivo\"";
$proto98["m_srcTableName"] = "public.tbl_lotes";
$proto98["m_expr"]=$obj;
$proto98["m_alias"] = "";
$obj = new SQLFieldListItem($proto98);

$proto0["m_fieldlist"][]=$obj;
						$proto100=array();
			$obj = new SQLField(array(
	"m_strName" => "Pavimentacao",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto100["m_sql"] = "\"Pavimentacao\"";
$proto100["m_srcTableName"] = "public.tbl_lotes";
$proto100["m_expr"]=$obj;
$proto100["m_alias"] = "";
$obj = new SQLFieldListItem($proto100);

$proto0["m_fieldlist"][]=$obj;
						$proto102=array();
			$obj = new SQLField(array(
	"m_strName" => "Tipo Imove",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto102["m_sql"] = "\"Tipo Imove\"";
$proto102["m_srcTableName"] = "public.tbl_lotes";
$proto102["m_expr"]=$obj;
$proto102["m_alias"] = "";
$obj = new SQLFieldListItem($proto102);

$proto0["m_fieldlist"][]=$obj;
						$proto104=array();
			$obj = new SQLField(array(
	"m_strName" => "Isento",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto104["m_sql"] = "\"Isento\"";
$proto104["m_srcTableName"] = "public.tbl_lotes";
$proto104["m_expr"]=$obj;
$proto104["m_alias"] = "";
$obj = new SQLFieldListItem($proto104);

$proto0["m_fieldlist"][]=$obj;
						$proto106=array();
			$obj = new SQLField(array(
	"m_strName" => "Quantidade de BPs Analisados",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto106["m_sql"] = "\"Quantidade de BPs Analisados\"";
$proto106["m_srcTableName"] = "public.tbl_lotes";
$proto106["m_expr"]=$obj;
$proto106["m_alias"] = "";
$obj = new SQLFieldListItem($proto106);

$proto0["m_fieldlist"][]=$obj;
						$proto108=array();
			$obj = new SQLField(array(
	"m_strName" => "Valor Venal do Terreno",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto108["m_sql"] = "\"Valor Venal do Terreno\"";
$proto108["m_srcTableName"] = "public.tbl_lotes";
$proto108["m_expr"]=$obj;
$proto108["m_alias"] = "";
$obj = new SQLFieldListItem($proto108);

$proto0["m_fieldlist"][]=$obj;
						$proto110=array();
			$obj = new SQLField(array(
	"m_strName" => "Valor Venal da Edificacao",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto110["m_sql"] = "\"Valor Venal da Edificacao\"";
$proto110["m_srcTableName"] = "public.tbl_lotes";
$proto110["m_expr"]=$obj;
$proto110["m_alias"] = "";
$obj = new SQLFieldListItem($proto110);

$proto0["m_fieldlist"][]=$obj;
						$proto112=array();
			$obj = new SQLField(array(
	"m_strName" => "Valor Venal do Imovel",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto112["m_sql"] = "\"Valor Venal do Imovel\"";
$proto112["m_srcTableName"] = "public.tbl_lotes";
$proto112["m_expr"]=$obj;
$proto112["m_alias"] = "";
$obj = new SQLFieldListItem($proto112);

$proto0["m_fieldlist"][]=$obj;
						$proto114=array();
			$obj = new SQLField(array(
	"m_strName" => "Ultima Aliquota IPTU",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto114["m_sql"] = "\"Ultima Aliquota IPTU\"";
$proto114["m_srcTableName"] = "public.tbl_lotes";
$proto114["m_expr"]=$obj;
$proto114["m_alias"] = "";
$obj = new SQLFieldListItem($proto114);

$proto0["m_fieldlist"][]=$obj;
						$proto116=array();
			$obj = new SQLField(array(
	"m_strName" => "Valor do IPTU",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto116["m_sql"] = "\"Valor do IPTU\"";
$proto116["m_srcTableName"] = "public.tbl_lotes";
$proto116["m_expr"]=$obj;
$proto116["m_alias"] = "";
$obj = new SQLFieldListItem($proto116);

$proto0["m_fieldlist"][]=$obj;
						$proto118=array();
			$obj = new SQLField(array(
	"m_strName" => "Situacao de Cobranca",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto118["m_sql"] = "\"Situacao de Cobranca\"";
$proto118["m_srcTableName"] = "public.tbl_lotes";
$proto118["m_expr"]=$obj;
$proto118["m_alias"] = "";
$obj = new SQLFieldListItem($proto118);

$proto0["m_fieldlist"][]=$obj;
						$proto120=array();
			$obj = new SQLField(array(
	"m_strName" => "Codigo Loteamento",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto120["m_sql"] = "\"Codigo Loteamento\"";
$proto120["m_srcTableName"] = "public.tbl_lotes";
$proto120["m_expr"]=$obj;
$proto120["m_alias"] = "";
$obj = new SQLFieldListItem($proto120);

$proto0["m_fieldlist"][]=$obj;
						$proto122=array();
			$obj = new SQLField(array(
	"m_strName" => "Tipo Loteamento",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto122["m_sql"] = "\"Tipo Loteamento\"";
$proto122["m_srcTableName"] = "public.tbl_lotes";
$proto122["m_expr"]=$obj;
$proto122["m_alias"] = "";
$obj = new SQLFieldListItem($proto122);

$proto0["m_fieldlist"][]=$obj;
						$proto124=array();
			$obj = new SQLField(array(
	"m_strName" => "Nome Loteamento",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto124["m_sql"] = "\"Nome Loteamento\"";
$proto124["m_srcTableName"] = "public.tbl_lotes";
$proto124["m_expr"]=$obj;
$proto124["m_alias"] = "";
$obj = new SQLFieldListItem($proto124);

$proto0["m_fieldlist"][]=$obj;
						$proto126=array();
			$obj = new SQLField(array(
	"m_strName" => "ObsArq01",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto126["m_sql"] = "\"ObsArq01\"";
$proto126["m_srcTableName"] = "public.tbl_lotes";
$proto126["m_expr"]=$obj;
$proto126["m_alias"] = "";
$obj = new SQLFieldListItem($proto126);

$proto0["m_fieldlist"][]=$obj;
						$proto128=array();
			$obj = new SQLField(array(
	"m_strName" => "ObsArq02",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto128["m_sql"] = "\"ObsArq02\"";
$proto128["m_srcTableName"] = "public.tbl_lotes";
$proto128["m_expr"]=$obj;
$proto128["m_alias"] = "";
$obj = new SQLFieldListItem($proto128);

$proto0["m_fieldlist"][]=$obj;
						$proto130=array();
			$obj = new SQLField(array(
	"m_strName" => "ObsArq03",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto130["m_sql"] = "\"ObsArq03\"";
$proto130["m_srcTableName"] = "public.tbl_lotes";
$proto130["m_expr"]=$obj;
$proto130["m_alias"] = "";
$obj = new SQLFieldListItem($proto130);

$proto0["m_fieldlist"][]=$obj;
						$proto132=array();
			$obj = new SQLField(array(
	"m_strName" => "DestinacaoPredominante",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto132["m_sql"] = "\"DestinacaoPredominante\"";
$proto132["m_srcTableName"] = "public.tbl_lotes";
$proto132["m_expr"]=$obj;
$proto132["m_alias"] = "";
$obj = new SQLFieldListItem($proto132);

$proto0["m_fieldlist"][]=$obj;
						$proto134=array();
			$obj = new SQLField(array(
	"m_strName" => "Inscricao da Face",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto134["m_sql"] = "\"Inscricao da Face\"";
$proto134["m_srcTableName"] = "public.tbl_lotes";
$proto134["m_expr"]=$obj;
$proto134["m_alias"] = "";
$obj = new SQLFieldListItem($proto134);

$proto0["m_fieldlist"][]=$obj;
						$proto136=array();
			$obj = new SQLField(array(
	"m_strName" => "ValidaFace",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto136["m_sql"] = "\"ValidaFace\"";
$proto136["m_srcTableName"] = "public.tbl_lotes";
$proto136["m_expr"]=$obj;
$proto136["m_alias"] = "";
$obj = new SQLFieldListItem($proto136);

$proto0["m_fieldlist"][]=$obj;
						$proto138=array();
			$obj = new SQLField(array(
	"m_strName" => "relacao",
	"m_strTable" => "public.tbl_lotes",
	"m_srcTableName" => "public.tbl_lotes"
));

$proto138["m_sql"] = "relacao";
$proto138["m_srcTableName"] = "public.tbl_lotes";
$proto138["m_expr"]=$obj;
$proto138["m_alias"] = "";
$obj = new SQLFieldListItem($proto138);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto140=array();
$proto140["m_link"] = "SQLL_MAIN";
			$proto141=array();
$proto141["m_strName"] = "public.tbl_lotes";
$proto141["m_srcTableName"] = "public.tbl_lotes";
$proto141["m_columns"] = array();
$proto141["m_columns"][] = "inscricao";
$proto141["m_columns"][] = "exercicio";
$proto141["m_columns"][] = "ICTerreno";
$proto141["m_columns"][] = "chave";
$proto141["m_columns"][] = "Codigo Calculo";
$proto141["m_columns"][] = "Simulacao";
$proto141["m_columns"][] = "Data Calculo";
$proto141["m_columns"][] = "Data Referencia Imovel";
$proto141["m_columns"][] = "Data Fatores IPTU";
$proto141["m_columns"][] = "Data Fatores Calculo";
$proto141["m_columns"][] = "Situacao Calculo IPTU";
$proto141["m_columns"][] = "Reemissao";
$proto141["m_columns"][] = "zh";
$proto141["m_columns"][] = "cod_log";
$proto141["m_columns"][] = "Tipo de Logradouro";
$proto141["m_columns"][] = "Logradouro";
$proto141["m_columns"][] = "Numero do Imovel";
$proto141["m_columns"][] = "Complemento";
$proto141["m_columns"][] = "Tipo de Bairro";
$proto141["m_columns"][] = "Bairro";
$proto141["m_columns"][] = "CEP";
$proto141["m_columns"][] = "Proprietario Principal";
$proto141["m_columns"][] = "Mensagem";
$proto141["m_columns"][] = "Pedologia";
$proto141["m_columns"][] = "Topografia";
$proto141["m_columns"][] = "Situacao";
$proto141["m_columns"][] = "Fped";
$proto141["m_columns"][] = "Ftop";
$proto141["m_columns"][] = "Fsit";
$proto141["m_columns"][] = "Testada Referencia";
$proto141["m_columns"][] = "VBU";
$proto141["m_columns"][] = "Fator Testada";
$proto141["m_columns"][] = "Fator Gleba";
$proto141["m_columns"][] = "Profundidade Maxima";
$proto141["m_columns"][] = "Profundidade Minima";
$proto141["m_columns"][] = "Fator Profundidade";
$proto141["m_columns"][] = "Fator Ponderacao";
$proto141["m_columns"][] = "Testada Principal";
$proto141["m_columns"][] = "Area do Terreno";
$proto141["m_columns"][] = "Fracao Ideal";
$proto141["m_columns"][] = "Area Construida";
$proto141["m_columns"][] = "Profundidade";
$proto141["m_columns"][] = "Tipo de Profundidade";
$proto141["m_columns"][] = "SP";
$proto141["m_columns"][] = "RP";
$proto141["m_columns"][] = "AP";
$proto141["m_columns"][] = "Transporte Coletivo";
$proto141["m_columns"][] = "Pavimentacao";
$proto141["m_columns"][] = "Tipo Imove";
$proto141["m_columns"][] = "Isento";
$proto141["m_columns"][] = "Quantidade de BPs Analisados";
$proto141["m_columns"][] = "Valor Venal do Terreno";
$proto141["m_columns"][] = "Valor Venal da Edificacao";
$proto141["m_columns"][] = "Valor Venal do Imovel";
$proto141["m_columns"][] = "Ultima Aliquota IPTU";
$proto141["m_columns"][] = "Valor do IPTU";
$proto141["m_columns"][] = "Situacao de Cobranca";
$proto141["m_columns"][] = "Codigo Loteamento";
$proto141["m_columns"][] = "Tipo Loteamento";
$proto141["m_columns"][] = "Nome Loteamento";
$proto141["m_columns"][] = "ObsArq01";
$proto141["m_columns"][] = "ObsArq02";
$proto141["m_columns"][] = "ObsArq03";
$proto141["m_columns"][] = "DestinacaoPredominante";
$proto141["m_columns"][] = "Inscricao da Face";
$proto141["m_columns"][] = "ValidaFace";
$proto141["m_columns"][] = "relacao";
$obj = new SQLTable($proto141);

$proto140["m_table"] = $obj;
$proto140["m_sql"] = "\"public\".tbl_lotes";
$proto140["m_alias"] = "";
$proto140["m_srcTableName"] = "public.tbl_lotes";
$proto142=array();
$proto142["m_sql"] = "";
$proto142["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto142["m_column"]=$obj;
$proto142["m_contained"] = array();
$proto142["m_strCase"] = "";
$proto142["m_havingmode"] = false;
$proto142["m_inBrackets"] = false;
$proto142["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto142);

$proto140["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto140);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.tbl_lotes";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_public_tbl_lotes = createSqlQuery_public_tbl_lotes();


	
		;

																																																																			

$tdatapublic_tbl_lotes[".sqlquery"] = $queryData_public_tbl_lotes;

$tableEvents["public.tbl_lotes"] = new eventsBase;
$tdatapublic_tbl_lotes[".hasEvents"] = false;

?>