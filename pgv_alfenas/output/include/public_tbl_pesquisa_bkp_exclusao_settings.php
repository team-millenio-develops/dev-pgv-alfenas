<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_tbl_pesquisa_bkp_exclusao = array();
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".truncateText"] = true;
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".NumberOfChars"] = 80;
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".ShortName"] = "public_tbl_pesquisa_bkp_exclusao";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".OwnerID"] = "";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".OriginalTable"] = "public.tbl_pesquisa_bkp_exclusao";

//	field labels
$fieldLabelspublic_tbl_pesquisa_bkp_exclusao = array();
$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao = array();
$pageTitlespublic_tbl_pesquisa_bkp_exclusao = array();
$placeHolderspublic_tbl_pesquisa_bkp_exclusao = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"] = array();
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"] = array();
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"] = array();
	$pageTitlespublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"] = array();
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["pesquisa"] = "Pesquisa";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["pesquisa"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["pesquisa"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["data"] = "Data";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["data"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["data"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["fotos"] = "Fotos";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["fotos"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["fotos"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["codlog"] = "Codlog";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["codlog"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["codlog"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["endereco_imovel"] = "Endereco Imovel";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["endereco_imovel"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["endereco_imovel"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["numero"] = "Numero";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["numero"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["numero"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["complemento"] = "Complemento";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["complemento"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["complemento"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["bairro_loteamento"] = "Bairro Loteamento";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["bairro_loteamento"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["bairro_loteamento"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["preco"] = "Preco";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["preco"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["preco"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["condicoes_pagamento"] = "Condicoes Pagamento";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["condicoes_pagamento"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["condicoes_pagamento"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["documentacao"] = "Documentacao";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["documentacao"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["documentacao"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["modalidade"] = "Modalidade";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["modalidade"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["modalidade"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["data_modalidade"] = "Data Modalidade";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["data_modalidade"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["data_modalidade"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["fonte"] = "Fonte";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["fonte"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["fonte"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["nome_fonte"] = "Nome Fonte";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["nome_fonte"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["nome_fonte"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["telefone_fonte"] = "Telefone Fonte";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["telefone_fonte"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["telefone_fonte"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["ic_imobiliaria"] = "Ic Imobiliaria";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["ic_imobiliaria"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["ic_imobiliaria"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["zona_homogenea"] = "Zona Homogenea";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["zona_homogenea"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["zona_homogenea"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["testada"] = "Testada";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["testada"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["testada"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["area_terreno"] = "Area Terreno";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["area_terreno"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["area_terreno"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["area_iptu"] = "Area Iptu";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["area_iptu"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["area_iptu"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["area_estimada"] = "Area Estimada";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["area_estimada"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["area_estimada"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["area_atualizacao"] = "Area Atualizacao";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["area_atualizacao"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["area_atualizacao"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["forma"] = "Forma";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["forma"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["forma"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["topografia"] = "Topografia";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["topografia"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["topografia"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["posicao"] = "Posicao";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["posicao"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["posicao"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["benfeitoria"] = "Benfeitoria";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["benfeitoria"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["benfeitoria"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["classe"] = "Classe";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["classe"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["classe"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["tipologia_construtiva"] = "Tipologia Construtiva";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["tipologia_construtiva"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["tipologia_construtiva"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["padrao_construtivo"] = "Padrao Construtivo";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["padrao_construtivo"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["padrao_construtivo"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["conservacao"] = "Conservacao";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["conservacao"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["conservacao"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["estrutura"] = "Estrutura";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["estrutura"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["estrutura"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["dependencia"] = "Dependencia";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["dependencia"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["dependencia"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["elevador"] = "Elevador";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["elevador"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["elevador"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["vao"] = "Vao";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["vao"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["vao"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["cobertura"] = "Cobertura";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["cobertura"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["cobertura"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["condominio"] = "Condominio";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["condominio"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["condominio"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["idade_edificacao"] = "Idade Edificacao";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["idade_edificacao"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["idade_edificacao"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["qt_dormitorio"] = "Qt Dormitorio";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["qt_dormitorio"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["qt_dormitorio"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["qt_banheiro"] = "Qt Banheiro";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["qt_banheiro"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["qt_banheiro"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["observacao"] = "Observacao";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["observacao"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["observacao"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["cadastrador"] = "Cadastrador";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["cadastrador"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["cadastrador"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["data_cadastro"] = "Data Cadastro";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["data_cadastro"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["data_cadastro"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["ident"] = "Ident";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["ident"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["ident"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["agua"] = "Agua";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["agua"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["agua"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["esgoto"] = "Esgoto";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["esgoto"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["esgoto"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["energia"] = "Energia";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["energia"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["energia"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["iluminacao"] = "Iluminacao";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["iluminacao"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["iluminacao"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["guia_sargeta"] = "Guia Sargeta";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["guia_sargeta"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["guia_sargeta"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["pavimento"] = "Pavimento";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["pavimento"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["pavimento"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["drenagem"] = "Drenagem";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["drenagem"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["drenagem"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["telefone"] = "Telefone";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["telefone"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["telefone"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["coleta"] = "Coleta";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["coleta"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["coleta"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["transporte"] = "Transporte";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["transporte"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["transporte"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["foto_print"] = "Foto Print";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["foto_print"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["foto_print"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["ocupacao"] = "Ocupacao";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["ocupacao"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["ocupacao"] = "";
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["concluido"] = "Concluido";
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["concluido"] = "";
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]["concluido"] = "";
	if (count($fieldToolTipspublic_tbl_pesquisa_bkp_exclusao["Portuguese(Brazil)"]))
		$tdatapublic_tbl_pesquisa_bkp_exclusao[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_tbl_pesquisa_bkp_exclusao[""] = array();
	$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao[""] = array();
	$placeHolderspublic_tbl_pesquisa_bkp_exclusao[""] = array();
	$pageTitlespublic_tbl_pesquisa_bkp_exclusao[""] = array();
	if (count($fieldToolTipspublic_tbl_pesquisa_bkp_exclusao[""]))
		$tdatapublic_tbl_pesquisa_bkp_exclusao[".isUseToolTips"] = true;
}


	$tdatapublic_tbl_pesquisa_bkp_exclusao[".NCSearch"] = true;



$tdatapublic_tbl_pesquisa_bkp_exclusao[".shortTableName"] = "public_tbl_pesquisa_bkp_exclusao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".nSecOptions"] = 0;
$tdatapublic_tbl_pesquisa_bkp_exclusao[".recsPerRowPrint"] = 1;
$tdatapublic_tbl_pesquisa_bkp_exclusao[".mainTableOwnerID"] = "";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".moveNext"] = 1;
$tdatapublic_tbl_pesquisa_bkp_exclusao[".entityType"] = 0;

$tdatapublic_tbl_pesquisa_bkp_exclusao[".strOriginalTableName"] = "public.tbl_pesquisa_bkp_exclusao";

	



$tdatapublic_tbl_pesquisa_bkp_exclusao[".showAddInPopup"] = false;

$tdatapublic_tbl_pesquisa_bkp_exclusao[".showEditInPopup"] = false;

$tdatapublic_tbl_pesquisa_bkp_exclusao[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapublic_tbl_pesquisa_bkp_exclusao[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_tbl_pesquisa_bkp_exclusao[".fieldsForRegister"] = array();

$tdatapublic_tbl_pesquisa_bkp_exclusao[".listAjax"] = false;

	$tdatapublic_tbl_pesquisa_bkp_exclusao[".audit"] = false;

	$tdatapublic_tbl_pesquisa_bkp_exclusao[".locking"] = false;



$tdatapublic_tbl_pesquisa_bkp_exclusao[".list"] = true;



$tdatapublic_tbl_pesquisa_bkp_exclusao[".reorderRecordsByHeader"] = true;


$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFormatting"] = 2;
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportDelimiter"] = ",";
		

$tdatapublic_tbl_pesquisa_bkp_exclusao[".import"] = true;

$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportTo"] = true;

$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFriendly"] = true;


$tdatapublic_tbl_pesquisa_bkp_exclusao[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatapublic_tbl_pesquisa_bkp_exclusao[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatapublic_tbl_pesquisa_bkp_exclusao[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatapublic_tbl_pesquisa_bkp_exclusao[".searchSaving"] = false;
//

$tdatapublic_tbl_pesquisa_bkp_exclusao[".showSearchPanel"] = true;
		$tdatapublic_tbl_pesquisa_bkp_exclusao[".flexibleSearch"] = true;

$tdatapublic_tbl_pesquisa_bkp_exclusao[".isUseAjaxSuggest"] = true;






$tdatapublic_tbl_pesquisa_bkp_exclusao[".ajaxCodeSnippetAdded"] = false;

$tdatapublic_tbl_pesquisa_bkp_exclusao[".buttonsAdded"] = false;

$tdatapublic_tbl_pesquisa_bkp_exclusao[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapublic_tbl_pesquisa_bkp_exclusao[".isUseTimeForSearch"] = false;





$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"] = array();
$tdatapublic_tbl_pesquisa_bkp_exclusao[".filterFields"] = array();
$tdatapublic_tbl_pesquisa_bkp_exclusao[".requiredSearchFields"] = array();

$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "pesquisa";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "data";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "fotos";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "codlog";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "endereco_imovel";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "numero";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "complemento";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "bairro_loteamento";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "preco";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "condicoes_pagamento";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "documentacao";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "modalidade";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "data_modalidade";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "fonte";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "nome_fonte";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "telefone_fonte";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "ic_imobiliaria";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "zona_homogenea";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "testada";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "area_terreno";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "area_iptu";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "area_estimada";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "area_atualizacao";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "forma";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "topografia";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "posicao";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "benfeitoria";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "classe";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "tipologia_construtiva";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "padrao_construtivo";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "conservacao";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "estrutura";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "dependencia";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "elevador";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "vao";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "cobertura";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "condominio";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "idade_edificacao";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "qt_dormitorio";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "qt_banheiro";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "observacao";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "cadastrador";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "data_cadastro";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "ident";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "agua";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "esgoto";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "energia";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "iluminacao";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "guia_sargeta";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "pavimento";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "drenagem";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "telefone";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "coleta";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "transporte";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "foto_print";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "ocupacao";
	$tdatapublic_tbl_pesquisa_bkp_exclusao[".allSearchFields"][] = "concluido";
	

$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"] = array();
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "pesquisa";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "data";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "fotos";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "codlog";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "endereco_imovel";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "numero";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "complemento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "bairro_loteamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "preco";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "condicoes_pagamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "documentacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "data_modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "nome_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "telefone_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "ic_imobiliaria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "zona_homogenea";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "testada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "area_terreno";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "area_iptu";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "area_estimada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "area_atualizacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "forma";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "topografia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "posicao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "benfeitoria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "classe";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "tipologia_construtiva";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "padrao_construtivo";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "conservacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "estrutura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "dependencia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "elevador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "vao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "cobertura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "condominio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "idade_edificacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "qt_dormitorio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "qt_banheiro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "observacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "cadastrador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "data_cadastro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "ident";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "agua";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "esgoto";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "energia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "iluminacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "guia_sargeta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "pavimento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "drenagem";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "telefone";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "coleta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "transporte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "foto_print";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "ocupacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".googleLikeFields"][] = "concluido";


$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"] = array();
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "pesquisa";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "data";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "fotos";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "codlog";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "endereco_imovel";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "numero";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "complemento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "bairro_loteamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "preco";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "condicoes_pagamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "documentacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "data_modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "nome_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "telefone_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "ic_imobiliaria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "zona_homogenea";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "testada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "area_terreno";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "area_iptu";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "area_estimada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "area_atualizacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "forma";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "topografia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "posicao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "benfeitoria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "classe";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "tipologia_construtiva";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "padrao_construtivo";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "conservacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "estrutura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "dependencia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "elevador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "vao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "cobertura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "condominio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "idade_edificacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "qt_dormitorio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "qt_banheiro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "observacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "cadastrador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "data_cadastro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "ident";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "agua";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "esgoto";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "energia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "iluminacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "guia_sargeta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "pavimento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "drenagem";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "telefone";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "coleta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "transporte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "foto_print";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "ocupacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".advSearchFields"][] = "concluido";

$tdatapublic_tbl_pesquisa_bkp_exclusao[".tableType"] = "list";

$tdatapublic_tbl_pesquisa_bkp_exclusao[".printerPageOrientation"] = 0;
$tdatapublic_tbl_pesquisa_bkp_exclusao[".nPrinterPageScale"] = 100;

$tdatapublic_tbl_pesquisa_bkp_exclusao[".nPrinterSplitRecords"] = 40;

$tdatapublic_tbl_pesquisa_bkp_exclusao[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_tbl_pesquisa_bkp_exclusao[".geocodingEnabled"] = false;





$tdatapublic_tbl_pesquisa_bkp_exclusao[".listGridLayout"] = 3;

$tdatapublic_tbl_pesquisa_bkp_exclusao[".isDisplayLoading"] = true;




// view page pdf

// print page pdf


$tdatapublic_tbl_pesquisa_bkp_exclusao[".pageSize"] = 20;

$tdatapublic_tbl_pesquisa_bkp_exclusao[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_tbl_pesquisa_bkp_exclusao[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_tbl_pesquisa_bkp_exclusao[".orderindexes"] = array();

$tdatapublic_tbl_pesquisa_bkp_exclusao[".sqlHead"] = "SELECT pesquisa,  	\"data\",  	fotos,  	codlog,  	endereco_imovel,  	numero,  	complemento,  	bairro_loteamento,  	preco,  	condicoes_pagamento,  	documentacao,  	modalidade,  	data_modalidade,  	fonte,  	nome_fonte,  	telefone_fonte,  	ic_imobiliaria,  	zona_homogenea,  	testada,  	area_terreno,  	area_iptu,  	area_estimada,  	area_atualizacao,  	forma,  	topografia,  	posicao,  	benfeitoria,  	classe,  	tipologia_construtiva,  	padrao_construtivo,  	conservacao,  	estrutura,  	dependencia,  	elevador,  	vao,  	cobertura,  	condominio,  	idade_edificacao,  	qt_dormitorio,  	qt_banheiro,  	observacao,  	cadastrador,  	data_cadastro,  	ident,  	agua,  	esgoto,  	energia,  	iluminacao,  	guia_sargeta,  	pavimento,  	drenagem,  	telefone,  	coleta,  	transporte,  	foto_print,  	ocupacao,  	concluido";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".sqlFrom"] = "FROM \"public\".tbl_pesquisa_bkp_exclusao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".sqlWhereExpr"] = "";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_tbl_pesquisa_bkp_exclusao[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_tbl_pesquisa_bkp_exclusao[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_tbl_pesquisa_bkp_exclusao[".highlightSearchResults"] = true;

$tableKeyspublic_tbl_pesquisa_bkp_exclusao = array();
$tdatapublic_tbl_pesquisa_bkp_exclusao[".Keys"] = $tableKeyspublic_tbl_pesquisa_bkp_exclusao;

$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"] = array();
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "pesquisa";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "data";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "fotos";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "codlog";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "endereco_imovel";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "numero";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "complemento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "bairro_loteamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "preco";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "condicoes_pagamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "documentacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "data_modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "nome_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "telefone_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "ic_imobiliaria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "zona_homogenea";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "testada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "area_terreno";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "area_iptu";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "area_estimada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "area_atualizacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "forma";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "topografia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "posicao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "benfeitoria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "classe";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "tipologia_construtiva";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "padrao_construtivo";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "conservacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "estrutura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "dependencia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "elevador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "vao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "cobertura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "condominio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "idade_edificacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "qt_dormitorio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "qt_banheiro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "observacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "cadastrador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "data_cadastro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "ident";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "agua";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "esgoto";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "energia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "iluminacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "guia_sargeta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "pavimento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "drenagem";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "telefone";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "coleta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "transporte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "foto_print";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "ocupacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".listFields"][] = "concluido";

$tdatapublic_tbl_pesquisa_bkp_exclusao[".hideMobileList"] = array();


$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"] = array();
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "pesquisa";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "data";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "fotos";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "codlog";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "endereco_imovel";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "numero";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "complemento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "bairro_loteamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "preco";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "condicoes_pagamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "documentacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "data_modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "nome_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "telefone_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "ic_imobiliaria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "zona_homogenea";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "testada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "area_terreno";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "area_iptu";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "area_estimada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "area_atualizacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "forma";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "topografia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "posicao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "benfeitoria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "classe";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "tipologia_construtiva";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "padrao_construtivo";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "conservacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "estrutura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "dependencia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "elevador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "vao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "cobertura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "condominio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "idade_edificacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "qt_dormitorio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "qt_banheiro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "observacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "cadastrador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "data_cadastro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "ident";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "agua";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "esgoto";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "energia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "iluminacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "guia_sargeta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "pavimento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "drenagem";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "telefone";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "coleta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "transporte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "foto_print";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "ocupacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".viewFields"][] = "concluido";

$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"] = array();
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "pesquisa";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "data";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "fotos";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "codlog";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "endereco_imovel";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "numero";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "complemento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "bairro_loteamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "preco";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "condicoes_pagamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "documentacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "data_modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "nome_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "telefone_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "ic_imobiliaria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "zona_homogenea";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "testada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "area_terreno";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "area_iptu";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "area_estimada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "area_atualizacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "forma";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "topografia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "posicao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "benfeitoria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "classe";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "tipologia_construtiva";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "padrao_construtivo";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "conservacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "estrutura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "dependencia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "elevador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "vao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "cobertura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "condominio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "idade_edificacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "qt_dormitorio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "qt_banheiro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "observacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "cadastrador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "data_cadastro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "agua";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "esgoto";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "energia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "iluminacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "guia_sargeta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "pavimento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "drenagem";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "telefone";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "coleta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "transporte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "foto_print";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "ocupacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".addFields"][] = "concluido";

$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"] = array();
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "pesquisa";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "data";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "fotos";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "codlog";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "endereco_imovel";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "numero";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "complemento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "bairro_loteamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "preco";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "condicoes_pagamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "documentacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "data_modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "nome_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "telefone_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "ic_imobiliaria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "zona_homogenea";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "testada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "area_terreno";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "area_iptu";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "area_estimada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "area_atualizacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "forma";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "topografia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "posicao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "benfeitoria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "classe";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "tipologia_construtiva";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "padrao_construtivo";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "conservacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "estrutura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "dependencia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "elevador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "vao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "cobertura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "condominio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "idade_edificacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "qt_dormitorio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "qt_banheiro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "observacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "cadastrador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "data_cadastro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "ident";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "agua";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "esgoto";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "energia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "iluminacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "guia_sargeta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "pavimento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "drenagem";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "telefone";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "coleta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "transporte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "foto_print";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "ocupacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".masterListFields"][] = "concluido";

$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"] = array();
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "pesquisa";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "data";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "fotos";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "codlog";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "endereco_imovel";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "numero";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "complemento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "bairro_loteamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "preco";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "condicoes_pagamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "documentacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "data_modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "nome_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "telefone_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "ic_imobiliaria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "zona_homogenea";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "testada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "area_terreno";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "area_iptu";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "area_estimada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "area_atualizacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "forma";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "topografia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "posicao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "benfeitoria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "classe";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "tipologia_construtiva";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "padrao_construtivo";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "conservacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "estrutura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "dependencia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "elevador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "vao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "cobertura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "condominio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "idade_edificacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "qt_dormitorio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "qt_banheiro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "observacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "cadastrador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "data_cadastro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "agua";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "esgoto";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "energia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "iluminacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "guia_sargeta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "pavimento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "drenagem";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "telefone";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "coleta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "transporte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "foto_print";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "ocupacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineAddFields"][] = "concluido";

$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"] = array();
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "pesquisa";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "data";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "fotos";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "codlog";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "endereco_imovel";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "numero";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "complemento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "bairro_loteamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "preco";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "condicoes_pagamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "documentacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "data_modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "nome_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "telefone_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "ic_imobiliaria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "zona_homogenea";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "testada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "area_terreno";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "area_iptu";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "area_estimada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "area_atualizacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "forma";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "topografia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "posicao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "benfeitoria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "classe";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "tipologia_construtiva";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "padrao_construtivo";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "conservacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "estrutura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "dependencia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "elevador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "vao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "cobertura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "condominio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "idade_edificacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "qt_dormitorio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "qt_banheiro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "observacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "cadastrador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "data_cadastro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "agua";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "esgoto";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "energia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "iluminacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "guia_sargeta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "pavimento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "drenagem";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "telefone";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "coleta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "transporte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "foto_print";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "ocupacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".editFields"][] = "concluido";

$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"] = array();
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "pesquisa";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "data";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "fotos";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "codlog";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "endereco_imovel";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "numero";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "complemento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "bairro_loteamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "preco";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "condicoes_pagamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "documentacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "data_modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "nome_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "telefone_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "ic_imobiliaria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "zona_homogenea";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "testada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "area_terreno";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "area_iptu";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "area_estimada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "area_atualizacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "forma";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "topografia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "posicao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "benfeitoria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "classe";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "tipologia_construtiva";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "padrao_construtivo";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "conservacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "estrutura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "dependencia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "elevador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "vao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "cobertura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "condominio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "idade_edificacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "qt_dormitorio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "qt_banheiro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "observacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "cadastrador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "data_cadastro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "agua";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "esgoto";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "energia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "iluminacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "guia_sargeta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "pavimento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "drenagem";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "telefone";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "coleta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "transporte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "foto_print";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "ocupacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".inlineEditFields"][] = "concluido";

$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"] = array();
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "pesquisa";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "data";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "fotos";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "codlog";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "endereco_imovel";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "numero";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "complemento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "bairro_loteamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "preco";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "condicoes_pagamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "documentacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "data_modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "nome_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "telefone_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "ic_imobiliaria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "zona_homogenea";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "testada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "area_terreno";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "area_iptu";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "area_estimada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "area_atualizacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "forma";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "topografia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "posicao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "benfeitoria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "classe";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "tipologia_construtiva";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "padrao_construtivo";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "conservacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "estrutura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "dependencia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "elevador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "vao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "cobertura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "condominio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "idade_edificacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "qt_dormitorio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "qt_banheiro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "observacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "cadastrador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "data_cadastro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "agua";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "esgoto";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "energia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "iluminacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "guia_sargeta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "pavimento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "drenagem";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "telefone";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "coleta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "transporte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "foto_print";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "ocupacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".updateSelectedFields"][] = "concluido";


$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"] = array();
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "pesquisa";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "data";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "fotos";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "codlog";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "endereco_imovel";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "numero";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "complemento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "bairro_loteamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "preco";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "condicoes_pagamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "documentacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "data_modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "nome_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "telefone_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "ic_imobiliaria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "zona_homogenea";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "testada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "area_terreno";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "area_iptu";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "area_estimada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "area_atualizacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "forma";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "topografia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "posicao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "benfeitoria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "classe";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "tipologia_construtiva";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "padrao_construtivo";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "conservacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "estrutura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "dependencia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "elevador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "vao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "cobertura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "condominio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "idade_edificacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "qt_dormitorio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "qt_banheiro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "observacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "cadastrador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "data_cadastro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "ident";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "agua";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "esgoto";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "energia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "iluminacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "guia_sargeta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "pavimento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "drenagem";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "telefone";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "coleta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "transporte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "foto_print";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "ocupacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".exportFields"][] = "concluido";

$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"] = array();
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "pesquisa";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "data";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "fotos";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "codlog";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "endereco_imovel";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "numero";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "complemento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "bairro_loteamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "preco";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "condicoes_pagamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "documentacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "data_modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "nome_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "telefone_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "ic_imobiliaria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "zona_homogenea";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "testada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "area_terreno";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "area_iptu";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "area_estimada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "area_atualizacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "forma";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "topografia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "posicao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "benfeitoria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "classe";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "tipologia_construtiva";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "padrao_construtivo";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "conservacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "estrutura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "dependencia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "elevador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "vao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "cobertura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "condominio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "idade_edificacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "qt_dormitorio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "qt_banheiro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "observacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "cadastrador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "data_cadastro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "ident";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "agua";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "esgoto";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "energia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "iluminacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "guia_sargeta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "pavimento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "drenagem";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "telefone";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "coleta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "transporte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "foto_print";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "ocupacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".importFields"][] = "concluido";

$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"] = array();
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "pesquisa";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "data";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "fotos";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "codlog";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "endereco_imovel";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "numero";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "complemento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "bairro_loteamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "preco";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "condicoes_pagamento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "documentacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "data_modalidade";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "nome_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "telefone_fonte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "ic_imobiliaria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "zona_homogenea";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "testada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "area_terreno";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "area_iptu";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "area_estimada";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "area_atualizacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "forma";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "topografia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "posicao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "benfeitoria";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "classe";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "tipologia_construtiva";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "padrao_construtivo";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "conservacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "estrutura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "dependencia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "elevador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "vao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "cobertura";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "condominio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "idade_edificacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "qt_dormitorio";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "qt_banheiro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "observacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "cadastrador";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "data_cadastro";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "ident";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "agua";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "esgoto";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "energia";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "iluminacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "guia_sargeta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "pavimento";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "drenagem";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "telefone";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "coleta";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "transporte";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "foto_print";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "ocupacao";
$tdatapublic_tbl_pesquisa_bkp_exclusao[".printFields"][] = "concluido";


//	pesquisa
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "pesquisa";
	$fdata["GoodName"] = "pesquisa";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","pesquisa");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "pesquisa";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "pesquisa";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["pesquisa"] = $fdata;
//	data
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "data";
	$fdata["GoodName"] = "data";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","data");
	$fdata["FieldType"] = 7;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "data";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"data\"";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["data"] = $fdata;
//	fotos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "fotos";
	$fdata["GoodName"] = "fotos";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","fotos");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "fotos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fotos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=5000";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["fotos"] = $fdata;
//	codlog
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "codlog";
	$fdata["GoodName"] = "codlog";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","codlog");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "codlog";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "codlog";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["codlog"] = $fdata;
//	endereco_imovel
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "endereco_imovel";
	$fdata["GoodName"] = "endereco_imovel";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","endereco_imovel");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "endereco_imovel";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "endereco_imovel";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=200";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["endereco_imovel"] = $fdata;
//	numero
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "numero";
	$fdata["GoodName"] = "numero";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","numero");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "numero";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "numero";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["numero"] = $fdata;
//	complemento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "complemento";
	$fdata["GoodName"] = "complemento";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","complemento");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "complemento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "complemento";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["complemento"] = $fdata;
//	bairro_loteamento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "bairro_loteamento";
	$fdata["GoodName"] = "bairro_loteamento";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","bairro_loteamento");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "bairro_loteamento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "bairro_loteamento";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["bairro_loteamento"] = $fdata;
//	preco
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "preco";
	$fdata["GoodName"] = "preco";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","preco");
	$fdata["FieldType"] = 5;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "preco";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "preco";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["preco"] = $fdata;
//	condicoes_pagamento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "condicoes_pagamento";
	$fdata["GoodName"] = "condicoes_pagamento";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","condicoes_pagamento");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "condicoes_pagamento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "condicoes_pagamento";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["condicoes_pagamento"] = $fdata;
//	documentacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "documentacao";
	$fdata["GoodName"] = "documentacao";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","documentacao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "documentacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "documentacao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["documentacao"] = $fdata;
//	modalidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "modalidade";
	$fdata["GoodName"] = "modalidade";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","modalidade");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "modalidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "modalidade";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["modalidade"] = $fdata;
//	data_modalidade
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "data_modalidade";
	$fdata["GoodName"] = "data_modalidade";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","data_modalidade");
	$fdata["FieldType"] = 7;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "data_modalidade";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "data_modalidade";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["data_modalidade"] = $fdata;
//	fonte
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "fonte";
	$fdata["GoodName"] = "fonte";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","fonte");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "fonte";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fonte";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["fonte"] = $fdata;
//	nome_fonte
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "nome_fonte";
	$fdata["GoodName"] = "nome_fonte";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","nome_fonte");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "nome_fonte";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nome_fonte";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["nome_fonte"] = $fdata;
//	telefone_fonte
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "telefone_fonte";
	$fdata["GoodName"] = "telefone_fonte";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","telefone_fonte");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "telefone_fonte";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "telefone_fonte";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["telefone_fonte"] = $fdata;
//	ic_imobiliaria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "ic_imobiliaria";
	$fdata["GoodName"] = "ic_imobiliaria";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","ic_imobiliaria");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ic_imobiliaria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ic_imobiliaria";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["ic_imobiliaria"] = $fdata;
//	zona_homogenea
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 18;
	$fdata["strName"] = "zona_homogenea";
	$fdata["GoodName"] = "zona_homogenea";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","zona_homogenea");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "zona_homogenea";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "zona_homogenea";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["zona_homogenea"] = $fdata;
//	testada
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 19;
	$fdata["strName"] = "testada";
	$fdata["GoodName"] = "testada";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","testada");
	$fdata["FieldType"] = 5;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "testada";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "testada";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["testada"] = $fdata;
//	area_terreno
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 20;
	$fdata["strName"] = "area_terreno";
	$fdata["GoodName"] = "area_terreno";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","area_terreno");
	$fdata["FieldType"] = 5;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "area_terreno";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "area_terreno";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["area_terreno"] = $fdata;
//	area_iptu
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 21;
	$fdata["strName"] = "area_iptu";
	$fdata["GoodName"] = "area_iptu";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","area_iptu");
	$fdata["FieldType"] = 5;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "area_iptu";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "area_iptu";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["area_iptu"] = $fdata;
//	area_estimada
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 22;
	$fdata["strName"] = "area_estimada";
	$fdata["GoodName"] = "area_estimada";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","area_estimada");
	$fdata["FieldType"] = 5;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "area_estimada";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "area_estimada";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["area_estimada"] = $fdata;
//	area_atualizacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 23;
	$fdata["strName"] = "area_atualizacao";
	$fdata["GoodName"] = "area_atualizacao";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","area_atualizacao");
	$fdata["FieldType"] = 5;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "area_atualizacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "area_atualizacao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["area_atualizacao"] = $fdata;
//	forma
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 24;
	$fdata["strName"] = "forma";
	$fdata["GoodName"] = "forma";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","forma");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "forma";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "forma";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["forma"] = $fdata;
//	topografia
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 25;
	$fdata["strName"] = "topografia";
	$fdata["GoodName"] = "topografia";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","topografia");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "topografia";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "topografia";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["topografia"] = $fdata;
//	posicao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 26;
	$fdata["strName"] = "posicao";
	$fdata["GoodName"] = "posicao";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","posicao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "posicao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "posicao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["posicao"] = $fdata;
//	benfeitoria
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 27;
	$fdata["strName"] = "benfeitoria";
	$fdata["GoodName"] = "benfeitoria";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","benfeitoria");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "benfeitoria";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "benfeitoria";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["benfeitoria"] = $fdata;
//	classe
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 28;
	$fdata["strName"] = "classe";
	$fdata["GoodName"] = "classe";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","classe");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "classe";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "classe";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["classe"] = $fdata;
//	tipologia_construtiva
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 29;
	$fdata["strName"] = "tipologia_construtiva";
	$fdata["GoodName"] = "tipologia_construtiva";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","tipologia_construtiva");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "tipologia_construtiva";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tipologia_construtiva";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["tipologia_construtiva"] = $fdata;
//	padrao_construtivo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 30;
	$fdata["strName"] = "padrao_construtivo";
	$fdata["GoodName"] = "padrao_construtivo";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","padrao_construtivo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "padrao_construtivo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "padrao_construtivo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["padrao_construtivo"] = $fdata;
//	conservacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 31;
	$fdata["strName"] = "conservacao";
	$fdata["GoodName"] = "conservacao";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","conservacao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "conservacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "conservacao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["conservacao"] = $fdata;
//	estrutura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 32;
	$fdata["strName"] = "estrutura";
	$fdata["GoodName"] = "estrutura";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","estrutura");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "estrutura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "estrutura";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["estrutura"] = $fdata;
//	dependencia
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 33;
	$fdata["strName"] = "dependencia";
	$fdata["GoodName"] = "dependencia";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","dependencia");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "dependencia";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "dependencia";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["dependencia"] = $fdata;
//	elevador
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 34;
	$fdata["strName"] = "elevador";
	$fdata["GoodName"] = "elevador";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","elevador");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "elevador";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "elevador";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["elevador"] = $fdata;
//	vao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 35;
	$fdata["strName"] = "vao";
	$fdata["GoodName"] = "vao";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","vao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "vao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "vao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["vao"] = $fdata;
//	cobertura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 36;
	$fdata["strName"] = "cobertura";
	$fdata["GoodName"] = "cobertura";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","cobertura");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "cobertura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cobertura";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["cobertura"] = $fdata;
//	condominio
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 37;
	$fdata["strName"] = "condominio";
	$fdata["GoodName"] = "condominio";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","condominio");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "condominio";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "condominio";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["condominio"] = $fdata;
//	idade_edificacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 38;
	$fdata["strName"] = "idade_edificacao";
	$fdata["GoodName"] = "idade_edificacao";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","idade_edificacao");
	$fdata["FieldType"] = 20;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "idade_edificacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "idade_edificacao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["idade_edificacao"] = $fdata;
//	qt_dormitorio
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 39;
	$fdata["strName"] = "qt_dormitorio";
	$fdata["GoodName"] = "qt_dormitorio";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","qt_dormitorio");
	$fdata["FieldType"] = 20;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "qt_dormitorio";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "qt_dormitorio";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["qt_dormitorio"] = $fdata;
//	qt_banheiro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 40;
	$fdata["strName"] = "qt_banheiro";
	$fdata["GoodName"] = "qt_banheiro";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","qt_banheiro");
	$fdata["FieldType"] = 20;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "qt_banheiro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "qt_banheiro";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["qt_banheiro"] = $fdata;
//	observacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 41;
	$fdata["strName"] = "observacao";
	$fdata["GoodName"] = "observacao";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","observacao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "observacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "observacao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=250";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["observacao"] = $fdata;
//	cadastrador
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 42;
	$fdata["strName"] = "cadastrador";
	$fdata["GoodName"] = "cadastrador";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","cadastrador");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "cadastrador";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cadastrador";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["cadastrador"] = $fdata;
//	data_cadastro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 43;
	$fdata["strName"] = "data_cadastro";
	$fdata["GoodName"] = "data_cadastro";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","data_cadastro");
	$fdata["FieldType"] = 7;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "data_cadastro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "data_cadastro";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Short Date");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 13;
	$edata["InitialYearFactor"] = 100;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["data_cadastro"] = $fdata;
//	ident
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 44;
	$fdata["strName"] = "ident";
	$fdata["GoodName"] = "ident";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","ident");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ident";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ident";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["ident"] = $fdata;
//	agua
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 45;
	$fdata["strName"] = "agua";
	$fdata["GoodName"] = "agua";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","agua");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "agua";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "agua";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=3";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["agua"] = $fdata;
//	esgoto
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 46;
	$fdata["strName"] = "esgoto";
	$fdata["GoodName"] = "esgoto";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","esgoto");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "esgoto";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "esgoto";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=3";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["esgoto"] = $fdata;
//	energia
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 47;
	$fdata["strName"] = "energia";
	$fdata["GoodName"] = "energia";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","energia");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "energia";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "energia";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=3";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["energia"] = $fdata;
//	iluminacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 48;
	$fdata["strName"] = "iluminacao";
	$fdata["GoodName"] = "iluminacao";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","iluminacao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "iluminacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "iluminacao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=3";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["iluminacao"] = $fdata;
//	guia_sargeta
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 49;
	$fdata["strName"] = "guia_sargeta";
	$fdata["GoodName"] = "guia_sargeta";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","guia_sargeta");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "guia_sargeta";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "guia_sargeta";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=3";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["guia_sargeta"] = $fdata;
//	pavimento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 50;
	$fdata["strName"] = "pavimento";
	$fdata["GoodName"] = "pavimento";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","pavimento");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "pavimento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "pavimento";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=3";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["pavimento"] = $fdata;
//	drenagem
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 51;
	$fdata["strName"] = "drenagem";
	$fdata["GoodName"] = "drenagem";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","drenagem");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "drenagem";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "drenagem";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=3";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["drenagem"] = $fdata;
//	telefone
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 52;
	$fdata["strName"] = "telefone";
	$fdata["GoodName"] = "telefone";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","telefone");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "telefone";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "telefone";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=3";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["telefone"] = $fdata;
//	coleta
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 53;
	$fdata["strName"] = "coleta";
	$fdata["GoodName"] = "coleta";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","coleta");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "coleta";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "coleta";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=3";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["coleta"] = $fdata;
//	transporte
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 54;
	$fdata["strName"] = "transporte";
	$fdata["GoodName"] = "transporte";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","transporte");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "transporte";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "transporte";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=3";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["transporte"] = $fdata;
//	foto_print
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 55;
	$fdata["strName"] = "foto_print";
	$fdata["GoodName"] = "foto_print";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","foto_print");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "foto_print";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "foto_print";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["foto_print"] = $fdata;
//	ocupacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 56;
	$fdata["strName"] = "ocupacao";
	$fdata["GoodName"] = "ocupacao";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","ocupacao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "ocupacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ocupacao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["ocupacao"] = $fdata;
//	concluido
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 57;
	$fdata["strName"] = "concluido";
	$fdata["GoodName"] = "concluido";
	$fdata["ownerTable"] = "public.tbl_pesquisa_bkp_exclusao";
	$fdata["Label"] = GetFieldLabel("public_tbl_pesquisa_bkp_exclusao","concluido");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

		$fdata["bInlineAdd"] = true;

		$fdata["bEditPage"] = true;

		$fdata["bInlineEdit"] = true;

		$fdata["bUpdateSelected"] = true;


		$fdata["bViewPage"] = true;

		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "concluido";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "concluido";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=5";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_pesquisa_bkp_exclusao["concluido"] = $fdata;


$tables_data["public.tbl_pesquisa_bkp_exclusao"]=&$tdatapublic_tbl_pesquisa_bkp_exclusao;
$field_labels["public_tbl_pesquisa_bkp_exclusao"] = &$fieldLabelspublic_tbl_pesquisa_bkp_exclusao;
$fieldToolTips["public_tbl_pesquisa_bkp_exclusao"] = &$fieldToolTipspublic_tbl_pesquisa_bkp_exclusao;
$placeHolders["public_tbl_pesquisa_bkp_exclusao"] = &$placeHolderspublic_tbl_pesquisa_bkp_exclusao;
$page_titles["public_tbl_pesquisa_bkp_exclusao"] = &$pageTitlespublic_tbl_pesquisa_bkp_exclusao;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.tbl_pesquisa_bkp_exclusao"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.tbl_pesquisa_bkp_exclusao"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_tbl_pesquisa_bkp_exclusao()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "pesquisa,  	\"data\",  	fotos,  	codlog,  	endereco_imovel,  	numero,  	complemento,  	bairro_loteamento,  	preco,  	condicoes_pagamento,  	documentacao,  	modalidade,  	data_modalidade,  	fonte,  	nome_fonte,  	telefone_fonte,  	ic_imobiliaria,  	zona_homogenea,  	testada,  	area_terreno,  	area_iptu,  	area_estimada,  	area_atualizacao,  	forma,  	topografia,  	posicao,  	benfeitoria,  	classe,  	tipologia_construtiva,  	padrao_construtivo,  	conservacao,  	estrutura,  	dependencia,  	elevador,  	vao,  	cobertura,  	condominio,  	idade_edificacao,  	qt_dormitorio,  	qt_banheiro,  	observacao,  	cadastrador,  	data_cadastro,  	ident,  	agua,  	esgoto,  	energia,  	iluminacao,  	guia_sargeta,  	pavimento,  	drenagem,  	telefone,  	coleta,  	transporte,  	foto_print,  	ocupacao,  	concluido";
$proto0["m_strFrom"] = "FROM \"public\".tbl_pesquisa_bkp_exclusao";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "pesquisa",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto6["m_sql"] = "pesquisa";
$proto6["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "data",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto8["m_sql"] = "\"data\"";
$proto8["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "fotos",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto10["m_sql"] = "fotos";
$proto10["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "codlog",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto12["m_sql"] = "codlog";
$proto12["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "endereco_imovel",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto14["m_sql"] = "endereco_imovel";
$proto14["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "numero",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto16["m_sql"] = "numero";
$proto16["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "complemento",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto18["m_sql"] = "complemento";
$proto18["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "bairro_loteamento",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto20["m_sql"] = "bairro_loteamento";
$proto20["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "preco",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto22["m_sql"] = "preco";
$proto22["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "condicoes_pagamento",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto24["m_sql"] = "condicoes_pagamento";
$proto24["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "documentacao",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto26["m_sql"] = "documentacao";
$proto26["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "modalidade",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto28["m_sql"] = "modalidade";
$proto28["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "data_modalidade",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto30["m_sql"] = "data_modalidade";
$proto30["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "fonte",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto32["m_sql"] = "fonte";
$proto32["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "nome_fonte",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto34["m_sql"] = "nome_fonte";
$proto34["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto36=array();
			$obj = new SQLField(array(
	"m_strName" => "telefone_fonte",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto36["m_sql"] = "telefone_fonte";
$proto36["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto36["m_expr"]=$obj;
$proto36["m_alias"] = "";
$obj = new SQLFieldListItem($proto36);

$proto0["m_fieldlist"][]=$obj;
						$proto38=array();
			$obj = new SQLField(array(
	"m_strName" => "ic_imobiliaria",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto38["m_sql"] = "ic_imobiliaria";
$proto38["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto38["m_expr"]=$obj;
$proto38["m_alias"] = "";
$obj = new SQLFieldListItem($proto38);

$proto0["m_fieldlist"][]=$obj;
						$proto40=array();
			$obj = new SQLField(array(
	"m_strName" => "zona_homogenea",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto40["m_sql"] = "zona_homogenea";
$proto40["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto40["m_expr"]=$obj;
$proto40["m_alias"] = "";
$obj = new SQLFieldListItem($proto40);

$proto0["m_fieldlist"][]=$obj;
						$proto42=array();
			$obj = new SQLField(array(
	"m_strName" => "testada",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto42["m_sql"] = "testada";
$proto42["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto42["m_expr"]=$obj;
$proto42["m_alias"] = "";
$obj = new SQLFieldListItem($proto42);

$proto0["m_fieldlist"][]=$obj;
						$proto44=array();
			$obj = new SQLField(array(
	"m_strName" => "area_terreno",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto44["m_sql"] = "area_terreno";
$proto44["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto44["m_expr"]=$obj;
$proto44["m_alias"] = "";
$obj = new SQLFieldListItem($proto44);

$proto0["m_fieldlist"][]=$obj;
						$proto46=array();
			$obj = new SQLField(array(
	"m_strName" => "area_iptu",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto46["m_sql"] = "area_iptu";
$proto46["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto46["m_expr"]=$obj;
$proto46["m_alias"] = "";
$obj = new SQLFieldListItem($proto46);

$proto0["m_fieldlist"][]=$obj;
						$proto48=array();
			$obj = new SQLField(array(
	"m_strName" => "area_estimada",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto48["m_sql"] = "area_estimada";
$proto48["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto48["m_expr"]=$obj;
$proto48["m_alias"] = "";
$obj = new SQLFieldListItem($proto48);

$proto0["m_fieldlist"][]=$obj;
						$proto50=array();
			$obj = new SQLField(array(
	"m_strName" => "area_atualizacao",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto50["m_sql"] = "area_atualizacao";
$proto50["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto50["m_expr"]=$obj;
$proto50["m_alias"] = "";
$obj = new SQLFieldListItem($proto50);

$proto0["m_fieldlist"][]=$obj;
						$proto52=array();
			$obj = new SQLField(array(
	"m_strName" => "forma",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto52["m_sql"] = "forma";
$proto52["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto52["m_expr"]=$obj;
$proto52["m_alias"] = "";
$obj = new SQLFieldListItem($proto52);

$proto0["m_fieldlist"][]=$obj;
						$proto54=array();
			$obj = new SQLField(array(
	"m_strName" => "topografia",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto54["m_sql"] = "topografia";
$proto54["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto54["m_expr"]=$obj;
$proto54["m_alias"] = "";
$obj = new SQLFieldListItem($proto54);

$proto0["m_fieldlist"][]=$obj;
						$proto56=array();
			$obj = new SQLField(array(
	"m_strName" => "posicao",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto56["m_sql"] = "posicao";
$proto56["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto56["m_expr"]=$obj;
$proto56["m_alias"] = "";
$obj = new SQLFieldListItem($proto56);

$proto0["m_fieldlist"][]=$obj;
						$proto58=array();
			$obj = new SQLField(array(
	"m_strName" => "benfeitoria",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto58["m_sql"] = "benfeitoria";
$proto58["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto58["m_expr"]=$obj;
$proto58["m_alias"] = "";
$obj = new SQLFieldListItem($proto58);

$proto0["m_fieldlist"][]=$obj;
						$proto60=array();
			$obj = new SQLField(array(
	"m_strName" => "classe",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto60["m_sql"] = "classe";
$proto60["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto60["m_expr"]=$obj;
$proto60["m_alias"] = "";
$obj = new SQLFieldListItem($proto60);

$proto0["m_fieldlist"][]=$obj;
						$proto62=array();
			$obj = new SQLField(array(
	"m_strName" => "tipologia_construtiva",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto62["m_sql"] = "tipologia_construtiva";
$proto62["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto62["m_expr"]=$obj;
$proto62["m_alias"] = "";
$obj = new SQLFieldListItem($proto62);

$proto0["m_fieldlist"][]=$obj;
						$proto64=array();
			$obj = new SQLField(array(
	"m_strName" => "padrao_construtivo",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto64["m_sql"] = "padrao_construtivo";
$proto64["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto64["m_expr"]=$obj;
$proto64["m_alias"] = "";
$obj = new SQLFieldListItem($proto64);

$proto0["m_fieldlist"][]=$obj;
						$proto66=array();
			$obj = new SQLField(array(
	"m_strName" => "conservacao",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto66["m_sql"] = "conservacao";
$proto66["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto66["m_expr"]=$obj;
$proto66["m_alias"] = "";
$obj = new SQLFieldListItem($proto66);

$proto0["m_fieldlist"][]=$obj;
						$proto68=array();
			$obj = new SQLField(array(
	"m_strName" => "estrutura",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto68["m_sql"] = "estrutura";
$proto68["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto68["m_expr"]=$obj;
$proto68["m_alias"] = "";
$obj = new SQLFieldListItem($proto68);

$proto0["m_fieldlist"][]=$obj;
						$proto70=array();
			$obj = new SQLField(array(
	"m_strName" => "dependencia",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto70["m_sql"] = "dependencia";
$proto70["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto70["m_expr"]=$obj;
$proto70["m_alias"] = "";
$obj = new SQLFieldListItem($proto70);

$proto0["m_fieldlist"][]=$obj;
						$proto72=array();
			$obj = new SQLField(array(
	"m_strName" => "elevador",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto72["m_sql"] = "elevador";
$proto72["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto72["m_expr"]=$obj;
$proto72["m_alias"] = "";
$obj = new SQLFieldListItem($proto72);

$proto0["m_fieldlist"][]=$obj;
						$proto74=array();
			$obj = new SQLField(array(
	"m_strName" => "vao",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto74["m_sql"] = "vao";
$proto74["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto74["m_expr"]=$obj;
$proto74["m_alias"] = "";
$obj = new SQLFieldListItem($proto74);

$proto0["m_fieldlist"][]=$obj;
						$proto76=array();
			$obj = new SQLField(array(
	"m_strName" => "cobertura",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto76["m_sql"] = "cobertura";
$proto76["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto76["m_expr"]=$obj;
$proto76["m_alias"] = "";
$obj = new SQLFieldListItem($proto76);

$proto0["m_fieldlist"][]=$obj;
						$proto78=array();
			$obj = new SQLField(array(
	"m_strName" => "condominio",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto78["m_sql"] = "condominio";
$proto78["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto78["m_expr"]=$obj;
$proto78["m_alias"] = "";
$obj = new SQLFieldListItem($proto78);

$proto0["m_fieldlist"][]=$obj;
						$proto80=array();
			$obj = new SQLField(array(
	"m_strName" => "idade_edificacao",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto80["m_sql"] = "idade_edificacao";
$proto80["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto80["m_expr"]=$obj;
$proto80["m_alias"] = "";
$obj = new SQLFieldListItem($proto80);

$proto0["m_fieldlist"][]=$obj;
						$proto82=array();
			$obj = new SQLField(array(
	"m_strName" => "qt_dormitorio",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto82["m_sql"] = "qt_dormitorio";
$proto82["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto82["m_expr"]=$obj;
$proto82["m_alias"] = "";
$obj = new SQLFieldListItem($proto82);

$proto0["m_fieldlist"][]=$obj;
						$proto84=array();
			$obj = new SQLField(array(
	"m_strName" => "qt_banheiro",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto84["m_sql"] = "qt_banheiro";
$proto84["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto84["m_expr"]=$obj;
$proto84["m_alias"] = "";
$obj = new SQLFieldListItem($proto84);

$proto0["m_fieldlist"][]=$obj;
						$proto86=array();
			$obj = new SQLField(array(
	"m_strName" => "observacao",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto86["m_sql"] = "observacao";
$proto86["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto86["m_expr"]=$obj;
$proto86["m_alias"] = "";
$obj = new SQLFieldListItem($proto86);

$proto0["m_fieldlist"][]=$obj;
						$proto88=array();
			$obj = new SQLField(array(
	"m_strName" => "cadastrador",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto88["m_sql"] = "cadastrador";
$proto88["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto88["m_expr"]=$obj;
$proto88["m_alias"] = "";
$obj = new SQLFieldListItem($proto88);

$proto0["m_fieldlist"][]=$obj;
						$proto90=array();
			$obj = new SQLField(array(
	"m_strName" => "data_cadastro",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto90["m_sql"] = "data_cadastro";
$proto90["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto90["m_expr"]=$obj;
$proto90["m_alias"] = "";
$obj = new SQLFieldListItem($proto90);

$proto0["m_fieldlist"][]=$obj;
						$proto92=array();
			$obj = new SQLField(array(
	"m_strName" => "ident",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto92["m_sql"] = "ident";
$proto92["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto92["m_expr"]=$obj;
$proto92["m_alias"] = "";
$obj = new SQLFieldListItem($proto92);

$proto0["m_fieldlist"][]=$obj;
						$proto94=array();
			$obj = new SQLField(array(
	"m_strName" => "agua",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto94["m_sql"] = "agua";
$proto94["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto94["m_expr"]=$obj;
$proto94["m_alias"] = "";
$obj = new SQLFieldListItem($proto94);

$proto0["m_fieldlist"][]=$obj;
						$proto96=array();
			$obj = new SQLField(array(
	"m_strName" => "esgoto",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto96["m_sql"] = "esgoto";
$proto96["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto96["m_expr"]=$obj;
$proto96["m_alias"] = "";
$obj = new SQLFieldListItem($proto96);

$proto0["m_fieldlist"][]=$obj;
						$proto98=array();
			$obj = new SQLField(array(
	"m_strName" => "energia",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto98["m_sql"] = "energia";
$proto98["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto98["m_expr"]=$obj;
$proto98["m_alias"] = "";
$obj = new SQLFieldListItem($proto98);

$proto0["m_fieldlist"][]=$obj;
						$proto100=array();
			$obj = new SQLField(array(
	"m_strName" => "iluminacao",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto100["m_sql"] = "iluminacao";
$proto100["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto100["m_expr"]=$obj;
$proto100["m_alias"] = "";
$obj = new SQLFieldListItem($proto100);

$proto0["m_fieldlist"][]=$obj;
						$proto102=array();
			$obj = new SQLField(array(
	"m_strName" => "guia_sargeta",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto102["m_sql"] = "guia_sargeta";
$proto102["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto102["m_expr"]=$obj;
$proto102["m_alias"] = "";
$obj = new SQLFieldListItem($proto102);

$proto0["m_fieldlist"][]=$obj;
						$proto104=array();
			$obj = new SQLField(array(
	"m_strName" => "pavimento",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto104["m_sql"] = "pavimento";
$proto104["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto104["m_expr"]=$obj;
$proto104["m_alias"] = "";
$obj = new SQLFieldListItem($proto104);

$proto0["m_fieldlist"][]=$obj;
						$proto106=array();
			$obj = new SQLField(array(
	"m_strName" => "drenagem",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto106["m_sql"] = "drenagem";
$proto106["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto106["m_expr"]=$obj;
$proto106["m_alias"] = "";
$obj = new SQLFieldListItem($proto106);

$proto0["m_fieldlist"][]=$obj;
						$proto108=array();
			$obj = new SQLField(array(
	"m_strName" => "telefone",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto108["m_sql"] = "telefone";
$proto108["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto108["m_expr"]=$obj;
$proto108["m_alias"] = "";
$obj = new SQLFieldListItem($proto108);

$proto0["m_fieldlist"][]=$obj;
						$proto110=array();
			$obj = new SQLField(array(
	"m_strName" => "coleta",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto110["m_sql"] = "coleta";
$proto110["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto110["m_expr"]=$obj;
$proto110["m_alias"] = "";
$obj = new SQLFieldListItem($proto110);

$proto0["m_fieldlist"][]=$obj;
						$proto112=array();
			$obj = new SQLField(array(
	"m_strName" => "transporte",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto112["m_sql"] = "transporte";
$proto112["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto112["m_expr"]=$obj;
$proto112["m_alias"] = "";
$obj = new SQLFieldListItem($proto112);

$proto0["m_fieldlist"][]=$obj;
						$proto114=array();
			$obj = new SQLField(array(
	"m_strName" => "foto_print",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto114["m_sql"] = "foto_print";
$proto114["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto114["m_expr"]=$obj;
$proto114["m_alias"] = "";
$obj = new SQLFieldListItem($proto114);

$proto0["m_fieldlist"][]=$obj;
						$proto116=array();
			$obj = new SQLField(array(
	"m_strName" => "ocupacao",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto116["m_sql"] = "ocupacao";
$proto116["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto116["m_expr"]=$obj;
$proto116["m_alias"] = "";
$obj = new SQLFieldListItem($proto116);

$proto0["m_fieldlist"][]=$obj;
						$proto118=array();
			$obj = new SQLField(array(
	"m_strName" => "concluido",
	"m_strTable" => "public.tbl_pesquisa_bkp_exclusao",
	"m_srcTableName" => "public.tbl_pesquisa_bkp_exclusao"
));

$proto118["m_sql"] = "concluido";
$proto118["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto118["m_expr"]=$obj;
$proto118["m_alias"] = "";
$obj = new SQLFieldListItem($proto118);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto120=array();
$proto120["m_link"] = "SQLL_MAIN";
			$proto121=array();
$proto121["m_strName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto121["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto121["m_columns"] = array();
$proto121["m_columns"][] = "pesquisa";
$proto121["m_columns"][] = "data";
$proto121["m_columns"][] = "fotos";
$proto121["m_columns"][] = "codlog";
$proto121["m_columns"][] = "endereco_imovel";
$proto121["m_columns"][] = "numero";
$proto121["m_columns"][] = "complemento";
$proto121["m_columns"][] = "bairro_loteamento";
$proto121["m_columns"][] = "preco";
$proto121["m_columns"][] = "condicoes_pagamento";
$proto121["m_columns"][] = "documentacao";
$proto121["m_columns"][] = "modalidade";
$proto121["m_columns"][] = "data_modalidade";
$proto121["m_columns"][] = "fonte";
$proto121["m_columns"][] = "nome_fonte";
$proto121["m_columns"][] = "telefone_fonte";
$proto121["m_columns"][] = "ic_imobiliaria";
$proto121["m_columns"][] = "zona_homogenea";
$proto121["m_columns"][] = "testada";
$proto121["m_columns"][] = "area_terreno";
$proto121["m_columns"][] = "area_iptu";
$proto121["m_columns"][] = "area_estimada";
$proto121["m_columns"][] = "area_atualizacao";
$proto121["m_columns"][] = "forma";
$proto121["m_columns"][] = "topografia";
$proto121["m_columns"][] = "posicao";
$proto121["m_columns"][] = "benfeitoria";
$proto121["m_columns"][] = "classe";
$proto121["m_columns"][] = "tipologia_construtiva";
$proto121["m_columns"][] = "padrao_construtivo";
$proto121["m_columns"][] = "conservacao";
$proto121["m_columns"][] = "estrutura";
$proto121["m_columns"][] = "dependencia";
$proto121["m_columns"][] = "elevador";
$proto121["m_columns"][] = "vao";
$proto121["m_columns"][] = "cobertura";
$proto121["m_columns"][] = "condominio";
$proto121["m_columns"][] = "idade_edificacao";
$proto121["m_columns"][] = "qt_dormitorio";
$proto121["m_columns"][] = "qt_banheiro";
$proto121["m_columns"][] = "observacao";
$proto121["m_columns"][] = "cadastrador";
$proto121["m_columns"][] = "data_cadastro";
$proto121["m_columns"][] = "ident";
$proto121["m_columns"][] = "agua";
$proto121["m_columns"][] = "esgoto";
$proto121["m_columns"][] = "energia";
$proto121["m_columns"][] = "iluminacao";
$proto121["m_columns"][] = "guia_sargeta";
$proto121["m_columns"][] = "pavimento";
$proto121["m_columns"][] = "drenagem";
$proto121["m_columns"][] = "telefone";
$proto121["m_columns"][] = "coleta";
$proto121["m_columns"][] = "transporte";
$proto121["m_columns"][] = "foto_print";
$proto121["m_columns"][] = "ocupacao";
$proto121["m_columns"][] = "concluido";
$obj = new SQLTable($proto121);

$proto120["m_table"] = $obj;
$proto120["m_sql"] = "\"public\".tbl_pesquisa_bkp_exclusao";
$proto120["m_alias"] = "";
$proto120["m_srcTableName"] = "public.tbl_pesquisa_bkp_exclusao";
$proto122=array();
$proto122["m_sql"] = "";
$proto122["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto122["m_column"]=$obj;
$proto122["m_contained"] = array();
$proto122["m_strCase"] = "";
$proto122["m_havingmode"] = false;
$proto122["m_inBrackets"] = false;
$proto122["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto122);

$proto120["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto120);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.tbl_pesquisa_bkp_exclusao";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_public_tbl_pesquisa_bkp_exclusao = createSqlQuery_public_tbl_pesquisa_bkp_exclusao();


	
		;

																																																									

$tdatapublic_tbl_pesquisa_bkp_exclusao[".sqlquery"] = $queryData_public_tbl_pesquisa_bkp_exclusao;

$tableEvents["public.tbl_pesquisa_bkp_exclusao"] = new eventsBase;
$tdatapublic_tbl_pesquisa_bkp_exclusao[".hasEvents"] = false;

?>