<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_geo_face_quadra = array();
	$tdatapublic_geo_face_quadra[".truncateText"] = true;
	$tdatapublic_geo_face_quadra[".NumberOfChars"] = 80;
	$tdatapublic_geo_face_quadra[".ShortName"] = "public_geo_face_quadra";
	$tdatapublic_geo_face_quadra[".OwnerID"] = "";
	$tdatapublic_geo_face_quadra[".OriginalTable"] = "public.geo_face_quadra";

//	field labels
$fieldLabelspublic_geo_face_quadra = array();
$fieldToolTipspublic_geo_face_quadra = array();
$pageTitlespublic_geo_face_quadra = array();
$placeHolderspublic_geo_face_quadra = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"] = array();
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"] = array();
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"] = array();
	$pageTitlespublic_geo_face_quadra["Portuguese(Brazil)"] = array();
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"]["cod_log"] = "Cod Log";
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]["cod_log"] = "";
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"]["cod_log"] = "";
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"]["obs"] = "Obs";
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]["obs"] = "";
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"]["obs"] = "";
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"]["cod_face"] = "Cod Face";
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]["cod_face"] = "";
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"]["cod_face"] = "";
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"]["tipo_log"] = "Tipo Log";
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]["tipo_log"] = "";
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"]["tipo_log"] = "";
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"]["logradouro"] = "Logradouro";
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]["logradouro"] = "";
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"]["logradouro"] = "";
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"]["zh"] = "Zh";
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]["zh"] = "";
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"]["zh"] = "";
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"]["esgoto"] = "Esgoto";
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]["esgoto"] = "";
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"]["esgoto"] = "";
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"]["telefone"] = "Telefone";
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]["telefone"] = "";
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"]["telefone"] = "";
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"]["agua"] = "Agua";
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]["agua"] = "";
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"]["agua"] = "";
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"]["iluminacao"] = "Iluminacao";
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]["iluminacao"] = "";
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"]["iluminacao"] = "";
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"]["energia"] = "Energia";
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]["energia"] = "";
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"]["energia"] = "";
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"]["pavimento"] = "Pavimento";
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]["pavimento"] = "";
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"]["pavimento"] = "";
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"]["guia"] = "Guia";
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]["guia"] = "";
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"]["guia"] = "";
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"]["lixo"] = "Lixo";
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]["lixo"] = "";
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"]["lixo"] = "";
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"]["transporte"] = "Transporte";
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]["transporte"] = "";
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"]["transporte"] = "";
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"]["vbu_pmt"] = "Vbu Pm";
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]["vbu_pmt"] = "";
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"]["vbu_pmt"] = "";
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"]["vbu_pvg"] = "Vbu Pvg";
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]["vbu_pvg"] = "";
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"]["vbu_pvg"] = "";
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"]["drenagem"] = "Drenagem";
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]["drenagem"] = "";
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"]["drenagem"] = "";
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"]["bairro"] = "Bairro";
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]["bairro"] = "";
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"]["bairro"] = "";
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"]["inclusao"] = "Inclusao";
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]["inclusao"] = "";
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"]["inclusao"] = "";
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"]["limpeza"] = "Limpeza";
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]["limpeza"] = "";
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"]["limpeza"] = "";
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"]["enchente"] = "Enchente";
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]["enchente"] = "";
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"]["enchente"] = "";
	$fieldLabelspublic_geo_face_quadra["Portuguese(Brazil)"]["favela"] = "Favela";
	$fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]["favela"] = "";
	$placeHolderspublic_geo_face_quadra["Portuguese(Brazil)"]["favela"] = "";
	if (count($fieldToolTipspublic_geo_face_quadra["Portuguese(Brazil)"]))
		$tdatapublic_geo_face_quadra[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_geo_face_quadra[""] = array();
	$fieldToolTipspublic_geo_face_quadra[""] = array();
	$placeHolderspublic_geo_face_quadra[""] = array();
	$pageTitlespublic_geo_face_quadra[""] = array();
	if (count($fieldToolTipspublic_geo_face_quadra[""]))
		$tdatapublic_geo_face_quadra[".isUseToolTips"] = true;
}


	$tdatapublic_geo_face_quadra[".NCSearch"] = true;



$tdatapublic_geo_face_quadra[".shortTableName"] = "public_geo_face_quadra";
$tdatapublic_geo_face_quadra[".nSecOptions"] = 0;
$tdatapublic_geo_face_quadra[".recsPerRowPrint"] = 1;
$tdatapublic_geo_face_quadra[".mainTableOwnerID"] = "";
$tdatapublic_geo_face_quadra[".moveNext"] = 1;
$tdatapublic_geo_face_quadra[".entityType"] = 0;

$tdatapublic_geo_face_quadra[".strOriginalTableName"] = "public.geo_face_quadra";

	



$tdatapublic_geo_face_quadra[".showAddInPopup"] = true;

$tdatapublic_geo_face_quadra[".showEditInPopup"] = false;

$tdatapublic_geo_face_quadra[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
						
	;
$popupPagesLayoutNames["add"] = "add2";
$tdatapublic_geo_face_quadra[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_geo_face_quadra[".fieldsForRegister"] = array();

	$tdatapublic_geo_face_quadra[".listAjax"] = true;

	$tdatapublic_geo_face_quadra[".audit"] = true;

	$tdatapublic_geo_face_quadra[".locking"] = false;


$tdatapublic_geo_face_quadra[".add"] = true;
$tdatapublic_geo_face_quadra[".afterAddAction"] = 1;
$tdatapublic_geo_face_quadra[".closePopupAfterAdd"] = 1;
$tdatapublic_geo_face_quadra[".afterAddActionDetTable"] = "";

$tdatapublic_geo_face_quadra[".list"] = true;

$tdatapublic_geo_face_quadra[".inlineEdit"] = true;


$tdatapublic_geo_face_quadra[".reorderRecordsByHeader"] = true;


$tdatapublic_geo_face_quadra[".exportFormatting"] = 2;
$tdatapublic_geo_face_quadra[".exportDelimiter"] = ",";
		


$tdatapublic_geo_face_quadra[".exportTo"] = true;

$tdatapublic_geo_face_quadra[".printFriendly"] = true;

$tdatapublic_geo_face_quadra[".delete"] = true;

$tdatapublic_geo_face_quadra[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatapublic_geo_face_quadra[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatapublic_geo_face_quadra[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatapublic_geo_face_quadra[".searchSaving"] = false;
//

$tdatapublic_geo_face_quadra[".showSearchPanel"] = true;
		$tdatapublic_geo_face_quadra[".flexibleSearch"] = true;

$tdatapublic_geo_face_quadra[".isUseAjaxSuggest"] = true;






$tdatapublic_geo_face_quadra[".ajaxCodeSnippetAdded"] = false;

$tdatapublic_geo_face_quadra[".buttonsAdded"] = false;

$tdatapublic_geo_face_quadra[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapublic_geo_face_quadra[".isUseTimeForSearch"] = false;



$tdatapublic_geo_face_quadra[".badgeColor"] = "CD853F";


$tdatapublic_geo_face_quadra[".allSearchFields"] = array();
$tdatapublic_geo_face_quadra[".filterFields"] = array();
$tdatapublic_geo_face_quadra[".requiredSearchFields"] = array();

$tdatapublic_geo_face_quadra[".allSearchFields"][] = "cod_face";
	$tdatapublic_geo_face_quadra[".allSearchFields"][] = "cod_log";
	$tdatapublic_geo_face_quadra[".allSearchFields"][] = "tipo_log";
	$tdatapublic_geo_face_quadra[".allSearchFields"][] = "logradouro";
	$tdatapublic_geo_face_quadra[".allSearchFields"][] = "bairro";
	$tdatapublic_geo_face_quadra[".allSearchFields"][] = "inclusao";
	

$tdatapublic_geo_face_quadra[".googleLikeFields"] = array();
$tdatapublic_geo_face_quadra[".googleLikeFields"][] = "cod_log";
$tdatapublic_geo_face_quadra[".googleLikeFields"][] = "obs";
$tdatapublic_geo_face_quadra[".googleLikeFields"][] = "cod_face";
$tdatapublic_geo_face_quadra[".googleLikeFields"][] = "tipo_log";
$tdatapublic_geo_face_quadra[".googleLikeFields"][] = "logradouro";
$tdatapublic_geo_face_quadra[".googleLikeFields"][] = "zh";
$tdatapublic_geo_face_quadra[".googleLikeFields"][] = "esgoto";
$tdatapublic_geo_face_quadra[".googleLikeFields"][] = "telefone";
$tdatapublic_geo_face_quadra[".googleLikeFields"][] = "agua";
$tdatapublic_geo_face_quadra[".googleLikeFields"][] = "iluminacao";
$tdatapublic_geo_face_quadra[".googleLikeFields"][] = "energia";
$tdatapublic_geo_face_quadra[".googleLikeFields"][] = "pavimento";
$tdatapublic_geo_face_quadra[".googleLikeFields"][] = "guia";
$tdatapublic_geo_face_quadra[".googleLikeFields"][] = "lixo";
$tdatapublic_geo_face_quadra[".googleLikeFields"][] = "transporte";
$tdatapublic_geo_face_quadra[".googleLikeFields"][] = "vbu_pmt";
$tdatapublic_geo_face_quadra[".googleLikeFields"][] = "vbu_pvg";
$tdatapublic_geo_face_quadra[".googleLikeFields"][] = "drenagem";
$tdatapublic_geo_face_quadra[".googleLikeFields"][] = "bairro";
$tdatapublic_geo_face_quadra[".googleLikeFields"][] = "inclusao";
$tdatapublic_geo_face_quadra[".googleLikeFields"][] = "limpeza";
$tdatapublic_geo_face_quadra[".googleLikeFields"][] = "enchente";
$tdatapublic_geo_face_quadra[".googleLikeFields"][] = "favela";


$tdatapublic_geo_face_quadra[".advSearchFields"] = array();
$tdatapublic_geo_face_quadra[".advSearchFields"][] = "cod_face";
$tdatapublic_geo_face_quadra[".advSearchFields"][] = "cod_log";
$tdatapublic_geo_face_quadra[".advSearchFields"][] = "tipo_log";
$tdatapublic_geo_face_quadra[".advSearchFields"][] = "logradouro";
$tdatapublic_geo_face_quadra[".advSearchFields"][] = "bairro";
$tdatapublic_geo_face_quadra[".advSearchFields"][] = "inclusao";

$tdatapublic_geo_face_quadra[".tableType"] = "list";

$tdatapublic_geo_face_quadra[".printerPageOrientation"] = 0;
$tdatapublic_geo_face_quadra[".isPrinterPageFitToPage"] = 0;
$tdatapublic_geo_face_quadra[".nPrinterPageScale"] = 80;

$tdatapublic_geo_face_quadra[".nPrinterSplitRecords"] = 40;

$tdatapublic_geo_face_quadra[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_geo_face_quadra[".geocodingEnabled"] = false;





$tdatapublic_geo_face_quadra[".listGridLayout"] = 3;

$tdatapublic_geo_face_quadra[".isDisplayLoading"] = true;




// view page pdf

// print page pdf


$tdatapublic_geo_face_quadra[".pageSize"] = 20;

$tdatapublic_geo_face_quadra[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY cod_face";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_geo_face_quadra[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_geo_face_quadra[".orderindexes"] = array();
$tdatapublic_geo_face_quadra[".orderindexes"][] = array(3, (1 ? "ASC" : "DESC"), "cod_face");

$tdatapublic_geo_face_quadra[".sqlHead"] = "SELECT cod_log,  obs,  cod_face,  tipo_log,  logradouro,  zh,  esgoto,  telefone,  agua,  iluminacao,  energia,  pavimento,  guia,  lixo,  transporte,  vbu_pmt,  vbu_pvg,  drenagem,  bairro,  inclusao,  limpeza,  enchente,  favela";
$tdatapublic_geo_face_quadra[".sqlFrom"] = "FROM \"public\".geo_face_quadra";
$tdatapublic_geo_face_quadra[".sqlWhereExpr"] = "";
$tdatapublic_geo_face_quadra[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_geo_face_quadra[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_geo_face_quadra[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_geo_face_quadra[".highlightSearchResults"] = true;

$tableKeyspublic_geo_face_quadra = array();
$tableKeyspublic_geo_face_quadra[] = "cod_face";
$tdatapublic_geo_face_quadra[".Keys"] = $tableKeyspublic_geo_face_quadra;

$tdatapublic_geo_face_quadra[".listFields"] = array();
$tdatapublic_geo_face_quadra[".listFields"][] = "cod_face";
$tdatapublic_geo_face_quadra[".listFields"][] = "cod_log";
$tdatapublic_geo_face_quadra[".listFields"][] = "tipo_log";
$tdatapublic_geo_face_quadra[".listFields"][] = "logradouro";
$tdatapublic_geo_face_quadra[".listFields"][] = "bairro";
$tdatapublic_geo_face_quadra[".listFields"][] = "vbu_pmt";
$tdatapublic_geo_face_quadra[".listFields"][] = "vbu_pvg";
$tdatapublic_geo_face_quadra[".listFields"][] = "agua";
$tdatapublic_geo_face_quadra[".listFields"][] = "esgoto";
$tdatapublic_geo_face_quadra[".listFields"][] = "pavimento";
$tdatapublic_geo_face_quadra[".listFields"][] = "iluminacao";
$tdatapublic_geo_face_quadra[".listFields"][] = "energia";
$tdatapublic_geo_face_quadra[".listFields"][] = "lixo";
$tdatapublic_geo_face_quadra[".listFields"][] = "enchente";
$tdatapublic_geo_face_quadra[".listFields"][] = "obs";
$tdatapublic_geo_face_quadra[".listFields"][] = "zh";
$tdatapublic_geo_face_quadra[".listFields"][] = "inclusao";

$tdatapublic_geo_face_quadra[".hideMobileList"] = array();


$tdatapublic_geo_face_quadra[".viewFields"] = array();

$tdatapublic_geo_face_quadra[".addFields"] = array();
$tdatapublic_geo_face_quadra[".addFields"][] = "cod_face";
$tdatapublic_geo_face_quadra[".addFields"][] = "cod_log";
$tdatapublic_geo_face_quadra[".addFields"][] = "tipo_log";
$tdatapublic_geo_face_quadra[".addFields"][] = "logradouro";
$tdatapublic_geo_face_quadra[".addFields"][] = "bairro";
$tdatapublic_geo_face_quadra[".addFields"][] = "vbu_pmt";
$tdatapublic_geo_face_quadra[".addFields"][] = "vbu_pvg";
$tdatapublic_geo_face_quadra[".addFields"][] = "agua";
$tdatapublic_geo_face_quadra[".addFields"][] = "esgoto";
$tdatapublic_geo_face_quadra[".addFields"][] = "pavimento";
$tdatapublic_geo_face_quadra[".addFields"][] = "iluminacao";
$tdatapublic_geo_face_quadra[".addFields"][] = "energia";
$tdatapublic_geo_face_quadra[".addFields"][] = "lixo";
$tdatapublic_geo_face_quadra[".addFields"][] = "enchente";
$tdatapublic_geo_face_quadra[".addFields"][] = "obs";
$tdatapublic_geo_face_quadra[".addFields"][] = "zh";
$tdatapublic_geo_face_quadra[".addFields"][] = "inclusao";

$tdatapublic_geo_face_quadra[".masterListFields"] = array();
$tdatapublic_geo_face_quadra[".masterListFields"][] = "cod_face";
$tdatapublic_geo_face_quadra[".masterListFields"][] = "cod_log";
$tdatapublic_geo_face_quadra[".masterListFields"][] = "tipo_log";
$tdatapublic_geo_face_quadra[".masterListFields"][] = "logradouro";
$tdatapublic_geo_face_quadra[".masterListFields"][] = "bairro";
$tdatapublic_geo_face_quadra[".masterListFields"][] = "vbu_pmt";
$tdatapublic_geo_face_quadra[".masterListFields"][] = "vbu_pvg";
$tdatapublic_geo_face_quadra[".masterListFields"][] = "agua";
$tdatapublic_geo_face_quadra[".masterListFields"][] = "esgoto";
$tdatapublic_geo_face_quadra[".masterListFields"][] = "pavimento";
$tdatapublic_geo_face_quadra[".masterListFields"][] = "iluminacao";
$tdatapublic_geo_face_quadra[".masterListFields"][] = "energia";
$tdatapublic_geo_face_quadra[".masterListFields"][] = "guia";
$tdatapublic_geo_face_quadra[".masterListFields"][] = "lixo";
$tdatapublic_geo_face_quadra[".masterListFields"][] = "drenagem";
$tdatapublic_geo_face_quadra[".masterListFields"][] = "enchente";
$tdatapublic_geo_face_quadra[".masterListFields"][] = "obs";
$tdatapublic_geo_face_quadra[".masterListFields"][] = "transporte";
$tdatapublic_geo_face_quadra[".masterListFields"][] = "zh";
$tdatapublic_geo_face_quadra[".masterListFields"][] = "telefone";
$tdatapublic_geo_face_quadra[".masterListFields"][] = "inclusao";
$tdatapublic_geo_face_quadra[".masterListFields"][] = "limpeza";
$tdatapublic_geo_face_quadra[".masterListFields"][] = "favela";

$tdatapublic_geo_face_quadra[".inlineAddFields"] = array();

$tdatapublic_geo_face_quadra[".editFields"] = array();

$tdatapublic_geo_face_quadra[".inlineEditFields"] = array();
$tdatapublic_geo_face_quadra[".inlineEditFields"][] = "cod_face";
$tdatapublic_geo_face_quadra[".inlineEditFields"][] = "cod_log";
$tdatapublic_geo_face_quadra[".inlineEditFields"][] = "tipo_log";
$tdatapublic_geo_face_quadra[".inlineEditFields"][] = "logradouro";
$tdatapublic_geo_face_quadra[".inlineEditFields"][] = "bairro";
$tdatapublic_geo_face_quadra[".inlineEditFields"][] = "vbu_pvg";
$tdatapublic_geo_face_quadra[".inlineEditFields"][] = "agua";
$tdatapublic_geo_face_quadra[".inlineEditFields"][] = "esgoto";
$tdatapublic_geo_face_quadra[".inlineEditFields"][] = "pavimento";
$tdatapublic_geo_face_quadra[".inlineEditFields"][] = "iluminacao";
$tdatapublic_geo_face_quadra[".inlineEditFields"][] = "energia";
$tdatapublic_geo_face_quadra[".inlineEditFields"][] = "lixo";
$tdatapublic_geo_face_quadra[".inlineEditFields"][] = "enchente";
$tdatapublic_geo_face_quadra[".inlineEditFields"][] = "obs";
$tdatapublic_geo_face_quadra[".inlineEditFields"][] = "zh";

$tdatapublic_geo_face_quadra[".updateSelectedFields"] = array();


$tdatapublic_geo_face_quadra[".exportFields"] = array();
$tdatapublic_geo_face_quadra[".exportFields"][] = "cod_face";
$tdatapublic_geo_face_quadra[".exportFields"][] = "cod_log";
$tdatapublic_geo_face_quadra[".exportFields"][] = "tipo_log";
$tdatapublic_geo_face_quadra[".exportFields"][] = "logradouro";
$tdatapublic_geo_face_quadra[".exportFields"][] = "bairro";
$tdatapublic_geo_face_quadra[".exportFields"][] = "vbu_pmt";
$tdatapublic_geo_face_quadra[".exportFields"][] = "vbu_pvg";
$tdatapublic_geo_face_quadra[".exportFields"][] = "agua";
$tdatapublic_geo_face_quadra[".exportFields"][] = "esgoto";
$tdatapublic_geo_face_quadra[".exportFields"][] = "pavimento";
$tdatapublic_geo_face_quadra[".exportFields"][] = "iluminacao";
$tdatapublic_geo_face_quadra[".exportFields"][] = "energia";
$tdatapublic_geo_face_quadra[".exportFields"][] = "lixo";
$tdatapublic_geo_face_quadra[".exportFields"][] = "enchente";
$tdatapublic_geo_face_quadra[".exportFields"][] = "obs";
$tdatapublic_geo_face_quadra[".exportFields"][] = "zh";
$tdatapublic_geo_face_quadra[".exportFields"][] = "inclusao";

$tdatapublic_geo_face_quadra[".importFields"] = array();

$tdatapublic_geo_face_quadra[".printFields"] = array();
$tdatapublic_geo_face_quadra[".printFields"][] = "cod_face";
$tdatapublic_geo_face_quadra[".printFields"][] = "cod_log";
$tdatapublic_geo_face_quadra[".printFields"][] = "tipo_log";
$tdatapublic_geo_face_quadra[".printFields"][] = "logradouro";
$tdatapublic_geo_face_quadra[".printFields"][] = "bairro";
$tdatapublic_geo_face_quadra[".printFields"][] = "vbu_pmt";
$tdatapublic_geo_face_quadra[".printFields"][] = "vbu_pvg";
$tdatapublic_geo_face_quadra[".printFields"][] = "agua";
$tdatapublic_geo_face_quadra[".printFields"][] = "esgoto";
$tdatapublic_geo_face_quadra[".printFields"][] = "pavimento";
$tdatapublic_geo_face_quadra[".printFields"][] = "iluminacao";
$tdatapublic_geo_face_quadra[".printFields"][] = "energia";
$tdatapublic_geo_face_quadra[".printFields"][] = "lixo";
$tdatapublic_geo_face_quadra[".printFields"][] = "enchente";
$tdatapublic_geo_face_quadra[".printFields"][] = "obs";
$tdatapublic_geo_face_quadra[".printFields"][] = "zh";
$tdatapublic_geo_face_quadra[".printFields"][] = "inclusao";


//	cod_log
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "cod_log";
	$fdata["GoodName"] = "cod_log";
	$fdata["ownerTable"] = "public.geo_face_quadra";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra","cod_log");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
		$fdata["bInlineEdit"] = true;

	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "cod_log";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cod_log";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=254";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra["cod_log"] = $fdata;
//	obs
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "obs";
	$fdata["GoodName"] = "obs";
	$fdata["ownerTable"] = "public.geo_face_quadra";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra","obs");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
		$fdata["bInlineEdit"] = true;

	

	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "obs";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "obs";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=254";

		$edata["controlWidth"] = 254;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_face_quadra["obs"] = $fdata;
//	cod_face
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "cod_face";
	$fdata["GoodName"] = "cod_face";
	$fdata["ownerTable"] = "public.geo_face_quadra";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra","cod_face");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
		$fdata["bInlineEdit"] = true;

	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "cod_face";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cod_face";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=25";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra["cod_face"] = $fdata;
//	tipo_log
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "tipo_log";
	$fdata["GoodName"] = "tipo_log";
	$fdata["ownerTable"] = "public.geo_face_quadra";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra","tipo_log");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
		$fdata["bInlineEdit"] = true;

	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "tipo_log";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tipo_log";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=254";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra["tipo_log"] = $fdata;
//	logradouro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "logradouro";
	$fdata["GoodName"] = "logradouro";
	$fdata["ownerTable"] = "public.geo_face_quadra";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra","logradouro");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
		$fdata["bInlineEdit"] = true;

	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "logradouro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "logradouro";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=500";

		$edata["controlWidth"] = 304;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra["logradouro"] = $fdata;
//	zh
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "zh";
	$fdata["GoodName"] = "zh";
	$fdata["ownerTable"] = "public.geo_face_quadra";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra","zh");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
		$fdata["bInlineEdit"] = true;

	

	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "zh";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "zh";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=25";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_face_quadra["zh"] = $fdata;
//	esgoto
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "esgoto";
	$fdata["GoodName"] = "esgoto";
	$fdata["ownerTable"] = "public.geo_face_quadra";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra","esgoto");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
		$fdata["bInlineEdit"] = true;

	

	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "esgoto";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "esgoto";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

		$edata["HorizontalLookup"] = true;

	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Rede";
	$edata["LookupValues"][] = "Fossa";
	$edata["LookupValues"][] = "Vala";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_face_quadra["esgoto"] = $fdata;
//	telefone
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "telefone";
	$fdata["GoodName"] = "telefone";
	$fdata["ownerTable"] = "public.geo_face_quadra";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra","telefone");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "telefone";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "telefone";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

		$edata["HorizontalLookup"] = true;

	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "S";
	$edata["LookupValues"][] = "N";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_face_quadra["telefone"] = $fdata;
//	agua
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "agua";
	$fdata["GoodName"] = "agua";
	$fdata["ownerTable"] = "public.geo_face_quadra";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra","agua");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
		$fdata["bInlineEdit"] = true;

	

	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "agua";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "agua";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

		$edata["HorizontalLookup"] = true;

	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Rede";
	$edata["LookupValues"][] = "Poço";
	$edata["LookupValues"][] = "Sem";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_face_quadra["agua"] = $fdata;
//	iluminacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "iluminacao";
	$fdata["GoodName"] = "iluminacao";
	$fdata["ownerTable"] = "public.geo_face_quadra";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra","iluminacao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
		$fdata["bInlineEdit"] = true;

	

	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "iluminacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "iluminacao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

		$edata["HorizontalLookup"] = true;

	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Existente";
	$edata["LookupValues"][] = "Inexistente";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_face_quadra["iluminacao"] = $fdata;
//	energia
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "energia";
	$fdata["GoodName"] = "energia";
	$fdata["ownerTable"] = "public.geo_face_quadra";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra","energia");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
		$fdata["bInlineEdit"] = true;

	

	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "energia";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "energia";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

		$edata["HorizontalLookup"] = true;

	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Rede";
	$edata["LookupValues"][] = "Sem";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_face_quadra["energia"] = $fdata;
//	pavimento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "pavimento";
	$fdata["GoodName"] = "pavimento";
	$fdata["ownerTable"] = "public.geo_face_quadra";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra","pavimento");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
		$fdata["bInlineEdit"] = true;

	

	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "pavimento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "pavimento";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

		$edata["HorizontalLookup"] = true;

	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Asfalto";
	$edata["LookupValues"][] = "Demais";
	$edata["LookupValues"][] = "Sem";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_face_quadra["pavimento"] = $fdata;
//	guia
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "guia";
	$fdata["GoodName"] = "guia";
	$fdata["ownerTable"] = "public.geo_face_quadra";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra","guia");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "guia";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "guia";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

		$edata["HorizontalLookup"] = true;

	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "S";
	$edata["LookupValues"][] = "N";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_face_quadra["guia"] = $fdata;
//	lixo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "lixo";
	$fdata["GoodName"] = "lixo";
	$fdata["ownerTable"] = "public.geo_face_quadra";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra","lixo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
		$fdata["bInlineEdit"] = true;

	

	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "lixo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "lixo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

		$edata["HorizontalLookup"] = true;

	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Coleta";
	$edata["LookupValues"][] = "Sem";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_face_quadra["lixo"] = $fdata;
//	transporte
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "transporte";
	$fdata["GoodName"] = "transporte";
	$fdata["ownerTable"] = "public.geo_face_quadra";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra","transporte");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "transporte";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "transporte";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

		$edata["HorizontalLookup"] = true;

	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "S";
	$edata["LookupValues"][] = "N";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_face_quadra["transporte"] = $fdata;
//	vbu_pmt
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "vbu_pmt";
	$fdata["GoodName"] = "vbu_pmt";
	$fdata["ownerTable"] = "public.geo_face_quadra";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra","vbu_pmt");
	$fdata["FieldType"] = 5;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
	
	

	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "vbu_pmt";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "vbu_pmt";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 64;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_face_quadra["vbu_pmt"] = $fdata;
//	vbu_pvg
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "vbu_pvg";
	$fdata["GoodName"] = "vbu_pvg";
	$fdata["ownerTable"] = "public.geo_face_quadra";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra","vbu_pvg");
	$fdata["FieldType"] = 5;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
		$fdata["bInlineEdit"] = true;

	

	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "vbu_pvg";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "vbu_pvg";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 74;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_face_quadra["vbu_pvg"] = $fdata;
//	drenagem
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 18;
	$fdata["strName"] = "drenagem";
	$fdata["GoodName"] = "drenagem";
	$fdata["ownerTable"] = "public.geo_face_quadra";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra","drenagem");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "drenagem";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "drenagem";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

		$edata["HorizontalLookup"] = true;

	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "S";
	$edata["LookupValues"][] = "N";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_face_quadra["drenagem"] = $fdata;
//	bairro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 19;
	$fdata["strName"] = "bairro";
	$fdata["GoodName"] = "bairro";
	$fdata["ownerTable"] = "public.geo_face_quadra";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra","bairro");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
		$fdata["bInlineEdit"] = true;

	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "bairro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "bairro";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 264;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra["bairro"] = $fdata;
//	inclusao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 20;
	$fdata["strName"] = "inclusao";
	$fdata["GoodName"] = "inclusao";
	$fdata["ownerTable"] = "public.geo_face_quadra";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra","inclusao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "inclusao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "inclusao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra["inclusao"] = $fdata;
//	limpeza
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 21;
	$fdata["strName"] = "limpeza";
	$fdata["GoodName"] = "limpeza";
	$fdata["ownerTable"] = "public.geo_face_quadra";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra","limpeza");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "limpeza";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "limpeza";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

		$edata["HorizontalLookup"] = true;

	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "S";
	$edata["LookupValues"][] = "N";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_face_quadra["limpeza"] = $fdata;
//	enchente
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 22;
	$fdata["strName"] = "enchente";
	$fdata["GoodName"] = "enchente";
	$fdata["ownerTable"] = "public.geo_face_quadra";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra","enchente");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
		$fdata["bInlineEdit"] = true;

	

	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "enchente";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "enchente";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

		$edata["HorizontalLookup"] = true;

	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "Sim";
	$edata["LookupValues"][] = "Não";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_face_quadra["enchente"] = $fdata;
//	favela
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 23;
	$fdata["strName"] = "favela";
	$fdata["GoodName"] = "favela";
	$fdata["ownerTable"] = "public.geo_face_quadra";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra","favela");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "favela";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "favela";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	
		
	
// Begin Lookup settings
		$edata["LookupType"] = 0;
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 4;

		$edata["HorizontalLookup"] = true;

	
		$edata["LookupValues"] = array();
	$edata["LookupValues"][] = "S";
	$edata["LookupValues"][] = "N";

	
	
// End Lookup Settings


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_geo_face_quadra["favela"] = $fdata;


$tables_data["public.geo_face_quadra"]=&$tdatapublic_geo_face_quadra;
$field_labels["public_geo_face_quadra"] = &$fieldLabelspublic_geo_face_quadra;
$fieldToolTips["public_geo_face_quadra"] = &$fieldToolTipspublic_geo_face_quadra;
$placeHolders["public_geo_face_quadra"] = &$placeHolderspublic_geo_face_quadra;
$page_titles["public_geo_face_quadra"] = &$pageTitlespublic_geo_face_quadra;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.geo_face_quadra"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.geo_face_quadra"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_geo_face_quadra()
{
$proto1=array();
$proto1["m_strHead"] = "SELECT";
$proto1["m_strFieldList"] = "cod_log,  obs,  cod_face,  tipo_log,  logradouro,  zh,  esgoto,  telefone,  agua,  iluminacao,  energia,  pavimento,  guia,  lixo,  transporte,  vbu_pmt,  vbu_pvg,  drenagem,  bairro,  inclusao,  limpeza,  enchente,  favela";
$proto1["m_strFrom"] = "FROM \"public\".geo_face_quadra";
$proto1["m_strWhere"] = "";
$proto1["m_strOrderBy"] = "ORDER BY cod_face";
	
		;
			$proto1["cipherer"] = null;
$proto3=array();
$proto3["m_sql"] = "";
$proto3["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto3["m_column"]=$obj;
$proto3["m_contained"] = array();
$proto3["m_strCase"] = "";
$proto3["m_havingmode"] = false;
$proto3["m_inBrackets"] = false;
$proto3["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto3);

$proto1["m_where"] = $obj;
$proto5=array();
$proto5["m_sql"] = "";
$proto5["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto5["m_column"]=$obj;
$proto5["m_contained"] = array();
$proto5["m_strCase"] = "";
$proto5["m_havingmode"] = false;
$proto5["m_inBrackets"] = false;
$proto5["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto5);

$proto1["m_having"] = $obj;
$proto1["m_fieldlist"] = array();
						$proto7=array();
			$obj = new SQLField(array(
	"m_strName" => "cod_log",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto7["m_sql"] = "cod_log";
$proto7["m_srcTableName"] = "public.geo_face_quadra";
$proto7["m_expr"]=$obj;
$proto7["m_alias"] = "";
$obj = new SQLFieldListItem($proto7);

$proto1["m_fieldlist"][]=$obj;
						$proto9=array();
			$obj = new SQLField(array(
	"m_strName" => "obs",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto9["m_sql"] = "obs";
$proto9["m_srcTableName"] = "public.geo_face_quadra";
$proto9["m_expr"]=$obj;
$proto9["m_alias"] = "";
$obj = new SQLFieldListItem($proto9);

$proto1["m_fieldlist"][]=$obj;
						$proto11=array();
			$obj = new SQLField(array(
	"m_strName" => "cod_face",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto11["m_sql"] = "cod_face";
$proto11["m_srcTableName"] = "public.geo_face_quadra";
$proto11["m_expr"]=$obj;
$proto11["m_alias"] = "";
$obj = new SQLFieldListItem($proto11);

$proto1["m_fieldlist"][]=$obj;
						$proto13=array();
			$obj = new SQLField(array(
	"m_strName" => "tipo_log",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto13["m_sql"] = "tipo_log";
$proto13["m_srcTableName"] = "public.geo_face_quadra";
$proto13["m_expr"]=$obj;
$proto13["m_alias"] = "";
$obj = new SQLFieldListItem($proto13);

$proto1["m_fieldlist"][]=$obj;
						$proto15=array();
			$obj = new SQLField(array(
	"m_strName" => "logradouro",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto15["m_sql"] = "logradouro";
$proto15["m_srcTableName"] = "public.geo_face_quadra";
$proto15["m_expr"]=$obj;
$proto15["m_alias"] = "";
$obj = new SQLFieldListItem($proto15);

$proto1["m_fieldlist"][]=$obj;
						$proto17=array();
			$obj = new SQLField(array(
	"m_strName" => "zh",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto17["m_sql"] = "zh";
$proto17["m_srcTableName"] = "public.geo_face_quadra";
$proto17["m_expr"]=$obj;
$proto17["m_alias"] = "";
$obj = new SQLFieldListItem($proto17);

$proto1["m_fieldlist"][]=$obj;
						$proto19=array();
			$obj = new SQLField(array(
	"m_strName" => "esgoto",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto19["m_sql"] = "esgoto";
$proto19["m_srcTableName"] = "public.geo_face_quadra";
$proto19["m_expr"]=$obj;
$proto19["m_alias"] = "";
$obj = new SQLFieldListItem($proto19);

$proto1["m_fieldlist"][]=$obj;
						$proto21=array();
			$obj = new SQLField(array(
	"m_strName" => "telefone",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto21["m_sql"] = "telefone";
$proto21["m_srcTableName"] = "public.geo_face_quadra";
$proto21["m_expr"]=$obj;
$proto21["m_alias"] = "";
$obj = new SQLFieldListItem($proto21);

$proto1["m_fieldlist"][]=$obj;
						$proto23=array();
			$obj = new SQLField(array(
	"m_strName" => "agua",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto23["m_sql"] = "agua";
$proto23["m_srcTableName"] = "public.geo_face_quadra";
$proto23["m_expr"]=$obj;
$proto23["m_alias"] = "";
$obj = new SQLFieldListItem($proto23);

$proto1["m_fieldlist"][]=$obj;
						$proto25=array();
			$obj = new SQLField(array(
	"m_strName" => "iluminacao",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto25["m_sql"] = "iluminacao";
$proto25["m_srcTableName"] = "public.geo_face_quadra";
$proto25["m_expr"]=$obj;
$proto25["m_alias"] = "";
$obj = new SQLFieldListItem($proto25);

$proto1["m_fieldlist"][]=$obj;
						$proto27=array();
			$obj = new SQLField(array(
	"m_strName" => "energia",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto27["m_sql"] = "energia";
$proto27["m_srcTableName"] = "public.geo_face_quadra";
$proto27["m_expr"]=$obj;
$proto27["m_alias"] = "";
$obj = new SQLFieldListItem($proto27);

$proto1["m_fieldlist"][]=$obj;
						$proto29=array();
			$obj = new SQLField(array(
	"m_strName" => "pavimento",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto29["m_sql"] = "pavimento";
$proto29["m_srcTableName"] = "public.geo_face_quadra";
$proto29["m_expr"]=$obj;
$proto29["m_alias"] = "";
$obj = new SQLFieldListItem($proto29);

$proto1["m_fieldlist"][]=$obj;
						$proto31=array();
			$obj = new SQLField(array(
	"m_strName" => "guia",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto31["m_sql"] = "guia";
$proto31["m_srcTableName"] = "public.geo_face_quadra";
$proto31["m_expr"]=$obj;
$proto31["m_alias"] = "";
$obj = new SQLFieldListItem($proto31);

$proto1["m_fieldlist"][]=$obj;
						$proto33=array();
			$obj = new SQLField(array(
	"m_strName" => "lixo",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto33["m_sql"] = "lixo";
$proto33["m_srcTableName"] = "public.geo_face_quadra";
$proto33["m_expr"]=$obj;
$proto33["m_alias"] = "";
$obj = new SQLFieldListItem($proto33);

$proto1["m_fieldlist"][]=$obj;
						$proto35=array();
			$obj = new SQLField(array(
	"m_strName" => "transporte",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto35["m_sql"] = "transporte";
$proto35["m_srcTableName"] = "public.geo_face_quadra";
$proto35["m_expr"]=$obj;
$proto35["m_alias"] = "";
$obj = new SQLFieldListItem($proto35);

$proto1["m_fieldlist"][]=$obj;
						$proto37=array();
			$obj = new SQLField(array(
	"m_strName" => "vbu_pmt",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto37["m_sql"] = "vbu_pmt";
$proto37["m_srcTableName"] = "public.geo_face_quadra";
$proto37["m_expr"]=$obj;
$proto37["m_alias"] = "";
$obj = new SQLFieldListItem($proto37);

$proto1["m_fieldlist"][]=$obj;
						$proto39=array();
			$obj = new SQLField(array(
	"m_strName" => "vbu_pvg",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto39["m_sql"] = "vbu_pvg";
$proto39["m_srcTableName"] = "public.geo_face_quadra";
$proto39["m_expr"]=$obj;
$proto39["m_alias"] = "";
$obj = new SQLFieldListItem($proto39);

$proto1["m_fieldlist"][]=$obj;
						$proto41=array();
			$obj = new SQLField(array(
	"m_strName" => "drenagem",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto41["m_sql"] = "drenagem";
$proto41["m_srcTableName"] = "public.geo_face_quadra";
$proto41["m_expr"]=$obj;
$proto41["m_alias"] = "";
$obj = new SQLFieldListItem($proto41);

$proto1["m_fieldlist"][]=$obj;
						$proto43=array();
			$obj = new SQLField(array(
	"m_strName" => "bairro",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto43["m_sql"] = "bairro";
$proto43["m_srcTableName"] = "public.geo_face_quadra";
$proto43["m_expr"]=$obj;
$proto43["m_alias"] = "";
$obj = new SQLFieldListItem($proto43);

$proto1["m_fieldlist"][]=$obj;
						$proto45=array();
			$obj = new SQLField(array(
	"m_strName" => "inclusao",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto45["m_sql"] = "inclusao";
$proto45["m_srcTableName"] = "public.geo_face_quadra";
$proto45["m_expr"]=$obj;
$proto45["m_alias"] = "";
$obj = new SQLFieldListItem($proto45);

$proto1["m_fieldlist"][]=$obj;
						$proto47=array();
			$obj = new SQLField(array(
	"m_strName" => "limpeza",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto47["m_sql"] = "limpeza";
$proto47["m_srcTableName"] = "public.geo_face_quadra";
$proto47["m_expr"]=$obj;
$proto47["m_alias"] = "";
$obj = new SQLFieldListItem($proto47);

$proto1["m_fieldlist"][]=$obj;
						$proto49=array();
			$obj = new SQLField(array(
	"m_strName" => "enchente",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto49["m_sql"] = "enchente";
$proto49["m_srcTableName"] = "public.geo_face_quadra";
$proto49["m_expr"]=$obj;
$proto49["m_alias"] = "";
$obj = new SQLFieldListItem($proto49);

$proto1["m_fieldlist"][]=$obj;
						$proto51=array();
			$obj = new SQLField(array(
	"m_strName" => "favela",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto51["m_sql"] = "favela";
$proto51["m_srcTableName"] = "public.geo_face_quadra";
$proto51["m_expr"]=$obj;
$proto51["m_alias"] = "";
$obj = new SQLFieldListItem($proto51);

$proto1["m_fieldlist"][]=$obj;
$proto1["m_fromlist"] = array();
												$proto53=array();
$proto53["m_link"] = "SQLL_MAIN";
			$proto54=array();
$proto54["m_strName"] = "public.geo_face_quadra";
$proto54["m_srcTableName"] = "public.geo_face_quadra";
$proto54["m_columns"] = array();
$proto54["m_columns"][] = "cod_bairro";
$proto54["m_columns"][] = "cod_quadra";
$proto54["m_columns"][] = "cod_log";
$proto54["m_columns"][] = "zn";
$proto54["m_columns"][] = "grupos_per";
$proto54["m_columns"][] = "obs";
$proto54["m_columns"][] = "cod_face";
$proto54["m_columns"][] = "tipo_log";
$proto54["m_columns"][] = "logradouro";
$proto54["m_columns"][] = "zh";
$proto54["m_columns"][] = "revisao";
$proto54["m_columns"][] = "the_geom";
$proto54["m_columns"][] = "esgoto";
$proto54["m_columns"][] = "telefone";
$proto54["m_columns"][] = "agua";
$proto54["m_columns"][] = "iluminacao";
$proto54["m_columns"][] = "energia";
$proto54["m_columns"][] = "pavimento";
$proto54["m_columns"][] = "guia";
$proto54["m_columns"][] = "lixo";
$proto54["m_columns"][] = "transporte";
$proto54["m_columns"][] = "vbu_pmt";
$proto54["m_columns"][] = "vbu_pvg";
$proto54["m_columns"][] = "relacao_tbl_face_orig";
$proto54["m_columns"][] = "drenagem";
$proto54["m_columns"][] = "bairro";
$proto54["m_columns"][] = "gid";
$proto54["m_columns"][] = "inclusao";
$proto54["m_columns"][] = "limpeza";
$proto54["m_columns"][] = "observacao";
$proto54["m_columns"][] = "zh_mill";
$proto54["m_columns"][] = "paradigma_agua";
$proto54["m_columns"][] = "paradigma_esgoto";
$proto54["m_columns"][] = "paradigma_energia";
$proto54["m_columns"][] = "paradigma_lixo";
$proto54["m_columns"][] = "paradigma_guia";
$proto54["m_columns"][] = "paradigma_iluminacao";
$proto54["m_columns"][] = "paradigma_pavimento";
$proto54["m_columns"][] = "pesquisa";
$proto54["m_columns"][] = "relacionamento_cadastro";
$proto54["m_columns"][] = "coef_agu";
$proto54["m_columns"][] = "coef_esg";
$proto54["m_columns"][] = "coef_ene";
$proto54["m_columns"][] = "coef_lix";
$proto54["m_columns"][] = "coef_gui";
$proto54["m_columns"][] = "coef_ilu";
$proto54["m_columns"][] = "coef_pav";
$proto54["m_columns"][] = "coef_p_agu";
$proto54["m_columns"][] = "coef_p_esg";
$proto54["m_columns"][] = "coef_p_ene";
$proto54["m_columns"][] = "coef_p_lix";
$proto54["m_columns"][] = "coef_p_gui";
$proto54["m_columns"][] = "coef_p_ilu";
$proto54["m_columns"][] = "coef_p_pav";
$proto54["m_columns"][] = "coef_ie_face";
$proto54["m_columns"][] = "centralidade";
$proto54["m_columns"][] = "hier_viaria";
$proto54["m_columns"][] = "polo_comerc";
$proto54["m_columns"][] = "enchente";
$proto54["m_columns"][] = "favela";
$proto54["m_columns"][] = "ff_hier_viaria";
$proto54["m_columns"][] = "ff_polo_comerc";
$proto54["m_columns"][] = "ff_enchente";
$proto54["m_columns"][] = "ff_favela";
$proto54["m_columns"][] = "coef_result_f";
$proto54["m_columns"][] = "vbu_pvg_new_100";
$proto54["m_columns"][] = "vbu_pvg_new";
$proto54["m_columns"][] = "vbu_hom_face";
$proto54["m_columns"][] = "vbu_hom_zh_pesq_med";
$proto54["m_columns"][] = "vbu_hom_zh_pesq_min";
$proto54["m_columns"][] = "vbu_hom_zh_pesq_max";
$proto54["m_columns"][] = "vbu_ajust_zh_pesq_med";
$proto54["m_columns"][] = "vbu_ajust_zh_pesq_min";
$proto54["m_columns"][] = "vbu_ajust_zh_pesq_max";
$proto54["m_columns"][] = "var_abs_pgv_x_valor_ajuste";
$proto54["m_columns"][] = "var_pc_pgv_x_valor_ajuste";
$proto54["m_columns"][] = "data_atualizacao";
$proto54["m_columns"][] = "usuario";
$obj = new SQLTable($proto54);

$proto53["m_table"] = $obj;
$proto53["m_sql"] = "\"public\".geo_face_quadra";
$proto53["m_alias"] = "";
$proto53["m_srcTableName"] = "public.geo_face_quadra";
$proto55=array();
$proto55["m_sql"] = "";
$proto55["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto55["m_column"]=$obj;
$proto55["m_contained"] = array();
$proto55["m_strCase"] = "";
$proto55["m_havingmode"] = false;
$proto55["m_inBrackets"] = false;
$proto55["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto55);

$proto53["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto53);

$proto1["m_fromlist"][]=$obj;
$proto1["m_groupby"] = array();
$proto1["m_orderby"] = array();
												$proto57=array();
						$obj = new SQLField(array(
	"m_strName" => "cod_face",
	"m_strTable" => "public.geo_face_quadra",
	"m_srcTableName" => "public.geo_face_quadra"
));

$proto57["m_column"]=$obj;
$proto57["m_bAsc"] = 1;
$proto57["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto57);

$proto1["m_orderby"][]=$obj;					
$proto1["m_srcTableName"]="public.geo_face_quadra";		
$obj = new SQLQuery($proto1);

	return $obj;
}
$queryData_public_geo_face_quadra = createSqlQuery_public_geo_face_quadra();


	
		;

																							

$tdatapublic_geo_face_quadra[".sqlquery"] = $queryData_public_geo_face_quadra;

include_once(getabspath("include/public_geo_face_quadra_events.php"));
$tableEvents["public.geo_face_quadra"] = new eventclass_public_geo_face_quadra;
$tdatapublic_geo_face_quadra[".hasEvents"] = true;

?>