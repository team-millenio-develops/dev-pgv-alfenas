<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_tbl_aux_dependencia = array();
	$tdatapublic_tbl_aux_dependencia[".truncateText"] = true;
	$tdatapublic_tbl_aux_dependencia[".NumberOfChars"] = 80;
	$tdatapublic_tbl_aux_dependencia[".ShortName"] = "public_tbl_aux_dependencia";
	$tdatapublic_tbl_aux_dependencia[".OwnerID"] = "";
	$tdatapublic_tbl_aux_dependencia[".OriginalTable"] = "public.tbl_aux_dependencia";

//	field labels
$fieldLabelspublic_tbl_aux_dependencia = array();
$fieldToolTipspublic_tbl_aux_dependencia = array();
$pageTitlespublic_tbl_aux_dependencia = array();
$placeHolderspublic_tbl_aux_dependencia = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelspublic_tbl_aux_dependencia["Portuguese(Brazil)"] = array();
	$fieldToolTipspublic_tbl_aux_dependencia["Portuguese(Brazil)"] = array();
	$placeHolderspublic_tbl_aux_dependencia["Portuguese(Brazil)"] = array();
	$pageTitlespublic_tbl_aux_dependencia["Portuguese(Brazil)"] = array();
	$fieldLabelspublic_tbl_aux_dependencia["Portuguese(Brazil)"]["ident"] = "Ident";
	$fieldToolTipspublic_tbl_aux_dependencia["Portuguese(Brazil)"]["ident"] = "";
	$placeHolderspublic_tbl_aux_dependencia["Portuguese(Brazil)"]["ident"] = "";
	$fieldLabelspublic_tbl_aux_dependencia["Portuguese(Brazil)"]["descricao"] = "Descricao";
	$fieldToolTipspublic_tbl_aux_dependencia["Portuguese(Brazil)"]["descricao"] = "";
	$placeHolderspublic_tbl_aux_dependencia["Portuguese(Brazil)"]["descricao"] = "";
	if (count($fieldToolTipspublic_tbl_aux_dependencia["Portuguese(Brazil)"]))
		$tdatapublic_tbl_aux_dependencia[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_tbl_aux_dependencia[""] = array();
	$fieldToolTipspublic_tbl_aux_dependencia[""] = array();
	$placeHolderspublic_tbl_aux_dependencia[""] = array();
	$pageTitlespublic_tbl_aux_dependencia[""] = array();
	if (count($fieldToolTipspublic_tbl_aux_dependencia[""]))
		$tdatapublic_tbl_aux_dependencia[".isUseToolTips"] = true;
}


	$tdatapublic_tbl_aux_dependencia[".NCSearch"] = true;



$tdatapublic_tbl_aux_dependencia[".shortTableName"] = "public_tbl_aux_dependencia";
$tdatapublic_tbl_aux_dependencia[".nSecOptions"] = 0;
$tdatapublic_tbl_aux_dependencia[".recsPerRowPrint"] = 1;
$tdatapublic_tbl_aux_dependencia[".mainTableOwnerID"] = "";
$tdatapublic_tbl_aux_dependencia[".moveNext"] = 1;
$tdatapublic_tbl_aux_dependencia[".entityType"] = 0;

$tdatapublic_tbl_aux_dependencia[".strOriginalTableName"] = "public.tbl_aux_dependencia";

	



$tdatapublic_tbl_aux_dependencia[".showAddInPopup"] = false;

$tdatapublic_tbl_aux_dependencia[".showEditInPopup"] = false;

$tdatapublic_tbl_aux_dependencia[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapublic_tbl_aux_dependencia[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_tbl_aux_dependencia[".fieldsForRegister"] = array();

$tdatapublic_tbl_aux_dependencia[".listAjax"] = false;

	$tdatapublic_tbl_aux_dependencia[".audit"] = false;

	$tdatapublic_tbl_aux_dependencia[".locking"] = false;






$tdatapublic_tbl_aux_dependencia[".reorderRecordsByHeader"] = true;








$tdatapublic_tbl_aux_dependencia[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatapublic_tbl_aux_dependencia[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatapublic_tbl_aux_dependencia[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatapublic_tbl_aux_dependencia[".searchSaving"] = false;
//

$tdatapublic_tbl_aux_dependencia[".showSearchPanel"] = true;
		$tdatapublic_tbl_aux_dependencia[".flexibleSearch"] = true;

$tdatapublic_tbl_aux_dependencia[".isUseAjaxSuggest"] = true;






$tdatapublic_tbl_aux_dependencia[".ajaxCodeSnippetAdded"] = false;

$tdatapublic_tbl_aux_dependencia[".buttonsAdded"] = false;

$tdatapublic_tbl_aux_dependencia[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapublic_tbl_aux_dependencia[".isUseTimeForSearch"] = false;





$tdatapublic_tbl_aux_dependencia[".allSearchFields"] = array();
$tdatapublic_tbl_aux_dependencia[".filterFields"] = array();
$tdatapublic_tbl_aux_dependencia[".requiredSearchFields"] = array();



$tdatapublic_tbl_aux_dependencia[".googleLikeFields"] = array();
$tdatapublic_tbl_aux_dependencia[".googleLikeFields"][] = "ident";
$tdatapublic_tbl_aux_dependencia[".googleLikeFields"][] = "descricao";



$tdatapublic_tbl_aux_dependencia[".tableType"] = "list";

$tdatapublic_tbl_aux_dependencia[".printerPageOrientation"] = 0;
$tdatapublic_tbl_aux_dependencia[".nPrinterPageScale"] = 100;

$tdatapublic_tbl_aux_dependencia[".nPrinterSplitRecords"] = 40;

$tdatapublic_tbl_aux_dependencia[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_tbl_aux_dependencia[".geocodingEnabled"] = false;





$tdatapublic_tbl_aux_dependencia[".listGridLayout"] = 3;

$tdatapublic_tbl_aux_dependencia[".isDisplayLoading"] = true;




// view page pdf

// print page pdf


$tdatapublic_tbl_aux_dependencia[".pageSize"] = 20;

$tdatapublic_tbl_aux_dependencia[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_tbl_aux_dependencia[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_tbl_aux_dependencia[".orderindexes"] = array();

$tdatapublic_tbl_aux_dependencia[".sqlHead"] = "SELECT ident,  	descricao";
$tdatapublic_tbl_aux_dependencia[".sqlFrom"] = "FROM \"public\".tbl_aux_dependencia";
$tdatapublic_tbl_aux_dependencia[".sqlWhereExpr"] = "";
$tdatapublic_tbl_aux_dependencia[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_tbl_aux_dependencia[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_tbl_aux_dependencia[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_tbl_aux_dependencia[".highlightSearchResults"] = true;

$tableKeyspublic_tbl_aux_dependencia = array();
$tdatapublic_tbl_aux_dependencia[".Keys"] = $tableKeyspublic_tbl_aux_dependencia;

$tdatapublic_tbl_aux_dependencia[".listFields"] = array();

$tdatapublic_tbl_aux_dependencia[".hideMobileList"] = array();


$tdatapublic_tbl_aux_dependencia[".viewFields"] = array();

$tdatapublic_tbl_aux_dependencia[".addFields"] = array();

$tdatapublic_tbl_aux_dependencia[".masterListFields"] = array();
$tdatapublic_tbl_aux_dependencia[".masterListFields"][] = "ident";
$tdatapublic_tbl_aux_dependencia[".masterListFields"][] = "descricao";

$tdatapublic_tbl_aux_dependencia[".inlineAddFields"] = array();

$tdatapublic_tbl_aux_dependencia[".editFields"] = array();

$tdatapublic_tbl_aux_dependencia[".inlineEditFields"] = array();

$tdatapublic_tbl_aux_dependencia[".updateSelectedFields"] = array();


$tdatapublic_tbl_aux_dependencia[".exportFields"] = array();

$tdatapublic_tbl_aux_dependencia[".importFields"] = array();

$tdatapublic_tbl_aux_dependencia[".printFields"] = array();


//	ident
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ident";
	$fdata["GoodName"] = "ident";
	$fdata["ownerTable"] = "public.tbl_aux_dependencia";
	$fdata["Label"] = GetFieldLabel("public_tbl_aux_dependencia","ident");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "ident";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ident";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_aux_dependencia["ident"] = $fdata;
//	descricao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "descricao";
	$fdata["GoodName"] = "descricao";
	$fdata["ownerTable"] = "public.tbl_aux_dependencia";
	$fdata["Label"] = GetFieldLabel("public_tbl_aux_dependencia","descricao");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "descricao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "descricao";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_aux_dependencia["descricao"] = $fdata;


$tables_data["public.tbl_aux_dependencia"]=&$tdatapublic_tbl_aux_dependencia;
$field_labels["public_tbl_aux_dependencia"] = &$fieldLabelspublic_tbl_aux_dependencia;
$fieldToolTips["public_tbl_aux_dependencia"] = &$fieldToolTipspublic_tbl_aux_dependencia;
$placeHolders["public_tbl_aux_dependencia"] = &$placeHolderspublic_tbl_aux_dependencia;
$page_titles["public_tbl_aux_dependencia"] = &$pageTitlespublic_tbl_aux_dependencia;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.tbl_aux_dependencia"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.tbl_aux_dependencia"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_tbl_aux_dependencia()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ident,  	descricao";
$proto0["m_strFrom"] = "FROM \"public\".tbl_aux_dependencia";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ident",
	"m_strTable" => "public.tbl_aux_dependencia",
	"m_srcTableName" => "public.tbl_aux_dependencia"
));

$proto6["m_sql"] = "ident";
$proto6["m_srcTableName"] = "public.tbl_aux_dependencia";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "descricao",
	"m_strTable" => "public.tbl_aux_dependencia",
	"m_srcTableName" => "public.tbl_aux_dependencia"
));

$proto8["m_sql"] = "descricao";
$proto8["m_srcTableName"] = "public.tbl_aux_dependencia";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto10=array();
$proto10["m_link"] = "SQLL_MAIN";
			$proto11=array();
$proto11["m_strName"] = "public.tbl_aux_dependencia";
$proto11["m_srcTableName"] = "public.tbl_aux_dependencia";
$proto11["m_columns"] = array();
$proto11["m_columns"][] = "ident";
$proto11["m_columns"][] = "descricao";
$obj = new SQLTable($proto11);

$proto10["m_table"] = $obj;
$proto10["m_sql"] = "\"public\".tbl_aux_dependencia";
$proto10["m_alias"] = "";
$proto10["m_srcTableName"] = "public.tbl_aux_dependencia";
$proto12=array();
$proto12["m_sql"] = "";
$proto12["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto12["m_column"]=$obj;
$proto12["m_contained"] = array();
$proto12["m_strCase"] = "";
$proto12["m_havingmode"] = false;
$proto12["m_inBrackets"] = false;
$proto12["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto12);

$proto10["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto10);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.tbl_aux_dependencia";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_public_tbl_aux_dependencia = createSqlQuery_public_tbl_aux_dependencia();


	
		;

		

$tdatapublic_tbl_aux_dependencia[".sqlquery"] = $queryData_public_tbl_aux_dependencia;

$tableEvents["public.tbl_aux_dependencia"] = new eventsBase;
$tdatapublic_tbl_aux_dependencia[".hasEvents"] = false;

?>