<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_tbl_classes_tipologia = array();
	$tdatapublic_tbl_classes_tipologia[".truncateText"] = true;
	$tdatapublic_tbl_classes_tipologia[".NumberOfChars"] = 80;
	$tdatapublic_tbl_classes_tipologia[".ShortName"] = "public_tbl_classes_tipologia";
	$tdatapublic_tbl_classes_tipologia[".OwnerID"] = "";
	$tdatapublic_tbl_classes_tipologia[".OriginalTable"] = "public.tbl_classes_tipologia";

//	field labels
$fieldLabelspublic_tbl_classes_tipologia = array();
$fieldToolTipspublic_tbl_classes_tipologia = array();
$pageTitlespublic_tbl_classes_tipologia = array();
$placeHolderspublic_tbl_classes_tipologia = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelspublic_tbl_classes_tipologia["Portuguese(Brazil)"] = array();
	$fieldToolTipspublic_tbl_classes_tipologia["Portuguese(Brazil)"] = array();
	$placeHolderspublic_tbl_classes_tipologia["Portuguese(Brazil)"] = array();
	$pageTitlespublic_tbl_classes_tipologia["Portuguese(Brazil)"] = array();
	$fieldLabelspublic_tbl_classes_tipologia["Portuguese(Brazil)"]["ident"] = "Ident";
	$fieldToolTipspublic_tbl_classes_tipologia["Portuguese(Brazil)"]["ident"] = "";
	$placeHolderspublic_tbl_classes_tipologia["Portuguese(Brazil)"]["ident"] = "";
	$fieldLabelspublic_tbl_classes_tipologia["Portuguese(Brazil)"]["classe"] = "Classe";
	$fieldToolTipspublic_tbl_classes_tipologia["Portuguese(Brazil)"]["classe"] = "";
	$placeHolderspublic_tbl_classes_tipologia["Portuguese(Brazil)"]["classe"] = "";
	$fieldLabelspublic_tbl_classes_tipologia["Portuguese(Brazil)"]["tipo"] = "Tipo";
	$fieldToolTipspublic_tbl_classes_tipologia["Portuguese(Brazil)"]["tipo"] = "";
	$placeHolderspublic_tbl_classes_tipologia["Portuguese(Brazil)"]["tipo"] = "";
	$fieldLabelspublic_tbl_classes_tipologia["Portuguese(Brazil)"]["padrao"] = "Padrao";
	$fieldToolTipspublic_tbl_classes_tipologia["Portuguese(Brazil)"]["padrao"] = "";
	$placeHolderspublic_tbl_classes_tipologia["Portuguese(Brazil)"]["padrao"] = "";
	$fieldLabelspublic_tbl_classes_tipologia["Portuguese(Brazil)"]["descricao"] = "Descricao";
	$fieldToolTipspublic_tbl_classes_tipologia["Portuguese(Brazil)"]["descricao"] = "";
	$placeHolderspublic_tbl_classes_tipologia["Portuguese(Brazil)"]["descricao"] = "";
	$fieldLabelspublic_tbl_classes_tipologia["Portuguese(Brazil)"]["fotos"] = "Fotos";
	$fieldToolTipspublic_tbl_classes_tipologia["Portuguese(Brazil)"]["fotos"] = "";
	$placeHolderspublic_tbl_classes_tipologia["Portuguese(Brazil)"]["fotos"] = "";
	if (count($fieldToolTipspublic_tbl_classes_tipologia["Portuguese(Brazil)"]))
		$tdatapublic_tbl_classes_tipologia[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_tbl_classes_tipologia[""] = array();
	$fieldToolTipspublic_tbl_classes_tipologia[""] = array();
	$placeHolderspublic_tbl_classes_tipologia[""] = array();
	$pageTitlespublic_tbl_classes_tipologia[""] = array();
	if (count($fieldToolTipspublic_tbl_classes_tipologia[""]))
		$tdatapublic_tbl_classes_tipologia[".isUseToolTips"] = true;
}


	$tdatapublic_tbl_classes_tipologia[".NCSearch"] = true;



$tdatapublic_tbl_classes_tipologia[".shortTableName"] = "public_tbl_classes_tipologia";
$tdatapublic_tbl_classes_tipologia[".nSecOptions"] = 0;
$tdatapublic_tbl_classes_tipologia[".recsPerRowPrint"] = 1;
$tdatapublic_tbl_classes_tipologia[".mainTableOwnerID"] = "";
$tdatapublic_tbl_classes_tipologia[".moveNext"] = 1;
$tdatapublic_tbl_classes_tipologia[".entityType"] = 0;

$tdatapublic_tbl_classes_tipologia[".strOriginalTableName"] = "public.tbl_classes_tipologia";

	



$tdatapublic_tbl_classes_tipologia[".showAddInPopup"] = false;

$tdatapublic_tbl_classes_tipologia[".showEditInPopup"] = false;

$tdatapublic_tbl_classes_tipologia[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapublic_tbl_classes_tipologia[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_tbl_classes_tipologia[".fieldsForRegister"] = array();

$tdatapublic_tbl_classes_tipologia[".listAjax"] = false;

	$tdatapublic_tbl_classes_tipologia[".audit"] = false;

	$tdatapublic_tbl_classes_tipologia[".locking"] = false;

$tdatapublic_tbl_classes_tipologia[".edit"] = true;
$tdatapublic_tbl_classes_tipologia[".afterEditAction"] = 1;
$tdatapublic_tbl_classes_tipologia[".closePopupAfterEdit"] = 1;
$tdatapublic_tbl_classes_tipologia[".afterEditActionDetTable"] = "";

$tdatapublic_tbl_classes_tipologia[".add"] = true;
$tdatapublic_tbl_classes_tipologia[".afterAddAction"] = 1;
$tdatapublic_tbl_classes_tipologia[".closePopupAfterAdd"] = 1;
$tdatapublic_tbl_classes_tipologia[".afterAddActionDetTable"] = "";

$tdatapublic_tbl_classes_tipologia[".list"] = true;



$tdatapublic_tbl_classes_tipologia[".reorderRecordsByHeader"] = true;






$tdatapublic_tbl_classes_tipologia[".printFriendly"] = true;

$tdatapublic_tbl_classes_tipologia[".delete"] = true;

$tdatapublic_tbl_classes_tipologia[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatapublic_tbl_classes_tipologia[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatapublic_tbl_classes_tipologia[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatapublic_tbl_classes_tipologia[".searchSaving"] = false;
//

$tdatapublic_tbl_classes_tipologia[".showSearchPanel"] = true;
		$tdatapublic_tbl_classes_tipologia[".flexibleSearch"] = true;

$tdatapublic_tbl_classes_tipologia[".isUseAjaxSuggest"] = true;






$tdatapublic_tbl_classes_tipologia[".ajaxCodeSnippetAdded"] = false;

$tdatapublic_tbl_classes_tipologia[".buttonsAdded"] = false;

$tdatapublic_tbl_classes_tipologia[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapublic_tbl_classes_tipologia[".isUseTimeForSearch"] = false;





$tdatapublic_tbl_classes_tipologia[".allSearchFields"] = array();
$tdatapublic_tbl_classes_tipologia[".filterFields"] = array();
$tdatapublic_tbl_classes_tipologia[".requiredSearchFields"] = array();

$tdatapublic_tbl_classes_tipologia[".allSearchFields"][] = "classe";
	$tdatapublic_tbl_classes_tipologia[".allSearchFields"][] = "tipo";
	$tdatapublic_tbl_classes_tipologia[".allSearchFields"][] = "padrao";
	$tdatapublic_tbl_classes_tipologia[".allSearchFields"][] = "descricao";
	$tdatapublic_tbl_classes_tipologia[".allSearchFields"][] = "fotos";
	

$tdatapublic_tbl_classes_tipologia[".googleLikeFields"] = array();
$tdatapublic_tbl_classes_tipologia[".googleLikeFields"][] = "ident";
$tdatapublic_tbl_classes_tipologia[".googleLikeFields"][] = "classe";
$tdatapublic_tbl_classes_tipologia[".googleLikeFields"][] = "tipo";
$tdatapublic_tbl_classes_tipologia[".googleLikeFields"][] = "padrao";
$tdatapublic_tbl_classes_tipologia[".googleLikeFields"][] = "descricao";
$tdatapublic_tbl_classes_tipologia[".googleLikeFields"][] = "fotos";


$tdatapublic_tbl_classes_tipologia[".advSearchFields"] = array();
$tdatapublic_tbl_classes_tipologia[".advSearchFields"][] = "classe";
$tdatapublic_tbl_classes_tipologia[".advSearchFields"][] = "tipo";
$tdatapublic_tbl_classes_tipologia[".advSearchFields"][] = "padrao";
$tdatapublic_tbl_classes_tipologia[".advSearchFields"][] = "descricao";
$tdatapublic_tbl_classes_tipologia[".advSearchFields"][] = "fotos";

$tdatapublic_tbl_classes_tipologia[".tableType"] = "list";

$tdatapublic_tbl_classes_tipologia[".printerPageOrientation"] = 0;
$tdatapublic_tbl_classes_tipologia[".nPrinterPageScale"] = 100;

$tdatapublic_tbl_classes_tipologia[".nPrinterSplitRecords"] = 40;

$tdatapublic_tbl_classes_tipologia[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_tbl_classes_tipologia[".geocodingEnabled"] = false;





$tdatapublic_tbl_classes_tipologia[".listGridLayout"] = 3;

$tdatapublic_tbl_classes_tipologia[".isDisplayLoading"] = true;




// view page pdf

// print page pdf


$tdatapublic_tbl_classes_tipologia[".pageSize"] = 20;

$tdatapublic_tbl_classes_tipologia[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_tbl_classes_tipologia[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_tbl_classes_tipologia[".orderindexes"] = array();

$tdatapublic_tbl_classes_tipologia[".sqlHead"] = "SELECT ident,  	classe,  	tipo,  	padrao,  	descricao,  	fotos";
$tdatapublic_tbl_classes_tipologia[".sqlFrom"] = "FROM \"public\".tbl_classes_tipologia";
$tdatapublic_tbl_classes_tipologia[".sqlWhereExpr"] = "";
$tdatapublic_tbl_classes_tipologia[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_tbl_classes_tipologia[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_tbl_classes_tipologia[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_tbl_classes_tipologia[".highlightSearchResults"] = true;

$tableKeyspublic_tbl_classes_tipologia = array();
$tableKeyspublic_tbl_classes_tipologia[] = "ident";
$tdatapublic_tbl_classes_tipologia[".Keys"] = $tableKeyspublic_tbl_classes_tipologia;

$tdatapublic_tbl_classes_tipologia[".listFields"] = array();
$tdatapublic_tbl_classes_tipologia[".listFields"][] = "classe";
$tdatapublic_tbl_classes_tipologia[".listFields"][] = "tipo";
$tdatapublic_tbl_classes_tipologia[".listFields"][] = "padrao";
$tdatapublic_tbl_classes_tipologia[".listFields"][] = "descricao";
$tdatapublic_tbl_classes_tipologia[".listFields"][] = "fotos";

$tdatapublic_tbl_classes_tipologia[".hideMobileList"] = array();


$tdatapublic_tbl_classes_tipologia[".viewFields"] = array();

$tdatapublic_tbl_classes_tipologia[".addFields"] = array();
$tdatapublic_tbl_classes_tipologia[".addFields"][] = "classe";
$tdatapublic_tbl_classes_tipologia[".addFields"][] = "tipo";
$tdatapublic_tbl_classes_tipologia[".addFields"][] = "padrao";
$tdatapublic_tbl_classes_tipologia[".addFields"][] = "descricao";
$tdatapublic_tbl_classes_tipologia[".addFields"][] = "fotos";

$tdatapublic_tbl_classes_tipologia[".masterListFields"] = array();
$tdatapublic_tbl_classes_tipologia[".masterListFields"][] = "ident";
$tdatapublic_tbl_classes_tipologia[".masterListFields"][] = "classe";
$tdatapublic_tbl_classes_tipologia[".masterListFields"][] = "tipo";
$tdatapublic_tbl_classes_tipologia[".masterListFields"][] = "padrao";
$tdatapublic_tbl_classes_tipologia[".masterListFields"][] = "descricao";
$tdatapublic_tbl_classes_tipologia[".masterListFields"][] = "fotos";

$tdatapublic_tbl_classes_tipologia[".inlineAddFields"] = array();

$tdatapublic_tbl_classes_tipologia[".editFields"] = array();
$tdatapublic_tbl_classes_tipologia[".editFields"][] = "classe";
$tdatapublic_tbl_classes_tipologia[".editFields"][] = "tipo";
$tdatapublic_tbl_classes_tipologia[".editFields"][] = "padrao";
$tdatapublic_tbl_classes_tipologia[".editFields"][] = "descricao";
$tdatapublic_tbl_classes_tipologia[".editFields"][] = "fotos";

$tdatapublic_tbl_classes_tipologia[".inlineEditFields"] = array();

$tdatapublic_tbl_classes_tipologia[".updateSelectedFields"] = array();
$tdatapublic_tbl_classes_tipologia[".updateSelectedFields"][] = "classe";
$tdatapublic_tbl_classes_tipologia[".updateSelectedFields"][] = "tipo";
$tdatapublic_tbl_classes_tipologia[".updateSelectedFields"][] = "padrao";
$tdatapublic_tbl_classes_tipologia[".updateSelectedFields"][] = "descricao";
$tdatapublic_tbl_classes_tipologia[".updateSelectedFields"][] = "fotos";


$tdatapublic_tbl_classes_tipologia[".exportFields"] = array();

$tdatapublic_tbl_classes_tipologia[".importFields"] = array();

$tdatapublic_tbl_classes_tipologia[".printFields"] = array();
$tdatapublic_tbl_classes_tipologia[".printFields"][] = "classe";
$tdatapublic_tbl_classes_tipologia[".printFields"][] = "tipo";
$tdatapublic_tbl_classes_tipologia[".printFields"][] = "padrao";
$tdatapublic_tbl_classes_tipologia[".printFields"][] = "descricao";
$tdatapublic_tbl_classes_tipologia[".printFields"][] = "fotos";


//	ident
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ident";
	$fdata["GoodName"] = "ident";
	$fdata["ownerTable"] = "public.tbl_classes_tipologia";
	$fdata["Label"] = GetFieldLabel("public_tbl_classes_tipologia","ident");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "ident";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ident";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_classes_tipologia["ident"] = $fdata;
//	classe
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "classe";
	$fdata["GoodName"] = "classe";
	$fdata["ownerTable"] = "public.tbl_classes_tipologia";
	$fdata["Label"] = GetFieldLabel("public_tbl_classes_tipologia","classe");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "classe";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "classe";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=25";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_classes_tipologia["classe"] = $fdata;
//	tipo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "tipo";
	$fdata["GoodName"] = "tipo";
	$fdata["ownerTable"] = "public.tbl_classes_tipologia";
	$fdata["Label"] = GetFieldLabel("public_tbl_classes_tipologia","tipo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "tipo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tipo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=25";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_classes_tipologia["tipo"] = $fdata;
//	padrao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "padrao";
	$fdata["GoodName"] = "padrao";
	$fdata["ownerTable"] = "public.tbl_classes_tipologia";
	$fdata["Label"] = GetFieldLabel("public_tbl_classes_tipologia","padrao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "padrao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "padrao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=25";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_classes_tipologia["padrao"] = $fdata;
//	descricao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "descricao";
	$fdata["GoodName"] = "descricao";
	$fdata["ownerTable"] = "public.tbl_classes_tipologia";
	$fdata["Label"] = GetFieldLabel("public_tbl_classes_tipologia","descricao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "descricao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "descricao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10000";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_classes_tipologia["descricao"] = $fdata;
//	fotos
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "fotos";
	$fdata["GoodName"] = "fotos";
	$fdata["ownerTable"] = "public.tbl_classes_tipologia";
	$fdata["Label"] = GetFieldLabel("public_tbl_classes_tipologia","fotos");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
		$fdata["bUpdateSelected"] = true;


	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "fotos";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fotos";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10000";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_classes_tipologia["fotos"] = $fdata;


$tables_data["public.tbl_classes_tipologia"]=&$tdatapublic_tbl_classes_tipologia;
$field_labels["public_tbl_classes_tipologia"] = &$fieldLabelspublic_tbl_classes_tipologia;
$fieldToolTips["public_tbl_classes_tipologia"] = &$fieldToolTipspublic_tbl_classes_tipologia;
$placeHolders["public_tbl_classes_tipologia"] = &$placeHolderspublic_tbl_classes_tipologia;
$page_titles["public_tbl_classes_tipologia"] = &$pageTitlespublic_tbl_classes_tipologia;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.tbl_classes_tipologia"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.tbl_classes_tipologia"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_tbl_classes_tipologia()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ident,  	classe,  	tipo,  	padrao,  	descricao,  	fotos";
$proto0["m_strFrom"] = "FROM \"public\".tbl_classes_tipologia";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ident",
	"m_strTable" => "public.tbl_classes_tipologia",
	"m_srcTableName" => "public.tbl_classes_tipologia"
));

$proto6["m_sql"] = "ident";
$proto6["m_srcTableName"] = "public.tbl_classes_tipologia";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "classe",
	"m_strTable" => "public.tbl_classes_tipologia",
	"m_srcTableName" => "public.tbl_classes_tipologia"
));

$proto8["m_sql"] = "classe";
$proto8["m_srcTableName"] = "public.tbl_classes_tipologia";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "tipo",
	"m_strTable" => "public.tbl_classes_tipologia",
	"m_srcTableName" => "public.tbl_classes_tipologia"
));

$proto10["m_sql"] = "tipo";
$proto10["m_srcTableName"] = "public.tbl_classes_tipologia";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "padrao",
	"m_strTable" => "public.tbl_classes_tipologia",
	"m_srcTableName" => "public.tbl_classes_tipologia"
));

$proto12["m_sql"] = "padrao";
$proto12["m_srcTableName"] = "public.tbl_classes_tipologia";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "descricao",
	"m_strTable" => "public.tbl_classes_tipologia",
	"m_srcTableName" => "public.tbl_classes_tipologia"
));

$proto14["m_sql"] = "descricao";
$proto14["m_srcTableName"] = "public.tbl_classes_tipologia";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "fotos",
	"m_strTable" => "public.tbl_classes_tipologia",
	"m_srcTableName" => "public.tbl_classes_tipologia"
));

$proto16["m_sql"] = "fotos";
$proto16["m_srcTableName"] = "public.tbl_classes_tipologia";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto18=array();
$proto18["m_link"] = "SQLL_MAIN";
			$proto19=array();
$proto19["m_strName"] = "public.tbl_classes_tipologia";
$proto19["m_srcTableName"] = "public.tbl_classes_tipologia";
$proto19["m_columns"] = array();
$proto19["m_columns"][] = "ident";
$proto19["m_columns"][] = "classe";
$proto19["m_columns"][] = "tipo";
$proto19["m_columns"][] = "padrao";
$proto19["m_columns"][] = "descricao";
$proto19["m_columns"][] = "fotos";
$obj = new SQLTable($proto19);

$proto18["m_table"] = $obj;
$proto18["m_sql"] = "\"public\".tbl_classes_tipologia";
$proto18["m_alias"] = "";
$proto18["m_srcTableName"] = "public.tbl_classes_tipologia";
$proto20=array();
$proto20["m_sql"] = "";
$proto20["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto20["m_column"]=$obj;
$proto20["m_contained"] = array();
$proto20["m_strCase"] = "";
$proto20["m_havingmode"] = false;
$proto20["m_inBrackets"] = false;
$proto20["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto20);

$proto18["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto18);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.tbl_classes_tipologia";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_public_tbl_classes_tipologia = createSqlQuery_public_tbl_classes_tipologia();


	
		;

						

$tdatapublic_tbl_classes_tipologia[".sqlquery"] = $queryData_public_tbl_classes_tipologia;

$tableEvents["public.tbl_classes_tipologia"] = new eventsBase;
$tdatapublic_tbl_classes_tipologia[".hasEvents"] = false;

?>