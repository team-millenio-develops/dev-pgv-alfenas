<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_tbl_aux_classe = array();
	$tdatapublic_tbl_aux_classe[".truncateText"] = true;
	$tdatapublic_tbl_aux_classe[".NumberOfChars"] = 80;
	$tdatapublic_tbl_aux_classe[".ShortName"] = "public_tbl_aux_classe";
	$tdatapublic_tbl_aux_classe[".OwnerID"] = "";
	$tdatapublic_tbl_aux_classe[".OriginalTable"] = "public.tbl_aux_classe";

//	field labels
$fieldLabelspublic_tbl_aux_classe = array();
$fieldToolTipspublic_tbl_aux_classe = array();
$pageTitlespublic_tbl_aux_classe = array();
$placeHolderspublic_tbl_aux_classe = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelspublic_tbl_aux_classe["Portuguese(Brazil)"] = array();
	$fieldToolTipspublic_tbl_aux_classe["Portuguese(Brazil)"] = array();
	$placeHolderspublic_tbl_aux_classe["Portuguese(Brazil)"] = array();
	$pageTitlespublic_tbl_aux_classe["Portuguese(Brazil)"] = array();
	$fieldLabelspublic_tbl_aux_classe["Portuguese(Brazil)"]["ident"] = "Ident";
	$fieldToolTipspublic_tbl_aux_classe["Portuguese(Brazil)"]["ident"] = "";
	$placeHolderspublic_tbl_aux_classe["Portuguese(Brazil)"]["ident"] = "";
	$fieldLabelspublic_tbl_aux_classe["Portuguese(Brazil)"]["descricao"] = "Descricao";
	$fieldToolTipspublic_tbl_aux_classe["Portuguese(Brazil)"]["descricao"] = "";
	$placeHolderspublic_tbl_aux_classe["Portuguese(Brazil)"]["descricao"] = "";
	if (count($fieldToolTipspublic_tbl_aux_classe["Portuguese(Brazil)"]))
		$tdatapublic_tbl_aux_classe[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_tbl_aux_classe[""] = array();
	$fieldToolTipspublic_tbl_aux_classe[""] = array();
	$placeHolderspublic_tbl_aux_classe[""] = array();
	$pageTitlespublic_tbl_aux_classe[""] = array();
	if (count($fieldToolTipspublic_tbl_aux_classe[""]))
		$tdatapublic_tbl_aux_classe[".isUseToolTips"] = true;
}


	$tdatapublic_tbl_aux_classe[".NCSearch"] = true;



$tdatapublic_tbl_aux_classe[".shortTableName"] = "public_tbl_aux_classe";
$tdatapublic_tbl_aux_classe[".nSecOptions"] = 0;
$tdatapublic_tbl_aux_classe[".recsPerRowPrint"] = 1;
$tdatapublic_tbl_aux_classe[".mainTableOwnerID"] = "";
$tdatapublic_tbl_aux_classe[".moveNext"] = 1;
$tdatapublic_tbl_aux_classe[".entityType"] = 0;

$tdatapublic_tbl_aux_classe[".strOriginalTableName"] = "public.tbl_aux_classe";

	



$tdatapublic_tbl_aux_classe[".showAddInPopup"] = false;

$tdatapublic_tbl_aux_classe[".showEditInPopup"] = false;

$tdatapublic_tbl_aux_classe[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapublic_tbl_aux_classe[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_tbl_aux_classe[".fieldsForRegister"] = array();

$tdatapublic_tbl_aux_classe[".listAjax"] = false;

	$tdatapublic_tbl_aux_classe[".audit"] = false;

	$tdatapublic_tbl_aux_classe[".locking"] = false;






$tdatapublic_tbl_aux_classe[".reorderRecordsByHeader"] = true;








$tdatapublic_tbl_aux_classe[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatapublic_tbl_aux_classe[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatapublic_tbl_aux_classe[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatapublic_tbl_aux_classe[".searchSaving"] = false;
//

$tdatapublic_tbl_aux_classe[".showSearchPanel"] = true;
		$tdatapublic_tbl_aux_classe[".flexibleSearch"] = true;

$tdatapublic_tbl_aux_classe[".isUseAjaxSuggest"] = true;






$tdatapublic_tbl_aux_classe[".ajaxCodeSnippetAdded"] = false;

$tdatapublic_tbl_aux_classe[".buttonsAdded"] = false;

$tdatapublic_tbl_aux_classe[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapublic_tbl_aux_classe[".isUseTimeForSearch"] = false;





$tdatapublic_tbl_aux_classe[".allSearchFields"] = array();
$tdatapublic_tbl_aux_classe[".filterFields"] = array();
$tdatapublic_tbl_aux_classe[".requiredSearchFields"] = array();



$tdatapublic_tbl_aux_classe[".googleLikeFields"] = array();
$tdatapublic_tbl_aux_classe[".googleLikeFields"][] = "ident";
$tdatapublic_tbl_aux_classe[".googleLikeFields"][] = "descricao";



$tdatapublic_tbl_aux_classe[".tableType"] = "list";

$tdatapublic_tbl_aux_classe[".printerPageOrientation"] = 0;
$tdatapublic_tbl_aux_classe[".nPrinterPageScale"] = 100;

$tdatapublic_tbl_aux_classe[".nPrinterSplitRecords"] = 40;

$tdatapublic_tbl_aux_classe[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_tbl_aux_classe[".geocodingEnabled"] = false;





$tdatapublic_tbl_aux_classe[".listGridLayout"] = 3;

$tdatapublic_tbl_aux_classe[".isDisplayLoading"] = true;




// view page pdf

// print page pdf


$tdatapublic_tbl_aux_classe[".pageSize"] = 20;

$tdatapublic_tbl_aux_classe[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_tbl_aux_classe[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_tbl_aux_classe[".orderindexes"] = array();

$tdatapublic_tbl_aux_classe[".sqlHead"] = "SELECT ident,  	descricao";
$tdatapublic_tbl_aux_classe[".sqlFrom"] = "FROM \"public\".tbl_aux_classe";
$tdatapublic_tbl_aux_classe[".sqlWhereExpr"] = "";
$tdatapublic_tbl_aux_classe[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_tbl_aux_classe[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_tbl_aux_classe[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_tbl_aux_classe[".highlightSearchResults"] = true;

$tableKeyspublic_tbl_aux_classe = array();
$tableKeyspublic_tbl_aux_classe[] = "ident";
$tdatapublic_tbl_aux_classe[".Keys"] = $tableKeyspublic_tbl_aux_classe;

$tdatapublic_tbl_aux_classe[".listFields"] = array();

$tdatapublic_tbl_aux_classe[".hideMobileList"] = array();


$tdatapublic_tbl_aux_classe[".viewFields"] = array();

$tdatapublic_tbl_aux_classe[".addFields"] = array();

$tdatapublic_tbl_aux_classe[".masterListFields"] = array();
$tdatapublic_tbl_aux_classe[".masterListFields"][] = "ident";
$tdatapublic_tbl_aux_classe[".masterListFields"][] = "descricao";

$tdatapublic_tbl_aux_classe[".inlineAddFields"] = array();

$tdatapublic_tbl_aux_classe[".editFields"] = array();

$tdatapublic_tbl_aux_classe[".inlineEditFields"] = array();

$tdatapublic_tbl_aux_classe[".updateSelectedFields"] = array();


$tdatapublic_tbl_aux_classe[".exportFields"] = array();

$tdatapublic_tbl_aux_classe[".importFields"] = array();

$tdatapublic_tbl_aux_classe[".printFields"] = array();


//	ident
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ident";
	$fdata["GoodName"] = "ident";
	$fdata["ownerTable"] = "public.tbl_aux_classe";
	$fdata["Label"] = GetFieldLabel("public_tbl_aux_classe","ident");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "ident";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "ident";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_aux_classe["ident"] = $fdata;
//	descricao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "descricao";
	$fdata["GoodName"] = "descricao";
	$fdata["ownerTable"] = "public.tbl_aux_classe";
	$fdata["Label"] = GetFieldLabel("public_tbl_aux_classe","descricao");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "descricao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "descricao";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=25";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_aux_classe["descricao"] = $fdata;


$tables_data["public.tbl_aux_classe"]=&$tdatapublic_tbl_aux_classe;
$field_labels["public_tbl_aux_classe"] = &$fieldLabelspublic_tbl_aux_classe;
$fieldToolTips["public_tbl_aux_classe"] = &$fieldToolTipspublic_tbl_aux_classe;
$placeHolders["public_tbl_aux_classe"] = &$placeHolderspublic_tbl_aux_classe;
$page_titles["public_tbl_aux_classe"] = &$pageTitlespublic_tbl_aux_classe;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.tbl_aux_classe"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.tbl_aux_classe"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_tbl_aux_classe()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "ident,  	descricao";
$proto0["m_strFrom"] = "FROM \"public\".tbl_aux_classe";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ident",
	"m_strTable" => "public.tbl_aux_classe",
	"m_srcTableName" => "public.tbl_aux_classe"
));

$proto6["m_sql"] = "ident";
$proto6["m_srcTableName"] = "public.tbl_aux_classe";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "descricao",
	"m_strTable" => "public.tbl_aux_classe",
	"m_srcTableName" => "public.tbl_aux_classe"
));

$proto8["m_sql"] = "descricao";
$proto8["m_srcTableName"] = "public.tbl_aux_classe";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto10=array();
$proto10["m_link"] = "SQLL_MAIN";
			$proto11=array();
$proto11["m_strName"] = "public.tbl_aux_classe";
$proto11["m_srcTableName"] = "public.tbl_aux_classe";
$proto11["m_columns"] = array();
$proto11["m_columns"][] = "ident";
$proto11["m_columns"][] = "descricao";
$obj = new SQLTable($proto11);

$proto10["m_table"] = $obj;
$proto10["m_sql"] = "\"public\".tbl_aux_classe";
$proto10["m_alias"] = "";
$proto10["m_srcTableName"] = "public.tbl_aux_classe";
$proto12=array();
$proto12["m_sql"] = "";
$proto12["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto12["m_column"]=$obj;
$proto12["m_contained"] = array();
$proto12["m_strCase"] = "";
$proto12["m_havingmode"] = false;
$proto12["m_inBrackets"] = false;
$proto12["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto12);

$proto10["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto10);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.tbl_aux_classe";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_public_tbl_aux_classe = createSqlQuery_public_tbl_aux_classe();


	
		;

		

$tdatapublic_tbl_aux_classe[".sqlquery"] = $queryData_public_tbl_aux_classe;

$tableEvents["public.tbl_aux_classe"] = new eventsBase;
$tdatapublic_tbl_aux_classe[".hasEvents"] = false;

?>