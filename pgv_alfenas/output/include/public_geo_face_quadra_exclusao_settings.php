<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_geo_face_quadra_exclusao = array();
	$tdatapublic_geo_face_quadra_exclusao[".truncateText"] = true;
	$tdatapublic_geo_face_quadra_exclusao[".NumberOfChars"] = 80;
	$tdatapublic_geo_face_quadra_exclusao[".ShortName"] = "public_geo_face_quadra_exclusao";
	$tdatapublic_geo_face_quadra_exclusao[".OwnerID"] = "";
	$tdatapublic_geo_face_quadra_exclusao[".OriginalTable"] = "public.geo_face_quadra_exclusao";

//	field labels
$fieldLabelspublic_geo_face_quadra_exclusao = array();
$fieldToolTipspublic_geo_face_quadra_exclusao = array();
$pageTitlespublic_geo_face_quadra_exclusao = array();
$placeHolderspublic_geo_face_quadra_exclusao = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"] = array();
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"] = array();
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"] = array();
	$pageTitlespublic_geo_face_quadra_exclusao["Portuguese(Brazil)"] = array();
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["cod_bairro"] = "Cod Bairro";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["cod_bairro"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["cod_bairro"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["cod_quadra"] = "Cod Quadra";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["cod_quadra"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["cod_quadra"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["cod_log"] = "Cod Log";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["cod_log"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["cod_log"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["zn"] = "Zn";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["zn"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["zn"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["grupos_per"] = "Grupos Per";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["grupos_per"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["grupos_per"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["obs"] = "Obs";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["obs"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["obs"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["cod_face"] = "Cod Face";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["cod_face"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["cod_face"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["tipo_log"] = "Tipo Log";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["tipo_log"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["tipo_log"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["logradouro"] = "Logradouro";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["logradouro"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["logradouro"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["zh"] = "Zh";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["zh"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["zh"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["revisao"] = "Revisao";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["revisao"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["revisao"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["the_geom"] = "The Geom";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["the_geom"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["the_geom"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["esgoto"] = "Esgoto";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["esgoto"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["esgoto"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["telefone"] = "Telefone";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["telefone"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["telefone"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["agua"] = "Agua";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["agua"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["agua"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["iluminacao"] = "Iluminacao";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["iluminacao"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["iluminacao"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["energia"] = "Energia";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["energia"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["energia"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["pavimento"] = "Pavimento";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["pavimento"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["pavimento"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["guia"] = "Guia";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["guia"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["guia"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["lixo"] = "Lixo";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["lixo"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["lixo"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["transporte"] = "Transporte";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["transporte"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["transporte"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["vbu_pmt"] = "Vbu Pmt";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["vbu_pmt"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["vbu_pmt"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["vbu_pvg"] = "Vbu Pvg";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["vbu_pvg"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["vbu_pvg"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["relacao_tbl_face_orig"] = "Relacao Tbl Face Orig";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["relacao_tbl_face_orig"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["relacao_tbl_face_orig"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["drenagem"] = "Drenagem";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["drenagem"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["drenagem"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["bairro"] = "Bairro";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["bairro"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["bairro"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["gid"] = "Gid";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["gid"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["gid"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["inclusao"] = "Inclusao";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["inclusao"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["inclusao"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["limpeza"] = "Limpeza";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["limpeza"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["limpeza"] = "";
	$fieldLabelspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["observacao"] = "Observacao";
	$fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["observacao"] = "";
	$placeHolderspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]["observacao"] = "";
	if (count($fieldToolTipspublic_geo_face_quadra_exclusao["Portuguese(Brazil)"]))
		$tdatapublic_geo_face_quadra_exclusao[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_geo_face_quadra_exclusao[""] = array();
	$fieldToolTipspublic_geo_face_quadra_exclusao[""] = array();
	$placeHolderspublic_geo_face_quadra_exclusao[""] = array();
	$pageTitlespublic_geo_face_quadra_exclusao[""] = array();
	if (count($fieldToolTipspublic_geo_face_quadra_exclusao[""]))
		$tdatapublic_geo_face_quadra_exclusao[".isUseToolTips"] = true;
}


	$tdatapublic_geo_face_quadra_exclusao[".NCSearch"] = true;



$tdatapublic_geo_face_quadra_exclusao[".shortTableName"] = "public_geo_face_quadra_exclusao";
$tdatapublic_geo_face_quadra_exclusao[".nSecOptions"] = 0;
$tdatapublic_geo_face_quadra_exclusao[".recsPerRowPrint"] = 1;
$tdatapublic_geo_face_quadra_exclusao[".mainTableOwnerID"] = "";
$tdatapublic_geo_face_quadra_exclusao[".moveNext"] = 1;
$tdatapublic_geo_face_quadra_exclusao[".entityType"] = 0;

$tdatapublic_geo_face_quadra_exclusao[".strOriginalTableName"] = "public.geo_face_quadra_exclusao";

	



$tdatapublic_geo_face_quadra_exclusao[".showAddInPopup"] = false;

$tdatapublic_geo_face_quadra_exclusao[".showEditInPopup"] = false;

$tdatapublic_geo_face_quadra_exclusao[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapublic_geo_face_quadra_exclusao[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_geo_face_quadra_exclusao[".fieldsForRegister"] = array();

$tdatapublic_geo_face_quadra_exclusao[".listAjax"] = false;

	$tdatapublic_geo_face_quadra_exclusao[".audit"] = false;

	$tdatapublic_geo_face_quadra_exclusao[".locking"] = false;



$tdatapublic_geo_face_quadra_exclusao[".list"] = true;



$tdatapublic_geo_face_quadra_exclusao[".reorderRecordsByHeader"] = true;


$tdatapublic_geo_face_quadra_exclusao[".exportFormatting"] = 2;
$tdatapublic_geo_face_quadra_exclusao[".exportDelimiter"] = ",";
		

$tdatapublic_geo_face_quadra_exclusao[".import"] = true;

$tdatapublic_geo_face_quadra_exclusao[".exportTo"] = true;

$tdatapublic_geo_face_quadra_exclusao[".printFriendly"] = true;


$tdatapublic_geo_face_quadra_exclusao[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatapublic_geo_face_quadra_exclusao[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatapublic_geo_face_quadra_exclusao[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatapublic_geo_face_quadra_exclusao[".searchSaving"] = false;
//

$tdatapublic_geo_face_quadra_exclusao[".showSearchPanel"] = true;
		$tdatapublic_geo_face_quadra_exclusao[".flexibleSearch"] = true;

$tdatapublic_geo_face_quadra_exclusao[".isUseAjaxSuggest"] = true;






$tdatapublic_geo_face_quadra_exclusao[".ajaxCodeSnippetAdded"] = false;

$tdatapublic_geo_face_quadra_exclusao[".buttonsAdded"] = false;

$tdatapublic_geo_face_quadra_exclusao[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapublic_geo_face_quadra_exclusao[".isUseTimeForSearch"] = false;



$tdatapublic_geo_face_quadra_exclusao[".badgeColor"] = "6B8E23";


$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"] = array();
$tdatapublic_geo_face_quadra_exclusao[".filterFields"] = array();
$tdatapublic_geo_face_quadra_exclusao[".requiredSearchFields"] = array();

$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "cod_bairro";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "cod_quadra";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "cod_log";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "zn";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "grupos_per";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "obs";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "cod_face";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "tipo_log";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "logradouro";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "zh";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "revisao";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "the_geom";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "esgoto";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "telefone";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "agua";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "iluminacao";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "energia";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "pavimento";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "guia";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "lixo";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "transporte";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "vbu_pmt";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "vbu_pvg";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "relacao_tbl_face_orig";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "drenagem";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "bairro";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "gid";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "inclusao";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "limpeza";
	$tdatapublic_geo_face_quadra_exclusao[".allSearchFields"][] = "observacao";
	

$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"] = array();
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "cod_bairro";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "cod_quadra";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "cod_log";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "zn";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "grupos_per";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "obs";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "cod_face";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "tipo_log";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "logradouro";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "zh";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "revisao";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "the_geom";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "esgoto";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "telefone";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "agua";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "iluminacao";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "energia";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "pavimento";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "guia";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "lixo";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "transporte";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "vbu_pmt";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "vbu_pvg";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "relacao_tbl_face_orig";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "drenagem";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "bairro";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "gid";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "inclusao";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "limpeza";
$tdatapublic_geo_face_quadra_exclusao[".googleLikeFields"][] = "observacao";


$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"] = array();
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "cod_bairro";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "cod_quadra";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "cod_log";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "zn";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "grupos_per";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "obs";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "cod_face";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "tipo_log";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "logradouro";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "zh";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "revisao";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "the_geom";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "esgoto";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "telefone";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "agua";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "iluminacao";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "energia";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "pavimento";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "guia";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "lixo";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "transporte";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "vbu_pmt";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "vbu_pvg";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "relacao_tbl_face_orig";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "drenagem";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "bairro";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "gid";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "inclusao";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "limpeza";
$tdatapublic_geo_face_quadra_exclusao[".advSearchFields"][] = "observacao";

$tdatapublic_geo_face_quadra_exclusao[".tableType"] = "list";

$tdatapublic_geo_face_quadra_exclusao[".printerPageOrientation"] = 0;
$tdatapublic_geo_face_quadra_exclusao[".nPrinterPageScale"] = 100;

$tdatapublic_geo_face_quadra_exclusao[".nPrinterSplitRecords"] = 40;

$tdatapublic_geo_face_quadra_exclusao[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_geo_face_quadra_exclusao[".geocodingEnabled"] = false;





$tdatapublic_geo_face_quadra_exclusao[".listGridLayout"] = 3;

$tdatapublic_geo_face_quadra_exclusao[".isDisplayLoading"] = true;




// view page pdf

// print page pdf


$tdatapublic_geo_face_quadra_exclusao[".pageSize"] = 20;

$tdatapublic_geo_face_quadra_exclusao[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_geo_face_quadra_exclusao[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_geo_face_quadra_exclusao[".orderindexes"] = array();

$tdatapublic_geo_face_quadra_exclusao[".sqlHead"] = "SELECT cod_bairro,  	cod_quadra,  	cod_log,  	zn,  	grupos_per,  	obs,  	cod_face,  	tipo_log,  	logradouro,  	zh,  	revisao,  	the_geom,  	esgoto,  	telefone,  	agua,  	iluminacao,  	energia,  	pavimento,  	guia,  	lixo,  	transporte,  	vbu_pmt,  	vbu_pvg,  	relacao_tbl_face_orig,  	drenagem,  	bairro,  	gid,  	inclusao,  	limpeza,  	observacao";
$tdatapublic_geo_face_quadra_exclusao[".sqlFrom"] = "FROM \"public\".geo_face_quadra_exclusao";
$tdatapublic_geo_face_quadra_exclusao[".sqlWhereExpr"] = "";
$tdatapublic_geo_face_quadra_exclusao[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_geo_face_quadra_exclusao[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_geo_face_quadra_exclusao[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_geo_face_quadra_exclusao[".highlightSearchResults"] = true;

$tableKeyspublic_geo_face_quadra_exclusao = array();
$tdatapublic_geo_face_quadra_exclusao[".Keys"] = $tableKeyspublic_geo_face_quadra_exclusao;

$tdatapublic_geo_face_quadra_exclusao[".listFields"] = array();
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "cod_bairro";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "cod_quadra";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "cod_log";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "zn";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "grupos_per";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "obs";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "cod_face";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "tipo_log";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "logradouro";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "zh";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "revisao";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "the_geom";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "esgoto";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "telefone";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "agua";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "iluminacao";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "energia";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "pavimento";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "guia";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "lixo";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "transporte";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "vbu_pmt";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "vbu_pvg";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "relacao_tbl_face_orig";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "drenagem";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "bairro";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "gid";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "inclusao";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "limpeza";
$tdatapublic_geo_face_quadra_exclusao[".listFields"][] = "observacao";

$tdatapublic_geo_face_quadra_exclusao[".hideMobileList"] = array();


$tdatapublic_geo_face_quadra_exclusao[".viewFields"] = array();

$tdatapublic_geo_face_quadra_exclusao[".addFields"] = array();

$tdatapublic_geo_face_quadra_exclusao[".masterListFields"] = array();
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "cod_bairro";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "cod_quadra";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "cod_log";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "zn";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "grupos_per";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "obs";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "cod_face";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "tipo_log";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "logradouro";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "zh";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "revisao";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "the_geom";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "esgoto";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "telefone";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "agua";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "iluminacao";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "energia";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "pavimento";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "guia";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "lixo";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "transporte";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "vbu_pmt";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "vbu_pvg";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "relacao_tbl_face_orig";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "drenagem";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "bairro";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "gid";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "inclusao";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "limpeza";
$tdatapublic_geo_face_quadra_exclusao[".masterListFields"][] = "observacao";

$tdatapublic_geo_face_quadra_exclusao[".inlineAddFields"] = array();

$tdatapublic_geo_face_quadra_exclusao[".editFields"] = array();

$tdatapublic_geo_face_quadra_exclusao[".inlineEditFields"] = array();

$tdatapublic_geo_face_quadra_exclusao[".updateSelectedFields"] = array();


$tdatapublic_geo_face_quadra_exclusao[".exportFields"] = array();
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "cod_bairro";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "cod_quadra";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "cod_log";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "zn";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "grupos_per";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "obs";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "cod_face";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "tipo_log";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "logradouro";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "zh";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "revisao";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "the_geom";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "esgoto";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "telefone";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "agua";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "iluminacao";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "energia";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "pavimento";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "guia";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "lixo";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "transporte";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "vbu_pmt";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "vbu_pvg";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "relacao_tbl_face_orig";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "drenagem";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "bairro";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "gid";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "inclusao";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "limpeza";
$tdatapublic_geo_face_quadra_exclusao[".exportFields"][] = "observacao";

$tdatapublic_geo_face_quadra_exclusao[".importFields"] = array();
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "cod_bairro";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "cod_quadra";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "cod_log";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "zn";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "grupos_per";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "obs";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "cod_face";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "tipo_log";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "logradouro";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "zh";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "revisao";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "the_geom";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "esgoto";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "telefone";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "agua";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "iluminacao";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "energia";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "pavimento";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "guia";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "lixo";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "transporte";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "vbu_pmt";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "vbu_pvg";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "relacao_tbl_face_orig";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "drenagem";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "bairro";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "gid";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "inclusao";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "limpeza";
$tdatapublic_geo_face_quadra_exclusao[".importFields"][] = "observacao";

$tdatapublic_geo_face_quadra_exclusao[".printFields"] = array();
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "cod_bairro";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "cod_quadra";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "cod_log";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "zn";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "grupos_per";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "obs";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "cod_face";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "tipo_log";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "logradouro";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "zh";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "revisao";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "the_geom";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "esgoto";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "telefone";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "agua";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "iluminacao";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "energia";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "pavimento";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "guia";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "lixo";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "transporte";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "vbu_pmt";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "vbu_pvg";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "relacao_tbl_face_orig";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "drenagem";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "bairro";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "gid";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "inclusao";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "limpeza";
$tdatapublic_geo_face_quadra_exclusao[".printFields"][] = "observacao";


//	cod_bairro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "cod_bairro";
	$fdata["GoodName"] = "cod_bairro";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","cod_bairro");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "cod_bairro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cod_bairro";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=254";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["cod_bairro"] = $fdata;
//	cod_quadra
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "cod_quadra";
	$fdata["GoodName"] = "cod_quadra";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","cod_quadra");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "cod_quadra";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cod_quadra";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=254";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["cod_quadra"] = $fdata;
//	cod_log
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "cod_log";
	$fdata["GoodName"] = "cod_log";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","cod_log");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "cod_log";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cod_log";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=254";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["cod_log"] = $fdata;
//	zn
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "zn";
	$fdata["GoodName"] = "zn";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","zn");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "zn";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "zn";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=4";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["zn"] = $fdata;
//	grupos_per
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "grupos_per";
	$fdata["GoodName"] = "grupos_per";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","grupos_per");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "grupos_per";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "grupos_per";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=254";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["grupos_per"] = $fdata;
//	obs
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "obs";
	$fdata["GoodName"] = "obs";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","obs");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "obs";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "obs";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=254";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["obs"] = $fdata;
//	cod_face
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "cod_face";
	$fdata["GoodName"] = "cod_face";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","cod_face");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "cod_face";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "cod_face";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=25";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["cod_face"] = $fdata;
//	tipo_log
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "tipo_log";
	$fdata["GoodName"] = "tipo_log";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","tipo_log");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "tipo_log";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tipo_log";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=254";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["tipo_log"] = $fdata;
//	logradouro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "logradouro";
	$fdata["GoodName"] = "logradouro";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","logradouro");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "logradouro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "logradouro";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=500";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["logradouro"] = $fdata;
//	zh
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "zh";
	$fdata["GoodName"] = "zh";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","zh");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "zh";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "zh";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=25";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["zh"] = $fdata;
//	revisao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "revisao";
	$fdata["GoodName"] = "revisao";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","revisao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "revisao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "revisao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["revisao"] = $fdata;
//	the_geom
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "the_geom";
	$fdata["GoodName"] = "the_geom";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","the_geom");
	$fdata["FieldType"] = 13;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "the_geom";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "the_geom";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["the_geom"] = $fdata;
//	esgoto
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "esgoto";
	$fdata["GoodName"] = "esgoto";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","esgoto");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "esgoto";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "esgoto";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["esgoto"] = $fdata;
//	telefone
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "telefone";
	$fdata["GoodName"] = "telefone";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","telefone");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "telefone";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "telefone";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["telefone"] = $fdata;
//	agua
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "agua";
	$fdata["GoodName"] = "agua";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","agua");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "agua";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "agua";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["agua"] = $fdata;
//	iluminacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 16;
	$fdata["strName"] = "iluminacao";
	$fdata["GoodName"] = "iluminacao";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","iluminacao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "iluminacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "iluminacao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["iluminacao"] = $fdata;
//	energia
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 17;
	$fdata["strName"] = "energia";
	$fdata["GoodName"] = "energia";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","energia");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "energia";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "energia";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["energia"] = $fdata;
//	pavimento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 18;
	$fdata["strName"] = "pavimento";
	$fdata["GoodName"] = "pavimento";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","pavimento");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "pavimento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "pavimento";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["pavimento"] = $fdata;
//	guia
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 19;
	$fdata["strName"] = "guia";
	$fdata["GoodName"] = "guia";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","guia");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "guia";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "guia";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["guia"] = $fdata;
//	lixo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 20;
	$fdata["strName"] = "lixo";
	$fdata["GoodName"] = "lixo";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","lixo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "lixo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "lixo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["lixo"] = $fdata;
//	transporte
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 21;
	$fdata["strName"] = "transporte";
	$fdata["GoodName"] = "transporte";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","transporte");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "transporte";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "transporte";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["transporte"] = $fdata;
//	vbu_pmt
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 22;
	$fdata["strName"] = "vbu_pmt";
	$fdata["GoodName"] = "vbu_pmt";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","vbu_pmt");
	$fdata["FieldType"] = 5;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "vbu_pmt";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "vbu_pmt";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["vbu_pmt"] = $fdata;
//	vbu_pvg
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 23;
	$fdata["strName"] = "vbu_pvg";
	$fdata["GoodName"] = "vbu_pvg";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","vbu_pvg");
	$fdata["FieldType"] = 5;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "vbu_pvg";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "vbu_pvg";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Number");

	
	
	
	
	
	
		$vdata["DecimalDigits"] = 2;

	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["vbu_pvg"] = $fdata;
//	relacao_tbl_face_orig
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 24;
	$fdata["strName"] = "relacao_tbl_face_orig";
	$fdata["GoodName"] = "relacao_tbl_face_orig";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","relacao_tbl_face_orig");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "relacao_tbl_face_orig";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "relacao_tbl_face_orig";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["relacao_tbl_face_orig"] = $fdata;
//	drenagem
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 25;
	$fdata["strName"] = "drenagem";
	$fdata["GoodName"] = "drenagem";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","drenagem");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "drenagem";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "drenagem";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["drenagem"] = $fdata;
//	bairro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 26;
	$fdata["strName"] = "bairro";
	$fdata["GoodName"] = "bairro";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","bairro");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "bairro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "bairro";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["bairro"] = $fdata;
//	gid
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 27;
	$fdata["strName"] = "gid";
	$fdata["GoodName"] = "gid";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","gid");
	$fdata["FieldType"] = 20;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "gid";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "gid";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


		$edata["IsRequired"] = true;

	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["gid"] = $fdata;
//	inclusao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 28;
	$fdata["strName"] = "inclusao";
	$fdata["GoodName"] = "inclusao";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","inclusao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "inclusao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "inclusao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["inclusao"] = $fdata;
//	limpeza
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 29;
	$fdata["strName"] = "limpeza";
	$fdata["GoodName"] = "limpeza";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","limpeza");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "limpeza";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "limpeza";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=10";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["limpeza"] = $fdata;
//	observacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 30;
	$fdata["strName"] = "observacao";
	$fdata["GoodName"] = "observacao";
	$fdata["ownerTable"] = "public.geo_face_quadra_exclusao";
	$fdata["Label"] = GetFieldLabel("public_geo_face_quadra_exclusao","observacao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

	
	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "observacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "observacao";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_geo_face_quadra_exclusao["observacao"] = $fdata;


$tables_data["public.geo_face_quadra_exclusao"]=&$tdatapublic_geo_face_quadra_exclusao;
$field_labels["public_geo_face_quadra_exclusao"] = &$fieldLabelspublic_geo_face_quadra_exclusao;
$fieldToolTips["public_geo_face_quadra_exclusao"] = &$fieldToolTipspublic_geo_face_quadra_exclusao;
$placeHolders["public_geo_face_quadra_exclusao"] = &$placeHolderspublic_geo_face_quadra_exclusao;
$page_titles["public_geo_face_quadra_exclusao"] = &$pageTitlespublic_geo_face_quadra_exclusao;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.geo_face_quadra_exclusao"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.geo_face_quadra_exclusao"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_geo_face_quadra_exclusao()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "cod_bairro,  	cod_quadra,  	cod_log,  	zn,  	grupos_per,  	obs,  	cod_face,  	tipo_log,  	logradouro,  	zh,  	revisao,  	the_geom,  	esgoto,  	telefone,  	agua,  	iluminacao,  	energia,  	pavimento,  	guia,  	lixo,  	transporte,  	vbu_pmt,  	vbu_pvg,  	relacao_tbl_face_orig,  	drenagem,  	bairro,  	gid,  	inclusao,  	limpeza,  	observacao";
$proto0["m_strFrom"] = "FROM \"public\".geo_face_quadra_exclusao";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "cod_bairro",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto6["m_sql"] = "cod_bairro";
$proto6["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "cod_quadra",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto8["m_sql"] = "cod_quadra";
$proto8["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "cod_log",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto10["m_sql"] = "cod_log";
$proto10["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "zn",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto12["m_sql"] = "zn";
$proto12["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "grupos_per",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto14["m_sql"] = "grupos_per";
$proto14["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "obs",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto16["m_sql"] = "obs";
$proto16["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "cod_face",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto18["m_sql"] = "cod_face";
$proto18["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "tipo_log",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto20["m_sql"] = "tipo_log";
$proto20["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "logradouro",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto22["m_sql"] = "logradouro";
$proto22["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "zh",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto24["m_sql"] = "zh";
$proto24["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "revisao",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto26["m_sql"] = "revisao";
$proto26["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "the_geom",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto28["m_sql"] = "the_geom";
$proto28["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "esgoto",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto30["m_sql"] = "esgoto";
$proto30["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "telefone",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto32["m_sql"] = "telefone";
$proto32["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "agua",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto34["m_sql"] = "agua";
$proto34["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
						$proto36=array();
			$obj = new SQLField(array(
	"m_strName" => "iluminacao",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto36["m_sql"] = "iluminacao";
$proto36["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto36["m_expr"]=$obj;
$proto36["m_alias"] = "";
$obj = new SQLFieldListItem($proto36);

$proto0["m_fieldlist"][]=$obj;
						$proto38=array();
			$obj = new SQLField(array(
	"m_strName" => "energia",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto38["m_sql"] = "energia";
$proto38["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto38["m_expr"]=$obj;
$proto38["m_alias"] = "";
$obj = new SQLFieldListItem($proto38);

$proto0["m_fieldlist"][]=$obj;
						$proto40=array();
			$obj = new SQLField(array(
	"m_strName" => "pavimento",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto40["m_sql"] = "pavimento";
$proto40["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto40["m_expr"]=$obj;
$proto40["m_alias"] = "";
$obj = new SQLFieldListItem($proto40);

$proto0["m_fieldlist"][]=$obj;
						$proto42=array();
			$obj = new SQLField(array(
	"m_strName" => "guia",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto42["m_sql"] = "guia";
$proto42["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto42["m_expr"]=$obj;
$proto42["m_alias"] = "";
$obj = new SQLFieldListItem($proto42);

$proto0["m_fieldlist"][]=$obj;
						$proto44=array();
			$obj = new SQLField(array(
	"m_strName" => "lixo",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto44["m_sql"] = "lixo";
$proto44["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto44["m_expr"]=$obj;
$proto44["m_alias"] = "";
$obj = new SQLFieldListItem($proto44);

$proto0["m_fieldlist"][]=$obj;
						$proto46=array();
			$obj = new SQLField(array(
	"m_strName" => "transporte",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto46["m_sql"] = "transporte";
$proto46["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto46["m_expr"]=$obj;
$proto46["m_alias"] = "";
$obj = new SQLFieldListItem($proto46);

$proto0["m_fieldlist"][]=$obj;
						$proto48=array();
			$obj = new SQLField(array(
	"m_strName" => "vbu_pmt",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto48["m_sql"] = "vbu_pmt";
$proto48["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto48["m_expr"]=$obj;
$proto48["m_alias"] = "";
$obj = new SQLFieldListItem($proto48);

$proto0["m_fieldlist"][]=$obj;
						$proto50=array();
			$obj = new SQLField(array(
	"m_strName" => "vbu_pvg",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto50["m_sql"] = "vbu_pvg";
$proto50["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto50["m_expr"]=$obj;
$proto50["m_alias"] = "";
$obj = new SQLFieldListItem($proto50);

$proto0["m_fieldlist"][]=$obj;
						$proto52=array();
			$obj = new SQLField(array(
	"m_strName" => "relacao_tbl_face_orig",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto52["m_sql"] = "relacao_tbl_face_orig";
$proto52["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto52["m_expr"]=$obj;
$proto52["m_alias"] = "";
$obj = new SQLFieldListItem($proto52);

$proto0["m_fieldlist"][]=$obj;
						$proto54=array();
			$obj = new SQLField(array(
	"m_strName" => "drenagem",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto54["m_sql"] = "drenagem";
$proto54["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto54["m_expr"]=$obj;
$proto54["m_alias"] = "";
$obj = new SQLFieldListItem($proto54);

$proto0["m_fieldlist"][]=$obj;
						$proto56=array();
			$obj = new SQLField(array(
	"m_strName" => "bairro",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto56["m_sql"] = "bairro";
$proto56["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto56["m_expr"]=$obj;
$proto56["m_alias"] = "";
$obj = new SQLFieldListItem($proto56);

$proto0["m_fieldlist"][]=$obj;
						$proto58=array();
			$obj = new SQLField(array(
	"m_strName" => "gid",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto58["m_sql"] = "gid";
$proto58["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto58["m_expr"]=$obj;
$proto58["m_alias"] = "";
$obj = new SQLFieldListItem($proto58);

$proto0["m_fieldlist"][]=$obj;
						$proto60=array();
			$obj = new SQLField(array(
	"m_strName" => "inclusao",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto60["m_sql"] = "inclusao";
$proto60["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto60["m_expr"]=$obj;
$proto60["m_alias"] = "";
$obj = new SQLFieldListItem($proto60);

$proto0["m_fieldlist"][]=$obj;
						$proto62=array();
			$obj = new SQLField(array(
	"m_strName" => "limpeza",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto62["m_sql"] = "limpeza";
$proto62["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto62["m_expr"]=$obj;
$proto62["m_alias"] = "";
$obj = new SQLFieldListItem($proto62);

$proto0["m_fieldlist"][]=$obj;
						$proto64=array();
			$obj = new SQLField(array(
	"m_strName" => "observacao",
	"m_strTable" => "public.geo_face_quadra_exclusao",
	"m_srcTableName" => "public.geo_face_quadra_exclusao"
));

$proto64["m_sql"] = "observacao";
$proto64["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto64["m_expr"]=$obj;
$proto64["m_alias"] = "";
$obj = new SQLFieldListItem($proto64);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto66=array();
$proto66["m_link"] = "SQLL_MAIN";
			$proto67=array();
$proto67["m_strName"] = "public.geo_face_quadra_exclusao";
$proto67["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto67["m_columns"] = array();
$proto67["m_columns"][] = "cod_bairro";
$proto67["m_columns"][] = "cod_quadra";
$proto67["m_columns"][] = "cod_log";
$proto67["m_columns"][] = "zn";
$proto67["m_columns"][] = "grupos_per";
$proto67["m_columns"][] = "obs";
$proto67["m_columns"][] = "cod_face";
$proto67["m_columns"][] = "tipo_log";
$proto67["m_columns"][] = "logradouro";
$proto67["m_columns"][] = "zh";
$proto67["m_columns"][] = "revisao";
$proto67["m_columns"][] = "the_geom";
$proto67["m_columns"][] = "esgoto";
$proto67["m_columns"][] = "telefone";
$proto67["m_columns"][] = "agua";
$proto67["m_columns"][] = "iluminacao";
$proto67["m_columns"][] = "energia";
$proto67["m_columns"][] = "pavimento";
$proto67["m_columns"][] = "guia";
$proto67["m_columns"][] = "lixo";
$proto67["m_columns"][] = "transporte";
$proto67["m_columns"][] = "vbu_pmt";
$proto67["m_columns"][] = "vbu_pvg";
$proto67["m_columns"][] = "relacao_tbl_face_orig";
$proto67["m_columns"][] = "drenagem";
$proto67["m_columns"][] = "bairro";
$proto67["m_columns"][] = "gid";
$proto67["m_columns"][] = "inclusao";
$proto67["m_columns"][] = "limpeza";
$proto67["m_columns"][] = "observacao";
$obj = new SQLTable($proto67);

$proto66["m_table"] = $obj;
$proto66["m_sql"] = "\"public\".geo_face_quadra_exclusao";
$proto66["m_alias"] = "";
$proto66["m_srcTableName"] = "public.geo_face_quadra_exclusao";
$proto68=array();
$proto68["m_sql"] = "";
$proto68["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto68["m_column"]=$obj;
$proto68["m_contained"] = array();
$proto68["m_strCase"] = "";
$proto68["m_havingmode"] = false;
$proto68["m_inBrackets"] = false;
$proto68["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto68);

$proto66["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto66);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.geo_face_quadra_exclusao";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_public_geo_face_quadra_exclusao = createSqlQuery_public_geo_face_quadra_exclusao();


	
		;

																														

$tdatapublic_geo_face_quadra_exclusao[".sqlquery"] = $queryData_public_geo_face_quadra_exclusao;

$tableEvents["public.geo_face_quadra_exclusao"] = new eventsBase;
$tdatapublic_geo_face_quadra_exclusao[".hasEvents"] = false;

?>