<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_tbl_logradouros = array();
	$tdatapublic_tbl_logradouros[".truncateText"] = true;
	$tdatapublic_tbl_logradouros[".NumberOfChars"] = 80;
	$tdatapublic_tbl_logradouros[".ShortName"] = "public_tbl_logradouros";
	$tdatapublic_tbl_logradouros[".OwnerID"] = "";
	$tdatapublic_tbl_logradouros[".OriginalTable"] = "public.tbl_logradouros";

//	field labels
$fieldLabelspublic_tbl_logradouros = array();
$fieldToolTipspublic_tbl_logradouros = array();
$pageTitlespublic_tbl_logradouros = array();
$placeHolderspublic_tbl_logradouros = array();

if(mlang_getcurrentlang()=="Portuguese(Brazil)")
{
	$fieldLabelspublic_tbl_logradouros["Portuguese(Brazil)"] = array();
	$fieldToolTipspublic_tbl_logradouros["Portuguese(Brazil)"] = array();
	$placeHolderspublic_tbl_logradouros["Portuguese(Brazil)"] = array();
	$pageTitlespublic_tbl_logradouros["Portuguese(Brazil)"] = array();
	$fieldLabelspublic_tbl_logradouros["Portuguese(Brazil)"]["Codigo_de_Logradouro"] = "Codigo de Logradouro";
	$fieldToolTipspublic_tbl_logradouros["Portuguese(Brazil)"]["Codigo_de_Logradouro"] = "";
	$placeHolderspublic_tbl_logradouros["Portuguese(Brazil)"]["Codigo_de_Logradouro"] = "";
	$fieldLabelspublic_tbl_logradouros["Portuguese(Brazil)"]["Tipo_de_Logradouro"] = "Tipo de Logradouro";
	$fieldToolTipspublic_tbl_logradouros["Portuguese(Brazil)"]["Tipo_de_Logradouro"] = "";
	$placeHolderspublic_tbl_logradouros["Portuguese(Brazil)"]["Tipo_de_Logradouro"] = "";
	$fieldLabelspublic_tbl_logradouros["Portuguese(Brazil)"]["Nome_Logradouro"] = "Nome Logradouro";
	$fieldToolTipspublic_tbl_logradouros["Portuguese(Brazil)"]["Nome_Logradouro"] = "";
	$placeHolderspublic_tbl_logradouros["Portuguese(Brazil)"]["Nome_Logradouro"] = "";
	$fieldLabelspublic_tbl_logradouros["Portuguese(Brazil)"]["Observacao"] = "Observacao";
	$fieldToolTipspublic_tbl_logradouros["Portuguese(Brazil)"]["Observacao"] = "";
	$placeHolderspublic_tbl_logradouros["Portuguese(Brazil)"]["Observacao"] = "";
	$fieldLabelspublic_tbl_logradouros["Portuguese(Brazil)"]["Numero_Lei"] = "Numero Lei";
	$fieldToolTipspublic_tbl_logradouros["Portuguese(Brazil)"]["Numero_Lei"] = "";
	$placeHolderspublic_tbl_logradouros["Portuguese(Brazil)"]["Numero_Lei"] = "";
	$fieldLabelspublic_tbl_logradouros["Portuguese(Brazil)"]["Data_Lei"] = "Data Lei";
	$fieldToolTipspublic_tbl_logradouros["Portuguese(Brazil)"]["Data_Lei"] = "";
	$placeHolderspublic_tbl_logradouros["Portuguese(Brazil)"]["Data_Lei"] = "";
	$fieldLabelspublic_tbl_logradouros["Portuguese(Brazil)"]["Oficial"] = "Oficial";
	$fieldToolTipspublic_tbl_logradouros["Portuguese(Brazil)"]["Oficial"] = "";
	$placeHolderspublic_tbl_logradouros["Portuguese(Brazil)"]["Oficial"] = "";
	$fieldLabelspublic_tbl_logradouros["Portuguese(Brazil)"]["qt"] = "Qt";
	$fieldToolTipspublic_tbl_logradouros["Portuguese(Brazil)"]["qt"] = "";
	$placeHolderspublic_tbl_logradouros["Portuguese(Brazil)"]["qt"] = "";
	$fieldLabelspublic_tbl_logradouros["Portuguese(Brazil)"]["relacao"] = "Relacao";
	$fieldToolTipspublic_tbl_logradouros["Portuguese(Brazil)"]["relacao"] = "";
	$placeHolderspublic_tbl_logradouros["Portuguese(Brazil)"]["relacao"] = "";
	if (count($fieldToolTipspublic_tbl_logradouros["Portuguese(Brazil)"]))
		$tdatapublic_tbl_logradouros[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_tbl_logradouros[""] = array();
	$fieldToolTipspublic_tbl_logradouros[""] = array();
	$placeHolderspublic_tbl_logradouros[""] = array();
	$pageTitlespublic_tbl_logradouros[""] = array();
	if (count($fieldToolTipspublic_tbl_logradouros[""]))
		$tdatapublic_tbl_logradouros[".isUseToolTips"] = true;
}


	$tdatapublic_tbl_logradouros[".NCSearch"] = true;



$tdatapublic_tbl_logradouros[".shortTableName"] = "public_tbl_logradouros";
$tdatapublic_tbl_logradouros[".nSecOptions"] = 0;
$tdatapublic_tbl_logradouros[".recsPerRowPrint"] = 1;
$tdatapublic_tbl_logradouros[".mainTableOwnerID"] = "";
$tdatapublic_tbl_logradouros[".moveNext"] = 1;
$tdatapublic_tbl_logradouros[".entityType"] = 0;

$tdatapublic_tbl_logradouros[".strOriginalTableName"] = "public.tbl_logradouros";

	



$tdatapublic_tbl_logradouros[".showAddInPopup"] = false;

$tdatapublic_tbl_logradouros[".showEditInPopup"] = false;

$tdatapublic_tbl_logradouros[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapublic_tbl_logradouros[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_tbl_logradouros[".fieldsForRegister"] = array();

$tdatapublic_tbl_logradouros[".listAjax"] = false;

	$tdatapublic_tbl_logradouros[".audit"] = false;

	$tdatapublic_tbl_logradouros[".locking"] = false;


$tdatapublic_tbl_logradouros[".add"] = true;
$tdatapublic_tbl_logradouros[".afterAddAction"] = 1;
$tdatapublic_tbl_logradouros[".closePopupAfterAdd"] = 1;
$tdatapublic_tbl_logradouros[".afterAddActionDetTable"] = "Detail tables not found!";

$tdatapublic_tbl_logradouros[".list"] = true;



$tdatapublic_tbl_logradouros[".reorderRecordsByHeader"] = true;






$tdatapublic_tbl_logradouros[".printFriendly"] = true;


$tdatapublic_tbl_logradouros[".showSimpleSearchOptions"] = false;

// Allow Show/Hide Fields in GRID
$tdatapublic_tbl_logradouros[".allowShowHideFields"] = false;
//

// Allow Fields Reordering in GRID
$tdatapublic_tbl_logradouros[".allowFieldsReordering"] = false;
//

// search Saving settings
$tdatapublic_tbl_logradouros[".searchSaving"] = false;
//

$tdatapublic_tbl_logradouros[".showSearchPanel"] = true;
		$tdatapublic_tbl_logradouros[".flexibleSearch"] = true;

$tdatapublic_tbl_logradouros[".isUseAjaxSuggest"] = true;






$tdatapublic_tbl_logradouros[".ajaxCodeSnippetAdded"] = false;

$tdatapublic_tbl_logradouros[".buttonsAdded"] = false;

$tdatapublic_tbl_logradouros[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapublic_tbl_logradouros[".isUseTimeForSearch"] = false;





$tdatapublic_tbl_logradouros[".allSearchFields"] = array();
$tdatapublic_tbl_logradouros[".filterFields"] = array();
$tdatapublic_tbl_logradouros[".requiredSearchFields"] = array();

$tdatapublic_tbl_logradouros[".allSearchFields"][] = "Codigo de Logradouro";
	$tdatapublic_tbl_logradouros[".allSearchFields"][] = "Tipo de Logradouro";
	$tdatapublic_tbl_logradouros[".allSearchFields"][] = "Nome Logradouro";
	$tdatapublic_tbl_logradouros[".allSearchFields"][] = "Observacao";
	

$tdatapublic_tbl_logradouros[".googleLikeFields"] = array();
$tdatapublic_tbl_logradouros[".googleLikeFields"][] = "Codigo de Logradouro";
$tdatapublic_tbl_logradouros[".googleLikeFields"][] = "Tipo de Logradouro";
$tdatapublic_tbl_logradouros[".googleLikeFields"][] = "Nome Logradouro";
$tdatapublic_tbl_logradouros[".googleLikeFields"][] = "Observacao";
$tdatapublic_tbl_logradouros[".googleLikeFields"][] = "Numero Lei";
$tdatapublic_tbl_logradouros[".googleLikeFields"][] = "Data Lei";
$tdatapublic_tbl_logradouros[".googleLikeFields"][] = "Oficial";
$tdatapublic_tbl_logradouros[".googleLikeFields"][] = "qt";
$tdatapublic_tbl_logradouros[".googleLikeFields"][] = "relacao";


$tdatapublic_tbl_logradouros[".advSearchFields"] = array();
$tdatapublic_tbl_logradouros[".advSearchFields"][] = "Codigo de Logradouro";
$tdatapublic_tbl_logradouros[".advSearchFields"][] = "Tipo de Logradouro";
$tdatapublic_tbl_logradouros[".advSearchFields"][] = "Nome Logradouro";
$tdatapublic_tbl_logradouros[".advSearchFields"][] = "Observacao";

$tdatapublic_tbl_logradouros[".tableType"] = "list";

$tdatapublic_tbl_logradouros[".printerPageOrientation"] = 0;
$tdatapublic_tbl_logradouros[".nPrinterPageScale"] = 100;

$tdatapublic_tbl_logradouros[".nPrinterSplitRecords"] = 40;

$tdatapublic_tbl_logradouros[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_tbl_logradouros[".geocodingEnabled"] = false;





$tdatapublic_tbl_logradouros[".listGridLayout"] = 3;

$tdatapublic_tbl_logradouros[".isDisplayLoading"] = true;




// view page pdf

// print page pdf


$tdatapublic_tbl_logradouros[".pageSize"] = 20;

$tdatapublic_tbl_logradouros[".warnLeavingPages"] = true;



$tstrOrderBy = "";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_tbl_logradouros[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_tbl_logradouros[".orderindexes"] = array();

$tdatapublic_tbl_logradouros[".sqlHead"] = "SELECT \"Codigo de Logradouro\",  	\"Tipo de Logradouro\",  	\"Nome Logradouro\",  	\"Observacao\",  	\"Numero Lei\",  	\"Data Lei\",  	\"Oficial\",  	qt,  	relacao";
$tdatapublic_tbl_logradouros[".sqlFrom"] = "FROM \"public\".tbl_logradouros";
$tdatapublic_tbl_logradouros[".sqlWhereExpr"] = "";
$tdatapublic_tbl_logradouros[".sqlTail"] = "";












//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_tbl_logradouros[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_tbl_logradouros[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_tbl_logradouros[".highlightSearchResults"] = true;

$tableKeyspublic_tbl_logradouros = array();
$tdatapublic_tbl_logradouros[".Keys"] = $tableKeyspublic_tbl_logradouros;

$tdatapublic_tbl_logradouros[".listFields"] = array();
$tdatapublic_tbl_logradouros[".listFields"][] = "Codigo de Logradouro";
$tdatapublic_tbl_logradouros[".listFields"][] = "Tipo de Logradouro";
$tdatapublic_tbl_logradouros[".listFields"][] = "Nome Logradouro";
$tdatapublic_tbl_logradouros[".listFields"][] = "Observacao";

$tdatapublic_tbl_logradouros[".hideMobileList"] = array();


$tdatapublic_tbl_logradouros[".viewFields"] = array();

$tdatapublic_tbl_logradouros[".addFields"] = array();
$tdatapublic_tbl_logradouros[".addFields"][] = "Codigo de Logradouro";
$tdatapublic_tbl_logradouros[".addFields"][] = "Tipo de Logradouro";
$tdatapublic_tbl_logradouros[".addFields"][] = "Nome Logradouro";
$tdatapublic_tbl_logradouros[".addFields"][] = "Observacao";

$tdatapublic_tbl_logradouros[".masterListFields"] = array();
$tdatapublic_tbl_logradouros[".masterListFields"][] = "Codigo de Logradouro";
$tdatapublic_tbl_logradouros[".masterListFields"][] = "Tipo de Logradouro";
$tdatapublic_tbl_logradouros[".masterListFields"][] = "Nome Logradouro";
$tdatapublic_tbl_logradouros[".masterListFields"][] = "Observacao";
$tdatapublic_tbl_logradouros[".masterListFields"][] = "Numero Lei";
$tdatapublic_tbl_logradouros[".masterListFields"][] = "Data Lei";
$tdatapublic_tbl_logradouros[".masterListFields"][] = "Oficial";
$tdatapublic_tbl_logradouros[".masterListFields"][] = "qt";
$tdatapublic_tbl_logradouros[".masterListFields"][] = "relacao";

$tdatapublic_tbl_logradouros[".inlineAddFields"] = array();

$tdatapublic_tbl_logradouros[".editFields"] = array();

$tdatapublic_tbl_logradouros[".inlineEditFields"] = array();

$tdatapublic_tbl_logradouros[".updateSelectedFields"] = array();


$tdatapublic_tbl_logradouros[".exportFields"] = array();

$tdatapublic_tbl_logradouros[".importFields"] = array();

$tdatapublic_tbl_logradouros[".printFields"] = array();
$tdatapublic_tbl_logradouros[".printFields"][] = "Codigo de Logradouro";
$tdatapublic_tbl_logradouros[".printFields"][] = "Tipo de Logradouro";
$tdatapublic_tbl_logradouros[".printFields"][] = "Nome Logradouro";
$tdatapublic_tbl_logradouros[".printFields"][] = "Observacao";


//	Codigo de Logradouro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "Codigo de Logradouro";
	$fdata["GoodName"] = "Codigo_de_Logradouro";
	$fdata["ownerTable"] = "public.tbl_logradouros";
	$fdata["Label"] = GetFieldLabel("public_tbl_logradouros","Codigo_de_Logradouro");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "Codigo de Logradouro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Codigo de Logradouro\"";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=50";

		$edata["controlWidth"] = 142;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_logradouros["Codigo de Logradouro"] = $fdata;
//	Tipo de Logradouro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Tipo de Logradouro";
	$fdata["GoodName"] = "Tipo_de_Logradouro";
	$fdata["ownerTable"] = "public.tbl_logradouros";
	$fdata["Label"] = GetFieldLabel("public_tbl_logradouros","Tipo_de_Logradouro");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "Tipo de Logradouro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Tipo de Logradouro\"";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_logradouros["Tipo de Logradouro"] = $fdata;
//	Nome Logradouro
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Nome Logradouro";
	$fdata["GoodName"] = "Nome_Logradouro";
	$fdata["ownerTable"] = "public.tbl_logradouros";
	$fdata["Label"] = GetFieldLabel("public_tbl_logradouros","Nome_Logradouro");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "Nome Logradouro";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Nome Logradouro\"";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 462;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_logradouros["Nome Logradouro"] = $fdata;
//	Observacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Observacao";
	$fdata["GoodName"] = "Observacao";
	$fdata["ownerTable"] = "public.tbl_logradouros";
	$fdata["Label"] = GetFieldLabel("public_tbl_logradouros","Observacao");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
	
	
	

	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

	
		$fdata["strField"] = "Observacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Observacao\"";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 672;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tbl_logradouros["Observacao"] = $fdata;
//	Numero Lei
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "Numero Lei";
	$fdata["GoodName"] = "Numero_Lei";
	$fdata["ownerTable"] = "public.tbl_logradouros";
	$fdata["Label"] = GetFieldLabel("public_tbl_logradouros","Numero_Lei");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Numero Lei";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Numero Lei\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_logradouros["Numero Lei"] = $fdata;
//	Data Lei
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "Data Lei";
	$fdata["GoodName"] = "Data_Lei";
	$fdata["ownerTable"] = "public.tbl_logradouros";
	$fdata["Label"] = GetFieldLabel("public_tbl_logradouros","Data_Lei");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Data Lei";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Data Lei\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_logradouros["Data Lei"] = $fdata;
//	Oficial
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "Oficial";
	$fdata["GoodName"] = "Oficial";
	$fdata["ownerTable"] = "public.tbl_logradouros";
	$fdata["Label"] = GetFieldLabel("public_tbl_logradouros","Oficial");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "Oficial";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"Oficial\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=255";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_logradouros["Oficial"] = $fdata;
//	qt
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "qt";
	$fdata["GoodName"] = "qt";
	$fdata["ownerTable"] = "public.tbl_logradouros";
	$fdata["Label"] = GetFieldLabel("public_tbl_logradouros","qt");
	$fdata["FieldType"] = 2;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "qt";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "qt";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_logradouros["qt"] = $fdata;
//	relacao
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "relacao";
	$fdata["GoodName"] = "relacao";
	$fdata["ownerTable"] = "public.tbl_logradouros";
	$fdata["Label"] = GetFieldLabel("public_tbl_logradouros","relacao");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	

	
	
	
	
		$fdata["strField"] = "relacao";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "relacao";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

		
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	
		
	


	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=25";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tbl_logradouros["relacao"] = $fdata;


$tables_data["public.tbl_logradouros"]=&$tdatapublic_tbl_logradouros;
$field_labels["public_tbl_logradouros"] = &$fieldLabelspublic_tbl_logradouros;
$fieldToolTips["public_tbl_logradouros"] = &$fieldToolTipspublic_tbl_logradouros;
$placeHolders["public_tbl_logradouros"] = &$placeHolderspublic_tbl_logradouros;
$page_titles["public_tbl_logradouros"] = &$pageTitlespublic_tbl_logradouros;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.tbl_logradouros"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.tbl_logradouros"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_tbl_logradouros()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "\"Codigo de Logradouro\",  	\"Tipo de Logradouro\",  	\"Nome Logradouro\",  	\"Observacao\",  	\"Numero Lei\",  	\"Data Lei\",  	\"Oficial\",  	qt,  	relacao";
$proto0["m_strFrom"] = "FROM \"public\".tbl_logradouros";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "Codigo de Logradouro",
	"m_strTable" => "public.tbl_logradouros",
	"m_srcTableName" => "public.tbl_logradouros"
));

$proto6["m_sql"] = "\"Codigo de Logradouro\"";
$proto6["m_srcTableName"] = "public.tbl_logradouros";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "Tipo de Logradouro",
	"m_strTable" => "public.tbl_logradouros",
	"m_srcTableName" => "public.tbl_logradouros"
));

$proto8["m_sql"] = "\"Tipo de Logradouro\"";
$proto8["m_srcTableName"] = "public.tbl_logradouros";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "Nome Logradouro",
	"m_strTable" => "public.tbl_logradouros",
	"m_srcTableName" => "public.tbl_logradouros"
));

$proto10["m_sql"] = "\"Nome Logradouro\"";
$proto10["m_srcTableName"] = "public.tbl_logradouros";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "Observacao",
	"m_strTable" => "public.tbl_logradouros",
	"m_srcTableName" => "public.tbl_logradouros"
));

$proto12["m_sql"] = "\"Observacao\"";
$proto12["m_srcTableName"] = "public.tbl_logradouros";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "Numero Lei",
	"m_strTable" => "public.tbl_logradouros",
	"m_srcTableName" => "public.tbl_logradouros"
));

$proto14["m_sql"] = "\"Numero Lei\"";
$proto14["m_srcTableName"] = "public.tbl_logradouros";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "Data Lei",
	"m_strTable" => "public.tbl_logradouros",
	"m_srcTableName" => "public.tbl_logradouros"
));

$proto16["m_sql"] = "\"Data Lei\"";
$proto16["m_srcTableName"] = "public.tbl_logradouros";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "Oficial",
	"m_strTable" => "public.tbl_logradouros",
	"m_srcTableName" => "public.tbl_logradouros"
));

$proto18["m_sql"] = "\"Oficial\"";
$proto18["m_srcTableName"] = "public.tbl_logradouros";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "qt",
	"m_strTable" => "public.tbl_logradouros",
	"m_srcTableName" => "public.tbl_logradouros"
));

$proto20["m_sql"] = "qt";
$proto20["m_srcTableName"] = "public.tbl_logradouros";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "relacao",
	"m_strTable" => "public.tbl_logradouros",
	"m_srcTableName" => "public.tbl_logradouros"
));

$proto22["m_sql"] = "relacao";
$proto22["m_srcTableName"] = "public.tbl_logradouros";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto24=array();
$proto24["m_link"] = "SQLL_MAIN";
			$proto25=array();
$proto25["m_strName"] = "public.tbl_logradouros";
$proto25["m_srcTableName"] = "public.tbl_logradouros";
$proto25["m_columns"] = array();
$proto25["m_columns"][] = "Codigo de Logradouro";
$proto25["m_columns"][] = "Tipo de Logradouro";
$proto25["m_columns"][] = "Nome Logradouro";
$proto25["m_columns"][] = "Observacao";
$proto25["m_columns"][] = "Numero Lei";
$proto25["m_columns"][] = "Data Lei";
$proto25["m_columns"][] = "Oficial";
$proto25["m_columns"][] = "qt";
$proto25["m_columns"][] = "relacao";
$obj = new SQLTable($proto25);

$proto24["m_table"] = $obj;
$proto24["m_sql"] = "\"public\".tbl_logradouros";
$proto24["m_alias"] = "";
$proto24["m_srcTableName"] = "public.tbl_logradouros";
$proto26=array();
$proto26["m_sql"] = "";
$proto26["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto26["m_column"]=$obj;
$proto26["m_contained"] = array();
$proto26["m_strCase"] = "";
$proto26["m_havingmode"] = false;
$proto26["m_inBrackets"] = false;
$proto26["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto26);

$proto24["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto24);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
$proto0["m_srcTableName"]="public.tbl_logradouros";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_public_tbl_logradouros = createSqlQuery_public_tbl_logradouros();


	
		;

									

$tdatapublic_tbl_logradouros[".sqlquery"] = $queryData_public_tbl_logradouros;

$tableEvents["public.tbl_logradouros"] = new eventsBase;
$tdatapublic_tbl_logradouros[".hasEvents"] = false;

?>